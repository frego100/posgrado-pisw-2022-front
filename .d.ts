declare module "*.png" {
    import React = require('react')
    export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>
    const src: string
    export default src
}
declare module "*.svg" {
    import React = require('react')
    export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>
    const src: string
    export default src
}
declare module "*.jpeg" {
    // @ts-ignore
    export default "" as string;
}
declare module "*.jpg" {
    // @ts-ignore
    export default "" as string;
}
