import {
  Document, Page, StyleSheet, Text, View, Image, Link,
} from '@react-pdf/renderer';
import React from 'react';
import { TFunction } from 'react-i18next';

const primaryColor = '#141E42';
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: 'white',
    paddingBottom: 40,
    paddingTop: 15,
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  formCard: {
    borderRadius: 16,
    marginTop: 20,
    border: '1px solid #D8DCE6',
    boxShadow: '0 1px 2px #00000014',
    padding: 20,
  },
  title: {
    fontSize: 18,
    color: primaryColor,
  },
  subtitle: {
    fontSize: 10,
    color: primaryColor,
    marginBottom: 6,
  },
  formCardItemSection: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    flexDirection: 'row',
  },
  formCardItem: {
    height: 50,
    boxSizing: ' border-box',
    marginTop: 24,
  },
  formCardImage: {
    marginTop: 24,
  },
  box: {
    borderColor: '#d9d9d9',
    backgroundColor: '#F8F8F8',
    height: 40,
    borderRadius: 8,
    padding: '0 10px',
    display: 'flex',
    alignContent: 'center',
    border: '1px solid #d9d9d9',
  },
  button: {
    backgroundColor: primaryColor,
    borderColor: primaryColor,
    height: 40,
    borderRadius: 8,
    padding: '0 10px',
    display: 'flex',
    alignContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonLabel: {
    color: 'white',
    fontSize: 10,
    marginTop: 10,
  },
  boxValue: {
    color: primaryColor,
    fontSize: 10,
    marginTop: 10,
  },
  sectionTitle: {
    textAlign: 'center',
    color: primaryColor,
  },
  imageBox: {
    padding: '0 10px',
    alignContent: 'center',
  },
});

const getInputItem = (data: any, index: number, margin: boolean) => (
  <View
    style={{
      ...styles.formCardItem,
      ...data.pdfStyle,
      ...margin && (index % 2 ? { marginLeft: '2%' } : { marginLeft: '2%' }),
    }}
  >
    <Text style={styles.subtitle}>
      {data.label}
    </Text>
    <View style={styles.box}>
      <Text style={styles.boxValue}>
        {data.pdfValue}
      </Text>
    </View>
  </View>
);

const getButtonItem = (data: any, index: any, margin: boolean) => {
  if (!data.pdfValue) return <div />;
  return (
    <View
      style={{
        ...styles.formCardItem,
        ...data.pdfStyle,
        ...margin && (index % 2 ? { marginLeft: '2%' } : { marginLeft: '2%' }),
      }}
    >
      <Text style={styles.subtitle}>
        {data.label}
      </Text>
      <View style={{ ...styles.button, width: '100%' }}>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <Link src={data.pdfValue}>
          <Text style={styles.buttonLabel}>
            {data.pdfItemLabel}
          </Text>
        </Link>
      </View>
    </View>
  );
};

const getImageItem = (data: any) => {
  if (!data.pdfValue) return <div />;
  return (
    <View
      style={{
        ...styles.formCardImage,
        ...data.pdfStyle,
      }}
    >
      <Image src={data.pdfValue} style={{ objectFit: 'scale-down' }} />
    </View>
  );
};

const formCardItemTypes:{ [index: string]: any } = {
  input: getInputItem,
  image: getImageItem,
  // todo: change
  checkbox: getInputItem,
  button: getButtonItem,
  null: () => {},
};

const getFormCardPdf = (key: any, value: any, margin: true) => (
  <View style={styles.formCard}>
    <Text style={styles.title}>
      {key}
    </Text>
    <View style={styles.formCardItemSection}>
      {value.map((data, index) => formCardItemTypes[data.pdfItemType](data, index, margin))}
    </View>
  </View>
);

const getFormCardModalBox = (data: any, margin: boolean) => (
  <View style={styles.formCard}>
    <View style={styles.formCardItemSection}>
      {data.map((item, index) => formCardItemTypes[item.pdfItemType](item, index, margin))}
    </View>
  </View>
);

const getFormCardModalPdf = (title, items: any, margin: boolean) => (
  <View style={styles.formCard}>
    <Text style={styles.title}>
      {title}
    </Text>
    <View>
      {items.map((data) => getFormCardModalBox(data, margin))}
    </View>
  </View>
);
export const getProfessorPdf = ({ professorItems, degreesItems }: any, t: TFunction) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>{t('professorProfile')}</Text>
        {
          professorItems.map((itemObject) => (
            Object.entries(itemObject).map(([key, value]) => (
              getFormCardPdf(key, value, true)
            ))
          ))
        }
        {
          getFormCardModalPdf(t('degrees'), degreesItems, false)
        }
      </View>
    </Page>
  </Document>
);

export default {};
