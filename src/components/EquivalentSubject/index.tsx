import React, { useEffect, useState } from 'react';
import { useQuery } from '@apollo/client';
import {
  Col, Collapse, Form, Row, Select,
} from 'antd';
import { loader } from 'graphql.macro';
import { useLocation } from 'react-router-dom';
import './styles.scss';

interface EquivalentCourseProps {
    name: any
    restField: any,
    disabledComponents: boolean,
    t: any,
}

const getStudyPlansRequests = loader('./requests/getStudyPlans.gql');
const getSubjecstByStudyPlanRequest = loader('./requests/getSubjectsByStudyPlan.gql');

const EquivalentCourse:React.FC<EquivalentCourseProps> = ({
  name, restField, disabledComponents, t,
}) => {
  const location = useLocation();
  const studyProgramId = location?.state?.studyProgram;
  const groupEnrollments = Form.useWatch('groupEnrollments');
  const [equivalentSubject, setEquivalentSubject] = useState<any>(null);

  useEffect(() => {
    if (groupEnrollments && disabledComponents) {
      setEquivalentSubject(groupEnrollments[name]?.equivalentSubject);
    }
  }, [groupEnrollments]);

  const [studyPlans, setStudyPlans] = useState<any>([]);
  const [coursesByStudyPlan, setCoursesByStudyPlan] = useState<any>([]);

  const [studyPlan, setStudyPlan] = useState<any>();

  const { data: studyPlanData } = useQuery(
    getStudyPlansRequests,
    {
      variables: { studyProgramId },
      fetchPolicy: 'no-cache',
      skip: !studyProgramId || disabledComponents,
    },
  );
  useEffect(() => {
    if (studyPlanData) {
      const { getStudyPlans } = studyPlanData;

      const studyPlansArray = getStudyPlans.map(({ id, year }: any) => (
        { value: id, label: year }
      ));
      setStudyPlans(studyPlansArray);
    }
  }, [studyPlanData]);

  const { data: subjecstByStudyPlanData } = useQuery(
    getSubjecstByStudyPlanRequest,
    {
      variables: { studyPlanId: studyPlan },
      fetchPolicy: 'no-cache',
      skip: !studyPlan || disabledComponents,
    },
  );
  useEffect(() => {
    if (subjecstByStudyPlanData) {
      const { getStudyPlanSubjects } = subjecstByStudyPlanData;
      const studyPlanSubjectsArray = getStudyPlanSubjects?.map(({ id, subject }: any) => (
        { value: id, label: subject }
      ));
      setCoursesByStudyPlan(studyPlanSubjectsArray);
    }
  }, [subjecstByStudyPlanData]);

  return (
    !equivalentSubject && disabledComponents
      ? <div />
      : (
        <Col xs={24} md={24}>
          <Collapse>
            <Collapse.Panel header={t('equivalentCourse')} key="1">
              <Row gutter={[10, 0]} align="middle">
                <Col xs={24} md={8}>
                  <Form.Item
                    {...restField}
                    name={[name, 'studyPlan']}
                    label={t('studyPlan')}
                    labelCol={{ span: 24 }}
                    wrapperCol={{ span: 24 }}
                    rules={[{ required: false }]}
                  >
                    <Select
                      style={{ width: '100%' }}
                      options={studyPlans}
                      onChange={(value) => setStudyPlan(value)}
                      disabled={disabledComponents}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={16}>
                  <Form.Item
                    {...restField}
                    initialValue={null}
                    name={[name, 'equivalentSubject']}
                    label={t('equivalentCourse')}
                    labelCol={{ span: 24 }}
                    wrapperCol={{ span: 24 }}
                    rules={[{ required: false }]}
                  >
                    <Select
                      style={{ width: '100%' }}
                      options={coursesByStudyPlan}
                      disabled={disabledComponents}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Collapse.Panel>
          </Collapse>
        </Col>
      )
  );
};

export default EquivalentCourse;
