import React, {
  useEffect, useState,
} from 'react';
import { useQuery } from '@apollo/client';
import {
  Button,
  Col, Divider, Empty, Modal, Row,
} from 'antd';
import { FilePdfOutlined } from '@ant-design/icons';
import { loader } from 'graphql.macro';
import { useTranslation } from 'react-i18next';
import { generateUrlDownload } from '../../utils/tools';

interface DocumentsModalProps {
  isVisible: boolean,
  setIsVisible: any,
  programOpPlanId: string,
}

const getOperatingPlanProgramDocumentsRequest = loader('./getOperatingPlanProgramDocuments.gql');

const DocumentsModal:React.FC<DocumentsModalProps> = ({
  isVisible,
  setIsVisible,
  programOpPlanId,
}) => {
  const [documents, setDocuments] = useState([]);
  const { t } = useTranslation();

  const { data } = useQuery(
    getOperatingPlanProgramDocumentsRequest,
    {
      variables: { programOpPlanId },
      fetchPolicy: 'no-cache',
      skip: !isVisible,
    },
  );

  useEffect(() => {
    if (data) {
      const { getOperatingPlanProgramDocuments } = data;
      setDocuments(getOperatingPlanProgramDocuments);
    }
  }, [data]);

  return (
    <Modal
      title="Documentos del plan de funcionamiento"
      centered
      width={600}
      closable
      destroyOnClose
      open={isVisible}
      bodyStyle={{ overflowY: 'scroll', maxHeight: 600 }}
      footer={null}
      onCancel={() => { setIsVisible({ isVisible: false }); }}
    >
      {
      documents.length > 0
        ? documents.map((document: any) => (
          <div key={document.id}>
            <Row align="middle">
              <Col span={16}>
                <span>
                  {document.planDocumentName}
                </span>
              </Col>
              <Col span={8}>
                <Button
                  type="primary"
                  target="_blank"
                  style={{ width: '100%' }}
                  href={generateUrlDownload(document?.planDocumentUrl)}
                  icon={<FilePdfOutlined />}
                >
                  {t('download')}
                </Button>
              </Col>
            </Row>
            <Divider />
          </div>
        ))
        : (
          <Row justify="center">
            <Empty
              image={Empty.PRESENTED_IMAGE_SIMPLE}
              description="No hay documentos."
            />
          </Row>
        )
      }
    </Modal>
  );
};

export default DocumentsModal;
