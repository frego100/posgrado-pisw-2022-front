import React from 'react';
import {
  Card, Col, Row, Empty,
} from 'antd';

import './styles.scss';

interface EmptyCardProps {
  title: string,
  description: string,
  bordered?: boolean,
  widthContent?: string,
  action?: React.ReactNode,
}

const defaultProps: {
  action?: React.ReactNode,
  widthContent?: string,
  bordered?: boolean,
} = {
  action: null,
  bordered: true,
  widthContent: 'auto',
};

const EmptyCard: React.FC<EmptyCardProps> = ({
  title, description, bordered, action, widthContent,
}) => (
  <Card bordered={bordered}>
    <Row justify="center" align="middle" style={{ height: 500 }}>
      <Col flex={widthContent}>
        <Row justify="center" style={{ marginBottom: 6 }}>
          <span className="font-bold">{title}</span>
        </Row>
        <Row justify="center" style={{ marginBottom: 12 }}>
          <span className="font-regular">{description}</span>
        </Row>
        { action || (
        <Row justify="center">
          <Empty description={false} />
        </Row>
        )}
      </Col>
    </Row>
  </Card>
);

EmptyCard.defaultProps = defaultProps;

export default EmptyCard;
