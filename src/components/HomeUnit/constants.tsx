import { TFunction } from 'i18next';

export const studyProgramColums = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'program',
    title: translate('program'),
    dataIndex: 'program',
    sorterType: 'alphabetical',
  },
  {
    key: 'professorsNumber',
    title: translate('professors'),
    dataIndex: 'professorsNumber',
    sorterType: 'numerical',
  },
  {
    key: 'studentsNumber',
    title: translate('students'),
    dataIndex: 'studentsNumber',
    sorterType: 'numerical',
  },
  {
    key: 'coursesNumber',
    title: translate('groups'),
    dataIndex: 'coursesNumber',
    sorterType: 'numerical',
  },
];

export default {};
