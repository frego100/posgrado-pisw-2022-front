import React from 'react';
import { Steps } from 'antd';
import { useTranslation } from 'react-i18next';
import {
  APPROVED, IN_PROGRESS, OBSERVED, RECEIVED,
} from '../../../utils/constants';

interface ProgressOperatingPlanProps {
  status: string,
}

const ProgressOperatingPlan: React.FC<ProgressOperatingPlanProps> = ({
  status,
}) => {
  const { t } = useTranslation();

  const typeStatus = (statusItem: string) :{ [index: string]: 'process' | 'error' | 'wait' | 'finish' } => ({
    [APPROVED]: 'finish',
    [OBSERVED]: statusItem === IN_PROGRESS ? 'finish'
      : statusItem === APPROVED ? 'error' : 'finish',
    [IN_PROGRESS]: statusItem === IN_PROGRESS ? 'process' : 'wait',
    [RECEIVED]: statusItem === IN_PROGRESS ? 'finish'
      : statusItem === RECEIVED ? 'process' : 'wait',
  });

  return (
    <Steps
      size="small"
      direction="vertical"
      items={[
        {
          title: t('inProgress'),
          status: typeStatus(IN_PROGRESS)[status],
          description: 'Plan de funcionamiento en curso',
        },
        {
          title: t('sent'),
          status: typeStatus(RECEIVED)[status],
          description: 'Plan de funcionamiento enviado a la dirección de posgrado.',
        },
        {
          title: typeStatus(APPROVED)[status] === 'error' ? t('isObserved') : t('isApproved'),
          status: typeStatus(APPROVED)[status],
          description: typeStatus(APPROVED)[status] === 'error'
            ? 'Plan de funcionamiento observado.'
            : 'Plan de funcionamiento aprobado.',
        },
      ]}
    />
  );
};

export default ProgressOperatingPlan;
