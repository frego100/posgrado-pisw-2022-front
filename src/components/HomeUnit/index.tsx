import { useQuery } from '@apollo/client';
import {
  Avatar,
  Card, Col, Divider, Row, Select, Statistic,
} from 'antd';
import { loader } from 'graphql.macro';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import CustomTable from '../CustomTable';
import Progress from './ProgressOperatingPlan';
import { studyProgramColums } from './constants';
import { startsetOperatingPlanProcess } from '../../store/thunks/auth';
import { AppDispatch } from '../../store';
import './styles.scss';
import EmptyView from '../EmptyView';
import { UNIT_DIRECTOR } from '../../utils/constants';

const getAllProgramOperatingPlansRequest = loader('./requests/getAllProgramOperatingPlan.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');

const HomeUnit: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const {
    firstName, lastName, graduateUnitId, graduateUnitName, operatingPlanProcess,
  } = useSelector((state: any) => state.auth);
  const [process, setProcess] = useState<any>(operatingPlanProcess);
  const [processes, setProcesses] = useState<any>(null);
  const [studyPrograms, setStudyPrograms] = useState<any>([]);
  const [operatingPlanStatus, setOperatingPlanStatus] = useState<any>();

  const [numOfProfessors, setNumOfProfessors] = useState<number>(0);
  const [numOfStudents, setNumOfStudents] = useState<number>(0);
  const [numOfCourses, setNumOfCourses] = useState<number>(0);

  const {
    data: operatingPlanProcessesData,
    loading: operatingPlanProcessesLoading,
  } = useQuery(getOperatingPlanProcessesRequest);

  useEffect(() => {
    dispatch(startsetOperatingPlanProcess(process));
  }, [process]);

  useEffect(() => {
    if (operatingPlanProcessesData) {
      const { getOperatingPlanProcesses } = operatingPlanProcessesData;
      const operatingPlanProcessesArray: any = [];

      getOperatingPlanProcesses.forEach(({ id, year }: any) => {
        operatingPlanProcessesArray.push({ label: year, value: id });
      });
      if (!process) {
        setProcess(operatingPlanProcessesArray[0]?.value);
      }
      setProcesses(operatingPlanProcessesArray);
    }
  }, [operatingPlanProcessesData]);

  const { data, loading } = useQuery(
    getAllProgramOperatingPlansRequest,
    {
      variables: { unit: graduateUnitId, process },
      fetchPolicy: 'cache-and-network',
      skip: !graduateUnitId || !process,
    },
  );

  useEffect(() => {
    if (data) {
      const { getAllProgramOperatingPlans } = data;
      const studyProgramsArray = [];

      getAllProgramOperatingPlans.map(({ studyProgram }:any) => (
        studyProgramsArray.push({
          key: studyProgram.id,
          code: studyProgram.code,
          program: studyProgram.name,
          professorsNumber: studyProgram.numOfProfessors,
          studentsNumber: studyProgram.numOfStudents,
          coursesNumber: studyProgram.numOfCourseGroups,
        })
      ));

      const courses = studyProgramsArray.reduce(
        (total, { coursesNumber }) => total + coursesNumber,
        0,
      );
      setNumOfProfessors(getAllProgramOperatingPlans
        ?.[0]?.operatingPlan?.graduateUnit?.numOfProfessors);
      setNumOfStudents(getAllProgramOperatingPlans
        ?.[0]?.operatingPlan?.graduateUnit?.numOfStudents);
      setNumOfCourses(courses);
      setOperatingPlanStatus(getAllProgramOperatingPlans?.[0]?.operatingPlan?.status);
      setStudyPrograms(studyProgramsArray);
    }
  }, [data]);

  const numOfStudyPrograms = studyPrograms ? studyPrograms.length : 0;

  return (
    <div style={{ marginTop: '22px' }}>
      <div className="headerHome">
        <span className="header-title">{`${t('welcome')} ${firstName} ${lastName}`}</span>
        <span className="header-subtitle">{graduateUnitName}</span>
      </div>
      <Divider />
      {processes && processes.length === 0
        ? (
          <Card>
            <EmptyView
              type="emptyOperatingPlanProcess"
              userType={UNIT_DIRECTOR}
              viewTitle={false}
            />
          </Card>
        ) : (
          <>
            <Row gutter={16}>
              <Col xs={24} sm={24} lg={6}>
                <Card>
                  <Row gutter={[25, 25]}>
                    <Col span={24}>
                      <span className="label">
                        {t('academicYear')}
                      </span>
                      <Select
                        labelInValue
                        value={process}
                        style={{ width: '100%' }}
                        options={processes}
                        onChange={(newValue) => setProcess(newValue.value)}
                        loading={operatingPlanProcessesLoading}
                      />
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col xs={24} sm={24} lg={18}>
                <Row gutter={16}>
                  <Col xs={12} sm={12} md={6}>
                    <Card>
                      <Statistic title={t('programs')} value={numOfStudyPrograms} prefix={<Avatar src="https://cdn-icons-png.flaticon.com/512/29/29302.png" />} />
                    </Card>
                  </Col>
                  <Col xs={12} sm={12} md={6}>
                    <Card>
                      <Statistic title={t('professors')} value={numOfProfessors} prefix={<Avatar src="https://cdn-icons-png.flaticon.com/512/2815/2815419.png" />} />
                    </Card>
                  </Col>
                  <Col xs={12} sm={12} md={6}>
                    <Card>
                      <Statistic title={t('students')} value={numOfStudents} prefix={<Avatar src="https://as1.ftcdn.net/v2/jpg/01/05/29/62/1000_F_105296263_MX030meFkK57Jj7z4TaSZXw1T0KPQXmd.jpg" />} />
                    </Card>
                  </Col>
                  <Col xs={12} sm={12} md={6}>
                    <Card>
                      <Statistic title={t('groups')} value={numOfCourses} prefix={<Avatar src="https://cdn-icons-png.flaticon.com/512/29/29302.png" />} />
                    </Card>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xs={24} sm={24} lg={18}>
                <Card>
                  <b><p>{t('studyPrograms')}</p></b>
                  <Divider />
                  <CustomTable
                    dataSource={studyPrograms}
                    loading={loading}
                    columns={studyProgramColums(t)}
                  />
                </Card>
              </Col>
              <Col xs={24} sm={24} lg={6}>
                <Card>
                  <b><p>{t('operatingPlanStatus')}</p></b>
                  <Divider />
                  <Progress status={operatingPlanStatus} />
                  <Divider />
                </Card>
              </Col>
            </Row>
          </>
        )}
    </div>
  );
};

export default HomeUnit;
