import React from 'react';

import { TFunction } from 'react-i18next';
import {
  Button, Checkbox, Col, Input, InputNumber, Row, Select,
} from 'antd';
import Swal from 'sweetalert2';
import { FilePdfOutlined } from '@ant-design/icons';
import moment from 'moment';
import { mapIdentificationSet } from '../ProfessorView/constants';
import { generateUrlDownload } from '../../utils/tools';
import CustomUploadFile from '../CustomUploadFile';

const { Option } = Select;

const searchStudent = (
  t: TFunction,
  disabledDocumentNumber: boolean,
  getStudentByCuiLoading: boolean,
  form: any,
  getStudentByCuiQuery: any,
  setStudent: any,
  setDisabledDocumentNumber: any,
  setStudentExists: any,
  identificationType: any,
  setDisabledComponents: any,
  setPeriods: any,
  setSubjects: any,
  getStudentEnrollmentsQuery: any,
  studyProgram: any,
  process: any,
) => (
  {
    [t('search')]: [
      {
        name: 'identificationType',
        component: (
          <Select
            style={{ width: '100%' }}
          >
            <Option value="cui">CUI</Option>
          </Select>
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('type'),
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
      {
        name: 'documentNumber',
        component: (
          <InputNumber
            style={{ width: '100%' }}
            controls={false}
            maxLength={8}
          />
        ),
        rules: [
          () => ({
            validator(_: any, value: any) {
              if (value && value.toString().length === 8) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('¡Ingresa un documento válido!'));
            },
          }),
        ],
        label: t('number'),
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
      {
        component: (
          <Button
            type="primary"
            style={{ width: '100%' }}
            // disabled={disabledDocumentNumber}
            loading={getStudentByCuiLoading}
            onClick={() => {
              form.validateFields(['documentNumber', 'identificationType'])
                .then((values) => {
                  getStudentByCuiQuery({
                    variables: {
                      cui: values.documentNumber,
                    },
                  })
                    .then(({ data }: any) => {
                      const { getStudentByCui } = data;
                      if (getStudentByCui) {
                        const studentData = {
                          ...getStudentByCui,
                          ...getStudentByCui.user,
                          birthday: getStudentByCui?.user?.birthday
                            ? moment(getStudentByCui.user.birthday).format('DD/MM/YYYY')
                            : null,
                        };
                        getStudentEnrollmentsQuery({
                          variables: {
                            studentId: getStudentByCui?.id,
                            process,
                            program: studyProgram,
                          },
                        })
                          .then(({ data: getStudentEnrollmentsData }) => {
                            const { getStudentEnrollments } = getStudentEnrollmentsData;
                            if (getStudentEnrollments) {
                              const array = getStudentEnrollments[0]?.groupenrollmentSet
                                ?.map((item) => ({
                                  value: item.id, label: item.group.course.studyPlanSubject.subject,
                                }));
                              setSubjects(array);
                              setStudent({
                                ...studentData,
                                enrollmentId: getStudentEnrollments[0]?.id,
                              });
                              setPeriods(getStudentEnrollments.map(
                                (item) => ({ label: `${item.enrollmentPeriod.year} - ${item.enrollmentPeriod.period}`, value: item.id }),
                              ));
                              form.setFieldValue('periods', getStudentEnrollments[0]?.id);
                              setDisabledDocumentNumber(true);
                              setStudentExists(true);
                              setDisabledComponents(false);
                              form.setFieldsValue(studentData);

                              Swal.fire({
                                title: t('studentHasBeenFound'),
                                icon: 'info',
                                confirmButtonText: t('accept'),
                              });
                            } else {
                              Swal.fire({
                                title: t('studentHasBeenFoundEnrollment'),
                                icon: 'info',
                                confirmButtonText: t('accept'),
                              });
                            }
                          });
                      } else {
                        Swal.fire({
                          title: t('studentHasBeenNotFoundCui'),
                          icon: 'info',
                          confirmButtonText: t('accept'),
                        });
                      }
                    });
                });
            }}
          >
            {t('verify')}
          </Button>
        ),
        label: t('verifyDocumentNumber'),
        name: 'verifyProfessor',
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
    ],
  }
);

export const enrollmentRemovalItems = (
  t: TFunction,
  disabledDocumentNumber: boolean,
  getStudentByCuiLoading: boolean,
  form: any,
  getStudentByCuiQuery: any,
  setStudent: any,
  setDisabledDocumentNumber: any,
  setStudentExists: any,
  identificationType: any,
  setDisabledComponents: any,
  readOnly: boolean,
  disabledComponents: boolean,
  studentData: any,
  periods: any,
  subjects: any,
  setPeriods: any,
  setSubjects: any,
  getStudentEnrollmentsQuery: any,
  studyProgram: any,
  process: any,
) => [
  {
    ...!readOnly && searchStudent(
      t,
      disabledDocumentNumber,
      getStudentByCuiLoading,
      form,
      getStudentByCuiQuery,
      setStudent,
      setDisabledDocumentNumber,
      setStudentExists,
      identificationType,
      setDisabledComponents,
      setPeriods,
      setSubjects,
      getStudentEnrollmentsQuery,
      studyProgram,
      process,
    ),
    [t('personalInformation')]: [
      {
        name: 'firstName',
        component: <Input disabled />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('firstName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'lastName',
        component: <Input disabled />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('fatherLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'maternalLastName',
        component: <Input disabled />,
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('maternalLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'birthday',
        component: <Input disabled />,
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('birthDate'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'email',
        component: <Input disabled />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('email'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'phone',
        component: <Input disabled />,
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('phone'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      ...mapIdentificationSet(studentData, !readOnly, readOnly, t),
    ],
  },
  {
    [t('enrollmentRemoval')]: [
      {
        name: 'periods',
        component:
        disabledComponents ? <Input disabled /> : (
          <Select
            style={{ width: '100%' }}
            options={periods}
            disabled={disabledComponents}
          />
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('period'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'withdrawType',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('removalType'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'subjects',
        component: (
          <Checkbox.Group disabled={readOnly}>
            <Row gutter={[16, 8]}>
              {subjects?.map((item) => (
                <Col span={24}>
                  <Checkbox value={item.value}>{item.label}</Checkbox>
                </Col>
              ))}
            </Row>
          </Checkbox.Group>
        ),
        rules: [
          {
            required: true,
            message: 'Seleccione al menos un curso!',
          },
        ],
        label: t('subjects'),
        responsive: {
          xs: { span: 24 },
          md: { span: 24 },
        },
        display: true,
      },
      {
        name: 'removalResolution',
        component: (
          readOnly
            ? (
              <Button
                type="primary"
                target="_blank"
                style={{ width: '100%' }}
                href={generateUrlDownload(studentData?.removalResolution)}
                icon={<FilePdfOutlined />}
              >
                {t('download')}
              </Button>
            ) : (
              <CustomUploadFile
                disabled={disabledComponents}
                form={form}
                nameComponent="removalResolution"
              />
            )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('resolution'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'resolutionNumber',
        component: (
          <Input
            style={{ width: '100%' }}
            disabled={disabledComponents}
          />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('resolutionNumber'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
    ],
  },
];

export default {};
