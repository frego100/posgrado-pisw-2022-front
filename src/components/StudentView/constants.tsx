import { TFunction } from 'react-i18next';
import {
  Button, Card, Col, DatePicker, Divider, Form, Input, InputNumber, Radio, Row, Select,
} from 'antd';
import React from 'react';
import Swal from 'sweetalert2';
import {
  PlusOutlined,
  MinusCircleOutlined,
  FilePdfOutlined,
} from '@ant-design/icons';
import moment from 'moment';
import CustomUploadFile from '../CustomUploadFile';
import { mapIdentificationSet } from '../ProfessorView/constants';
import { generateUrlDownload, isUndefined } from '../../utils/tools';
import EquivalentCourse from '../EquivalentSubject';

const { Option } = Select;

// Constants gender
export const GENDER_MALE = 'MALE';
export const GENDER_FEMALE = 'FEMALE';
export const GENDER_MALE_BACKEND = 'A_1';
export const GENDER_FEMALE_BACKEND = 'A_2';

export const genders :{[index: string] : string} = ({
  A_1: 'MALE',
  A_2: 'FEMALE',
});

const searchStudentSection = (
  t: TFunction,
  disabledDocumentNumber: boolean,
  searchUserLoading: boolean,
  form: any,
  searchUserQuery: any,
  setStudent: any,
  setDisabledDocumentNumber: any,
  setStudentExists: any,
  identificationType: any,
  setDisabledComponents: any,
) => (
  {
    [t('search')]: [
      {
        name: 'identificationType',
        component: (
          <Select
            style={{ width: '100%' }}
          >
            <Option value="DNI">DNI</Option>
            <Option value="FOREIGN_CARD">Carnet de extranjería</Option>
          </Select>
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('type'),
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
      {
        name: 'documentNumber',
        component: (
          <InputNumber
            style={{ width: '100%' }}
            controls={false}
            maxLength={identificationType === 'DNI' ? 8 : 12}
          />
        ),
        rules: [
          () => ({
            validator(_: any, value: any) {
              const length = identificationType === 'DNI' ? 8 : 12;
              if (value && value.toString().length === length) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('¡Ingresa un documento válido!'));
            },
          }),
        ],
        label: t('number'),
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
      {
        component: (
          <Button
            type="primary"
            style={{ width: '100%' }}
            loading={searchUserLoading}
            onClick={() => {
              form.validateFields(['documentNumber', 'identificationType'])
                .then((values) => {
                  searchUserQuery({
                    variables: {
                      idType: values.identificationType === 'DNI' ? 1 : 2,
                      idNumber: values.documentNumber,
                    },
                  })
                    .then(({ data }: any) => {
                      const { searchUser } = data;
                      if (searchUser) {
                        const studentData = {
                          ...searchUser,
                          ...searchUser.student,
                          birthday: searchUser?.birthday ? moment(searchUser.birthday) : null,
                          gender: genders[searchUser?.gender],
                          numberCredits: 0,
                          numberCourses: 0,
                        };
                        form.setFieldsValue(studentData);
                        setStudent(studentData);

                        setDisabledDocumentNumber(true);
                        setStudentExists(true);
                        setDisabledComponents(false);

                        Swal.fire({
                          title: t('studentHasBeenFound'),
                          icon: 'info',
                          confirmButtonText: t('accept'),
                        });
                      } else {
                        const idType = values.identificationType;
                        const idNumber = values.documentNumber;
                        form.resetFields();
                        form.setFieldsValue({
                          identificationType: idType,
                          documentNumber: idNumber,
                        });
                        Swal.fire({
                          title: t('studentHasBeenNotFound'),
                          icon: 'info',
                          confirmButtonText: t('accept'),
                        });
                        // setStudentExists(false);
                        // setStudent({});
                        setDisabledComponents(false);
                      }
                    });
                });
            }}
          >
            {t('verify')}
          </Button>
        ),
        label: t('verifyDocumentNumber'),
        name: 'verifyProfessor',
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
    ],
  }
);

const studentViewItems = (
  createStudent: boolean,
  t: TFunction,
  disabledDocumentNumber: boolean,
  searchUserLoading: boolean,
  form: any,
  searchUserRequestQuery: any,
  setStudent: any,
  setDisabledDocumentNumber: any,
  setStudentExists: any,
  disabledComponents: boolean,
  studentExists: boolean,
  identificationType: any,
  courseGroups: any,
  setDisabledComponents: any,
  readOnly: boolean,
  studentData: any,
  lastEnrollmentYear: any,
  enrollmentUpdateResolution: any,
) => [
  {
    ...!readOnly && searchStudentSection(
      t,
      disabledDocumentNumber,
      searchUserLoading,
      form,
      searchUserRequestQuery,
      setStudent,
      setDisabledDocumentNumber,
      setStudentExists,
      identificationType,
      setDisabledComponents,
    ),
    [t('personalInformation')]: [
      {
        name: 'firstName',
        component: <Input disabled={disabledComponents} />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('firstName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'lastName',
        component: <Input disabled={disabledComponents} />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('fatherLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'maternalLastName',
        component: <Input disabled={disabledComponents} />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('maternalLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'birthday',
        component: (
          disabledComponents ? (
            <Input disabled />
          )
            : (
              <DatePicker
                className="component"
                allowClear={false}
                disabledDate={(current) => current && current > moment()}
              />
            )
        ),
        rules: [{ required: false }],
        label: t('birthDate'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'email',
        component: <Input disabled={disabledComponents} />,
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
          {
            type: 'email',
            message: '¡Ingrese un correo electronico válido!',
          },
        ],
        label: t('email'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'phone',
        component: <InputNumber style={{ width: '100%' }} disabled={disabledComponents} maxLength={15} controls={false} />,
        rules: [{ required: false }],
        label: t('phone'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'gender',
        component: (
          disabledComponents ? (
            <Input disabled />
          )
            : (
              <Radio.Group disabled={disabledComponents}>
                <Radio value={GENDER_MALE}> Masculino </Radio>
                <Radio value={GENDER_FEMALE}> Femenino </Radio>
              </Radio.Group>
            )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('gender'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      ...mapIdentificationSet(studentData, !readOnly, readOnly, t),
    ],
    ...readOnly && {
      [t('studentData')]: [
        {
          name: 'code',
          component: <Input disabled={disabledComponents} />,
          rules: [
            {
              required: true,
              message: 'Campo obligatorio!',
            },
          ],
          label: t('entrantCode'),
          responsive: {
            xs: { span: 24 },
            md: { span: 12 },
          },
          display: true,
        },
        {
          name: 'cui',
          component: (
            <InputNumber
              style={{ width: '100%' }}
              controls={false}
              maxLength={8}
              disabled={disabledComponents}
            />
          ),
          rules: [
            () => ({
              validator(_: any, value: any) {
                if (value && value.toString().length === 8) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('¡Ingresa un documento válido!'));
              },
            }),
          ],
          label: t('cui'),
          responsive: {
            xs: { span: 24 },
            md: { span: 12 },
          },
          display: true,
        },
      ],
    },
  },
  {
    ...!readOnly && {
      [t('studentData')]: [
        {
          name: 'code',
          component: <Input disabled={disabledComponents} maxLength={8} />,
          rules: [
            {
              required: true,
              message: 'Campo obligatorio!',
            },
          ],
          label: t('entrantCode'),
          responsive: {
            xs: { span: 24 },
            md: { span: 12 },
          },
          display: true,
        },
        {
          name: 'cui',
          component: (
            <InputNumber
              style={{ width: '100%' }}
              controls={false}
              maxLength={8}
              disabled={disabledComponents}
            />
          ),
          rules: [
            () => ({
              validator(_: any, value: any) {
                if (value && value.toString().length === 8) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('¡Ingresa un documento válido!'));
              },
            }),
          ],
          label: t('cui'),
          responsive: {
            xs: { span: 24 },
            md: { span: 12 },
          },
          display: true,
        },
      ],
    },
    [t('enrollmentDocuments')]: [
      {
        name: 'numberCourses',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('numberCourses'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'numberCredits',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('numberCredits'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'subjectEnrollment',
        component: (
          <div className="subject-enrollment-card">
            <Card>
              <Form.List
                name="groupEnrollments"
                rules={[
                  {
                    validator: async (_, groupEnrollments) => {
                      if (!groupEnrollments || groupEnrollments.length < 1) {
                        return Promise.reject(new Error('Seleccione al menos un curso'));
                      } return Promise.resolve();
                    },
                  },
                ]}
              >
                {(fields, { add, remove }) => (
                  <>
                    {fields.map(({ key, name, ...restField }) => (
                      <Row gutter={[16, 0]} align="middle">
                        <Col xs={24} md={16}>
                          <Form.Item
                            {...restField}
                            name={[name, 'courseGroup']}
                            label={t('subject')}
                            labelCol={{ span: 24 }}
                            wrapperCol={{ span: 24 }}
                            rules={[
                              {
                                required: true,
                                message: 'Campo obligatorio!',
                              },
                            ]}
                          >
                            <Select
                              style={{ width: '100%' }}
                              options={courseGroups}
                              disabled={disabledComponents}
                            />
                          </Form.Item>
                        </Col>
                        <Col xs={22} md={6}>
                          <Form.Item
                            {...restField}
                            name={[name, 'enrollmentNum']}
                            label={t('numberEnrollment')}
                            labelCol={{ span: 24 }}
                            wrapperCol={{ span: 24 }}
                            rules={[
                              {
                                required: true,
                                message: 'Campo obligatorio!',
                              },
                            ]}
                          >
                            <InputNumber
                              style={{ width: '100%' }}
                              controls={false}
                              maxLength={1}
                              disabled={disabledComponents}
                            />
                          </Form.Item>
                        </Col>
                        {!readOnly && (
                        <Col xs={2} md={2}>
                          <MinusCircleOutlined
                            style={{ fontSize: 28 }}
                            onClick={() => remove(name)}
                            disabled={disabledComponents}
                          />
                        </Col>
                        )}
                        <EquivalentCourse
                          name={name}
                          t={t}
                          restField={{ ...restField }}
                          disabledComponents={disabledComponents}
                        />
                        <Divider className="coursesDivider" />
                      </Row>
                    ))}
                    {!readOnly && (
                    <Form.Item style={{ marginBottom: 0 }}>
                      <Button
                        type="ghost"
                        onClick={() => add()}
                        block
                        icon={<PlusOutlined />}
                        disabled={disabledComponents}
                      >
                        {t('addCourse')}
                      </Button>
                    </Form.Item>
                    )}
                  </>
                )}
              </Form.List>
            </Card>
          </div>
        ),
        rules: [
          {
            required: true,
            message: 'Seleccione al menos un curso!',
          },
        ],
        label: t('subjectEnrollment'),
        responsive: {
          xs: { span: 24 },
          md: { span: 24 },
        },
        display: true,
      },
      {
        name: 'enrollmentUpdateResolution',
        component: (
          readOnly
            ? (
              <Button
                type="primary"
                target="_blank"
                style={{ width: '100%' }}
                href={generateUrlDownload(enrollmentUpdateResolution)}
                icon={<FilePdfOutlined />}
              >
                Descargar
              </Button>
            ) : (
              <CustomUploadFile
                disabled={disabledComponents}
                form={form}
                nameComponent="enrollmentUpdateResolution"
              />
            )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('resolution'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'resolutionNumber',
        component: (
          <Input
            style={{ width: '100%' }}
            disabled={disabledComponents}
            maxLength={50}
          />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('resolutionNumber'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      ...(isUndefined(lastEnrollmentYear) ? [] : [
        {
          name: 'lastEnrollmentYear',
          component: (
            <InputNumber
              style={{ width: '100%' }}
              controls={false}
              maxLength={4}
              minLength={4}
              disabled={disabledComponents}
            />
          ),
          rules: [
            {
              required: true,
              message: 'Campo obligatorio!',
            },
          ],
          label: t('lastEnrollmentYear'),
          responsive: {
            xs: { span: 24 },
            md: { span: 12 },
          },
          display: true,
        },
      ]),
    ],
  },

];

export default studentViewItems;
