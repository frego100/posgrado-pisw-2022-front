import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useForm } from 'antd/lib/form/Form';
import {
  Button, Col, Form, message, Row,
} from 'antd';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import moment from 'moment';
import studentViewItems, { GENDER_MALE_BACKEND, genders } from './constants';
import FormCard from '../FormCard';
import routesDictionary from '../../routes/routesDictionary';
import './styles.scss';
import { documentTypeKeys } from '../ProfessorView/constants';
import { enrollmentRemovalItems } from './enrollmentRemovalItems';
import { isUndefined } from '../../utils/tools';

interface StudentViewProps {
  createStudent: boolean
  updateEnrollment: boolean
  readOnly: boolean
}

const getStudentByCuiRequest = loader('./requests/getStudentByCui.gql');
const addManualEnrollmentRequest = loader('./requests/addManualEnrollment.gql');
const searchUserRequest = loader('./requests/searchUser.gql');
const getCourseGroupsByStudyProgramRequest = loader('./requests/getCourseGroupsByStudyProgram.gql');
const createEnrollmentUpdateRequest = loader('./requests/createEnrollmentUpdate.gql');
const createEnrollmentRemovalRequest = loader('./requests/createEnrollmentRemoval.gql');
const getEnrollmentUpdateRequest = loader('./requests/getEnrollmentUpdate.gql');
const getEnrollmentRemovalRequest = loader('./requests/getEnrollmentRemoval.gql');
const getStudentEnrollmentsRequest = loader('./requests/getStudentEnrollments.gql');

const StudentView:React.FC<StudentViewProps> = ({
  createStudent,
  updateEnrollment, readOnly,
}) => {
  const { t } = useTranslation();
  const [disabledDocumentNumber, setDisabledDocumentNumber] = useState<boolean>(false);
  const location = useLocation();

  const [studentData, setStudentData] = useState<any>();
  // @ts-ignore
  const studyProgram = location?.state?.studyProgram;
  const process = location?.state?.process;
  const [form] = useForm();
  const lastEnrollmentYear = Form.useWatch('endDate', form);

  const params = useParams();
  const { enrollmentUpdateId, enrollmentRemovalId } = params;

  // const [period, setPeriod] = useState<any>();
  const [periods, setPeriods] = useState<any>([]);
  const [subjects, setSubjects] = useState<any>();

  // eslint-disable-next-line
  const { pathname } = useLocation();

  const [getStudentEnrollmentsQuery] = useLazyQuery(getStudentEnrollmentsRequest);

  const [courseGroups, setCourseGroups] = useState<any>([]);
  // const [getStudentQuery,
  //   { loading: getStudentLoading }] = useLazyQuery(getStudentByCuiRequest);
  const [searchUserRequestQuery,
    { loading: searchUserLoading }] = useLazyQuery(searchUserRequest);

  const [getStudentByCuiQuery,
    { loading: getStudentByCuiLoading }] = useLazyQuery(getStudentByCuiRequest);

  const [createEnrollmentUpdateMutation] = useMutation(createEnrollmentUpdateRequest);
  const [createEnrollmentRemovalMutation] = useMutation(createEnrollmentRemovalRequest);

  const { data: getCourseGroupsData } = useQuery(
    getCourseGroupsByStudyProgramRequest,
    {
      variables: { studyProgramId: studyProgram },
      fetchPolicy: 'no-cache',
      skip: !updateEnrollment,
    },
  );

  const {
    data: getEnrollmentUpdateData,
  } = useQuery(
    getEnrollmentUpdateRequest,
    {
      variables: { enrollmentUpdateId },
      fetchPolicy: 'no-cache',
      skip: isUndefined(enrollmentUpdateId),
    },
  );

  const {
    data: getEnrollmentRemovalData,
  } = useQuery(
    getEnrollmentRemovalRequest,
    {
      variables: { enrollmentRemovalId },
      fetchPolicy: 'no-cache',
      skip: isUndefined(enrollmentRemovalId),
    },
  );

  useEffect(() => {
    if (getEnrollmentRemovalData) {
      const { getEnrollmentRemoval } = getEnrollmentRemovalData;

      if (getEnrollmentRemoval) {
        const identificationSet: any = {};
        const { enrollment } = getEnrollmentRemoval;
        getEnrollmentRemoval
          .enrollment
          .student
          .user
          .useridentificationSet?.forEach((identification: any) => {
            identificationSet[
              documentTypeKeys[identification.identificationType]
            ] = identification.number;
          });

        const studentObj = {
          ...enrollment?.student,
          ...enrollment?.student?.user,
          ...getEnrollmentRemoval,
          birthday: enrollment?.student?.user?.birthday
            ? moment(enrollment?.student?.user.birthday).format('DD/MM/YYYY')
            : null,
          gender: readOnly
            ? (enrollment?.student?.user?.gender === GENDER_MALE_BACKEND ? 'Masculino' : 'Femenino')
            : genders[enrollment?.student?.user?.gender],
          periods: `${getEnrollmentRemoval.enrollment.enrollmentPeriod.year} - ${getEnrollmentRemoval.enrollment.enrollmentPeriod.period}`,
          ...identificationSet,
        };

        setSubjects(getEnrollmentRemoval.groupEnrollments
          .map(
            (item) => ({
              value: '1',
              label: item.group.course.studyPlanSubject.subject,
            }),
          ));
        form.setFieldsValue({ ...studentObj, subjects: ['1'] });
        setStudentData(studentObj);
      }
    }
  }, [getEnrollmentRemovalData]);

  const [enrollmentUpdateResolution, setEnrollmentUpdateResolution] = useState();
  useEffect(() => {
    if (getEnrollmentUpdateData) {
      const { getEnrollmentUpdate } = getEnrollmentUpdateData;
      setEnrollmentUpdateResolution(getEnrollmentUpdate.enrollmentUpdateResolution);
      const enrollment = getEnrollmentUpdate.enrollments[0];
      const { student: studentEnrollment } = getEnrollmentUpdate;

      const identificationSet: any = {};
      getEnrollmentUpdate.student.user.useridentificationSet?.forEach((identification: any) => {
        identificationSet[
          documentTypeKeys[identification.identificationType]
        ] = identification.number;
      });

      const studentObj = {
        ...studentEnrollment,
        ...studentEnrollment?.user,
        numberCredits: enrollment?.credits,
        numberCourses: enrollment?.courses,
        birthday: studentEnrollment?.user?.birthday
          ? moment(studentEnrollment?.user.birthday).format('DD/MM/YYYY')
          : null,
        gender: readOnly
          ? (studentEnrollment?.user?.gender === GENDER_MALE_BACKEND ? 'Masculino' : 'Femenino')
          : genders[studentEnrollment?.user?.gender],
        groupEnrollments: enrollment?.groupenrollmentSet?.map((item) => ({
          courseGroup: item.group.course.studyPlanSubject.subject,
          enrollmentNum: item.enrollmentNum,
          studyPlan: item.equivalentSubject?.studyPlan?.year,
          equivalentSubject: item.equivalentSubject?.subject,
        })),
        ...identificationSet,
        resolutionFile: enrollment?.groupenrollmentSet[0].resolutionFile,
        resolutionNumber: getEnrollmentUpdate.resolutionNumber,
      };

      form.setFieldsValue(studentObj);
      setStudentData(studentObj);
    }
  }, [getEnrollmentUpdateData]);

  useEffect(() => {
    if (getCourseGroupsData) {
      const { getCourseGroupsByStudyProgram } = getCourseGroupsData;

      const courseGroupByProgramArray = getCourseGroupsByStudyProgram.map((item) => ({
        value: item.id,
        label: item.course.studyPlanSubject.subject,
        credits: item.course.studyPlanSubject.credits,
      }));
      setCourseGroups(courseGroupByProgramArray);
    }
  }, [getCourseGroupsData]);

  // eslint-disable-next-line
  const [student, setStudent] = useState<any>();
  const [studentExists, setStudentExists] = useState();
  const navigate = useNavigate();

  const [disabledComponents, setDisabledComponents] = useState(true);
  const identificationType = Form.useWatch('identificationType', form);
  const groupEnrollments = Form.useWatch('groupEnrollments', form);
  const subjectsEnrollments = Form.useWatch('subjects', form);

  useEffect(() => {
    if (subjects && subjectsEnrollments && !readOnly) {
      form.setFieldValue(
        'withdrawType',
        subjectsEnrollments?.length === subjects?.length ? 'Total' : 'Parcial',
      );
    }
  }, [subjectsEnrollments]);
  const disabled = (courseId) => {
    let isDisabled = false;
    groupEnrollments?.forEach((item) => {
      if (item?.courseGroup === courseId) {
        isDisabled = true;
      }
    });
    return isDisabled;
  };
  useEffect(() => {
    if (!readOnly) {
      form.setFieldValue('numberCourses', groupEnrollments?.length);
      form.setFieldValue('numberCredits', courseGroups.reduce((accumulator, current) => {
        if (disabled(current.value)) {
          return accumulator + current.credits;
        }
        return accumulator;
      }, 0));

      setCourseGroups(
        courseGroups.map(
          (courseGroup) => ({ ...courseGroup, disabled: disabled(courseGroup.value) }),
        ),
      );
    }
  }, [groupEnrollments]);
  // eslint-disable-next-line
  const [addManualEnrollmentMutation,
    { loading: addManualEnrollmentLoading }] = useMutation(addManualEnrollmentRequest);

  const handleCreateUpdateEnrollment = () => {
    form.validateFields()
      .then((values) => {
        if (updateEnrollment) {
          createEnrollmentUpdateMutation({
            variables: {
              input: {
                student: {
                  email: values.email,
                  firstName: values.firstName,
                  lastName: values.lastName,
                  maternalLastName: values.maternalLastName,
                  birthday: moment(values.birthday).format('YYYY-MM-DD'),
                  gender: values.gender,
                  phone: values.phone,
                  userIdentifications: [{
                    identificationType: values.identificationType,
                    number: values.documentNumber,
                  }],
                  code: values.code,
                  cui: values.cui,
                },
                groupEnrollments: values.groupEnrollments.map((item) => {
                  if (!item.equivalentSubject) item.equivalentSubject = null;
                  const { studyPlan, ...courseEnrollments } = item;
                  return courseEnrollments;
                }),
                studyProgram,
                process,
                enrollmentUpdateResolution: values.enrollmentUpdateResolution.originFileObj,
                resolutionNumber: values.resolutionNumber,
              },
            },
          })
            .then(({ data }) => {
              const { createEnrollmentUpdate } = data;
              if (createEnrollmentUpdate.feedback.status === 'SUCCESS') {
                message.success(createEnrollmentUpdate.feedback.message);
                navigate(routesDictionary.enrollmentUpdate.route);
              } else {
                message.error(createEnrollmentUpdate.feedback.message);
                createEnrollmentUpdate.errors.forEach((error) => {
                  form.setFields([{
                    name: error.field,
                    errors: error.messages,
                  }]);
                });
              }
            }).catch((e) => {
              // eslint-disable-next-line no-console
              console.log(e.message);
            });
        } else {
          createEnrollmentRemovalMutation({
            variables: {
              input: {
                enrollment: studentData.enrollmentId,
                groupEnrollments: values.subjects,
                removalResolution: values.removalResolution.originFileObj,
                resolutionNumber: values.resolutionNumber,
              },
            },
          })
            .then(({ data }) => {
              const { createEnrollmentRemoval } = data;
              if (createEnrollmentRemoval.feedback.status === 'SUCCESS') {
                message.success(createEnrollmentRemoval.feedback.message);
                navigate(routesDictionary.enrollmentRemoval.route);
              } else {
                message.error(createEnrollmentRemoval.feedback.message);
                createEnrollmentRemoval.errors.forEach((error) => {
                  form.setFields([{
                    name: error.field,
                    errors: error.messages,
                  }]);
                });
              }
            });
        }
      });
  };

  const items = updateEnrollment
    ? studentViewItems(
      createStudent,
      t,
      disabledDocumentNumber,
      searchUserLoading,
      form,
      searchUserRequestQuery,
      setStudent,
      setDisabledDocumentNumber,
      setStudentExists,
      disabledComponents,
      studentExists,
      identificationType,
      courseGroups,
      setDisabledComponents,
      readOnly,
      studentData,
      lastEnrollmentYear,
      enrollmentUpdateResolution,
    )
    : enrollmentRemovalItems(
      t,
      disabledDocumentNumber,
      getStudentByCuiLoading,
      form,
      getStudentByCuiQuery,
      setStudentData,
      setDisabledDocumentNumber,
      setStudentExists,
      identificationType,
      setDisabledComponents,
      readOnly,
      disabledComponents,
      studentData,
      periods,
      subjects,
      setPeriods,
      setSubjects,
      getStudentEnrollmentsQuery,
      studyProgram,
      process,
    );

  return (
    <div className="student-view">
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ identificationType: updateEnrollment ? 'DNI' : 'CUI' }}
        autoComplete="off"
        scrollToFirstError
        form={form}
      >
        <Row gutter={[16, 16]}>
          {items.map((itemObject, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <Col xl={12} lg={24} md={24} sm={24} xs={24} className="col" key={index}>
              {Object.entries(itemObject).map(([key, value]) => (
                <FormCard title={key} itemList={value} key={key} readonly />
              ))}
            </Col>
          ))}
        </Row>
        {!readOnly && (
        <Row justify="center">
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              disabled={disabledComponents}
              style={{ width: 250, height: 30 }}
              onClick={handleCreateUpdateEnrollment}
              loading={addManualEnrollmentLoading}
            >
              {t('saveChanges')}
            </Button>
          </Form.Item>
        </Row>
        )}
      </Form>
    </div>
  );
};

export default StudentView;
