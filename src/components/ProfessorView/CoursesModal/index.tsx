import React, { useEffect, useState } from 'react';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useTranslation } from 'react-i18next';
import {
  Empty, Form, Modal, Tabs,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useNavigate } from 'react-router-dom';
import CustomLoader from '../../CustomLoader';
import FormCard from '../../FormCard';
import { courseModalItem } from './constants';

interface CoursesModalProps {
  isVisible: boolean,
  setIsVisible: any,
  professorRegistrationId: any,
  programOperatingPlanId?: any,
  operatingPlanId?: any,
  backProfessorListView?: any,
}

const defaultProps: {
  programOperatingPlanId: any,
  operatingPlanId: any,
  backProfessorListView: any,
} = {
  programOperatingPlanId: null,
  operatingPlanId: null,
  backProfessorListView: false,
};

const getProfessorGroupsRequest = loader('./requests/getProfessorGroups.gql');

const CoursesModal:React.FC<CoursesModalProps> = ({
  isVisible, setIsVisible, professorRegistrationId,
  programOperatingPlanId, operatingPlanId, backProfessorListView,
}) => {
  const { t } = useTranslation();

  const [programCourses, setProgramCourses] = useState([]);
  const [externalCourses, setExternalCourses] = useState([]);
  const [form] = useForm();
  const navigate = useNavigate();

  const { data, loading } = useQuery(
    getProfessorGroupsRequest,
    {
      variables: { professorRegistrationId },
      fetchPolicy: 'no-cache',
      skip: !isVisible,
    },
  );

  useEffect(() => {
    if (data) {
      const { getProfessorGroups } = data;
      const { programGroups, externalGroups } = getProfessorGroups;
      setProgramCourses(programGroups.map((item, index) => {
        const courseObject = {
          id: item.course.id,
          [`program-subject-${index}`]: item.course.studyPlanSubject.subject,
          [`program-code-${index}`]: item.course.studyPlanSubject.code,
          [`program-group-${index}`]: item.group,
        };
        form.setFieldsValue(courseObject);
        return courseObject;
      }));

      setExternalCourses(externalGroups.map((item, index) => {
        const courseObject = {
          id: item.course.id,
          [`external-subject-${index}`]: item.course.studyPlanSubject.subject,
          [`external-code-${index}`]: item.course.studyPlanSubject.code,
          [`external-group-${index}`]: item.group,
        };
        form.setFieldsValue(courseObject);
        return courseObject;
      }));
    }
  }, [data]);

  return (
    <Modal
      title={t('courses')}
      open={isVisible}
      footer={null}
      width={800}
      destroyOnClose
      onCancel={() => setIsVisible(false)}
    >
      {loading
        ? <CustomLoader />
        : (
          <Form
            name="basic"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            autoComplete="off"
            form={form}
          >
            <Tabs
              type="card"
              defaultActiveKey="1"
              items={[
                {
                  label: t('programCourses'),
                  key: '1',
                  children: (
                    programCourses.length === 0
                      ? (
                        <Empty description={t('coursesEmpty')} />
                      )
                      : programCourses.map((item, index) => (
                        <FormCard
                          withoutTitle
                          withoutDeleteIcon
                          itemList={
                            courseModalItem(
                              item,
                              t,
                              index,
                              'program',
                              navigate,
                              true,
                              operatingPlanId,
                              programOperatingPlanId,
                              professorRegistrationId,
                              backProfessorListView,
                            )
                          }
                          readonly
                        />
                      ))
                  ),
                },
                {
                  label: t('externalCourses'),
                  key: '2',
                  children: (
                    externalCourses.length === 0
                      ? (
                        <Empty description={t('coursesEmpty')} />
                      )
                      : externalCourses.map((item, index) => (
                        <FormCard
                          withoutTitle
                          withoutDeleteIcon
                          itemList={
                        courseModalItem(
                          item,
                          t,
                          index,
                          'external',
                          navigate,
                          false,
                          operatingPlanId,
                          programOperatingPlanId,
                          professorRegistrationId,
                          backProfessorListView,
                        )
                        }
                          readonly
                        />
                      ))
                  ),
                },
              ]}
            />
          </Form>
        )}
    </Modal>
  );
};

CoursesModal.defaultProps = defaultProps;

export default CoursesModal;
