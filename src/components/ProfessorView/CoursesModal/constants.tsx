import React from 'react';
import { Button, Input } from 'antd';
import { TFunction } from 'react-i18next';
import routesDictionary from '../../../routes/routesDictionary';

export const courseModalItem = (
  item: any,
  t:TFunction,
  index,
  type: string,
  navigate: any,
  showNavigationButton: boolean,
  operatingPlanId: any,
  programOperatingPlanId: any,
  professorRegistrationId: any,
  backProfessorListView: any,
) => [
  {
    name: `${type}-subject-${index}`,
    component: <Input disabled />,
    rules: [],
    label: t('subject'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: `${type}-code-${index}`,
    component: <Input disabled />,
    rules: [],
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    label: t('code'),
    display: true,
  },
  {
    name: `${type}-group-${index}`,
    component: <Input disabled />,
    rules: [],
    label: t('group'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
  },
  {
    name: 'viewCourse',
    component: (
      <Button
        type="primary"
        style={{ width: '100%' }}
        onClick={() => navigate(
          backProfessorListView
            ? routesDictionary.subjectDetails.func(item.id)
            : routesDictionary
              .operatingPlanProgramSubject.func(operatingPlanId, programOperatingPlanId, item.id),
          { state: { professorView: true, professorRegistrationId } },
        )}
      >
        {t('viewCourse')}
      </Button>),
    rules: [
      {
        required: false,
      },
    ],
    label: t('emptyCharacter'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: showNavigationButton,
  },
];

export default {};
