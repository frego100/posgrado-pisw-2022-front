import React, { useEffect, useState } from 'react';
import {
  Button, Col, Form, message, Row,
} from 'antd';
import {
  useApolloClient, useLazyQuery, useMutation, useQuery,
} from '@apollo/client';
import { useForm } from 'antd/lib/form/Form';
import moment from 'moment';
import { TFunction, useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';
import { loader } from 'graphql.macro';
import { pdf } from '@react-pdf/renderer';
import { FilePdfOutlined } from '@ant-design/icons';
import FormCard from '../FormCard';
import professorViewItems, {
  degreesValue,
  documentTypeKeys, GENDER_MALE_BACKEND, genders, TITLE, TYPE_UNIVERSITY, typeProfessor,
} from './constants';
import CustomLoader from '../CustomLoader';
import DegreesModal from './DegreesModal';

import './styles.scss';
import routesDictionary from '../../routes/routesDictionary';
import CoursesModal from './CoursesModal';
import { getProfessorPdf } from '../../utils/pdf/utils';
import degreesModalItems, { degreesTypeLabels } from './DegreesModal/constants';
import { isUndefined } from '../../utils/tools';

interface TeacherViewProps {
  readonly: boolean,
  createProfessor: boolean,
  backProfessorListView: boolean,
  studyProgramId?: any,
  graduateUnitId?: any,
}

const defaultProps: {
  studyProgramId?: any,
  graduateUnitId?: any
} = {
  studyProgramId: null,
  graduateUnitId: null,
};

const addProfessorRequest = loader('./requests/addProfessor.gql');
const createProfessorRegistrationRequest = loader('./requests/createProfessorRegistration.gql');
const editProfessorRequest = loader('./requests/editProfessor.gql');
const getProfessorWithIdRequest = loader('./requests/getProfessorWithId.gql');

// PDF
const getDegreesRequest = loader('./DegreesModal/requests/getDegrees.gql');
const getProfessorGroupsRequest = loader('./CoursesModal/requests/getProfessorGroups.gql');

export const downloadBlob = (blob, name) => {
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  document.body.appendChild(a);
  a.href = url;
  a.download = name;
  a.click();
  setTimeout(() => {
    window.URL.revokeObjectURL(url);
    a.parentNode.removeChild(a);
  }, 0);
};
const handleThing = (something: any, t: TFunction) => {
  pdf(getProfessorPdf(something, t)).toBlob().then((blob) => {
    downloadBlob(blob, 'Perfil docente');
  })
    // eslint-disable-next-line no-console
    .catch((e) => { console.info(e); });
};

const ProfessorView: React.FC<TeacherViewProps> = ({
  readonly, createProfessor, backProfessorListView, studyProgramId, graduateUnitId,
}) => {
  const [disabledComponents, setDisabledComponents] = useState(createProfessor);
  const [degreesModalVisible, setDegreesModalVisible] = useState<boolean>(false);
  const [coursesModalVisible, setCoursesModalVisible] = useState<boolean>(false);
  const [professorData, setProfessorData] = useState<any>();
  const [disabledDocumentNumber, setDisabledDocumentNumber] = useState<boolean>(false);
  const [loadingVerificationButton, setLoadingVerificationButton] = useState<boolean>(false);
  const [professorExists, setProfessorExists] = useState<boolean>();
  const [professorIdExist, setProfessorIdExist] = useState();
  const [getDegreesQuery,
    { loading: getDegreesLoading }] = useLazyQuery(getDegreesRequest);

  // eslint-disable-next-line no-unused-vars
  const [getProfessorGroupsQuery,
    { loading: getProfessorGroupsLoading }] = useLazyQuery(getProfessorGroupsRequest);

  const [degreesForm, setDegreesForm] = useState([]);

  const [countries, setCountries] = useState([]);
  const navigate = useNavigate();

  const params = useParams();

  const { professorRegistrationId, operatingPlanId, programOperatingPlanId } = params;
  const [professorId, setProfessorId] = useState(createProfessor ? professorRegistrationId : null);

  const client = useApolloClient();
  const [form] = useForm();
  const { t } = useTranslation();

  useEffect(() => {
    if (professorRegistrationId) {
      // @ts-ignore
      setProfessorIdExist(true);
    }
  }, [professorRegistrationId]);

  const { data, loading } = useQuery(
    getProfessorWithIdRequest,
    {
      variables: { professorRegistrationId },
      fetchPolicy: 'no-cache',
      skip: createProfessor,
    },
  );

  const [editProfessorMutation,
    { loading: loadingEditProfessor }] = useMutation(editProfessorRequest);
  const [addProfessorMutation,
    { loading: loadingCreateProfessor }] = useMutation(addProfessorRequest);
  const [createRegistrationProfessorMutation] = useMutation(createProfessorRegistrationRequest);

  useEffect(() => {
    if (data) {
      const { getProfessor, getCountries } = data;
      const { professor } = getProfessor;
      const { user } = professor;
      const { useridentificationSet } = user;
      const identificationSet: any = {};
      useridentificationSet?.forEach((identification: any) => {
        identificationSet[
          documentTypeKeys[identification.identificationType]
        ] = identification.number;
      });

      const professorArrayData = {
        ...professor,
        ...user,
        ...useridentificationSet[0],
        birthday: readonly ? moment(user.birthday, 'YYYY-MM-DD').format('DD/MM/YYYY') : moment(user.birthday),
        // TODO: set identificationType
        identificationType: documentTypeKeys[useridentificationSet[0].identificationType],
        professorType: readonly ? (professor.professorType === TYPE_UNIVERSITY ? 'De la univesidad' : 'Invitado') : typeProfessor[professor.professorType],
        country: readonly ? user.country.name : user.country.code,
        positions: readonly
          ? getProfessor.isDirector && getProfessor.isCoordinator
            ? t('directorAndCoordinator')
            : getProfessor.isDirector
              ? 'director'
              : getProfessor.isCoordinator
                ? t('coordinator') : t('noCharges')
          : [getProfessor.isDirector && 'isDirector', getProfessor.isCoordinator && 'isCoordinator'],
        gender: readonly ? (user.gender === GENDER_MALE_BACKEND ? 'Masculino' : 'Femenino') : genders[user.gender],
        ...identificationSet,
      };
      form.setFieldsValue(professorArrayData);
      setProfessorId(professor.id);
      setProfessorData(professorArrayData);
      setCountries(getCountries);
    }
  }, [data]);

  useEffect(() => {
    if (createProfessor) {
      let topDegreeValue = -1;
      let topDegree = t('No posee algún grado');
      let universityTitle = t('No tiene titulo universitario');
      degreesForm.forEach((degree) => {
        if (degree.degreeType === TITLE) universityTitle = degree.name;
        const aux = degreesValue[degree.degreeType];
        if (!isUndefined(aux) && aux > topDegreeValue) {
          topDegreeValue = aux;
          topDegree = degree.name;
        }
      });
      form.setFieldsValue({ universityTitle, topDegree });
    }
  }, [degreesForm]);

  useEffect(() => {
    if (createProfessor && !professorExists) {
      setProfessorData({});
      form.setFieldsValue({
        universityTitle: 'No tiene titulo universitario',
        topDegree: 'No posee algún grado',
      });
    }
  }, [createProfessor, form]);

  const onFinish = (values: any) => {
    if (professorExists || professorId) {
      editProfessorMutation({
        variables: {
          input: {
            professorId: professorId || professorData.id,
            firstName: values.firstName,
            lastName: values.lastName,
            maternalLastName: values.maternalLastName,
            birthday: moment(values.birthday).format('YYYY-MM-DD'),
            country: values.country,
            gender: values.gender,
            ...values.email !== professorData.email && { email: values.email },
            phone: values.phone,
            professorType: values.professorType,
            orcidId: values.orcidId,
            ...!professorExists && {
              operatingPlanProgram: programOperatingPlanId ?? studyProgramId,
              isDirector: values.positions?.includes('isDirector') ?? false,
              isCoordinator: values.positions?.includes('isCoordinator') ?? false,
            },

            // Documents
            ...values.curriculum && { curriculum: values.curriculum.originFileObj },
            ...values.investigations && { investigations: values.investigations.originFileObj },
            ...values.suneduFile && { suneduFile: values.suneduFile.originFileObj },

            // Picture
            ...values.profilePicture && { profilePicture: values.profilePicture.originFileObj },
          },
        },
        fetchPolicy: 'no-cache',
      }).then(({ data: editProfessorData }: any) => {
        const { editProfessor } = editProfessorData;
        if (editProfessor.feedback.status === 'SUCCESS') {
          if (professorExists) {
            createRegistrationProfessorMutation({
              variables: {
                input: {
                  operatingPlanProgram: programOperatingPlanId,
                  professor: professorData.id,
                  isDirector: values.positions?.includes('isDirector') ?? false,
                  isCoordinator: values.positions?.includes('isCoordinator') ?? false,
                },
              },
            }).then(({ data: createRegistrationData }) => {
              const { createProfessorRegistration } = createRegistrationData;
              if (createProfessorRegistration.feedback.status === 'SUCCESS') {
                message.success(createProfessorRegistration.feedback.message);
                navigate(
                  backProfessorListView
                    ? routesDictionary.professors.route
                    : routesDictionary.operatingPlanProgram.func(
                      operatingPlanId,
                      programOperatingPlanId,
                    ),
                );
              } else {
                message.error(createProfessorRegistration.feedback.message);
                navigate(
                  backProfessorListView
                    ? routesDictionary.professors.route
                    : routesDictionary.operatingPlanProgram.func(
                      operatingPlanId,
                      programOperatingPlanId,
                    ),
                );
              }
            });
          } else {
            message.success(editProfessor.feedback.message);
            navigate(
              backProfessorListView
                ? routesDictionary.professors.route
                : routesDictionary.operatingPlanProgram.func(
                  operatingPlanId,
                  programOperatingPlanId,
                ),
            );
          }
        } else {
          message.error(editProfessor.feedback.message);
          editProfessor.errors.map((error) => (
            form.setFields([{
              name: error.field,
              errors: error.messages,
            }])
          ));
          window.scroll({
            top: 0,
            behavior: 'smooth',
          });
        }
      });
    } else {
      addProfessorMutation({
        variables: {
          input: {
            email: values.email,
            firstName: values.firstName,
            lastName: values.lastName,
            maternalLastName: values.maternalLastName,
            birthday: moment(values.birthday).format('YYYY-MM-DD'),
            gender: values.gender,
            phone: values.phone,
            country: values.country,
            operatingPlanProgram: programOperatingPlanId,
            professorType: values.professorType,
            orcidId: values.orcidId,
            isDirector: values.positions?.includes('isDirector') ?? false,
            isCoordinator: values.positions?.includes('isCoordinator') ?? false,

            // Documents
            ...values.curriculum && { curriculum: values.curriculum.originFileObj },
            ...values.investigations && { investigations: values.investigations.originFileObj },
            ...values.suneduFile && { suneduFile: values.suneduFile.originFileObj },

            userIdentifications: [{
              identificationType: values.identificationType,
              number: values.number,
            }],
            degreeSet: professorExists ? [] : degreesForm,
            // degreeSet: [],

            // Picture
            ...values.profilePicture && { profilePicture: values.profilePicture.originFileObj },

          },
        },
      }).then(({ data: createProfessorData }) => {
        const { addProfessor } = createProfessorData;
        if (addProfessor.feedback.status === 'SUCCESS') {
          message.success(addProfessor.feedback.message);
          navigate(
            backProfessorListView
              ? routesDictionary.professors.route
              : routesDictionary.operatingPlanProgram.func(
                operatingPlanId,
                programOperatingPlanId,
              ),
          );
        } else {
          message.error(addProfessor.feedback.message);
          addProfessor.errors.forEach((error) => {
            if (error.field === 'user') {
              form.setFields([{
                name: 'email',
                errors: ['Ya existe usuario con este Correo electrónico.'],
              }]);
            }
          });
          window.scroll({
            top: 0,
            behavior: 'smooth',
          });
        }
      });
    }
  };
  const identificationType = Form.useWatch('identificationType', form);

  // eslint-disable-next-line no-unused-vars
  const onFinishFailed = (errorInfo: any) => {
  };
  if (loading || !professorData) return <CustomLoader />;

  const professorItems = professorViewItems(
    createProfessor,
    disabledComponents,
    setDisabledComponents,
    client,
    form,
    setDegreesModalVisible,
    setProfessorData,
    disabledDocumentNumber,
    setDisabledDocumentNumber,
    loadingVerificationButton,
    setLoadingVerificationButton,
    professorExists,
    setProfessorExists,
    professorData,
    t,
    readonly,
    countries,
    setCountries,
    identificationType,
    setCoursesModalVisible,
    programOperatingPlanId,
  );

  const getPdfItems = async () => {
    const { data: degreesData } = await getDegreesQuery({
      variables: { professorId },
    });

    const degreesItemsArray = degreesData.getDegrees.map((item) => ({
      ...item,
      country: degreesData.getCountries.find((country: any) => item.country === country.id)?.name,
      degreeType: degreesTypeLabels[item.degreeType],
    }));

    const degreesItems = degreesItemsArray.map((degree, index) => (
      degreesModalItems(
        index,
        form,
        index,
        t,
        readonly,
        degreesItemsArray,
        countries,
      )
    ));

    handleThing({ professorItems, degreesItems }, t);
  };
  return (
    <div>
      {readonly && (
      <Row justify="end">
        <Button
          loading={getDegreesLoading || getProfessorGroupsLoading}
          onClick={() => getPdfItems()}
          type="primary"
          icon={<FilePdfOutlined />}
          className="export-button"
        >
          {t('export')}
        </Button>
      </Row>
      )}
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ identificationType: 'DNI' }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        scrollToFirstError
        form={form}
      >
        <Row gutter={[16, 16]}>
          {professorItems.map((itemObject, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <Col xl={12} lg={24} md={24} sm={24} xs={24} className="col" key={index}>
              {Object.entries(itemObject).map(([key, value]) => (
                <FormCard title={key} itemList={value} key={key} readonly={readonly} />
              ))}
            </Col>
          ))}
        </Row>
        {!readonly && (
          <Row justify="center">
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                disabled={disabledComponents}
                style={{ width: 250, height: 30 }}
                loading={createProfessor ? loadingCreateProfessor : loadingEditProfessor}
              >
                {t('saveChanges')}
              </Button>
            </Form.Item>
          </Row>
        )}
      </Form>
      <DegreesModal
        isVisible={degreesModalVisible}
        setIsVisible={setDegreesModalVisible}
        professorId={professorId || professorData.id}
        professor={professorData}
        readonly={readonly}
        professorViewForm={form}
        professorExists={createProfessor ? professorExists : professorIdExist}
        degreesForm={degreesForm}
        setDegreesForm={setDegreesForm}
        professorRegistrationId={professorRegistrationId}
      />
      <CoursesModal
        isVisible={coursesModalVisible}
        setIsVisible={setCoursesModalVisible}
        professorRegistrationId={professorRegistrationId}
        programOperatingPlanId={programOperatingPlanId ?? studyProgramId}
        operatingPlanId={operatingPlanId ?? graduateUnitId}
        backProfessorListView={backProfessorListView}
      />
    </div>
  );
};

ProfessorView.defaultProps = defaultProps;

export default ProfessorView;
