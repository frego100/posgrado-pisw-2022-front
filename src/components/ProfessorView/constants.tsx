import {
  Button, Input, Select, Radio, DatePicker, InputNumber, Checkbox,
} from 'antd';
import React from 'react';
// import React, { forwardRef, useCallback } from 'react';
import moment from 'moment';
import { FilePdfOutlined } from '@ant-design/icons';
import Swal from 'sweetalert2';
import { loader } from 'graphql.macro';
// import PhoneInput from 'react-phone-number-input/input';
import CustomUploadImage from '../CustomUploadImage';
import './styles.scss';
import CustomUploadFile from '../CustomUploadFile';
import { BACKEND_URL } from '../../utils/constants';
import { generateUrlDownload } from '../../utils/tools';

const { Option } = Select;
const searchUserRequest = loader('./requests/searchUser.gql');

// Constants gender
export const GENDER_MALE = 'MALE';
export const GENDER_FEMALE = 'FEMALE';
export const GENDER_MALE_BACKEND = 'A_1';
export const GENDER_FEMALE_BACKEND = 'A_2';

export const TYPE_UNIVERSITY = 'INVITED';
export const TYPE_INVITED = 'FROM_UNIVERSITY';

export const documentTypeKeys:{[index: string]: string} = {
  A_1: 'DNI',
  A_2: 'FOREIGN_CARD',
};

export const documentTypeLabel:{[index:string]: string} = {
  A_1: 'dni',
  A_2: 'foreignCard',
};

export const genders :{[index: string] : string} = ({
  A_1: 'MALE',
  A_2: 'FEMALE',
});

export const typeProfessor:{[index: string] : string} = ({
  A_1: 'INVITED',
  A_2: 'FROM_UNIVERSITY',
});

export const TITLE = 'TITLE';
export const BACHELOR = 'BACHELOR';
export const MASTER = 'MASTER';
export const DOCTORATE = 'DOCTORATE';

export const degreesValue:{[index: string]: number} = {
  [BACHELOR]: 0,
  [MASTER]: 1,
  [DOCTORATE]: 2,
};

const identifyDocumentSectionWithValidation = (
  setDisabledComponents: any,
  client: any,
  form: any,
  setProfessor: any,
  disabledDocumentNumber: boolean,
  setDisabledDocumentNumber: any,
  loadingVerificationButton: boolean,
  setLoadingVerificationButton: any,
  professorExists: any,
  setProfessorExists: any,
  t: any,
  setCountries: any,
  identificationType: any,
  programOperatingPlanId: any,
) => (
  {
    [t('search')]: [
      {
        name: 'identificationType',
        component: (
          <Select
            className="component"
            // disabled={disabledDocumentNumber}
          >
            <Option value="DNI">DNI</Option>
            <Option value="FOREIGN_CARD">Carnet de extranjería</Option>
          </Select>
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('type'),
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
      {
        name: 'number',
        component: (
          <InputNumber
            style={{ width: '100%' }}
            controls={false}
            maxLength={identificationType === 'DNI' ? 8 : 12}
            // disabled={disabledDocumentNumber}
          />
        ),
        rules: [
          () => ({
            validator(_: any, value: any) {
              const length = identificationType === 'DNI' ? 8 : 12;
              if (value && value.toString().length === length) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('¡Ingresa un documento válido!'));
            },
          }),
        ],
        label: t('documentNumber'),
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
      {
        // ...(!isUndefined(professorExists) && (
        //   {
        //     help: professorExists
        //       ? t('professorHasBeenFound')
        //       : t('professorHasBeenNotFound'),
        //   }
        // )),
        component: (
          <Button
            type="primary"
            className="component"
            // disabled={disabledDocumentNumber}
            loading={loadingVerificationButton}
            onClick={() => {
              setLoadingVerificationButton(true);
              form.validateFields(['identificationType', 'number'])
                .then((values) => {
                  client.query({
                    query: searchUserRequest,
                    fetchPolicy: 'no-cache',
                    variables: {
                      idType: (values.identificationType === 'DNI') ? 1 : 2,
                      idNumber: values.number,
                      opPlanProgramId: programOperatingPlanId,
                    },
                  }).then(({ data }: any) => {
                    const { searchUser, getCountries } = data;
                    setCountries(getCountries);
                    if (searchUser) {
                      const {
                        country, professor, useridentificationSet, ...user
                      } = searchUser;
                      if (professor?.professorregistrationSet?.[0]) {
                        const idType = values.identificationType;
                        Swal.fire({
                          title: t('professorAlredyExists', {
                            firstName: user.firstName,
                            lastName: user.lastName,
                            maternalLastName: user.maternalLastName,
                          }),
                          icon: 'info',
                          confirmButtonText: 'Aceptar',
                        });
                        form.resetFields();
                        form.setFieldsValue({
                          identificationType: idType,
                          universityTitle: 'No tiene titulo universitario',
                          topDegree: 'No posee algún grado',
                        });
                        setDisabledComponents(true);
                        setProfessor({});
                        setLoadingVerificationButton(false);
                      } else {
                        const professorData = {
                          ...user,
                          ...professor,
                          ...useridentificationSet[0],
                          birthday: user.birthday ? moment(user.birthday) : null,
                          identificationType: documentTypeKeys[
                            useridentificationSet[0].identificationType],
                          professorType: typeProfessor[professor?.professorType],
                          country: country.code,
                          gender: genders[user.gender],
                        };

                        form.setFieldsValue(professorData);
                        setProfessor(professorData);

                        setProfessorExists(!!professor);

                        setDisabledComponents(false);

                        setLoadingVerificationButton(false);

                        Swal.fire({
                          title: professor ? t('professorHasBeenFound') : t('professorIsNotRegistered'),
                          icon: 'info',
                          confirmButtonText: 'Aceptar',
                        });
                      }
                    } else {
                      const idType = values.identificationType;
                      const idNumber = values.number;
                      form.resetFields();
                      form.setFieldsValue({
                        identificationType: idType,
                        number: idNumber,
                        universityTitle: 'No tiene titulo universitario',
                        topDegree: 'No posee algún grado',
                      });
                      setDisabledComponents(false);
                      setProfessorExists(false);
                      setProfessor({});
                      setLoadingVerificationButton(false);
                      Swal.fire({
                        title: t('professorHasBeenNotFound'),
                        icon: 'info',
                        confirmButtonText: 'Aceptar',
                      });
                    }
                  });
                })
                .catch(() => {
                  setLoadingVerificationButton(false);
                });
            }}
          >
            {t('verify')}
          </Button>
        ),
        label: t('verifyDocumentNumber'),
        name: 'verifyProfessor',
        responsive: {
          xs: { span: 24 },
          md: { span: 8 },
        },
        display: true,
      },
    ],
  }
);

export const mapIdentificationSet = (
  professor: any,
  createProfessor: boolean,
  readonly:boolean,
  t: any,
) => {
  const identificationSet:any [] = [];
  if (!createProfessor) {
    professor?.useridentificationSet?.forEach((identification: any) => {
      identificationSet.push({
        name: documentTypeKeys[identification.identificationType],
        component: (readonly
          ? (
            <Input
              disabled
              value={professor?.user.useridentificationSet[0].number}
            />
          )
          : (
            <InputNumber
              style={{ width: '100%' }}
              controls={false}
              maxLength={documentTypeKeys[identification.identificationType] === 'DNI' ? 8 : 12}
            />
          )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
          () => ({
            validator(_: any, value: any) {
              const length = documentTypeKeys[identification.identificationType] === 'DNI' ? 8 : 12;
              if (value.toString().length === length) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('¡Ingresa un documento válido!'));
            },
          }),
        ],
        label: t(documentTypeLabel[identification.identificationType]),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.user.useridentificationSet[0].number,
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      });
    });
  }
  return identificationSet;
};

const professorViewItems = (
  createProfessor: boolean,
  disabledComponents: boolean,
  setDisabledComponents: any,
  client: any,
  form: any,
  setDegreesModalVisible: any,
  setProfessor: any,
  disabledDocumentNumber: boolean,
  setDisabledDocumentNumber: any,
  loadingVerificationButton: boolean,
  setLoadingVerificationButton: any,
  professorExists: any,
  setProfessorExists: any,
  professor: any,
  t: any,
  readonly:boolean,
  countries: any[],
  setCountries: any,
  identificationType: any,
  setCoursesModalVisible: any,
  programOperatingPlanId: any,
) => [
  {
    ...createProfessor && identifyDocumentSectionWithValidation(
      setDisabledComponents,
      client,
      form,
      setProfessor,
      disabledDocumentNumber,
      setDisabledDocumentNumber,
      loadingVerificationButton,
      setLoadingVerificationButton,
      professorExists,
      setProfessorExists,
      t,
      setCountries,
      identificationType,
      programOperatingPlanId,
    ),

    [t('personalInformation')]: [
      {
        name: 'profilePicture',
        responsive: {
          xs: { span: 24 },
          md: { span: 24 },
        },
        component: (readonly ? (
          <CustomUploadImage
            disabled
            form={form}
            nameComponent="profilePicture"
            {...professor.profilePicture && { defaultValue: `${BACKEND_URL}/media/${professor.profilePicture}` }}
          />
        )
          : (
            <CustomUploadImage
              disabled={disabledComponents}
              form={form}
              nameComponent="profilePicture"
              {...professor.profilePicture && { defaultValue: `${BACKEND_URL}/media/${professor.profilePicture}` }}
            />
          )
        ),
        display: true,
        pdfValue: professor.profilePicture ? `${BACKEND_URL}/media/${professor.profilePicture}` : '',
        pdfStyle: {
          flex: '0 0 100%',
          maxHeight: 170,
        },
        pdfItemType: 'image',
      },
      {
        name: 'firstName',
        component: (
          readonly
            ? (
              <Input
                disabled
                value={professor?.user.firstName}
              />
            )
            : <Input disabled={disabledComponents} />
        ),

        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('firstName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor.user?.firstName || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'lastName',
        component: (
          readonly
            ? (
              <Input
                disabled
              />
            )
            : <Input disabled={disabledComponents} />
        ),

        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('fatherLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor.user?.lastName || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'maternalLastName',
        component: (
          readonly
            ? (
              <Input
                disabled
              />
            )
            : <Input disabled={disabledComponents} />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('maternalLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor.user?.maternalLastName || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'birthday',
        component: (
          readonly ? (
            <Input disabled />
          )
            : (
              <DatePicker
                className="component"
                disabled={disabledComponents}
                allowClear={false}
                disabledDate={(current) => current && current > moment()}
              />
            )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('birthDate'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.birthday || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'country',
        component: (
          readonly
            ? (
              <Input disabled />
            )
            : (
              <Select
                className="component"
                disabled={disabledComponents}
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) => (
                    option!.children as unknown as string)
                  .toLowerCase().includes(input.toLowerCase())}
              >
                {countries.map((country) => (
                  <Option key={country.name} value={country.id}>{country.name}</Option>
                ))}
              </Select>
            )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('country'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.country || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'gender',
        component: (readonly
          ? (
            <Input disabled />
          )
          : (
            <Radio.Group disabled={disabledComponents}>
              <Radio value={GENDER_MALE}> Masculino </Radio>
              <Radio value={GENDER_FEMALE}> Femenino </Radio>
            </Radio.Group>
          )
        ),

        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('gender'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.gender || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      ...mapIdentificationSet(professor, createProfessor, readonly, t),
    ],
  },
  {
    [t('contactInformation')]: [
      {
        name: 'email',
        component: (
          readonly
            ? (
              <Input disabled />
            )
            : <Input disabled={disabledComponents} />
        ),

        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
          {
            type: 'email',
            message: '¡Ingrese un correo electronico válido!',
          },
        ],
        label: t('email'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.email || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'phone',
        component: (
          readonly
            ? (
              <InputNumber style={{ width: '100%' }} disabled controls={false} maxLength={15} />
            )
            : <InputNumber style={{ width: '100%' }} disabled={disabledComponents} controls={false} maxLength={15} />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
          {
            pattern: /^[0-9\b]+$/,
            message: '¡Ingrese un numero válido!',
          },
        ],
        label: t('phone'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.phone || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
    ],
    [t('academicData')]: [
      {
        name: 'professorType',
        component: (
          readonly
            ? (
              <Input disabled />
            )
            : (
              <Select
                className="component"
                disabled={disabledComponents}
              >
                <Option value="INVITED">Invitado</Option>
                <Option value="FROM_UNIVERSITY">De la universidad</Option>
              </Select>
            )
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('professorType'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.professorType || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'positions',
        component: (readonly
          ? <Input disabled />
          : (
            <Checkbox.Group style={{ width: '100%' }} disabled={disabledComponents}>
              <Checkbox value="isDirector">{t('director')}</Checkbox>
              <Checkbox value="isCoordinator">{t('coodinator')}</Checkbox>
            </Checkbox.Group>
          )
        ),
        rules: [
          {
            required: false,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('positions'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.positions || '-',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'checkbox',
      },
      {
        name: 'universityTitle',
        component: (
          readonly
            ? (
              <Input
                disabled
                value={professor?.universityTitle}
              />
            )
            : <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('universityTitle'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.universityTitle || '',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'orcidId',
        component: (
          readonly
            ? (
              <Input
                disabled
                value={professor?.orcidId}
                maxLength={16}
              />
            )
            : <Input disabled={disabledComponents} maxLength={16} />
        ),
        label: t('orcidId'),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.orcidId || '',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'topDegree',
        component: (
          readonly
            ? (
              <Input
                disabled
                value={professor?.topDegree}
              />
            )
            : <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('topDegree'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor?.topDegree || '',
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'input',
      },
      {
        name: 'investigations',
        component: (readonly
          ? (professor.investigations
            ? (
              <Button
                type="primary"
                target="_blank"
                style={{ width: '100%' }}
                href={generateUrlDownload(professor.investigationsUrl)}
                icon={<FilePdfOutlined />}
              >
                Descargar
              </Button>
            )
            : (
              <Input
                disabled
                value="-"
              />
            ))
          : (
            <CustomUploadFile
              disabled={disabledComponents}
              form={form}
              nameComponent="investigations"
              urlFile={professor.investigationsUrl
                  && generateUrlDownload(professor.investigationsUrl)}
            />
          )
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('investigations'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor.investigationsUrl
          ? generateUrlDownload(professor.investigationsUrl) : null,
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'button',
        pdfItemLabel: t('download'),
      },
      {
        name: 'curriculum',
        component: (
          readonly
            ? (professor.curriculum
              ? (
                <Button
                  type="primary"
                  target="_blank"
                  style={{ width: '100%' }}
                  href={generateUrlDownload(professor?.curriculumUrl)}
                  icon={<FilePdfOutlined />}
                >
                  Descargar
                </Button>
              )
              : (
                <Input
                  disabled
                  value="-"
                />
              )
            )
            : (
              <CustomUploadFile
                disabled={disabledComponents}
                form={form}
                nameComponent="curriculum"
                urlFile={professor?.curriculumUrl && generateUrlDownload(professor.curriculumUrl)}
              />
            )
        ),

        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('curriculumVitae'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor.curriculumUrl
          ? generateUrlDownload(professor.curriculumUrl) : null,
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'button',
        pdfItemLabel: t('download'),
      },
      {
        name: 'suneduFile',
        component: (readonly
          ? (professor.suneduFile
            ? (
              <Button
                type="primary"
                target="_blank"
                style={{ width: '100%' }}
                href={generateUrlDownload(professor.suneduFileUrl)}
                icon={<FilePdfOutlined />}
              >
                Descargar
              </Button>
            )
            : (
              <Input
                disabled
                value="-"
              />
            ))
          : (
            <CustomUploadFile
              disabled={disabledComponents}
              form={form}
              nameComponent="suneduFile"
              urlFile={professor?.suneduFileUrl && generateUrlDownload(professor?.suneduFileUrl)}
            />
          )
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('sunedu'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: professor.suneduFileUrl
          ? generateUrlDownload(professor.suneduFileUrl) : null,
        pdfStyle: {
          flex: '0 0 48%',
        },
        pdfItemType: 'button',
        pdfItemLabel: t('download'),
      },
      {
        name: 'grades',
        component: (
          <Button
            type="primary"
            disabled={disabledComponents}
            style={{ width: '100%' }}
            onClick={() => setDegreesModalVisible(true)}
          >
            {t('viewDegrees')}
          </Button>

        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('degrees'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
        pdfValue: 'MODAL DE DEGRESS',
        pdfStyle: {
          flex: '0 0 48%',
          marginRight: '2%',
        },
        pdfItemType: 'null',
      },
      {
        name: 'courses',
        component: (
          <Button
            type="primary"
            disabled={disabledComponents}
            style={{ width: '100%' }}
            onClick={() => setCoursesModalVisible(true)}
          >
            {t('viewCourses')}
          </Button>

        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('courses'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: !createProfessor,
        pdfValue: 'MODAL DE COURSES',
        pdfStyle: {
          flex: '0 0 48%',
          marginLeft: '2%',
        },
        pdfItemType: 'null',
      },
    ],
  },
];

export default professorViewItems;
