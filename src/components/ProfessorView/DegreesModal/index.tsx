import React, { useEffect, useState } from 'react';
import {
  Modal, Row, Form, Button, Empty, message,
} from 'antd';
import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { PlusCircleOutlined } from '@ant-design/icons';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import moment from 'moment';
import FormCard from '../../FormCard';
import degreesModalItems, { degreesTypeKeys, degreesTypeLabels } from './constants';
import CustomLoader from '../../CustomLoader';
import './styles.scss';
import { Degree, DegreesModalProps } from './interfaces';

const addDegreeRequest = loader('./requests/addDegree.gql');
const deleteDegreeRequest = loader('./requests/deleteDegree.gql');
const editDegreeRequest = loader('./requests/editDegree.gql');
const getCountriesRequest = loader('./requests/getCountries.gql');
const getDegreesRequest = loader('./requests/getDegrees.gql');
const getTopDegreeRequest = loader('./requests/getTopDegree.gql');

const DegreesModal:React.FC<DegreesModalProps> = ({
  isVisible,
  setIsVisible,
  professorId,
  readonly,
  professorViewForm,
  professorExists,
  setDegreesForm,
  professor,
  professorRegistrationId,
}) => {
  const [degrees, setDegrees] = useState<Degree[]>([]);
  const [professorName, setProfessorName] = useState<string>('');
  const [countries, setCountries] = useState<any>([]);
  const { t } = useTranslation();
  const [form] = useForm();
  const client = useApolloClient();

  const { data, loading, refetch } = useQuery(
    getDegreesRequest,
    {
      variables: { professorId },
      fetchPolicy: 'no-cache',
      skip: !isVisible || !professorId,
    },
  );

  const { data: dataCountries } = useQuery(getCountriesRequest, { skip: professorExists });

  const [addDegreeMutation] = useMutation(addDegreeRequest);
  const [editDegreeMutation] = useMutation(editDegreeRequest);
  const [deleteDegreeMutation] = useMutation(deleteDegreeRequest);

  useEffect(() => {
    if (!professorId) {
      form.resetFields();
      setDegrees([]);
      setProfessorName('');
    }
  }, [professorId, professor]);

  useEffect(() => {
    if (dataCountries) {
      const { getCountries } = dataCountries;
      setCountries(getCountries);
    }
  }, [dataCountries]);

  useEffect(() => {
    if (data) {
      const { getDegrees, getCountries } = data;
      setDegrees(getDegrees);
      setCountries(getCountries);
      setProfessorName(`${getDegrees[0]?.professor.user.firstName || ''} ${getDegrees[0]?.professor.user.lastName || ''}`);
      form.setFieldsValue({
        degrees: getDegrees.map((degree: any) => ({
          ...degree,
          year: readonly ? degree.year : moment(degree.year, 'YYYY'),
          degreeType: readonly
            ? degreesTypeLabels[degree.degreeType] : degreesTypeKeys[degree.degreeType],
          ...readonly && {
            country: getCountries.find((country: any) => degree.country === country.id)?.name,
          },
        }
        )),
      });

      const documents: any = {};
      getDegrees.forEach((degree: any, index: number) => {
        documents[`certificate-${index}`] = degree.certificate;
        documents[`suneduCertificate-${index}`] = degree.suneduCertificate;
      });
      form.setFieldsValue(documents);
    }
  }, [data, form]);

  const handleAddDegrees = () => {
    form.validateFields().then((values) => {
      const degreesArray = values?.degrees.map((degree: any, index: number) => {
        const certificateDocument = form.getFieldValue([`certificate-${index}`]);
        const suneduCertificateDocument = form.getFieldValue([`suneduCertificate-${index}`]);
        return ({
          ...professorExists && { degreeId: degree.id },
          name: degree.name,
          degreeType: degree.degreeType,
          year: moment(degree.year).format('YYYY'),
          country: degree.country,
          institution: degree.institution,
          ...(certificateDocument && typeof certificateDocument !== 'string')
          && { certificate: form.getFieldValue([`certificate-${index}`]).originFileObj },
          ...(suneduCertificateDocument && typeof suneduCertificateDocument !== 'string')
          && { suneduCertificate: form.getFieldValue([`suneduCertificate-${index}`]).originFileObj },
        });
      });

      if (professorExists) {
        const newDegrees = degreesArray.filter((degree: any) => !degree.degreeId);
        const editDegrees = degreesArray.filter((degree: any) => degree.degreeId);

        if (newDegrees.length > 0) {
          addDegreeMutation({
            variables: {
              input: newDegrees.map((newDegree: any) => ({
                ...newDegree,
                professor: professorId,
              })),
            },
          }).then(({ data: addDegreeData }: any) => {
            const { addDegrees } = addDegreeData;
            if (addDegrees.feedback.status === 'SUCCESS') {
              // setIsVisible(false);
            }
          });
        }

        if (editDegrees.length > 0) {
          editDegreeMutation({
            variables: {
              input: editDegrees,
            },
          }).then(({ data: editDegreeData }: any) => {
            const { editDegrees: editDegreesFeed } = editDegreeData;
            if (editDegreesFeed.feedback.status === 'SUCCESS') {
              // setIsVisible(false);
            }
          });
        }
        form.resetFields();
        client.query({
          query: getTopDegreeRequest,
          variables: { professorRegistrationId },
          fetchPolicy: 'no-cache',
        }).then(({ data: getTopDegreeData }) => {
          const { getProfessor } = getTopDegreeData;
          const { universityTitle, topDegree } = getProfessor.professor;
          professorViewForm.setFieldsValue({ universityTitle, topDegree });
        });
      } else {
        setDegreesForm(degreesArray);
      }
      setIsVisible(false);
      message.success('Grados academicos registrados!');
    });
  };
  return (
    <Modal
      title={(
        <Row justify="center" className="modal-title">
          {t('degrees')}
        </Row>
          )}
      open={isVisible}
      {...readonly && { footer: null }}
      okText={t('saveChanges')}
      cancelText={t('cancel')}
      width={800}
      destroyOnClose
      onOk={handleAddDegrees}
      onCancel={() => setIsVisible(false)}
    >
      {loading
        ? <CustomLoader />
        : (
          <Form
            name="basic"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            autoComplete="off"
            form={form}
          >
            <Form.List name="degrees">
              {(fields, { add, remove }) => (
                <>
                  <Row align="middle">
                    <p className="subtitle">
                      {professorName}
                    </p>
                    <div className="ml-auto">
                      {!readonly
                      && (
                      <Button
                        type="primary"
                        onClick={() => add()}
                        icon={<PlusCircleOutlined />}
                      >
                        {t('add')}
                      </Button>
                      )}
                    </div>
                  </Row>
                  {fields.length > 0
                    ? (
                      <div className="degreesModalContent">
                        {fields.map(({ key, name, ...restField }) => (
                          <FormCard
                            key={key}
                            withoutTitle
                            readonly={readonly}
                            itemList={degreesModalItems(
                              name,
                              form,
                              key,
                              t,
                              readonly,
                              degrees,
                              countries,
                            )}
                            onClick={() => {
                              const degreeField = form.getFieldValue('degrees')[name];
                              if (degreeField && degreeField.id) {
                                deleteDegreeMutation({
                                  variables: {
                                    degreeId: degreeField.id,
                                  },
                                }).then(({ data: deleteDegreeData }) => {
                                  const { deleteDegree } = deleteDegreeData;
                                  if (deleteDegree.feedback.status === 'SUCCESS') {
                                    refetch();
                                  }
                                });
                              } else {
                                remove(name);
                              }
                              form.setFieldsValue({ [`certificate-${key}`]: '' });
                            }}
                            {...restField}
                          />
                        ))}
                      </div>
                    )
                    : (
                      <Row justify="center">
                        <Empty
                          image={Empty.PRESENTED_IMAGE_SIMPLE}
                          description={t('degreesEmpty')}
                        />
                      </Row>
                    )}
                </>
              )}
            </Form.List>
          </Form>
        )}
    </Modal>
  );
};

export default DegreesModal;
