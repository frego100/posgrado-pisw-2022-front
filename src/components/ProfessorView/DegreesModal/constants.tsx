import {
  Button, DatePicker, Input, Select,
} from 'antd';
import React from 'react';
import { FilePdfOutlined } from '@ant-design/icons';
import './styles.scss';
import moment from 'moment';
import CustomUploadFile from '../../CustomUploadFile';
import { generateUrlDownload } from '../../../utils/tools';

const { Option } = Select;

const typeDegreeOptions = [
  { value: 'TITLE', label: 'Titulo' },
  { value: 'BACHELOR', label: 'Bachiller' },
  { value: 'MASTER', label: 'Maestria' },
  { value: 'DOCTORATE', label: 'Doctorado' },
];

export const degreesTypeKeys:{[index: string]: string} = {
  A_0: 'TITLE',
  A_1: 'BACHELOR',
  A_2: 'MASTER',
  A_3: 'DOCTORATE',
};

export const degreesTypeLabels:{[index: string]: string} = {
  A_0: 'Titulo',
  A_1: 'Bachiller',
  A_2: 'Maestria',
  A_3: 'Doctorado',
};

const degreesModalItems = (
  name: any,
  form: any,
  key: number,
  t: any,
  readonly: boolean,
  degrees: any,
  countries: any,
) => [
  {
    name: [name, 'institution'],
    component: (readonly
      ? (
        <Input disabled />
      )
      : <Input />
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('institution'),
    responsive: {
      xs: { span: 24 },
      md: { span: 16 },
    },
    display: true,
    pdfValue: degrees[key]?.institution,
    pdfStyle: {
      flex: '0 0 64.6%',
      marginRight: '2%',
    },
    pdfItemType: 'input',
  },
  {
    name: [name, 'country'],
    component: (readonly
      ? (
        <Input disabled />
      )
      : (
        <Select
          className="component"
          showSearch
          getPopupContainer={(trigger) => trigger.parentNode}
          optionFilterProp="children"
          filterOption={(input, option) => (
                        option!.children as unknown as string)
            .toLowerCase().includes(input.toLowerCase())}
        >
          {countries.map((country: any) => (
            <Option key={country.name} value={country.id}>{country.name}</Option>
          ))}
        </Select>
      )
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('country'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
    pdfValue: degrees[key]?.country,
    pdfStyle: {
      flex: '0 0 31.4%',
      marginLeft: '2%',
    },
    pdfItemType: 'input',
  },
  {
    name: [name, 'name'],
    component: (readonly
      ? (
        <Input
          disabled
          value={degrees[key].name}
        />
      )
      : <Input />
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('mention'),
    responsive: {
      xs: { span: 24 },
      md: { span: 16 },
    },
    display: true,
    pdfValue: degrees[key]?.name,
    pdfStyle: {
      flex: '0 0 64.6%',
      marginRight: '2%',
    },
    pdfItemType: 'input',
  },
  {
    name: [name, 'degreeType'],
    component: (readonly
      ? (
        <Input
          disabled
        />
      )
      : (
        <Select
          getPopupContainer={(trigger) => trigger.parentNode}
          className="component"
        >
          {typeDegreeOptions.map(({ value, label }) => (
            <Option key={value} value={value}>{label}</Option>
          ))}
        </Select>
      )
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('degree'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
    pdfValue: degrees[key]?.degreeType,
    pdfStyle: {
      flex: '0 0 31.4%',
      marginLeft: '2%',
    },
    pdfItemType: 'input',
  },
  {
    name: [name, 'year'],
    component: (
      readonly
        ? (
          <Input
            disabled
            value={degrees[key].year}
          />
        )
        : (
          <DatePicker
            format="YYYY"
            className="component"
            allowClear={false}
            picker="year"
            disabledDate={(current) => current && current > moment()}
          />
        )
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('year'),
    responsive: {
      xs: { span: 24 },
      md: { span: 4 },
    },
    display: true,
    pdfValue: degrees[key]?.year,
    pdfStyle: {
      flex: '0 0 16%',
    },
    pdfItemType: 'input',
  },
  {
    name: [name, `suneduCertificate-${key}`],
    component: (
      readonly
        ? (
          degrees[key].suneduCertificate
            ? (
              <Button
                type="primary"
                target="_blank"
                style={{ width: '100%' }}
                href={generateUrlDownload(degrees[key].suneduCertificateUrl)}
                icon={<FilePdfOutlined />}
              >
                Descargar
              </Button>
            )
            : (
              <Input
                disabled
                value="-"
              />
            )
        )
        : (
          <CustomUploadFile
            disabled={false}
            form={form}
            nameComponent={`suneduCertificate-${key}`}
            urlFile={degrees[key]?.suneduCertificateUrl
                && generateUrlDownload(degrees[key]?.suneduCertificateUrl)}
          />
        )
    ),
    rules: [
      () => ({
        validator() {
          const fieldValue = form.getFieldValue([`suneduCertificate-${key}`]);
          if (fieldValue) {
            return Promise.resolve();
          }
          return Promise.reject(new Error('¡Campo obligatorio!'));
        },
      }),
    ],
    label: t('constancySunedu'),
    responsive: {
      xs: { span: 24 },
      md: { span: 10 },
    },
    display: true,
    pdfValue: degrees[key]?.suneduCertificateUrl
      ? generateUrlDownload(degrees[key]?.suneduCertificateUrl) : null,
    pdfStyle: {
      flex: '0 0 40%',
      marginLeft: '2%',
    },
    pdfItemType: 'button',
    pdfItemLabel: t('download'),
  },
  {
    name: [name, `certificate-${key}`],
    component: (
      readonly
        ? (
          degrees[key].certificate
            ? (
              <Button
                type="primary"
                target="_blank"
                style={{ width: '100%' }}
                href={generateUrlDownload(degrees[key].certificateUrl)}
                icon={<FilePdfOutlined />}
              >
                Descargar
              </Button>
            )
            : (
              <Input
                disabled
                value="-"
              />
            )
        )
        : (
          <CustomUploadFile
            disabled={false}
            form={form}
            nameComponent={`certificate-${key}`}
            urlFile={degrees[key]?.certificateUrl
                && generateUrlDownload(degrees[key]?.certificateUrl)}
          />
        )
    ),
    rules: [
      () => ({
        validator() {
          const fieldValue = form.getFieldValue([`certificate-${key}`]);
          if (fieldValue) {
            return Promise.resolve();
          }
          return Promise.reject(new Error('¡Campo obligatorio!'));
        },
      }),
    ],
    label: t('degreeDiploma'),
    responsive: {
      xs: { span: 24 },
      md: { span: 10 },
    },
    display: true,
    pdfValue: degrees[key]?.certificateUrl
      ? generateUrlDownload(degrees[key]?.certificateUrl) : null,
    pdfStyle: {
      flex: '0 0 40%',
      marginLeft: '2%',
    },
    pdfItemType: 'button',
    pdfItemLabel: t('download'),
  },
];

export default degreesModalItems;
