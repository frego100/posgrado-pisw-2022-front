import { Dispatch, SetStateAction } from 'react';

export interface Degree {
  id: string,
  name: string,
  degreeType: string,
  certificate: string,
}

export interface DegreesModalProps {
  readonly: boolean,
  isVisible: boolean,
  setIsVisible: Dispatch<SetStateAction<boolean>>,
  professorId: string | string[] | undefined,
  professorViewForm: any
  professorExists: boolean | undefined
  professor: any
  degreesForm: any,
  setDegreesForm: any,
  professorRegistrationId: any,
}

export default { };
