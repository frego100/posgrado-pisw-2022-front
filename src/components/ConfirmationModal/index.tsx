import { useMutation } from '@apollo/client';
import {
  Button, message, Modal, Row,
} from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import confirmationModalTypes from './constants';

interface ConfirmationModalProps {
  confirmationModalData: any
  setConfirmationModalData: any
  sizeModal?: number,
}

const defaultProps: {
  sizeModal?: number,
} = {
  sizeModal: 450,
};

const approveOperatingPlanRequest = loader('./requests/approveOperatingPlan.gql');
const sendOperatingPlanRequest = loader('./requests/sendOperatingPlan.gql');
const observeOperatingPlanRequest = loader('./requests/observeOperatingPlan.gql');
const deleteProfessorRegistrationRequest = loader('./requests/deleteProfessorRegistration.gql');
const deleteAcademicProgressRequest = loader('./requests/deleteAcademicProgress.gql');
const deletePeriodEnrollmentsRequest = loader('./requests/deletePeriodEnrollments.gql');
const deleteCourseEnrollmentsRequest = loader('./requests/deleteCourseEnrollments.gql');
const deleteUnitManagerRequest = loader('./requests/deleteUnitManager.gql');
const deleteSchoolManagerRequest = loader('./requests/deleteSchoolManager.gql');
const openStudentAttendanceRequest = loader('./requests/openStudentAttendance.gql');

const ConfirmationModal:React.FC<ConfirmationModalProps> = ({
  confirmationModalData,
  setConfirmationModalData,
  sizeModal,
}) => {
  const { t } = useTranslation();
  const [sendOperatingPlan] = useMutation(sendOperatingPlanRequest);
  const [approveOperatingPlan] = useMutation(approveOperatingPlanRequest);
  const [observeOperatingPlan] = useMutation(observeOperatingPlanRequest);

  const [deleteProfessorRegistrationMutation] = useMutation(deleteProfessorRegistrationRequest);
  const [deleteAcademicProgressMutation] = useMutation(deleteAcademicProgressRequest);
  const [deletePeriodEnrollmentsMutation] = useMutation(deletePeriodEnrollmentsRequest);
  const [deleteCourseEnrollmentsMutation] = useMutation(deleteCourseEnrollmentsRequest);
  const [deleteUnitManagerMutation] = useMutation(deleteUnitManagerRequest);
  const [deleteSchoolManagerMutation] = useMutation(deleteSchoolManagerRequest);
  const [openStudentAttendanceMutation] = useMutation(openStudentAttendanceRequest);

  const handleOk = () => {
    if (confirmationModalData.type === 'sendOperatingPlan') {
      sendOperatingPlan({
        variables: { operatingPlanId: confirmationModalData.operatingPlanId },
      }).then(({ data }) => {
        const { sendOperatingPlan: sendOperatingPlanData } = data;
        if (sendOperatingPlanData.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(sendOperatingPlanData.feedback.message);
          confirmationModalData.refetch();
        }
      });
    } else if (confirmationModalData.type === 'observeOperatingPlan') {
      observeOperatingPlan({
        variables: { opPlanId: confirmationModalData.operatingPlanId },
      }).then(({ data }) => {
        const { observeOperatingPlan: observeOperatingPlanData } = data;
        if (observeOperatingPlanData.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(observeOperatingPlanData.feedback.message);
          confirmationModalData.refetch();
        }
      });
    } else if (confirmationModalData.type === 'approveOperatingPlan') {
      confirmationModalData.form.validateFields().then((values: any) => {
        approveOperatingPlan({
          variables: {
            opPlanId: confirmationModalData.operatingPlanId,
            approvalRd: values.approvalRd.originFileObj,
          },
        }).then(({ data }) => {
          const { approveOperatingPlan: approveOperatingPlanData } = data;
          if (approveOperatingPlanData.feedback.status === 'SUCCESS') {
            setConfirmationModalData({ ...confirmationModalData, isVisible: false });
            message.success(approveOperatingPlanData.feedback.message);
            confirmationModalData.refetch();
          }
        });
      });
    } else if (confirmationModalData.type === 'deleteRegistrationProfessor' || confirmationModalData.type === 'disableProfessorRegistration') {
      deleteProfessorRegistrationMutation({
        variables: {
          registrationId: confirmationModalData.registrationId,
        },
      }).then(({ data }) => {
        const { deleteProfessorRegistration } = data;
        if (deleteProfessorRegistration.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(deleteProfessorRegistration.feedback.message);
          confirmationModalData.refetch();
        } else {
          message.error(deleteProfessorRegistration.feedback.message);
        }
      });
    } else if (confirmationModalData.type === 'deleteAcademicProgress') {
      deleteAcademicProgressMutation({
        variables: {
          courseProgressId: confirmationModalData.courseProgressId,
        },
      }).then(({ data: deleteCourseProgressData }: any) => {
        const { deleteCourseProgress } = deleteCourseProgressData;
        if (deleteCourseProgress.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(deleteCourseProgressData.feedback.message);
          confirmationModalData.refetch();
        }
      });
    } else if (confirmationModalData.type === 'deleteStudentProgramList') {
      deletePeriodEnrollmentsMutation({
        variables: {
          periodId: confirmationModalData.period.value,
        },
      }).then(({ data: deletePeriodEnrollmentsData }: any) => {
        const { deletePeriodEnrollments } = deletePeriodEnrollmentsData;
        if (deletePeriodEnrollments.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(deletePeriodEnrollments.feedback.message);
          confirmationModalData.refetch();
        }
      });
    } else if (confirmationModalData.type === 'deleteCourseEnrollments') {
      deleteCourseEnrollmentsMutation({
        variables: {
          courseId: confirmationModalData.courseId,
        },
      }).then(({ data: deleteCourseEnrollmentsData }: any) => {
        const { deleteCourseEnrollments } = deleteCourseEnrollmentsData;
        if (deleteCourseEnrollments.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(deleteCourseEnrollments.feedback.message);
          confirmationModalData.refetch();
        }
      });
    } else if (confirmationModalData.type === 'deleteUnitManager') {
      deleteUnitManagerMutation({
        variables: {
          unitManagerId: confirmationModalData.unitManagerId,
        },
      }).then(({ data: deleteUnitManagerData }: any) => {
        const { deleteUnitManager } = deleteUnitManagerData;
        if (deleteUnitManager.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(deleteUnitManager.feedback.message);
          confirmationModalData.refetchUnitManager();
        }
      });
    } else if (confirmationModalData.type === 'deleteSchoolManager') {
      deleteSchoolManagerMutation({
        variables: {
          userId: confirmationModalData.userId,
        },
      }).then(({ data: deleteSchoolManagerData }: any) => {
        const { deleteSchoolManager } = deleteSchoolManagerData;
        if (deleteSchoolManager.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(deleteSchoolManager.feedback.message);
          confirmationModalData.refetchSchoolManager();
        }
      });
    } else if (confirmationModalData.type === 'allowStudentAttendance') {
      openStudentAttendanceMutation({
        variables: { academicSessionId: confirmationModalData.academicSessionId },
      }).then(({ data }) => {
        const { openStudentAttendanceRegistration } = data;
        if (openStudentAttendanceRegistration.feedback.status === 'SUCCESS') {
          setConfirmationModalData({ ...confirmationModalData, isVisible: false });
          message.success(openStudentAttendanceRegistration.feedback.message);
        }
      });
    }
  };

  const handleCancel = () => {
    setConfirmationModalData({ ...confirmationModalData, isVisible: false });
  };

  return (
    <Modal
      title={t(confirmationModalData.type)}
      centered
      width={sizeModal}
      closable={false}
      open={confirmationModalData.isVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      destroyOnClose
      footer={[
        <Button key="cancel" onClick={handleCancel}>
          {t('cancel')}
        </Button>,
        <Button key="ok" type="primary" onClick={handleOk}>
          {t('confirm')}
        </Button>,
      ]}
    >
      <p style={{ marginBottom: '10px', textAlign: 'justify' }}>
        {confirmationModalTypes(t)[confirmationModalData.type]?.text}
      </p>
      {confirmationModalData?.component && (
      <Row justify="center" align="middle">
        {confirmationModalData.component}
      </Row>
      )}
    </Modal>
  );
};

ConfirmationModal.defaultProps = defaultProps;
export default ConfirmationModal;
