const confirmationModalTypes = (
  t: any,
) :{
    [index: string] : any
} => ({
  approveOperatingPlan: {
    text: t('approveOperatingPlanText'),
  },
  sendOperatingPlan: {
    text: t('sendOperatingPlanText'),
  },
  observeOperatingPlan: {
    text: t('observeOperatingPlanText'),
  },
  deleteRegistrationProfessor: {
    text: t('deleteRegistrationProfessorText'),
  },
  deleteAcademicProgress: {
    text: t('deleteAcademicProgressText'),
  },
  disableProfessorRegistration: {
    text: t('disableProfessorRegistrationText'),
  },
  deleteStudentProgramList: {
    text: t('deleteStudentListText'),
  },
  deleteCourseEnrollments: {
    text: t('enrollmentsDelete'),
  },
  deleteUnitManager: {
    text: t('deleteUnitManagerText'),
  },
  deleteSchoolManager: {
    text: t('deleteSchoolManagerText'),
  },
  allowStudentAttendance: {
    text: t('allowStudentAttendanceText'),
  },
});

export default confirmationModalTypes;
