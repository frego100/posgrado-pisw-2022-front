import React from 'react';
import { Button, Row } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { ReactComponent as LogoUnsaIcon } from '../../media/icons/LogoUnsaIcon.svg';
import './styles.scss';

interface GoogleLoginButtonProps {
  loading: boolean,
  onClickFunc: () => void,
  title: string,
}

const GoogleLoginButton: React.FC<GoogleLoginButtonProps> = ({
  loading,
  onClickFunc, title,
}) => {
  if (loading) {
    return (
      <Button
        type="primary"
        icon={<LoadingOutlined />}
        onClick={onClickFunc}
        loading={loading}
        className="google-login-button"
      >
        {title}
      </Button>
    );
  }
  return (
    <Button
      type="primary"
      icon={(
        <Row className="anticon" role="img">
          <LogoUnsaIcon className="unsa-logo" />
        </Row>
      )}
      onClick={onClickFunc}
      loading={loading}
      className="google-login-button"
    >
      <div className="google-login-button__text">
        <span>
          {title}
        </span>
      </div>
    </Button>
  );
};

export default GoogleLoginButton;
