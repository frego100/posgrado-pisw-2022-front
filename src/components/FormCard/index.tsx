import React, { ReactNode } from 'react';
import {
  Card,
  Col, Form, Row,
} from 'antd';
import { FaMinusCircle } from 'react-icons/fa';
import './styles.scss';

interface Item {
  help?: any;
  name: any,
  component: ReactNode,
  rules?: any,
  label?: string,
  disabled?: boolean,
  responsive?: any
  display: boolean,
}

interface FormCardProps {
  title?: string,
  action?: ReactNode,
  itemList: Array<Item>,
  withoutTitle?: boolean,
  onClick?: any,
  readonly: boolean,
  withoutDeleteIcon?: boolean
  classnameTitle?: any
}

const defaultProps: {
  action?: ReactNode,
  withoutTitle?: boolean,
  title?: string,
  onClick?: any,
  withoutDeleteIcon?: boolean,
  classnameTitle?: any
} = {
  action: null,
  withoutTitle: false,
  title: '',
  onClick: null,
  withoutDeleteIcon: false,
  classnameTitle: 'title',
};

const FormCard: React.FC<FormCardProps> = ({
  title,
  action,
  itemList,
  withoutTitle,
  onClick,
  readonly,
  withoutDeleteIcon,
  classnameTitle,
  ...restField
}) => (
  <Card key="card" className={withoutTitle ? 'card-without-title' : 'card'}>
    {
        // eslint-disable-next-line no-nested-ternary
        withoutTitle
          ? (
            withoutDeleteIcon
              ? (
                <div />
              )
              : (
                <div className="delete-icon">
                  { !readonly
                && (
                  <FaMinusCircle
                    onClick={onClick}
                    style={{
                      color: '#081d40',
                      fontSize: 30,
                      cursor: 'pointer',
                      background: 'white',
                    }}
                  />
                )}
                </div>
              )
          )
          : (
            action
              ? (
                <Row>
                  <h1 className={classnameTitle}>{title}</h1>
                  <div className="action">{action}</div>
                </Row>
              )
              : (
                <Row justify="start">
                  <h1 className={classnameTitle}>{title}</h1>
                </Row>
              )
          )
      }
    <div key="content" className="content">
      <Row gutter={[28, 0]}>
        {itemList?.map((item) => (
          item.display
            ? (
              <Col {...item.responsive} key={item.name}>
                <Form.Item
                  label={item.label}
                  rules={item.rules}
                  key={item.name}
                  name={item.name}
                  {...withoutTitle && { ...restField }}
                  help={item.help}
                >
                  {item.component}
                </Form.Item>
              </Col>
            )
            : <React.Fragment key={item.name} />
        ))}
      </Row>
    </div>
  </Card>
);

FormCard.defaultProps = defaultProps;

export default FormCard;
