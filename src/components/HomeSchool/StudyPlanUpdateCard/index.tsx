import React from 'react';
import {
  Button, Card, Carousel, Col, Row, Tag, Tooltip,
} from 'antd';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { RightCircleOutlined, LeftCircleOutlined } from '@ant-design/icons';
import { alertStatus, typeStatus } from '../../CustomFilter/constants';
import { SCHOOL_DIRECTOR } from '../../../utils/constants';
import routesDictionary from '../../../routes/routesDictionary';
import '../styles.scss';

interface StudyPlanUpdateCardProps {
    studyPlansUpdated,
}

const StudyPlanUpdateCard:React.FC<StudyPlanUpdateCardProps> = (
  { studyPlansUpdated },
) => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  return (
    <Carousel
      autoplay
      arrows
      nextArrow={<RightCircleOutlined />}
      prevArrow={<LeftCircleOutlined />}
    >
      {studyPlansUpdated.map((studyPlan: any) => {
        const color = alertStatus[studyPlan.status] === 'info' ? 'processing' : alertStatus[studyPlan.status];
        return (
          <Card key={studyPlan.key}>
            <span className="label">{studyPlan.program}</span>
            <p>{studyPlan.unit}</p>
            <Row gutter={[0, 8]} justify="space-between">
              <Col>
                <Tag color={color}>
                  {typeStatus(SCHOOL_DIRECTOR)[studyPlan.status]}
                </Tag>
              </Col>
              <Col>
                <Tooltip title={t('requestDate')}>
                  <Tag>{studyPlan.requestDate}</Tag>
                </Tooltip>
              </Col>
            </Row>
            <br />
            <Row justify="center">
              <Button
                className="carouselCardButton"
                key="ok"
                type="primary"
                onClick={() => {
                  navigate(
                    routesDictionary.programStudyPlans.func(studyPlan.programId),
                  );
                }}
              >
                {t('view')}
              </Button>
            </Row>
          </Card>
        );
      })}
    </Carousel>
  );
};

export default StudyPlanUpdateCard;
