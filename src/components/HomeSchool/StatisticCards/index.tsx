import React from 'react';
import {
  Avatar, Card, Col, Row, Statistic,
} from 'antd';
import { useTranslation } from 'react-i18next';

interface StatisticCardsProps {
    unitsNumber: number,
    professorsNumber: number,
    studentsNumber: number,
    coursesNumber: number,
}

const StatisticCards:React.FC<StatisticCardsProps> = (
  {
    unitsNumber, professorsNumber, studentsNumber, coursesNumber,
  },
) => {
  const { t } = useTranslation();

  return (
    <Row gutter={16}>
      <Col xs={12} sm={12} md={6}>
        <Card>
          <Statistic title={t('units')} value={unitsNumber} prefix={<Avatar src="https://cdn-icons-png.flaticon.com/512/29/29302.png" />} />
        </Card>
      </Col>
      <Col xs={12} sm={12} md={6}>
        <Card>
          <Statistic title={t('professors')} value={professorsNumber} prefix={<Avatar src="https://cdn-icons-png.flaticon.com/512/2815/2815419.png" />} />
        </Card>
      </Col>
      <Col xs={12} sm={12} md={6}>
        <Card>
          <Statistic title={t('students')} value={studentsNumber} prefix={<Avatar src="https://as1.ftcdn.net/v2/jpg/01/05/29/62/1000_F_105296263_MX030meFkK57Jj7z4TaSZXw1T0KPQXmd.jpg" />} />
        </Card>
      </Col>
      <Col xs={12} sm={12} md={6}>
        <Card>
          <Statistic title={t('groups')} value={coursesNumber} prefix={<Avatar src="https://cdn-icons-png.flaticon.com/512/29/29302.png" />} />
        </Card>
      </Col>
    </Row>
  );
};

export default StatisticCards;
