import React, { useEffect, useState } from 'react';
import {
  Card, Col, Divider, Empty, Progress, Row, Select,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import CustomTable from '../CustomTable';
import { operatingPlansColums } from './constants';
import { AppDispatch } from '../../store';
import { startsetOperatingPlanProcess } from '../../store/thunks/auth';
import './styles.scss';
import { SCHOOL_DIRECTOR } from '../../utils/constants';
import { studyPlanRequestBackend } from '../../views/StudyPlans/StudyPlansProgram/StudyPlanRequestModal/constants';
import EmptyView from '../EmptyView';
import StatisticCards from './StatisticCards';
import StudyPlanUpdateCard from './StudyPlanUpdateCard';

const getSchoolDashboardRequest = loader('./requests/getSchoolDashboard.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');

const HomeSchool: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const { firstName, lastName, operatingPlanProcess } = useSelector((state: any) => state.auth);
  const [studyPlansUpdated, setStudyPlansUpdated] = useState<any>(null);

  const [process, setProcess] = useState<any>(operatingPlanProcess);
  const [processes, setProcesses] = useState<any>(null);

  const [numOfProfessors, setNumOfProfessors] = useState<number>(0);
  const [numOfStudents, setNumOfStudents] = useState<number>(0);
  const [numOfCourses, setNumOfCourses] = useState<number>(0);

  const {
    data: operatingPlanProcessesData,
    loading: operatingPlanProcessesLoading,
  } = useQuery(getOperatingPlanProcessesRequest);

  useEffect(() => {
    dispatch(startsetOperatingPlanProcess(process));
  }, [process]);

  useEffect(() => {
    if (operatingPlanProcessesData) {
      const { getOperatingPlanProcesses } = operatingPlanProcessesData;
      const operatingPlanProcessesArray: any = [];

      getOperatingPlanProcesses.forEach(({ id, year }: any) => {
        operatingPlanProcessesArray.push({ label: year, value: id });
      });
      if (!process) {
        setProcess(operatingPlanProcessesArray[0]?.value);
      }
      setProcesses(operatingPlanProcessesArray);
    }
  }, [operatingPlanProcessesData]);

  const [operatingPlanProgress, setOperatingPlanProgress] = useState<any>(0);
  const [operatingPlans, setOperatingPlans] = useState<any>(null);

  const { data, loading } = useQuery(getSchoolDashboardRequest, {
    variables: { process },
    fetchPolicy: 'cache-and-network',
    skip: !process,
  });

  useEffect(() => {
    if (data) {
      const { getSchoolDashboard } = data;
      const {
        graduateUnits, studyPlans, operatingPlansPercentage,
        numberOfProfessors,
        numberOfStudents,
      } = getSchoolDashboard;

      const studyPlansArray: any = [];
      const graduateUnitsArray: any = [];

      graduateUnits.forEach((item: any) => {
        graduateUnitsArray.push({
          key: item.id,
          unit: item.name,
          professorsNumber: item.numOfProfessors,
          studentsNumber: item.numOfStudents,
          coursesNumber: item.numOfCourseGroups,
        });
      });

      studyPlans.forEach((item: any) => {
        studyPlansArray.push({
          key: item.id,
          unit: item.program.graduateUnit.name,
          program: item.program.name,
          status: studyPlanRequestBackend[item.status],
          requestDate: item.requestDate,
          programId: item.program.id,
        });
      });

      const courses = graduateUnitsArray.reduce(
        (total, { coursesNumber }) => total + coursesNumber,
        0,
      );
      setNumOfProfessors(numberOfProfessors);
      setNumOfStudents(numberOfStudents);
      setNumOfCourses(courses);

      setOperatingPlanProgress(operatingPlansPercentage);
      setOperatingPlans(graduateUnitsArray);
      setStudyPlansUpdated(studyPlansArray);
    }
  }, [data]);

  const numOfOperatingPlans = operatingPlans ? operatingPlans.length : 0;

  return (
    <div style={{ marginTop: '22px' }}>
      <div className="headerHome">
        <span className="header-title">
          {`${t('welcome')} ${firstName} ${lastName}`}
        </span>
        <span className="header-subtitle">{t('graduateSchool')}</span>
      </div>
      <Divider />
      {processes && processes.length === 0
        ? (
          <Card>
            <EmptyView
              type="emptyOperatingPlanProcess"
              userType={SCHOOL_DIRECTOR}
              viewTitle={false}
            />
          </Card>
        ) : (
          <>
            <Row gutter={16}>
              <Col xs={24} sm={24} lg={7}>
                <Card>
                  <Row gutter={[25, 25]}>
                    <Col span={24}>
                      <span className="label">
                        {t('academicYear')}
                      </span>
                      <Select
                        labelInValue
                        value={process}
                        style={{ width: '100%' }}
                        options={processes}
                        onChange={(newValue) => setProcess(newValue.value)}
                        loading={operatingPlanProcessesLoading}
                      />
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col xs={24} sm={24} lg={17}>
                <StatisticCards
                  unitsNumber={numOfOperatingPlans}
                  professorsNumber={numOfProfessors}
                  studentsNumber={numOfStudents}
                  coursesNumber={numOfCourses}
                />
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xs={24} sm={24} lg={17}>
                <Card>
                  <b><p>{t('graduateUnits')}</p></b>
                  <Divider />
                  <CustomTable
                    dataSource={operatingPlans}
                    loading={loading}
                    columns={operatingPlansColums(t)}
                  />
                </Card>
              </Col>
              <Col xs={24} sm={24} lg={7}>
                <Card>
                  <b><p>{t('operatingPlanProgress')}</p></b>
                  <Divider />
                  <div className="progress">
                    <Progress type="circle" percent={operatingPlanProgress} />
                  </div>
                </Card>
                <Card>
                  <b><p>{t('studyPlanUpdateRequests')}</p></b>
                  <Divider />
                  {studyPlansUpdated?.length > 0
                    ? (
                      <>
                        <StudyPlanUpdateCard studyPlansUpdated={studyPlansUpdated} />
                        <p>{`${t('Total')} ${studyPlansUpdated?.length} ${t('items')}`}</p>
                      </>
                    )
                    : <Empty description={t('emptyStudyPlanUpdateRequests')} /> }
                </Card>
              </Col>
            </Row>
          </>
        )}
    </div>
  );
};

export default HomeSchool;
