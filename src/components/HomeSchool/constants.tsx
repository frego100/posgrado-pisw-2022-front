import { TFunction } from 'i18next';

export const operatingPlansColums = (translate: TFunction) => [
  {
    key: 'unit',
    title: translate('unit'),
    dataIndex: 'unit',
    sorterType: 'alphabetical',
  },
  {
    key: 'professorsNumber',
    title: translate('professors'),
    dataIndex: 'professorsNumber',
    sorterType: 'numerical',
  },
  {
    key: 'studentsNumber',
    title: translate('students'),
    dataIndex: 'studentsNumber',
    sorterType: 'numerical',
  },
  {
    key: 'coursesNumber',
    title: translate('groups'),
    dataIndex: 'coursesNumber',
    sorterType: 'numerical',
  },
];

export const studyPlansColums = (translate: TFunction) => [
  {
    key: 'unit',
    title: translate('unit'),
    dataIndex: 'unit',
    sorterType: 'alphabetical',
  },
  {
    key: 'program',
    title: translate('program'),
    dataIndex: 'program',
    sorterType: 'alphabetical',
  },
  {
    key: 'status',
    title: translate('status'),
    dataIndex: 'status',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    sorterType: 'numerical',
  },
];

export default {};
