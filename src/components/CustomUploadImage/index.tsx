import React, { useEffect, useState } from 'react';
import {
  Modal, Upload,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './styles.scss';

interface CustomUploadImageProps {
  disabled: boolean,
  form: any,
  nameComponent: string,
  defaultValue?: string | null,
}

const defaultProps: {
  defaultValue?: string | null
} = {
  defaultValue: null,
};

const getBase64 = (file: any) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);

  reader.onload = () => resolve(reader.result);

  reader.onerror = (error) => reject(error);
});
const CustomUploadImage:React.FC<CustomUploadImageProps> = ({
  disabled,
  form,
  nameComponent,
  defaultValue,
}) => {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState(defaultValue || '');
  const [previewTitle, setPreviewTitle] = useState('');
  const [fileList, setFileList] = useState(defaultValue ? [{
    uid: '-1',
    name: 'image.png',
    status: 'done',
    url: defaultValue,

  }] : []);

  useEffect(() => {
    if (defaultValue) {
      setFileList([{
        uid: '-1',
        name: 'image.png',
        status: 'done',
        // @ts-ignore
        url: defaultValue,
      }]);
    }
  }, [defaultValue]);

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
    );
  };

  // @ts-ignore
  const handleChange = ({ fileList: newFileList }) => {
    if (newFileList[0]) {
      form.setFieldsValue({ [nameComponent]: newFileList[0] });
    }
    setFileList(newFileList);
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div
        style={{
          marginTop: 8,
          width: 300,
        }}
      >
        Upload
      </div>
    </div>
  );
  return (
    <div style={{ height: 300, marginBottom: 28 }}>
      <Upload
        disabled={disabled}
        listType="picture-card"
          // @ts-ignore
        fileList={fileList}
        onPreview={handlePreview}
        onChange={handleChange}
        beforeUpload={() => false}
        accept="image/*"
        className="upload"
        style={{ height: '100%' }}
      >
        {fileList.length > 0 ? null : uploadButton}
      </Upload>
      <Modal
        open={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img
          alt="example"
          style={{
            width: '100%',
          }}
          src={previewImage}
        />
      </Modal>
    </div>
  );
};

CustomUploadImage.defaultProps = defaultProps;

export default CustomUploadImage;
