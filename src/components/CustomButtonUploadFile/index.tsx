import React from 'react';
import {
  Button,
  Row,
} from 'antd';
import { FileAddFilled } from '@ant-design/icons';

interface CustomButtonUploadFileProps {
  name: string,
  onClick?: any
}

const defaultProps: {
  onClick?: any,
  } = {
    onClick: null,
  };

const CustomButtonUploadFile:React.FC<CustomButtonUploadFileProps> = ({
  name, onClick,
}) => (
  <Row>
    <Button
      type="primary"
      style={{ width: '100%' }}
      icon={<FileAddFilled />}
      onClick={onClick}
    >
      {name}
    </Button>
  </Row>
);

CustomButtonUploadFile.defaultProps = defaultProps;

export default CustomButtonUploadFile;
