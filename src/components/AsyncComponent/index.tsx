import React, { Component } from 'react';

export default function asyncComponent(importComponent: ()
    => PromiseLike<{ default: any; }> | { default: any; }) {
  class AsyncComponent extends Component {
    constructor(props : any) {
      super(props);

      this.state = {
        component: null,
      };
    }

    async componentDidMount() {
      const { default: component } = await importComponent();

      this.setState({
        component,
      });
    }

    render() {
      // @ts-ignore
      const { component: DownloadComponent } = this.state;

      return DownloadComponent ? <DownloadComponent {...this.props} /> : null;
    }
  }

  return AsyncComponent;
}
