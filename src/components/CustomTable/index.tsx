import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  Empty, Table,
} from 'antd';
import CustomLoader from '../CustomLoader';
import './styles.scss';
import { getColumns } from './functions';

interface CustomTableProps {
  dataSource: any,
  loading: boolean,
  columns: any,
  bordered?: boolean
  showPagination?: boolean,
}

const defaultProps: {
    bordered?: boolean,
    showPagination?: boolean,
} = {
  bordered: false,
  showPagination: true,
};

const CustomTable:React.FC<CustomTableProps> = ({
  dataSource,
  loading,
  columns,
  bordered,
  showPagination,
}) => {
  const { t } = useTranslation();

  return (
    <Table
      className="table"
      dataSource={loading ? [] : dataSource}
      bordered={bordered}
      columns={getColumns(columns)}
      loading={{
        indicator: (
          <div style={{ marginTop: 55 }}>
            <CustomLoader size={40} />
          </div>),
        spinning: loading,
      }}
      scroll={{ x: 'max-content' }}
      pagination={
        showPagination ? {
          total: dataSource?.length,
          showTotal: ((total, range) => `${range[0]}-${range[1]} ${t('of')} ${total} ${t('items')}`),
        } : false
    }
      rowClassName={(record, index) => (index % 2 === 0 ? 'table-row-light' : 'table-row-dark')}
      locale={{
        emptyText: loading ? <div style={{ height: 500 }} /> : (
          <Empty
            style={{ height: 500 }}
            image={Empty.PRESENTED_IMAGE_SIMPLE}
          />
        ),
        triggerAsc: t('sortAscending'),
        triggerDesc: t('sortDescending'),
        cancelSort: t('cancelSorting'),
      }}
    />
  );
};
CustomTable.defaultProps = defaultProps;

export default CustomTable;
