const alphabeticalSort = (key) => (a, b) => a[key]?.toLowerCase()
  .localeCompare(b[key]?.toLowerCase());

const numericalSort = (key) => (a, b) => a[key] - b[key];

const dateSort = (key) => (a, b) => a[key]?.format('YYYY-MM-DD').toLowerCase()
  .localeCompare(b[key]?.format('YYYY-MM-DD').toLowerCase());

const typeSorts = {
  alphabetical: alphabeticalSort,
  numerical: numericalSort,
  date: dateSort,
};

export const getColumns = (columns) => columns.map((item) => {
  if (item.sorterType) {
    return ({ ...item, sorter: typeSorts[item.sorterType](item.key) });
  }
  return item;
});

export default {};
