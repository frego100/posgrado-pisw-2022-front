export const typeProgram: { [index: string]: string } = {
  A_1: 'Maestria',
  A_2: 'Doctorado',
  A_3: 'Diplomado',
};

export const typeDocument: { [index: string]: string } = {
  A_1: 'DNI',
  A_2: 'CE',
};

export const typeGender: { [index: string]: string } = {
  A_1: 'Masculino',
  A_2: 'Femenino',
};
export const typeProfessor:{[index: string] : string} = {
  A_1: 'Invitado',
  A_2: 'De la universidad',
};

export default {};
