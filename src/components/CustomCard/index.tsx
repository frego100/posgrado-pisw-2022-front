import React from 'react';
import { Animate } from 'react-simple-animate';
import { Card } from 'antd';

interface CustomCardProps {
  children: React.ReactNode
  sequenceIndex?: number,
  className?: string,
}

const defaultProps: {
    className?: string,
    sequenceIndex?: number

} = {
  className: '',
  sequenceIndex: 2,
};

const CustomCard:React.FC<CustomCardProps> = ({ children, sequenceIndex, className }) => (
  <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }} sequenceIndex={sequenceIndex}>
    <Card className={className}>
      {children}
    </Card>
  </Animate>
);

CustomCard.defaultProps = defaultProps;

export default CustomCard;
