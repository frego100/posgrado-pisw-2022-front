import React from 'react';
import { TFunction } from 'react-i18next';
import { PaperClipOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import CustomUploadFile from '../CustomUploadFile';
import { handleDownloadFile } from '../../views/Payments/PaymentManagement/PaymentsProgram/functions';
import { PAYMENTS_TEMPLATE_FILE } from '../../utils/constants';

export const enrollmentModalTypes = (t: any):{ [index:string]:any } => (
  {
    programStudentsList: t('registerProgramStudentsText'),
    courseStudentsList: t('registerSubjectStudentsText'),
    updatePaymentRecord: t('updatePaymentRecordText'),
  }
);

export const Items = (
  form: any,
  requestData: any,
  t: TFunction,
) => [
  {
    name: 'admissionYear',
    component: <p>{requestData.admissionYear || '-'}</p>,
    rules: [{ required: false }],
    label: t('entrants'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: requestData.type === 'updatePaymentRecord',
  },
  {
    name: 'paymentsTemplate',
    component: (
      <Button
        type="link"
        target="_blank"
        rel="noreferrer"
        style={{ width: '100%', padding: 0 }}
        onClick={() => handleDownloadFile(requestData?.paymentScheduleId, PAYMENTS_TEMPLATE_FILE)}
        icon={<PaperClipOutlined />}
      >
        {t('downloadPaymentsTemplate')}
      </Button>
    ),
    rules: [
      {
        required: false,
      },
    ],
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: requestData.type === 'updatePaymentRecord',
  },
  {
    name: 'academicYear',
    component: <p>{requestData?.year}</p>,
    rules: [{ required: false }],
    label: t('academicYear'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: requestData.type === 'programStudentsList',
  },
  {
    name: 'enrollmentPeriod',
    component: <p>{requestData?.period?.label}</p>,
    rules: [{ required: false }],
    label: t('enrollmentPeriod'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: requestData.type === 'programStudentsList',
  },
  {
    name: 'uploadFile',
    component: (
      <CustomUploadFile
        disabled={false}
        form={form}
        nameComponent="uploadFile"
        typeFile=".xlsx"
      />
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('uploadFile'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
];

export const formItems = (
  form: any,
  modalData: any,
  t: TFunction,
) => [
  ...Items(
    form,
    modalData,
    t,
  ),
];
export default {};
