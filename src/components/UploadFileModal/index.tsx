import React from 'react';
import {
  Button, Card, Form, message, Modal,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { useMutation } from '@apollo/client';
import { loader } from 'graphql.macro';
import FormCard from '../FormCard';
import { enrollmentModalTypes, formItems } from './constants';
import './styles.scss';

interface UploadFileModalProps {
    uploadFileModalData: any
    setUploadFileModalData: any
}

const addStudentsToProgramRequest = loader('./requests/addStudentsToProgram.gql');
const addEnrollmentToCourseRequest = loader('./requests/addEnrollmentToCourse.gql');
const saveStudentPaymentsRequest = loader('./requests/saveStudentPayments.gql');

const UploadFileModal:React.FC<UploadFileModalProps> = ({
  uploadFileModalData,
  setUploadFileModalData,
}) => {
  const { t } = useTranslation();
  const [form] = useForm();
  const params = useParams();
  const { subjectId } = params;

  const [addStudentsToProgramMutation, { loading: addStudentsToProgramLoading }] = useMutation(
    addStudentsToProgramRequest,
  );
  const [addEnrollmentToCourseMutation, {
    loading: addEnrollmentTocourseLoading,
  }] = useMutation(
    addEnrollmentToCourseRequest,
  );
  const [saveStudentPaymentsMutation, {
    loading: saveStudentPaymentsLoading,
  }] = useMutation(
    saveStudentPaymentsRequest,
  );

  const handleOk = () => {
    if (uploadFileModalData.type === 'programStudentsList') {
      form.validateFields()
        .then((values) => {
          addStudentsToProgramMutation({
            variables: {
              studentsFile: values.uploadFile.originFileObj,
              enrollmentPeriodId: uploadFileModalData?.period?.value,
            },
          })
            .then(({ data }) => {
              const { addStudents: resultData } = data;
              if (resultData.feedback.status === 'SUCCESS') {
                setUploadFileModalData({ isVisible: false });
                message.success(resultData.feedback.message);
                uploadFileModalData.coursesAndEnrollmentsRefetch();
                uploadFileModalData.periodsRefetch();
              } else {
                message.error(resultData.feedback.message);
              }
            }).catch(() => {
              form.setFields([{
                name: 'uploadFile',
                errors: [t('invalidFile')],
              }]);
            });
        });
    } else if (uploadFileModalData.type === 'courseStudentsList') {
      form.validateFields()
        .then((values) => {
          addEnrollmentToCourseMutation({
            variables: {
              enrollmentsFile: values.uploadFile.originFileObj,
              courseId: subjectId,
            },
          })
            .then(({ data }) => {
              const { addGroupEnrollments: resultData } = data;
              if (resultData.feedback.status === 'SUCCESS') {
                setUploadFileModalData({ isVisible: false });
                message.success(resultData.feedback.message);
                uploadFileModalData.refetch();
              } else {
                message.error(resultData.feedback.message);
              }
            }).catch(() => {
              message.error(t('errorAddGroupEnrollment'));
            });
        });
    } else if (uploadFileModalData.type === 'updatePaymentRecord') {
      form.validateFields()
        .then((values) => {
          saveStudentPaymentsMutation({
            variables: {
              paymentsSheet: values.uploadFile.originFileObj,
              paymentScheduleId: uploadFileModalData.paymentScheduleId,
            },
          })
            .then(({ data }) => {
              const { saveStudentsPayments: resultData } = data;
              if (resultData.feedback.status === 'SUCCESS') {
                setUploadFileModalData({ ...uploadFileModalData, isVisible: false });
                message.success(resultData.feedback.message);
                uploadFileModalData.refetch();
                uploadFileModalData.paymentScheduleRefetch();
              } else {
                message.error(resultData.feedback.message);
              }
            }).catch(() => {
              message.error(t('errorAddGroupEnrollment'));
            });
        });
    }
  };

  const handleCancel = () => {
    setUploadFileModalData({ ...uploadFileModalData, isVisible: false });
  };

  if (!uploadFileModalData?.period && uploadFileModalData.type === 'programStudentsList') {
    return (
      <Modal
        key="modal"
        className="messageModal"
        title={t('emptyEnrollmentPeriodsTitle')}
        centered
        width={500}
        closable
        destroyOnClose
        open={uploadFileModalData.isVisible}
        onCancel={handleCancel}
        footer={(
          <Button key="ok" type="primary" onClick={handleCancel}>
            {t('accept')}
          </Button>
        )}
      >
        <Card className="messageContent">
          <p>{t('emptyEnrollmentPeriodsDescription')}</p>
        </Card>
      </Modal>
    );
  }

  return (
    <Modal
      key="modal"
      title={t(uploadFileModalData.text)}
      centered
      width={500}
      closable={false}
      destroyOnClose
      open={uploadFileModalData.isVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={[
        <Button key="cancel" onClick={handleCancel}>
          {t('cancel')}
        </Button>,
        <Button
          key="ok"
          type="primary"
          onClick={handleOk}
          loading={
            addStudentsToProgramLoading
            || addEnrollmentTocourseLoading
            || saveStudentPaymentsLoading
          }
        >
          {t('confirm')}
        </Button>,
      ]}
    >
      <p style={{ marginBottom: '10px', textAlign: 'justify' }}>
        {enrollmentModalTypes(t)[uploadFileModalData.type]}
      </p>
      <Form
        key="form"
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
        form={form}
      >
        <FormCard
          withoutTitle
          withoutDeleteIcon
          itemList={formItems(
            form,
            uploadFileModalData,
            t,
          )}
          readonly={false}
        />
      </Form>
    </Modal>
  );
};

export default UploadFileModal;
