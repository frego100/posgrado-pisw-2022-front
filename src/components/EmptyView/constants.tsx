import { STUDENT, UNIT_DIRECTOR } from '../../utils/constants';

const emptyViewTypes = (
  t: any,
  userType: string,
) :{
      [index: string] : any
  } => ({
  emptyOperatingPlanProcess: {
    title: t('emptyOperatingPlanTitle'),
    description: t(userType === UNIT_DIRECTOR ? 'emptyOperatingPlanDescriptionUnitDirector' : 'emptyOperatingPlanProcessDescriptionSchoolDirector'),
  },
  operatingPlanNotApproved: {
    title: t('operatingPlanNotApprovedTitle'),
    description: t(userType === UNIT_DIRECTOR ? 'operatingPlanNotApprovedDescriptionUnitDirector' : 'operatingPlanNotApprovedDescriptionSchoolDirector'),
  },
  emptyOperatingPlanPrograms: {
    title: t('emptyOperatingPlanProgramsTitle'),
    description: t(userType === STUDENT ? 'emptyOperatingPlanProgramsDescriptionStudent' : 'emptyOperatingPlanProgramsDescriptionProfessor'),
  },
  emptyEnrollmentPeriods: {
    title: t('operatingPlanNotApprovedTitle'),
    description: t(userType === UNIT_DIRECTOR ? 'emptyEnrollmentPeriodsDescription' : 'operatingPlanNotApprovedDescriptionSchoolDirector'),
  },
  emptyPaymentSchedules: {
    title: t('emptyPaymentSchedules'),
    description: t(userType === UNIT_DIRECTOR ? 'emptyPaymentSchedulesDescription' : 'emptyPaymentSchedulesDescriptionSchoolDirector'),
  },
});

export default emptyViewTypes;
