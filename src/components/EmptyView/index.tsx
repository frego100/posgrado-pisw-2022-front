import React from 'react';
import { Row } from 'antd';
import { useTranslation } from 'react-i18next';
import TitleView from '../TitleView';
import EmptyCard from '../EmptyCard';
import { ReactComponent as DataArranging } from '../../media/images/ilustrations/Data Arranging.svg';
import { ReactComponent as CreationProcess } from '../../media/images/ilustrations/creationProcess.svg';
import emptyViewTypes from './constants';

interface EmptyViewProps {
  userType: string,
  type: string,
  title?: string,
  viewTitle?: boolean,
  viewBackButton?: boolean,
  onClickBackButton?: any,
}

const defaultProps: {
  title?: string,
  onClickBackButton?: any,
  viewTitle?: boolean,
  viewBackButton?: boolean,

} = {
  title: '',
  onClickBackButton: null,
  viewTitle: true,
  viewBackButton: false,
};

const EmptyView: React.FC<EmptyViewProps> = (
  {
    title, type, userType, viewBackButton, onClickBackButton, viewTitle,
  },
) => {
  const { t } = useTranslation();

  if (!viewTitle) {
    return (
      <EmptyCard
        title={emptyViewTypes(t, userType)[type].title}
        description={emptyViewTypes(t, userType)[type].description}
        bordered={false}
        action={<Row align="middle" gutter={[0, 16]} justify="center"><DataArranging /></Row>}
      />
    );
  }

  return (
    <>
      <TitleView
        title={title}
        viewBackButton={viewBackButton}
        onClickBackButton={onClickBackButton}
      />
      <EmptyCard
        title={emptyViewTypes(t, userType)[type].title}
        description={emptyViewTypes(t, userType)[type].description}
        action={(
          <Row align="middle" gutter={[0, 16]} justify="center">
            <CreationProcess style={{ height: 200 }} />
          </Row>
      )}
      />
    </>
  );
};

EmptyView.defaultProps = defaultProps;

export default EmptyView;
