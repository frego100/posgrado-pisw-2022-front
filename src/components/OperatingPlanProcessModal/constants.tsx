import React from 'react';
import locale from 'antd/es/date-picker/locale/es_ES';
import { FilePdfOutlined, PaperClipOutlined } from '@ant-design/icons';
import {
  Button, DatePicker, Input, Select,
} from 'antd';
import { TFunction } from 'react-i18next';
import moment from 'moment';
import { generateUrlDownload, getMediaLink } from '../../utils/tools';
import CustomUploadFile from '../CustomUploadFile';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../utils/constants';

const { Option } = Select;

const disabledDate = (current, date) => current && current < moment(date).endOf('day');
const disabledBeforeDate = (current, date) => current && current > moment(date).endOf('day');

export const Items = (
  form: any,
  readOnly: boolean,
  requestData: any,
  t: TFunction,
  templateOperatingPlanIndex: any,
  userType: any,
  handleUpdateOperatingPlanProcess: any,
  updateOperatingPlanProcessLoading: any,
  endDateForm: any,
  startDateForm: any,
) => [
  {
    name: 'year',
    component: (readOnly
      ? (
        <Input disabled />
      )
      : (
        <Select className="component" defaultValue="2022">
          <Option value="2022">2022</Option>
          <Option value="2023">2023</Option>
        </Select>
      )
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('year'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'startDate',
    component: (
      readOnly
        ? (
          <Input disabled />
        )
        : (
          <DatePicker
            style={{ width: '100%' }}
            locale={locale}
            format="YYYY-MM-DD"
            {...endDateForm
            && { disabledDate: (current) => disabledBeforeDate(current, endDateForm) }}
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('startDate'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: true,
  },
  {
    name: 'endDate',
    component: (
      readOnly
        ? (
          <Input disabled />
        )
        : (
          <DatePicker
            style={{ width: '100%' }}
            locale={locale}
            {...startDateForm
            && { disabledDate: (current) => disabledDate(current, startDateForm) }}
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('endDate'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: true,
  },
  {
    name: 'postponementDate',
    component: (
      requestData.postponementDate
        ? <Input disabled />
        : (
          <DatePicker
            className="w-100"
            disabledDate={(current) => disabledDate(current, requestData.endDate)}
            locale={locale}
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('postponementDate'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: readOnly && moment().isAfter(requestData.endDate)
        && (userType === SCHOOL_DIRECTOR
            || (userType === UNIT_DIRECTOR && requestData.postponementDate)),
  },
  {
    name: 'postpone',
    component: (
      <Button
        type="primary"
        className="w-100"
        onClick={handleUpdateOperatingPlanProcess}
        loading={updateOperatingPlanProcessLoading}
        hidden={requestData.postponementDate}
      >
        {t('postpone')}
      </Button>
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('emptyCharacter'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: readOnly && userType === SCHOOL_DIRECTOR && moment().isAfter(requestData.endDate),
  },
  {
    name: 'processLetter',
    component: (
      readOnly
        ? (
          <Button
            type="primary"
            target="_blank"
            style={{ width: '100%' }}
            href={getMediaLink(requestData.processLetter)}
            icon={<FilePdfOutlined />}
          >
            {t('download')}
          </Button>
        )
        : (
          <CustomUploadFile
            disabled={readOnly}
            form={form}
            nameComponent="processLetter"
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('processLetter'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'descriptiveIndexFile',
    component: (
      readOnly
        ? (
          <Button
            type="primary"
            target="_blank"
            style={{ width: '100%' }}
            href={getMediaLink(requestData.descriptiveIndexFile)}
            icon={<FilePdfOutlined />}
          >
            {t('download')}
          </Button>
        )
        : (
          <CustomUploadFile
            disabled={readOnly}
            typeFile=".docx, .pdf"
            form={form}
            nameComponent="descriptiveIndexFile"
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('descriptiveIndex'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'operatinPlanTemplate',
    component: (
      <a
        className="link-component"
        target="_blank"
        href={generateUrlDownload(templateOperatingPlanIndex?.templateFile)}
        rel="noreferrer"
      >
        <PaperClipOutlined />
        {' '}
        {t('downloadOperatingPlanIndexTemplate')}
      </a>
    ),
    rules: [
      {
        required: false,
      },
    ],
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: !readOnly,
  },
  {
    name: 'opIndex',
    component: (
      <CustomUploadFile
        disabled={readOnly}
        form={form}
        typeFile=".xlsx"
        nameComponent="opIndex"
      />
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('opIndex'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: !readOnly,
  },
];

export const formItems = (
  form: any,
  readOnly: boolean,
  modalData: any,
  t: TFunction,
  templateOperatingPlanIndex: any,
  userType: any,
  handleUpdateOperatingPlanProcess: any,
  updateOperatingPlanProcessLoading: any,
  endDateForm: any,
  startDateForm: any,
) => [
  ...Items(
    form,
    readOnly,
    modalData,
    t,
    templateOperatingPlanIndex,
    userType,
    handleUpdateOperatingPlanProcess,
    updateOperatingPlanProcessLoading,
    endDateForm,
    startDateForm,
  ),
];
export default {};
