import React, { useEffect, useState } from 'react';
import {
  Button, Form, message, Modal,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { useForm } from 'antd/lib/form/Form';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import moment from 'moment';
import { useSelector } from 'react-redux';
import FormCard from '../FormCard';
import { formItems } from './constants';

interface OperatingPlanProcessModalProps {
  modalData: any,
  setModalData: any,
  readOnly: boolean
}
const createOperatingPlanProcessRequest = loader('./requests/createOperatingPlanProcess.gql');
const updateOperatingPlanProcessRequest = loader('./requests/updateOperatingPlanProcess.gql');
const getTemplateByCodeRequest = loader('./requests/getTemplateByCode.gql');

const OperatingPlanProcessModal:React.FC<OperatingPlanProcessModalProps> = (
  { modalData, setModalData, readOnly },
) => {
  const { t } = useTranslation();
  const [form] = useForm();
  const postponementDateForm = Form.useWatch('postponementDate', form);

  const [templateOperatinPlanIndex, setTemplateOperatinPlanIndex] = useState<any>();
  const templateCode = 'IPF';
  const { userType } = useSelector((state: any) => state.auth);
  const endDateForm = Form.useWatch('endDate', form);
  const startDateForm = Form.useWatch('startDate', form);

  const { data: templateData } = useQuery(
    getTemplateByCodeRequest,
    {
      variables: { templateCode },
      fetchPolicy: 'no-cache',
    },
  );

  useEffect(() => {
    if (templateData) {
      const { getTemplateByCode } = templateData;
      setTemplateOperatinPlanIndex(getTemplateByCode);
    }
  }, [templateData]);

  const [createOperatingPlanProcessMutation, { loading }] = useMutation(
    createOperatingPlanProcessRequest,
  );

  const [updateOperatingPlanProcessMutation,
    { loading: updateOperatingPlanProcessLoading }] = useMutation(
    updateOperatingPlanProcessRequest,
  );

  const handleCancel = () => {
    setModalData({ ...modalData, visible: false });
  };

  useEffect(() => {
    form.setFieldsValue(modalData);
  }, [modalData]);

  useEffect(() => {
    if (modalData.postponementDate && readOnly && !postponementDateForm) {
      form.setFieldsValue({
        postponementDate: modalData.postponementDate.format('YYYY-MM-DD'),
      });
    }
  }, [modalData]);

  const handleUpdateOperatingPlanProcess = () => {
    form.validateFields(['postponementDate'])
      .then((values) => {
        updateOperatingPlanProcessMutation({
          variables: {
            input: {
              processId: modalData.processId,
              year: modalData.year,
              startDate: modalData.startDate,
              endDate: modalData.endDate,
              postponementDate: values.postponementDate.format('YYYY-MM-DD'),
            },
          },
        })
          .then(({ data }) => {
            const { updateOperatingPlanProcess } = data;
            if (updateOperatingPlanProcess.feedback.status === 'SUCCESS') {
              setModalData({ ...modalData, visible: false });
              message.success(updateOperatingPlanProcess.feedback.message);
              modalData.refetch();
            } else {
              message.error(updateOperatingPlanProcess.feedback.message);
            }
          });
      });
  };

  const handleCreateOperatingPlanProcess = () => {
    form.validateFields()
      .then((values) => {
        createOperatingPlanProcessMutation({
          variables: {
            input: {
              year: values.year,
              startDate: moment(values.startDate).format('YYYY-MM-DD'),
              endDate: moment(values.endDate).format('YYYY-MM-DD'),
              processLetter: values.processLetter.originFileObj,
              descriptiveIndexFile: values.descriptiveIndexFile.originFileObj,
              opIndex: values.opIndex.originFileObj,
            },
          },
        })
          .then(({ data }) => {
            const { createOperatingPlanProcess: resultData } = data;
            if (resultData.feedback.status === 'SUCCESS') {
              setModalData({ ...modalData, visible: false });
              message.success(resultData.feedback.message);
              modalData.refetch();
            } else {
              message.error(resultData.feedback.message);
              modalData.refetch();
            }
          }).catch((e) => {
            message.error(e.message);
          });
      });
  };

  return (
    <Modal
      title={
        readOnly ? (
          t('operatingPlan')
        ) : (t('openOperatingPlan'))
      }
      centered
      width={550}
      closable
      destroyOnClose
      open={modalData.visible}
      onCancel={handleCancel}
      bodyStyle={{ maxHeight: '80vh', overflowY: 'auto' }}
      footer={[
        (!readOnly && [
          <Button key="back" onClick={handleCancel}>
            {t('cancel')}
          </Button>,
          <Button key="observe" type="primary" onClick={handleCreateOperatingPlanProcess} loading={loading}>
            {t('save')}
          </Button>,
        ]),
      ]}
    >
      <Form
        name="createOperatingPlanProcess"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ year: '2022' }}
        autoComplete="off"
        scrollToFirstError
        form={form}
      >
        {!readOnly && (
          <p style={{ textAlign: 'justify' }}>
            {t('operatingPlanProcessInfoText')}
          </p>
        )}
        <FormCard
          withoutTitle
          withoutDeleteIcon
          itemList={formItems(
            form,
            readOnly,
            modalData,
            t,
            templateOperatinPlanIndex,
            userType,
            handleUpdateOperatingPlanProcess,
            updateOperatingPlanProcessLoading,
            endDateForm,
            startDateForm,
          )}
          readonly={false}
        />
      </Form>
    </Modal>
  );
};

export default OperatingPlanProcessModal;
