export const filterByValue = (items: any[], key: any, value: any, defaultValues: any[]) => {
  if (value === '') return defaultValues;
  return defaultValues.filter((item) => item[key].indexOf(value) !== -1 || item[key] === value);
};

// eslint-disable-next-line max-len
export const applyFilters = (dataSource: any[], activeFilters: any) => dataSource?.filter((data) => {
  let filtered = true;
  Object.entries(activeFilters).forEach(([key, value]) => {
    // @ts-ignore
    if (data[key].toLowerCase().indexOf(value.toLowerCase()) === -1) {
      filtered = false;
    }
  });
  return filtered;
});
