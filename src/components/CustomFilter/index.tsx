import React, { useState } from 'react';
import { Col, Row } from 'antd';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import customFilterComponents from './constants';
import { applyFilters } from './functions';
import useDidMountEffect from '../../customHooks/useDidMountEffect';
import './styles.scss';

interface CustomFilterProps {
  typeFilter: 'operatingPlanFilters'
      | 'professorListFilters' | 'subjectListFilters'
      | 'subjectListApprovePlan' | 'entrantsListFilters'
      | 'professorListApproveOperatingPlan'
      | 'studentListFilters'
      | 'attendanceStudentListFilters' | 'subjectStudentListFilters'
      | 'userList' | 'paymentStudentsFilters'
  ,
  operatingPlanStatus?: string
  approvalDocument?: any,
  setDataSource: any,
  defaultDataSource: any,
  programOperatingPlanId?: any,
  handleChangeProgramOpPlanId?: any,
  studyPrograms?: any,
  year?: any,
  handleChangeYear?: any,
  sentLate?: any,
  sentOPDate?: any,
  courseGroupsAvailable?: any,
  handleChangeAttendanceStatus?: any,
  paymentsLastUptdate?: any,
}

const defaultProps: {
  approvalDocument?: any,
  programOperatingPlanId?: any,
  handleChangeProgramOpPlanId?: any,
  studyPrograms?: any,
  year?: any,
  handleChangeYear?: any,
  operatingPlanStatus?: string
  sentLate?: any,
  sentOPDate?: any,
  courseGroupsAvailable?: any,
  handleChangeAttendanceStatus?: any,
  attendanceIsEditable?: any,
  paymentsLastUptdate?: any,
} = {
  approvalDocument: null,
  programOperatingPlanId: null,
  handleChangeProgramOpPlanId: null,
  studyPrograms: null,
  year: null,
  handleChangeYear: null,
  operatingPlanStatus: null,
  sentLate: null,
  sentOPDate: null,
  courseGroupsAvailable: null,
  handleChangeAttendanceStatus: null,
  paymentsLastUptdate: null,
};

const CustomFilter:React.FC<CustomFilterProps> = ({
  typeFilter,
  operatingPlanStatus,
  approvalDocument,
  setDataSource,
  defaultDataSource,
  programOperatingPlanId,
  handleChangeProgramOpPlanId,
  studyPrograms,
  year,
  handleChangeYear,
  sentLate,
  sentOPDate,
  courseGroupsAvailable,
  handleChangeAttendanceStatus,
  paymentsLastUptdate,
}) => {
  const { t } = useTranslation();
  const { userType } = useSelector((state: any) => state.auth);
  const navigate = useNavigate();

  const [activeFilters, setActiveFilters] = useState<any>({});
  useDidMountEffect(() => {
    setDataSource(applyFilters(defaultDataSource, activeFilters));
  }, [activeFilters, defaultDataSource]);

  return (
    <Row gutter={[24, 24]} className="custom-filter">
      {
        customFilterComponents(
          t,
          operatingPlanStatus,
          userType,
          approvalDocument,
          activeFilters,
          setActiveFilters,
          defaultDataSource,
          programOperatingPlanId,
          handleChangeProgramOpPlanId,
          studyPrograms,
          year,
          handleChangeYear,
          navigate,
          sentLate,
          sentOPDate,
          courseGroupsAvailable,
          handleChangeAttendanceStatus,
          paymentsLastUptdate,
        )[typeFilter]
          .map(({
            key,
            label,
            component,
            flex,
            style,
          }) => (
            <Col key={key} flex={flex} style={style}>
              <Row gutter={[0, 4]}>
                <Col span={24}>
                  <span className="custom-filter__label">
                    {label}
                  </span>
                </Col>
                <Col span={24}>
                  {component}
                </Col>
              </Row>
            </Col>
          ))
}
    </Row>
  );
};

CustomFilter.defaultProps = defaultProps;
export default CustomFilter;
