import {
  Alert, Button, Input, Select, Tag,
} from 'antd';
import React, { ReactNode } from 'react';
import {
  FilePdfOutlined, SearchOutlined, UserAddOutlined, ClockCircleOutlined,
} from '@ant-design/icons';
import moment from 'moment';
import {
  APPROVED, IN_PROGRESS, OBSERVED, RECEIVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR,
} from '../../utils/constants';
import './styles.scss';
import { generateUrlDownload } from '../../utils/tools';
import {
  STUDY_PLAN_REQUEST_APPROVED,
  STUDY_PLAN_REQUEST_OBSERVED,
  STUDY_PLAN_REQUEST_REJECTED,
  STUDY_PLAN_REQUEST_SEND,
  STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR,
  STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS,
} from '../../views/StudyPlans/StudyPlansProgram/StudyPlanRequestModal/constants';
import routesDictionary from '../../routes/routesDictionary';

const { Option } = Select;

interface filter {
  key: string,
  label: string,
  component: ReactNode,
  flex: string,
  style?: any,
}

export const typeStatus = (
  userType: string,
  sentLate: boolean = false,
) :{ [index: string]: string} => ({
  'En curso': 'En curso',
  Recibido: userType === UNIT_DIRECTOR ? `${sentLate ? 'Enviado con retraso' : 'Enviado'}` : 'Recibido',
  Aprobado: 'Aprobado',
  Observado: 'Observado',
  Enviado: userType === UNIT_DIRECTOR ? `${sentLate ? 'Enviado con retraso' : 'Enviado'}` : 'Recibido',
  [STUDY_PLAN_REQUEST_SEND]: userType === UNIT_DIRECTOR ? 'Enviado' : 'Recibido',
  [STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR]: 'En Revision: Subdireccion Academica',
  [STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS]: 'En Revision: Consejo Directivo',
  [STUDY_PLAN_REQUEST_OBSERVED]: 'Observado',
  [STUDY_PLAN_REQUEST_APPROVED]: 'Aprobado',
  [STUDY_PLAN_REQUEST_REJECTED]: 'Rechazado',
});

export const alertStatus:{ [index: string]: 'success' | 'info' | 'warning' | 'error'} = {
  [APPROVED]: 'success',
  [IN_PROGRESS]: 'info',
  [RECEIVED]: 'info',
  [OBSERVED]: 'warning',
  [STUDY_PLAN_REQUEST_SEND]: 'info',
  [STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR]: 'info',
  [STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS]: 'info',
  [STUDY_PLAN_REQUEST_OBSERVED]: 'warning',
  [STUDY_PLAN_REQUEST_APPROVED]: 'success',
  [STUDY_PLAN_REQUEST_REJECTED]: 'error',
};

const customFilterComponents = (
  translate: any,
  operatingPlanStatus: string,
  userType: string,
  approvalDocument: any,
  activeFilters: any,
  setActiveFilters: any,
  defaultDataSource: any,
  programOperatingPlanId: any,
  handleChangeProgramOpPlanId: any,
  studyPrograms: any,
  year: any,
  handleChangeYear: any,
  navigate: any,
  sentLate: any,
  sentOPDate: any,
  courseGroupsAvailable: any,
  handleChangeAttendanceStatus: any,
  paymentsLastUptdate: any,
) :{[index: string] : Array<filter>} => ({
  operatingPlanFilters: [
    {
      key: 'code',
      label: translate('code'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, code: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'program',
      label: translate('program'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, name: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'typeProgram',
      label: translate('programType'),
      component: (
        <Select
          className="select-component"
          defaultValue=""
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, type: e });
          }}
        >
          <Option value="">Todos</Option>
          <Option value="Maestria">Maestria</Option>
          <Option value="Doctorado">Doctorado</Option>
        </Select>
      ),
      flex: '250px',
    },

    ...(operatingPlanStatus === APPROVED
      ? [
        {
          key: 'approvalDocument',
          label: translate('approvalRD'),
          component: (
            <Button
              type="primary"
              target="_blank"
              style={{ width: '100%' }}
              href={generateUrlDownload(approvalDocument?.approvalRdUrl)}
              icon={<FilePdfOutlined />}
            >
              {approvalDocument?.approvalRdName}
            </Button>
          ),
          flex: '250px',
          style: {
            marginLeft: 'auto',
          },
        },
      ]
      : []
    ),
    {
      key: 'status',
      label: translate('status'),
      component: (
        <Alert
          message={translate(typeStatus(userType, sentLate)[operatingPlanStatus])}
          type={alertStatus[operatingPlanStatus]}
          showIcon
        />
      ),
      flex: '250px',
      style: {
        ...operatingPlanStatus !== APPROVED && { marginLeft: 'auto' },
      },
    },
    ...(operatingPlanStatus !== IN_PROGRESS
      ? [
        {
          key: 'dateOpSent',
          label: translate('dateOpSent'),
          component: (
            <Input
              disabled
              value={moment(sentOPDate).format('YYYY-MM-DD')}
            />
          ),
          flex: '250px',
        },
      ]
      : []
    ),
  ],
  professorListApproveOperatingPlan: [
    {
      key: 'FirstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'professorType',
      label: translate('professorType'),
      component: (
        <Select
          className="select-component"
          defaultValue=""
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, professorType: e });
          }}
        >
          <Option value="">Todos</Option>
          <Option value="De la universidad">De la universidad</Option>
          <Option value="Invitado">Invitado</Option>
        </Select>
      ),
      flex: '250px',
    },
    {
      key: 'createProfessor',
      label: translate('ㅤ'),
      component: (
        <Button
          type="primary"
          icon={<UserAddOutlined />}
          style={{ width: '100%' }}
          onClick={() => {
            navigate(
              routesDictionary
                .createProfessor.func(
                  programOperatingPlanId,
                ),
            );
          }}
          hidden={userType === SCHOOL_DIRECTOR || !programOperatingPlanId}
        >
          {translate('newProfessor')}
        </Button>
      ),
      flex: '250px',
      style: { marginLeft: 'auto' },
    },
  ],
  professorListFilters: [
    {
      key: 'FirstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'numberDocument',
      label: translate('documentNumber'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, numberDocument: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'email',
      label: translate('email'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, email: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
  ],
  subjectListFilters: [
    {
      key: 'code',
      label: translate('code'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, code: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'subjectName',
      label: translate('subject'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, subjectName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
  ],
  subjectListApprovePlan: [
    {
      key: 'subjectName',
      label: translate('subject'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, subjectName: e.target.value });
          }}
        />
      ),
      flex: '400px',
    },
  ],
  entrantsListFilters: [
    {
      key: 'FirstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'documentNumber',
      label: translate('documentNumber'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, documentNumber: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'email',
      label: translate('email'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, email: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
  ],
  studentListFilters: [
    {
      key: 'firstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'email',
      label: translate('email'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, email: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
  ],
  attendanceStudentListFilters: [
    {
      key: 'firstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'email',
      label: translate('email'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, email: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    ...(defaultDataSource?.[0]?.actions?.isEditable
      ? [
        {
          key: 'openAttendanceStudents',
          label: translate('ㅤ'),
          component: (
            <Button
              type="primary"
              // icon={<PlusCircleOutlined />}
              style={{ width: '100%' }}
              onClick={handleChangeAttendanceStatus}
            >
              {translate('allowAttendance')}
            </Button>
          ),
          flex: '250px',
          style: { marginLeft: 'auto' },
        },
      ]
      : []
    ),
  ],
  subjectStudentListFilters: [
    {
      key: 'firstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'email',
      label: translate('email'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, email: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'groupCourseStudent',
      label: translate('group'),
      component: (
        <Select
          className="select-component"
          defaultValue=""
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, groupCourseStudent: e });
          }}
        >
          <Option value="">{translate('all')}</Option>
          {courseGroupsAvailable?.map(({ label }: any) => (
            <Option value={label}>{label}</Option>
          ))}
        </Select>
      ),
      flex: '250px',
    },
  ],
  userList: [
    {
      key: 'FirstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'email',
      label: translate('email'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, email: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
  ],
  paymentStudentsFilters: [
    {
      key: 'cui',
      label: translate('cui'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, cui: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'firstName',
      label: translate('firstName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
          )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, firstName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    {
      key: 'lastName',
      label: translate('lastName'),
      component: (
        <Input
          suffix={(
            <SearchOutlined />
              )}
          onChange={(e) => {
            setActiveFilters({ ...activeFilters, lastName: e.target.value });
          }}
        />
      ),
      flex: '250px',
    },
    ...(paymentsLastUptdate
      ? [
        {
          key: 'status',
          label: translate('lastUpdate'),
          component: (
            <Tag
              icon={<ClockCircleOutlined />}
              color="default"
              style={{ width: '100%', lineHeight: '30px', borderRadius: 8 }}
            >
              {paymentsLastUptdate}
            </Tag>
          ),
          flex: '200px',
          style: { marginLeft: 'auto' },
        },
      ]
      : []
    ),
  ],
});

export default customFilterComponents;
