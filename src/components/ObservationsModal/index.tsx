import {
  Modal, Checkbox, Row, Col, Divider,
} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation, useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import CustomLoader from '../CustomLoader';

interface ObservationsModalProps {
  observationsModal: any,
  setObservationsModal: any
}

const getAllOperatingPlanObservationsRequest = loader('./getAllOperatingPlanObservations.gql');
const editObservationRequest = loader('./editObservation.gql');

const ObservationsModal:React.FC<ObservationsModalProps> = ({
  observationsModal,
  setObservationsModal,
}) => {
  const { t } = useTranslation();
  const [observations, setObservations] = useState<any>([]);

  const [editObservationMutation] = useMutation(editObservationRequest);

  const { data, loading } = useQuery(
    getAllOperatingPlanObservationsRequest,
    {
      variables: { programOpPlanId: observationsModal.programOperatingPlanId },
      fetchPolicy: 'no-cache',
      skip: !observationsModal.programOperatingPlanId,
    },
  );

  useEffect(() => {
    if (data) {
      const { getAllOperatingPlanObservations } = data;
      setObservations(getAllOperatingPlanObservations);
    }
  }, [data]);

  const handleCancel = () => {
    setObservationsModal({ ...observationsModal, isVisible: false });
  };
  if (loading) return <CustomLoader />;

  return (
    <Modal
      title={t('observationModalTittle')}
      centered
      width={600}
      closable
      open={observationsModal.isVisible}
      onCancel={handleCancel}
      bodyStyle={{ overflowY: 'scroll', maxHeight: 600 }}
      footer={null}
    >
      {observationsModal.programOperatingPlanName && (
        <Row style={{ marginBottom: '10px' }}>
          <b>{observationsModal.programOperatingPlanName}</b>
        </Row>
      )}
      {observationsModal.readOnly ? (
        <div>
          {t('observationsSentModalText')}
        </div>
      ) : (
        <div>
          {t('observationsModalText')}
        </div>
      )}

      <Divider />
      {observations.map((observation: any) => (
        <div key={observation.id}>
          <Row align="middle">
            <Col span={8}>
              <Checkbox
                defaultChecked={observation.isApproved}
                disabled={observationsModal.readOnly}
                onChange={(e) => {
                  editObservationMutation({
                    variables: {
                      input: {
                        isApproved: e.target.checked,
                        category: observation.category.id,
                        operatingPlanProgram: observationsModal.programOperatingPlanId,
                        observationId: observation.id,
                      },
                    },
                  }).then(({ data: editObservationData }) => {
                    const { editObservation } = editObservationData;
                    setObservations(observations.map((item: any) => (
                      item.id === editObservation.observation.id
                        ? editObservation.observation : item)));
                  });
                }}
              >
                {observation.category.name}
              </Checkbox>
            </Col>
            <Col span={16}>
              <TextArea
                {...!observationsModal.readOnly && { placeholder: t('observationsPlaceholder') }}
                disabled={observationsModal.readOnly}
                defaultValue={observation.comment}
                onBlur={(e) => {
                  editObservationMutation({
                    variables: {
                      input: {
                        category: observation.category.id,
                        operatingPlanProgram: observationsModal.programOperatingPlanId,
                        observationId: observation.id,
                        comment: e.target.value,
                      },
                    },
                  }).then(({ data: editObservationData }) => {
                    const { editObservation } = editObservationData;
                    setObservations(observations.map((item: any) => (item.id
                      === editObservation.observation.id
                      ? editObservation.observation : item)));
                  });
                }}
              />
            </Col>
          </Row>
          <Divider />
        </div>
      ))}
    </Modal>
  );
};

export default ObservationsModal;
