import React, { ReactNode } from 'react';
import { Col, Row } from 'antd';
import { RiArrowLeftSLine } from 'react-icons/ri';
import './styles.scss';
import { Animate } from 'react-simple-animate';

interface TitleViewProps {
  title: string,
  viewBackButton: boolean,
  action?: ReactNode,
  onClickBackButton?: any,
  sizeAction?: number,
  sequenceIndex?: number,
  style?: any
}

const defaultProps: {
  action?: ReactNode
  onClickBackButton?: any,
  sizeAction?: number,
  sequenceIndex?: number
  style?: any,
} = {
  action: null,
  onClickBackButton: null,
  sizeAction: 250,
  sequenceIndex: 0,
  style: null,
};

const TitleView:React.FC<TitleViewProps> = ({
  title, action, viewBackButton, onClickBackButton, sizeAction, sequenceIndex, style,
}) => (
  <Row gutter={[0, 24]} className="title-view" align="middle" wrap={false} {...style && { style }}>
    <Col flex="auto">
      <Row align="middle" wrap={false}>
        {viewBackButton
          && (
          <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }} sequenceIndex={sequenceIndex}>
            <RiArrowLeftSLine
              onClick={onClickBackButton}
              style={{
                color: '#081d40',
                fontSize: 45,
                cursor: 'pointer',
                marginRight: 10,
                marginTop: 8,
                minWidth: 40,
              }}
            />
          </Animate>
          )}
        <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }} sequenceIndex={sequenceIndex}>
          <span className="title-view__headline">
            {title}
          </span>
        </Animate>
      </Row>
    </Col>
    {action && (
      <Col flex={`${sizeAction}px`} style={{ marginLeft: 12 }}>
        <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }} sequenceIndex={sequenceIndex}>
          {action}
        </Animate>
      </Col>
    )}
  </Row>
);

TitleView.defaultProps = defaultProps;

export default TitleView;
