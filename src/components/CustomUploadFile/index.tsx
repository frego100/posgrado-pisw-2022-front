import React, { useRef, useState } from 'react';
import {
  Button, Col,
  Input, InputRef, Row, Upload,
} from 'antd';
import './styles.scss';
import { useTranslation } from 'react-i18next';

interface CustomUploadFileProps {
  disabled: boolean,
  form: any
  nameComponent: string
  uploadFunction?: any,
  typeFile?: any,
  urlFile?: string | null,
}

const defaultProps: {
  uploadFunction?: any,
  typeFile?: any,
  urlFile?: any,
} = {
  uploadFunction: null,
  typeFile: '.pdf',
  urlFile: null,
};

const downloadFile = (urlFile: string) => {
  const link = document.createElement('a');
  link.href = urlFile;
  link.target = '_blank';
  document.body.appendChild(link);
  link.click();
  link.parentNode.removeChild(link);
};

const CustomUploadFile:React.FC<CustomUploadFileProps> = ({
  disabled, form, nameComponent, uploadFunction, typeFile, urlFile,
}) => {
  const inputRef = useRef<InputRef>(null);
  const [url, setUrl] = useState(urlFile);
  const { t } = useTranslation();

  useState(() => {
    if (urlFile) {
      setUrl(urlFile);
    }
    // @ts-ignore
  }, [urlFile]);

  const handleUploadFile = (info: any) => {
    if (info.file.percent === 100) {
      setUrl(URL.createObjectURL(info.file.originFileObj));
      form.setFieldsValue({ [nameComponent]: info.file });
      if (uploadFunction) {
        uploadFunction();
      }
    }
  };
  return (
    <Row className="upload custom-upload-file">
      <Col span={16}>
        <Input
          ref={inputRef}
          disabled={disabled}
          value={typeof form.getFieldValue(nameComponent) === 'object'
            ? form.getFieldValue(nameComponent)?.name
            : form.getFieldValue(nameComponent)}
          onClick={() => {
            inputRef.current!.blur();
            if (url) {
              downloadFile(url);
            }
          }}
        />
      </Col>
      <Col span={8}>
        <Upload
          accept={typeFile}
          showUploadList={false}
          onChange={handleUploadFile}
          disabled={disabled}
          style={{ width: '100%' }}
        >
          <Button
            type="primary"
            disabled={disabled}
          >
            {t('upload')}
          </Button>
        </Upload>
      </Col>
    </Row>
  );
};

CustomUploadFile.defaultProps = defaultProps;

export default CustomUploadFile;
