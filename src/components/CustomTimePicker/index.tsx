import React, { useEffect, useState } from 'react';
import {
  TimePicker, Row, Col, Form,
} from 'antd';
import moment from 'moment';
import './styles.scss';

interface CustomTimePickerProps {
  form: any,
  name: any,
  nameElement: any
}

const CustomTimePicker:React.FC<CustomTimePickerProps> = ({ form, name, nameElement }) => {
  const [selectedTime, setSelectedTime] = useState(form.getFieldValue('schedules')[name]?.[nameElement]);
  const schedules = Form.useWatch('schedules', form);

  useEffect(() => {
    if (form.getFieldValue('schedules')[name]?.[nameElement]) {
      setSelectedTime(form.getFieldValue('schedules')[name]?.[nameElement]);
    }
  }, [form.getFieldValue('schedules')[name]?.[nameElement]]);

  return (
    <Row>
      <Col span={24}>
        <TimePicker
          className="component"
          popupClassName="timepicker"
          allowClear={false}
          use12Hours
          format="h:mm a"
          value={selectedTime ?? null}
          onSelect={(value) => {
            const timeString = moment(value);
            setSelectedTime(timeString);
            form.setFieldsValue({
              schedules: schedules.map((item, index) => {
                if (index === name) {
                  return { ...item, [nameElement]: timeString };
                }
                return item;
              }),
            });
          }}
        />
      </Col>
    </Row>
  );
};

export default CustomTimePicker;
