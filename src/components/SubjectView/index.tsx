import React, { useEffect, useState } from 'react';
import {
  Button, Card,
  Col, Empty, Form, message, Row,
} from 'antd';
import { useMutation } from '@apollo/client';
import { useForm } from 'antd/lib/form/Form';
import { PlusCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import FormCard from '../FormCard';
import subjectViewItems, { availableGroups, subjectFormItems } from './constants';

import ScheduleModal from './ScheduleModal';
import './styles.scss';
import routesDictionary from '../../routes/routesDictionary';

interface SubjectViewProps {
  readonly: boolean,
  subjectData: any,
  refetch: any,
  backSubjectListView?: boolean,
}

const defaultProps: {
  backSubjectListView?: boolean
} = {
  backSubjectListView: false,
};

const addGroupRequest = loader('./requests/addGroup.gql');
const deleteGroupRequest = loader('./requests/deleteGroup.gql');
const editCourseRequest = loader('./requests/editCourse.gql');
const editGroupRequest = loader('./requests/editGroup.gql');

const SubjectView: React.FC<SubjectViewProps> = ({
  readonly, subjectData, refetch, backSubjectListView,
}) => {
  const [form] = useForm();
  const navigate = useNavigate();
  const params = useParams();

  const { subjectId, operatingPlanId, programOperatingPlanId } = params;
  const { t } = useTranslation();

  const [disabledComponents, setDisabledComponents] = useState(false);
  const [scheduleModalVisible, setScheduleModalVisible] = useState<boolean>(false);
  const [loadingCreateGroups, setLoadingCreateGroups] = useState(false);

  const [groups, setGroups] = useState<any>([]);
  const [courseGroupId, setCourseGroupId] = useState();
  const [professors, setProfessors] = useState<any>([]);

  const [filteredGroups, setFilteredGroups] = useState<any>(availableGroups);
  const groupSelected = Form.useWatch('groups', form);

  useEffect(() => {
    setFilteredGroups(availableGroups.filter((group) => form.getFieldValue('groups')?.every((item) => item?.group !== group)));
  }, [groupSelected]);

  useEffect(() => {
    const { studyPlanSubject, coursegroupSet } = subjectData;
    const courseGroupsList = coursegroupSet.map((courseGroup: any) => (
      {
        ...courseGroup,
        professor: readonly ? (courseGroup?.professor ? `${courseGroup.professor?.professor?.user?.firstName} ${courseGroup.professor?.professor?.user?.lastName}` : '-') : courseGroup?.professor?.id,
        ...(courseGroup.startDate && courseGroup.endDate)
        && {
          dates: readonly ? ((courseGroup.startDate && courseGroup.endDate)
            ? `${moment(courseGroup.startDate).format('DD/MM/YYYY')
            } - ${moment(courseGroup.endDate).format('DD/MM/YYYY')}`
            : '-') : [moment(courseGroup.startDate), moment(courseGroup.endDate)],
        },
      }
    ));

    form.setFieldsValue({
      studyProgram: studyPlanSubject.studyPlan.studyProgram.name,
      code: subjectData.id,
      hours: studyPlanSubject.hours,
      syllabus: subjectData.syllabus,
      subjectName: studyPlanSubject.subject,
      credits: studyPlanSubject.credits,
      semester: studyPlanSubject.semester,
    });
    form.setFieldsValue({ groups: courseGroupsList });
    setGroups(courseGroupsList);

    const professorsArray = [];
    subjectData.operatingPlanProgram
      .professorregistrationSet.forEach((professor: any) => {
        if (!professor.isDeleted) {
          professorsArray.push({
            id: professor.id,
            name: `${professor.professor.user.firstName} ${professor.professor.user.lastName}`,
          });
        }
      });
    setProfessors(professorsArray);
  }, [form, subjectData]);

  const [editSubjectMutation, { loading: loadingEditSubject }] = useMutation(editCourseRequest);
  const [addGroupMutation, { loading: loadingAddGroup }] = useMutation(addGroupRequest);
  const [editGroupMutation] = useMutation(editGroupRequest);
  const [deleteGroupMutation] = useMutation(deleteGroupRequest);

  const handleSaveGroups = () => {
    form.validateFields()
      .then((values) => {
        setLoadingCreateGroups(true);
        values.groups?.map((group: any) => (
          editGroupMutation({
            variables: {
              input: {
                courseGroupId: group.id,
                professor: group.professor,
                group: group.group,
                capacity: group.capacity,
                classroom: group.classroom,
                startDate: moment(group.dates[0]).format('YYYY-MM-DD'),
                endDate: moment(group.dates[1]).format('YYYY-MM-DD'),
              },
            },
          })
        ));
        setLoadingCreateGroups(false);
        message.success('Grupos registrados!');
        navigate(
          backSubjectListView
            ? routesDictionary.subjects.route
            : routesDictionary.operatingPlanProgram.func(operatingPlanId, programOperatingPlanId),
          { state: { displaySubjects: true } },
        );
      });
  };
  return (
    <Form
      name="basic"
      form={form}
      labelCol={{ span: 24 }}
      wrapperCol={{ span: 24 }}
      initialValues={{ remember: true }}
      autoComplete="off"
    >
      <Row gutter={[16, 16]}>
        {subjectViewItems(
          disabledComponents,
          setDisabledComponents,
          form,
          editSubjectMutation,
          loadingEditSubject,
          operatingPlanId,
          refetch,
          subjectId,
          t,
          readonly,
          subjectData,
        ).map((itemObject, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Col xl={12} lg={24} md={24} sm={24} xs={24} className="col" key={index}>
            {Object.entries(itemObject).map(([key, value]) => (
              <FormCard title={key} itemList={value} key={key} readonly />
            ))}
          </Col>
        ))}
        <Col xl={12} lg={24} md={24} sm={24} xs={24} className="col">
          <Card className="subject-view-card">
            <Row justify="start">
              <h1 className="title">
                {t('groups')}
              </h1>
              <div className="ml-auto">
                {!readonly
                  && (
                    <Button
                      type="primary"
                      disabled={disabledComponents}
                      loading={loadingAddGroup}
                      icon={<PlusCircleOutlined />}
                      onClick={
                        () => {
                          addGroupMutation({
                            variables: {
                              input: {
                                course: subjectId,
                              },
                            },
                          }).then(({ data: addGroupData }: any) => {
                            const { addGroup } = addGroupData;
                            if (addGroup.feedback.status === 'SUCCESS') {
                              form.setFieldsValue({ groups: [...form.getFieldValue('groups'), addGroup.group] });
                              setGroups([...groups, addGroup.group]);
                            }
                          });
                        }
                      }
                    >
                      {t('addGroup')}
                    </Button>
                  )}

              </div>
            </Row>
            <Form.List name="groups">
              {(fields, { remove }) => (
                <div>
                  <div className="groups-container">
                    {fields.length > 0
                      ? fields.map(({ key, name, ...restField }) => (
                        <FormCard
                          key={key}
                          withoutTitle
                          readonly={readonly}
                          itemList={
                            subjectFormItems(
                              name,
                              // @ts-ignore
                              groups[key]?.id,
                              setScheduleModalVisible,
                              setCourseGroupId,
                              t,
                              readonly,
                              subjectData,
                              key,
                              professors,
                              filteredGroups,
                            )
                          }
                          onClick={() => {
                            const scheduleField = form.getFieldValue('groups')[name];
                            deleteGroupMutation({
                              variables: {
                                groupId: scheduleField.id,
                              },
                            }).then(({ data: deleteGroupData }: any) => {
                              const { deleteGroup } = deleteGroupData;
                              if (deleteGroup.feedback.status === 'SUCCESS') {
                                remove(name);
                              }
                            });
                          }}
                          {...restField}
                        />
                      ))
                      : (
                        <Row justify="center" align="middle" style={{ height: '100%' }}>
                          <Empty
                            image={Empty.PRESENTED_IMAGE_SIMPLE}
                            description={t('groupsEmpty')}
                          />
                        </Row>
                      )}
                  </div>
                </div>
              )}
            </Form.List>

          </Card>
        </Col>
      </Row>
      <Row justify="center">
        <Form.Item>
          {!readonly
            && (
              <Button
                type="primary"
                style={{ width: 250, height: 30 }}
                onClick={handleSaveGroups}
                loading={loadingCreateGroups}
              >
                {t('saveChanges')}
              </Button>
            )}
        </Form.Item>
      </Row>
      <ScheduleModal
        readonly={readonly}
        isVisible={scheduleModalVisible}
        setIsVisible={setScheduleModalVisible}
        courseGroupId={courseGroupId}
        subjectName={form.getFieldValue('subjectName')}
      />
    </Form>
  );
};

SubjectView.defaultProps = defaultProps;
export default SubjectView;
