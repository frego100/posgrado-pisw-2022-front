import React, {
  Dispatch, SetStateAction, useEffect, useState,
} from 'react';
import {
  Button, Empty, Form, message, Modal, Row,
} from 'antd';
import { useMutation, useQuery } from '@apollo/client';
import { PlusCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import CustomLoader from '../../CustomLoader';
import FormCard from '../../FormCard';
import scheduleModalItems, { weekdays } from './constants';
import '../../ProfessorView/DegreesModal/styles.scss';

interface ScheduleModalProps {
  readonly: boolean,
  isVisible: boolean,
  setIsVisible: Dispatch<SetStateAction<boolean>>,
  courseGroupId: string | undefined,
  subjectName: string,
}

const addScheduleRequest = loader('./requests/addSchedule.gql');
const deleteScheduleRequest = loader('./requests/deleteSchedule.gql');
const editScheduleRequest = loader('./requests/editSchedule.gql');
const getSchedulesRequest = loader('./requests/getSchedules.gql');

const ScheduleModal:React.FC<ScheduleModalProps> = ({
  readonly,
  isVisible,
  setIsVisible,
  courseGroupId,
  subjectName,
}) => {
  const [schedulesCount, setSchedulesCount] = useState<number>(1);
  const [form] = useForm();
  const { t } = useTranslation();

  const [addScheduleMutation, { loading: loadingAddSchedule }] = useMutation(addScheduleRequest);
  const [editScheduleMutation] = useMutation(editScheduleRequest);
  const [deleteScheduleMutation] = useMutation(deleteScheduleRequest);

  const { data, loading, refetch } = useQuery(
    getSchedulesRequest,
    {
      variables: { courseGroupId },
      fetchPolicy: 'no-cache',
      skip: !isVisible,
    },
  );

  useEffect(() => {
    if (data) {
      const { getSchedules } = data;
      setSchedulesCount(getSchedules.length);
      const schedulesDataArray = getSchedules.map((schedule: any) => ({
        scheduleId: schedule.id,
        day: schedule.day,
        startTime: readonly ? moment(schedule.startTime, 'h:mm a').format('h:mm a') : moment(schedule.startTime, 'h:mm a'),
        endTime: readonly ? moment(schedule.endTime, 'h:mm a').format('h:mm a') : moment(schedule.endTime, 'h:mm a'),
      }));
      form.setFieldsValue({ schedules: schedulesDataArray });
    }
  }, [data, form]);

  const handleAddSchedule = () => {
    form.validateFields().then((values) => {
      const schedulesArray = values?.schedules.map((schedule: any) => ({
        ...schedule,
        day: weekdays[schedule.day],
        startTime: moment(schedule.startTime).format('HH:mm:ss'),
        endTime: moment(schedule.endTime).format('HH:mm:ss'),
        ...!schedule.scheduleId && { course: courseGroupId },
      }));

      const newSchedules = schedulesArray.filter((schedule: any) => !schedule.scheduleId);
      const editSchedules = schedulesArray.filter((schedule: any) => schedule.scheduleId);

      if (newSchedules.length > 0) {
        addScheduleMutation({
          variables: {
            input: newSchedules,
          },
        }).then(({ data: addScheduleData }: any) => {
          const { addSchedule } = addScheduleData;
          if (addSchedule.feedback.status === 'SUCCESS') {
            setIsVisible(false);
            message.success(addSchedule.feedback.message);
          } else {
            message.error(addSchedule.feedback.message);
            message.warning(addSchedule.errors?.[0].messages);
          }
        });
      }

      if (editSchedules.length > 0) {
        editScheduleMutation({
          variables: {
            input: editSchedules,
          },
        }).then(({ data: editScheduleData }: any) => {
          const { editSchedule } = editScheduleData;
          if (editSchedule.feedback.status === 'SUCCESS') {
            setIsVisible(false);
            message.success(editSchedule.feedback.message);
          } else {
            message.error(editSchedule.feedback.message);
            message.warning(editSchedule.errors?.[0].messages);
          }
        });
      }
      // setIsVisible(false);
      // message.success('Horarios registrados!');
    });
  };

  return (
    <Modal
      title={t('schedules')}
      open={isVisible}
      {...readonly && { footer: null }}
      okText={t('saveSchedules')}
      cancelText={t('cancel')}
      onOk={handleAddSchedule}
      onCancel={() => setIsVisible(false)}
      width={800}
      okButtonProps={{ loading: loadingAddSchedule, disabled: schedulesCount === 0 }}
    >
      {loading
        ? <CustomLoader />
        : (
          <Form
            name="basic"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            autoComplete="off"
            form={form}
          >
            <Form.List name="schedules">
              {(fields, { add, remove }) => {
                setSchedulesCount(fields.length);
                return (
                  <>
                    <Row align="middle">
                      <p className="subtitle">
                        {subjectName}
                      </p>
                      <div className="ml-auto">
                        { !readonly
                        && (
                        <Button
                          type="primary"
                          onClick={() => add()}
                          icon={<PlusCircleOutlined />}
                        >
                          {t('addSchedule')}
                        </Button>
                        )}

                      </div>
                    </Row>
                    {fields.length > 0
                      ? (
                        <div className="schedulesModalContent">
                          {fields.map(({ key, name, ...restField }) => (
                            <FormCard
                              key={key}
                              withoutTitle
                              readonly={readonly}
                              itemList={scheduleModalItems(name, t, readonly, form)}
                              onClick={() => {
                                const scheduleField = form.getFieldValue('schedules')[name];

                                if (scheduleField && scheduleField.scheduleId) {
                                  deleteScheduleMutation({
                                    variables: {
                                      scheduleId: scheduleField.scheduleId,
                                    },
                                  }).then(({ data: deleteScheduleData }: any) => {
                                    const { deleteSchedule } = deleteScheduleData;
                                    if (deleteSchedule.feedback.status === 'SUCCESS') {
                                      refetch();
                                    }
                                  });
                                } else {
                                  remove(name);
                                }
                              }}
                              {...restField}
                            />
                          ))}
                        </div>
                      )
                      : (
                        <Row justify="center">
                          <Empty
                            image={Empty.PRESENTED_IMAGE_SIMPLE}
                            description={t('schedulesEmpty')}
                          />
                        </Row>
                      ) }
                  </>
                );
              }}
            </Form.List>
          </Form>
        )}
    </Modal>
  );
};

export default ScheduleModal;
