import React from 'react';
import { Input, Select } from 'antd';
import './styles.scss';
import CustomTimePicker from '../../CustomTimePicker';

const { Option } = Select;

export const weekdays:{[index: string] : string} = {
  Lunes: 'MONDAY',
  Martes: 'TUESDAY',
  Miercoles: 'WEDNESDAY',
  Jueves: 'THURSDAY',
  Viernes: 'FRIDAY',
  Sabado: 'SATURDAY',
  Domingo: 'SUNDAY',
};

const scheduleModalItems = (name: any, t: any, readonly:any, form: any) => [
  {
    name: [name, 'day'],
    component: (
      readonly
        ? (
          <Input
            disabled
          />
        )
        : (
          <Select className="component">
            {/* eslint-disable-next-line no-shadow,no-unused-vars */}
            {Object.entries(weekdays).map(([key, value]) => (
              <Option value={key} key={key}>{key}</Option>
            ))}
          </Select>
        )
    ),

    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('day'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
  },
  {
    name: [name, 'startTime'],
    component: (
      readonly
        ? (
          <Input
            disabled
          />
        )
        : (
          <CustomTimePicker
            form={form}
            name={name}
            nameElement="startTime"
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('startTime'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
  },
  {
    name: [name, 'endTime'],
    component: (
      readonly
        ? (
          <Input
            disabled
          />
        )
        : (
          <CustomTimePicker
            form={form}
            name={name}
            nameElement="endTime"
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('endTime'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
  },
];

export default scheduleModalItems;
