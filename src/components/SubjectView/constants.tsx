import {
  Button, DatePicker, Input, InputNumber, message, Select,
} from 'antd';
import React from 'react';
import moment from 'moment';
import { FilePdfOutlined } from '@ant-design/icons';
import CustomUploadFile from '../CustomUploadFile';
import '../ProfessorView/DegreesModal/styles.scss';
import { generateUrlDownload } from '../../utils/tools';

export const availableGroups = ['A', 'B', 'C', 'D', 'E'];

const { Option } = Select;
const { RangePicker } = DatePicker;
const subjectViewItems = (
  disabledComponents: boolean,
  setDisabledComponents: any,
  form: any,
  editSubjectMutation: any,
  loadingEditSubject: any,
  operatingPlanProgram: any,
  refetch: any,
  subjectId: any,
  t: any,
  readonly: boolean,
  subject: any,
) => [
  {
    [t('subject')]: [
      {
        name: 'studyProgram',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('program'),
        responsive: {
          xs: { span: 24 },
          md: { span: 24 },
        },
        display: true,
      },
      {
        name: 'subjectName',
        component: (
          <Input
            disabled
            value={subject?.studyPlanSubject?.subject}
          />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('subject'),
        responsive: {
          xs: { span: 24 },
          md: { span: 24 },
        },
        display: true,
      },
      {
        name: 'code',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('code'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'hours',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('hours'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },

      {
        name: 'credits',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('credits'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'semester',
        component: (
          <Input disabled />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('semester'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'syllabus',
        component: (
          readonly
            ? (subject.syllabus
              ? (
                <Button
                  type="primary"
                  target="_blank"
                  style={{ width: '100%' }}
                  href={generateUrlDownload(subject.syllabusUrl)}
                  icon={<FilePdfOutlined />}
                >
                  {t('download')}
                </Button>
              )
              : (
                <span>
                  {t('missingSyllabus')}
                </span>
              ))
            : (
              <CustomUploadFile
                disabled={false}
                form={form}
                nameComponent="syllabus"
                urlFile={subject.syllabusUrl && generateUrlDownload(subject.syllabusUrl)}
                uploadFunction={() => {
                  if (form.getFieldValue('syllabus')?.originFileObj) {
                    editSubjectMutation({
                      variables: {
                        input: {
                          courseId: subjectId,
                          syllabus: form.getFieldValue('syllabus').originFileObj,
                        },
                      },
                    }).then(({ data }: any) => {
                      const { editCourse } = data;
                      if (editCourse.feedback.status === 'SUCCESS') {
                        message.success('¡Se ha registrado el silabo correctamente!');
                      }
                    });
                  } else {
                    form.setFieldValue([{ name: 'syllabus', errors: ['Campo obligatorio'] }]);
                  }
                }}
              />
            )
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('syllabus'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
    ],
  },
];

export const subjectFormItems = (
  name: any,
  courseGroupId: string,
  setScheduleModalVisible: any,
  setCourseGroupId: any,
  t: any,
  readonly: boolean,
  subject: any,
  key:number,
  professors: any,
  filteredGroups: any,
) => [
  {
    name: [name, 'professor'],
    component: (
      readonly
        ? (
          <Input disabled />
        )
        : (
          <Select className="component">
            {professors?.map((professor: any) => (
              <Option key={professor.id} value={professor.id}>
                {professor.name}
              </Option>
            ))}
          </Select>
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('professor'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: true,
  },
  {
    name: [name, 'dates'],
    component: (
      readonly
        ? (
          <Input
            disabled
            value={
              (subject.coursegroupSet[key].startDate && subject.coursegroupSet[key].endDate)
                ? `${moment(subject?.coursegroupSet[key].startDate).format('DD/MM/YYYY')
                } - ${moment(subject?.coursegroupSet[key].endDate).format('DD/MM/YYYY')}`
                : '-'
            }
          />
        )
        : (
          <RangePicker style={{ width: '100%' }} />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('startEndDate'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: true,
  },
  {
    name: [name, 'capacity'],
    component: (
      readonly
        ? (
          <InputNumber
            style={{ width: '100%' }}
            disabled
            value={subject.coursegroupSet[key].capacity ?? '-'}
          />
        )
        : (
          <InputNumber style={{ width: '100%' }} min={0} controls={false} />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('capacity'),
    responsive: {
      xs: { span: 24 },
      sm: { span: 12 },
      md: { span: 5 },
    },
    display: true,
  },
  {
    name: [name, 'classroom'],
    component: (
      readonly
        ? (
          <Input
            disabled
            value={subject.coursegroupSet[key].classroom ?? '-'}
          />
        )
        : (
          <Input />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('classroom'),
    responsive: {
      xs: { span: 24 },
      sm: { span: 12 },
      md: { span: 5 },
    },
    display: true,
  },
  {
    name: [name, 'group'],
    component: (
      readonly
        ? (
          <Input
            disabled
            value={subject.coursegroupSet[key].group ?? '-'}
          />
        )
        : (
          <Select className="component">
            {filteredGroups.map((item) => <Option key={item} value={item}>{item}</Option>)}
          </Select>
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('group'),
    responsive: {
      xs: { span: 24 },
      sm: { span: 12 },
      md: { span: 5 },
    },
    display: true,
  },
  {
    name: [name, 'endDate'],
    component:
      (
        <Button
          type="primary"
          style={{ width: '100%' }}
          onClick={() => {
            setCourseGroupId(courseGroupId);
            setScheduleModalVisible(true);
          }}
        >
          {
            readonly
              ? t('viewSchedules')
              : t('registerSchedules')
          }
        </Button>
      ),
    rules: [
      {
        required: false,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('schedules'),
    responsive: {
      xs: { span: 24 },
      sm: { span: 12 },
      md: { span: 9 },
    },
    display: true,
  },
];
export default subjectViewItems;
