import React from 'react';
import {
  Button, Form, Space,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import {
  IN_PROGRESS, OBSERVED, RECEIVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR,
} from '../../utils/constants';
import CustomUploadFile from '../CustomUploadFile';

interface ObservationsButtonProps {
  userType: any,
  operatingPlanStatus: string
  confirmationModalData: any
  setConfirmationModalData: any
    isActive: boolean
}
const ObservationsButton:React.FC<ObservationsButtonProps> = ({
  userType,
  operatingPlanStatus,
  confirmationModalData,
  setConfirmationModalData,
  isActive,
}) => {
  const { t } = useTranslation();
  const [form] = useForm();

  if (userType === UNIT_DIRECTOR
    && (operatingPlanStatus === IN_PROGRESS || operatingPlanStatus === OBSERVED) && isActive) {
    return (
      <Button
        type="primary"
        style={{ width: 250 }}
        onClick={() => {
          setConfirmationModalData({
            ...confirmationModalData,
            isVisible: true,
            type: 'sendOperatingPlan',
          });
        }}
      >
        {t('send')}
      </Button>
    );
  }

  if (userType === SCHOOL_DIRECTOR && operatingPlanStatus === RECEIVED) {
    const documentComponent = (
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
        form={form}
      >
        <Form.Item
          label="R.D. de aprobación"
          name="approvalRd"
          rules={[{ required: true, message: '¡Campo obligatorio!' }]}
        >
          <CustomUploadFile
            disabled={false}
            form={form}
            nameComponent="approvalRd"
          />
        </Form.Item>
      </Form>
    );

    return (
      <Space>
        <Button
          type="primary"
          style={{ width: 250 }}
          onClick={() => {
            setConfirmationModalData({
              ...confirmationModalData,
              isVisible: true,
              component: '',
              type: 'observeOperatingPlan',
            });
          }}
        >
          {t('sendObservations')}
        </Button>
        <Button
          type="primary"
          style={{ width: 250 }}
          onClick={() => {
            setConfirmationModalData({
              ...confirmationModalData,
              isVisible: true,
              type: 'approveOperatingPlan',
              component: documentComponent,
              form,
            });
          }}
        >
          {t('approve')}
        </Button>
      </Space>
    );
  }

  return (
    <div />
  );
};

export default ObservationsButton;
