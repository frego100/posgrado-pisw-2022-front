import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import TitleView from '../../components/TitleView';
import { startUpdateBreadcrumb } from '../../store/thunks/ui';
import { AppDispatch } from '../../store';
import './styles.scss';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../utils/constants';
import HomeUnit from '../../components/HomeUnit';
import HomeSchool from '../../components/HomeSchool';

const HomeView = () => {
  const dispatch = useDispatch<AppDispatch>();
  const { userType } = useSelector((state: any) => state.auth);
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('home'),
        ],
      ),
    ).then();
  }, [dispatch]);

  if (userType === UNIT_DIRECTOR) return <HomeUnit />;
  if (userType === SCHOOL_DIRECTOR) return <HomeSchool />;

  return (
    <div>
      <TitleView
        title={t('homeView')}
        viewBackButton={false}
      />
    </div>
  );
};

export default HomeView;
