import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { Tabs } from 'antd';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import CustomTable from '../../../components/CustomTable';
import { academicProgressColumns, studentsColumns } from './constants';
import CustomFilter from '../../../components/CustomFilter';
import CustomCard from '../../../components/CustomCard';
import AcademicProgressModal from '../../Professsor/ProfessorSubjects/SubjectView/AcademicProgressModal';

const getAllCoursesProgressRequest = loader('./requests/getAllCoursesProgress.gql');
const getCourseGroupStudentsRequest = loader('./requests/getCourseGroupStudents.gql');

const SubjectIdView = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const params = useParams();
  const { courseGroupId } = params;
  const VOID_GRADE = '-';
  const location = useLocation();
  // @ts-ignore
  const academicSessionTab = location?.state?.displayAcademicSession;

  // Filters
  const [defaultStudentValues, setDefaultStudentValues] = useState<any>([]);

  // Data table
  const [courseGroupStudents, setCourseGroupStudents] = useState<any>([]);
  const [academicProgress, setAcademicProgress] = useState<any>([]);

  const [courseData, setCourseData] = useState<any>();

  const [academicProgressModal, setAcademicProgressModal] = useState<any>({
    visible: false,
  });

  const handleCancelAcademicProgress = () => {
    setAcademicProgressModal({ visible: false });
  };

  const { data: courseProgressData, loading: courseProgressLoading } = useQuery(
    getAllCoursesProgressRequest,
    { variables: { courseGroupId }, fetchPolicy: 'no-cache' },
  );

  useEffect(() => {
    if (courseProgressData) {
      const { getAllCoursesProgress } = courseProgressData;
      const academicProgressArray: any[] = [];

      getAllCoursesProgress.forEach(({
        id, tittle, description, percentage, academicSession,
      } : any, index: number) => {
        academicProgressArray.push({
          code: index + 1,
          tittle: tittle || '-',
          description: description || '-',
          date: academicSession.date,
          percentage: percentage || '-',
          actions: {
            setAcademicProgressModal,
            courseprogressId: id,
            navigate,
            courseGroupId,
          },
        });
      });
      const today = new Date();
      setAcademicProgress(academicProgressArray.filter((item) => new Date(item.date) < today));
    }
  }, [courseProgressData]);

  const {
    data: courseStudentsData,
    loading: courseStudentsLoading,
  } = useQuery(
    getCourseGroupStudentsRequest,
    {
      variables: { courseGroupId },
      fetchPolicy: 'no-cache',
    },
  );

  useEffect(() => {
    if (courseStudentsData) {
      const { getCourseGroupStudents, getCourseGroup } = courseStudentsData;
      const courseStudentsArray: any[] = [];

      getCourseGroupStudents?.forEach(({
        enrollment, studentgrade, attendances,
      }: any, index: any) => {
        const { student } = enrollment;
        courseStudentsArray.push({
          key: index + 1,
          cui: student.cui,
          firstName: student.user.firstName,
          lastName: `${student.user.lastName} ${student.user.maternalLastName}`,
          email: student.user.email,
          grade: studentgrade?.score ?? VOID_GRADE,
          attendances,
        });
      });
      setCourseGroupStudents(courseStudentsArray);
      setDefaultStudentValues(courseStudentsArray);

      const { course, group } = getCourseGroup;
      const { studyPlanSubject } = course;
      setCourseData({
        name: studyPlanSubject.subject,
        group,
        programName: studyPlanSubject.studyPlan.studyProgram.name,
      });
    }
  }, [courseStudentsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('academicManagement'),
          courseData?.programName,
        ],
      ),
    ).then();
  }, [dispatch, courseData, t]);

  return (
    <div>
      <TitleView
        title={courseData && `${courseData.name} - ${courseData.group}`}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.academicManagement.route);
        }}
      />
      <Tabs
        type="card"
        defaultActiveKey={academicSessionTab ? '2' : '1'}
        // activeKey={activeTab}
        // onChange={(value) => setActiveTab(value)}
        items={[
          {
            label: t('students'),
            key: '1',
            children: (
              <CustomCard className="tabContainer">
                <TitleView
                  title={t('students')}
                  viewBackButton={false}
                />
                <CustomFilter
                  typeFilter="studentListFilters"
                  setDataSource={setCourseGroupStudents}
                  defaultDataSource={defaultStudentValues}
                />
                <CustomTable
                  dataSource={courseGroupStudents}
                  loading={courseStudentsLoading}
                  columns={studentsColumns(t)}
                />
              </CustomCard>
            ),
          },
          {
            label: t('academicSessions'),
            key: '2',
            children: (
              <CustomCard className="tabContainer">
                <TitleView
                  title={t('academicSessions')}
                  viewBackButton={false}
                />
                <CustomTable
                  loading={courseProgressLoading}
                  dataSource={academicProgress}
                  columns={academicProgressColumns(t)}
                />
                <AcademicProgressModal
                  academicProgressModal={academicProgressModal}
                  handleCancelAcademicProgress={handleCancelAcademicProgress}
                  refetch={null}
                  setAcademicProgressModal={setAcademicProgressModal}
                />
              </CustomCard>
            ),
          },
        ]}
      />
    </div>
  );
};

export default SubjectIdView;
