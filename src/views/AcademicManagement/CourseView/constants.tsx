import {
  Button, Col, Row, Tag, Tooltip,
} from 'antd';
import React from 'react';
import { TFunction } from 'react-i18next';
import { InfoCircleOutlined, EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../routes/routesDictionary';

export const studentsColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'cui',
    title: translate('cui'),
    dataIndex: 'cui',
    sorterType: 'alphabetical',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'grade',
    title: translate('grade'),
    dataIndex: 'grade',
  },
  {
    key: 'attendance',
    title: (
      <Row align="middle">
        <span style={{ marginRight: 10 }}>
          {translate('attendance')}
        </span>
        <Tooltip title={translate('attendanceTooltip')}>
          <InfoCircleOutlined />
        </Tooltip>
      </Row>
    ),
    dataIndex: 'attendances',
    width: 250,
    render: (attendances) => (
      <Row gutter={[10, 0]}>
        <Col>
          <Tag>{`P: ${attendances.onTime}`}</Tag>
        </Col>
        <Col>
          <Tag>{`T: ${attendances.late}`}</Tag>
        </Col>
        <Col>
          <Tag>{`A: ${attendances.absent}`}</Tag>
        </Col>
        <Col>
          <Tag>{`J: ${attendances.excused}`}</Tag>
        </Col>
      </Row>
    ),
  },
];

export const academicProgressColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('N°'),
    dataIndex: 'code',
  },
  {
    key: 'tittle',
    title: translate('topic'),
    dataIndex: 'tittle',
    sorterType: 'alphabetical',
  },
  {
    key: 'description',
    title: translate('description'),
    dataIndex: 'description',
    sorterType: 'alphabetical',
  },
  {
    key: 'date',
    title: translate('date'),
    dataIndex: 'date',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    width: 400,
    render: ({
      setAcademicProgressModal,
      courseprogressId, navigate, courseGroupId,
    }: any) => (
      <Row gutter={[12, 0]}>
        <Col>
          <Button
            type="ghost"
            icon={<EyeOutlined />}
            onClick={() => {
              setAcademicProgressModal({
                visible: true,
                isEditable: false,
                courseprogressId,
              });
            }}
          >
            {translate('academicProgress')}
          </Button>
        </Col>
        <Col>
          <Button
            type="ghost"
            icon={<EyeOutlined />}
            onClick={() => navigate(
              routesDictionary.programSubjectAttendance
                .func(courseGroupId, courseprogressId),
            )}
          >
            {translate('attendance')}
          </Button>
        </Col>
      </Row>
    ),
  },
];
export default {};
