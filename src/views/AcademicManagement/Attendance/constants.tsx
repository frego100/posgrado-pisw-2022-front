import { TFunction } from 'react-i18next';

export const optionLabel = {
  1: 'present',
  2: 'absent',
  3: 'late',
  4: 'excusedAbsence',
};

export const studentColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'attendance',
    title: translate('attendance'),
    dataIndex: 'attendance',
  },
];

export default {};
