import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useDispatch } from 'react-redux';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomTable from '../../../components/CustomTable';
import { optionLabel, studentColumns } from './constants';
import CustomCard from '../../../components/CustomCard';
import TitleView from '../../../components/TitleView';
import { AppDispatch } from '../../../store';
import CustomFilter from '../../../components/CustomFilter';
import routesDictionary from '../../../routes/routesDictionary';

const getStudentAttendancesRequest = loader('./requests/getStudentAttendances.gql');

const AttendanceView = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const params = useParams();
  const { courseGroupId, academicSessionId } = params;
  const dispatch = useDispatch<AppDispatch>();

  // State for students table
  const [students, setStudents] = useState<any>([]);
  const [defaultValues, setDefaultValues] = useState<any>([]);

  const { loading, data } = useQuery(
    getStudentAttendancesRequest,
    { variables: { academicSessionId }, fetchPolicy: 'no-cache' },
  );

  useEffect(() => {
    if (data) {
      const { getStudentAttendancesProfessor } = data;
      const studentsArray: any[] = [];
      getStudentAttendancesProfessor.forEach((studentAttendance, index: number) => {
        const { user } = studentAttendance.groupEnrollment.enrollment.student;
        studentsArray.push({
          key: index + 1,
          firstName: user.firstName,
          lastName: `${user.lastName} ${user.maternalLastName}`,
          email: user.email,
          attendance: t(optionLabel[studentAttendance.attendance]),
        });
      });
      setStudents(studentsArray);
      setDefaultValues(studentsArray);
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('academicManagement'),
          t('attendance'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('attendance')}
        viewBackButton
        onClickBackButton={() => navigate(
          routesDictionary.programSubjectAcademicManagement.func(courseGroupId),
          { state: { displayAcademicSession: true } },
        )}
      />
      <CustomCard>
        <CustomFilter
          typeFilter="studentListFilters"
          setDataSource={setStudents}
          defaultDataSource={defaultValues}
          operatingPlanStatus=""
        />
        <CustomTable
          loading={loading}
          dataSource={students}
          columns={studentColumns(t)}
        />
      </CustomCard>
    </div>

  );
};

export default AttendanceView;
