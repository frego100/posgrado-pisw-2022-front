import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import {
  Col, Divider, Row, Select,
} from 'antd';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import './styles.scss';
import CustomFilter from '../../../components/CustomFilter';
import CustomTable from '../../../components/CustomTable';
import { subjectListTableEnrollmentColumns } from './constants';
import CustomCard from '../../../components/CustomCard';
import { APPROVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import { isUndefined } from '../../../utils/tools';
import EmptyView from '../../../components/EmptyView';
import CustomLoader from '../../../components/CustomLoader';

interface Subject {
  key: string,
  code: string,
  name: string,
  semester: number,
  year: string,
  actions: {
  }
}

const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getAllProgramOperatingPlansRequest = loader('./requests/getAllProgramOperatingPlans.gql');

const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');
const getCoursesGroupApprovedRequest = loader('./requests/getCoursesGroupApproved.gql');

const CourseList = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);

  // processes
  const [processes, setProcesses] = useState<any>(null);
  const [process, setProcess] = useState<any>();

  const [studyProgramName, setStudyProgramName] = useState<any>();

  // enrollments and courses by period
  const [semesterCourses, setSemesterCourses] = useState<Subject[]>([]);

  // filters
  const [defaultSubjectsValues, setDefaultSubjectsValues] = useState<any>([]);

  const {
    data: getProcessesData,
    loading: getProcessesLoading,
    // refetch: getProcessesRefetch,
  } = useQuery(getOperatingPlanProcessesRequest);

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const operatingPlanProcessesArray = getOperatingPlanProcesses.map((item: any) => (
        { label: item.year, value: item.id }
      ));
      if (!process) setProcess(operatingPlanProcessesArray[0]);
      setProcesses(operatingPlanProcessesArray);
    }
  }, [getProcessesData]);

  // Get graduate units
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId || {});

  const {
    data: getGraduateUnitsData, loading: getGraduateUnitsLoading,
  } = useQuery(getGraduateUnitsRequest, { skip: userType === UNIT_DIRECTOR });

  useEffect(() => {
    if (getGraduateUnitsData) {
      const { getGraduateUnits } = getGraduateUnitsData;

      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      setGraduateUnit(graduateUnitsArray[0]);
    }
  }, [getGraduateUnitsData]);

  // Get Programs
  const [operatingPlanPrograms, setOperatingPlanPrograms] = useState<any>([]);
  const [operatingPlanProgram, setOperatingPlanProgram] = useState<any>(null);

  const {
    data: getAllProgramOperatingPlansData,
    loading: getAllProgramOperatingPlansLoading,
  } = useQuery(
    getAllProgramOperatingPlansRequest,
    {
      variables: {
        unit: graduateUnitId || graduateUnit.value,
        process: process?.value,
      },
      fetchPolicy: 'no-cache',
      skip: userType === UNIT_DIRECTOR ? !graduateUnitId : isUndefined(graduateUnit?.value)
      && isUndefined(process),
    },
  );

  useEffect(() => {
    if (getAllProgramOperatingPlansData) {
      const { getAllProgramOperatingPlans } = getAllProgramOperatingPlansData;

      const operatingPlanProgramsArray = getAllProgramOperatingPlans.map((programOp) => {
        const { studyProgram, operatingPlan } = programOp;
        return { value: studyProgram.id, label: studyProgram.name, status: operatingPlan.status };
      });
      setOperatingPlanPrograms(operatingPlanProgramsArray);
      setOperatingPlanProgram(operatingPlanProgramsArray[0]);
    }
  }, [getAllProgramOperatingPlansData]);

  const {
    data: getCoursesGroupApprovedData,
    loading: getCoursesGroupApprovedLoading,
    // refetch: getCoursesGroupApprovedRefetch
  } = useQuery(
    getCoursesGroupApprovedRequest,
    {
      variables: {
        studyProgramId: operatingPlanProgram?.value,
        process: process?.value,
      },
      fetchPolicy: 'no-cache',
      skip: !operatingPlanProgram?.value || isUndefined(process),
    },
  );

  useEffect(() => {
    if (getCoursesGroupApprovedData) {
      const { getCoursesGroupApproved, getStudyProgram } = getCoursesGroupApprovedData;
      const semesterCoursesArray: Subject[] = [];

      getCoursesGroupApproved?.forEach(({ id, group, course }: any, index: number) => {
        const { studyPlanSubject } = course;
        semesterCoursesArray.push({
          // @ts-ignore
          key: index + 1,
          code: studyPlanSubject.code,
          subjectName: studyPlanSubject.subject,
          group,
          semester: studyPlanSubject.semester,
          actions: {
            courseGroupId: id,
            studyProgramId: operatingPlanProgram?.value,
            navigate,
          },
        });
      });
      setStudyProgramName(getStudyProgram?.name);
      setSemesterCourses(semesterCoursesArray);
      setDefaultSubjectsValues(semesterCoursesArray);
    }
  }, [getCoursesGroupApprovedData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('academicManagement'),
          studyProgramName,
        ],
      ),
    ).then();
  }, [dispatch, studyProgramName, t]);

  if (getProcessesLoading) return <CustomLoader />;

  if (processes && processes.length === 0) {
    return (
      <EmptyView
        title={t('academicManagement')}
        viewBackButton={false}
        userType={userType}
        type="emptyOperatingPlanProcess"
      />
    );
  }

  return (
    <div>
      <TitleView
        title={t('academicManagement')}
        viewBackButton={false}
        action={(
          <Row justify="end" gutter={[10, 4]}>
            <Col className="label" span={24}>
              <span className="label">
                {t('academicYear')}
              </span>
            </Col>
            <Col span={24}>
              <Select
                labelInValue
                style={{ width: '100%' }}
                options={processes}
                value={process}
                onChange={(newValue) => setProcess(newValue)}
                loading={getProcessesLoading}
              />
            </Col>
          </Row>
        )}
      />
      <CustomCard>
        <Row className="graduate-unit-filter" gutter={[24, 24]}>
          {userType === SCHOOL_DIRECTOR && (
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('graduateUnit')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={graduateUnit}
                  options={graduateUnits}
                  loading={getGraduateUnitsLoading}
                  labelInValue
                  onChange={(newValue) => setGraduateUnit(newValue)}
                />
              </Col>
            </Row>
          </Col>
          )}
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('program')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={operatingPlanProgram?.value}
                  options={operatingPlanPrograms}
                  loading={getAllProgramOperatingPlansLoading}
                  labelInValue
                  onChange={(value, option) => setOperatingPlanProgram(option)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Divider />
        { operatingPlanProgram && operatingPlanProgram.status !== APPROVED
          ? (
            <EmptyView
              type="operatingPlanNotApproved"
              userType={userType}
              viewTitle={false}
            />
          ) : (
            <>
              <CustomFilter
                typeFilter="subjectListFilters"
                setDataSource={setSemesterCourses}
                defaultDataSource={defaultSubjectsValues}
              />
              <CustomTable
                columns={subjectListTableEnrollmentColumns(t)}
                dataSource={semesterCourses}
                loading={getCoursesGroupApprovedLoading}
              />
            </>
          )}
      </CustomCard>
    </div>
  );
};

export default CourseList;
