import React from 'react';
import { TFunction } from 'react-i18next';
import { Button } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../routes/routesDictionary';

export const subjectListTableEnrollmentColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectName',
    title: translate('name'),
    dataIndex: 'subjectName',
    sorterType: 'alphabetical',
  },
  {
    key: 'group',
    title: translate('group'),
    dataIndex: 'group',
    sorterType: 'alphabetical',
  },
  {
    key: 'semester',
    title: translate('semester'),
    dataIndex: 'semester',
    sorterType: 'numerical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      courseGroupId, navigate,
    }: any) => (
      <Button
        icon={<EyeOutlined />}
        type="ghost"
        onClick={() => {
          navigate(
            routesDictionary.programSubjectAcademicManagement.func(courseGroupId),
          );
        }}
      >
        {translate('viewStudents')}
      </Button>
    ),
  },
];

export default {};
