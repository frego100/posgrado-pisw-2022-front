import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { PlusCircleOutlined } from '@ant-design/icons';
import {
  Button, Col, Row, Select,
} from 'antd';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import CustomFilter from '../../../components/CustomFilter';
import CustomTable from '../../../components/CustomTable';
import { entrantsColumns } from './constants';
import CustomCard from '../../../components/CustomCard';
import EmptyCard from '../../../components/EmptyCard';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import AdmissionProcessModal from '../EntrantsProgramList/AdmissionProcessModal';
import CustomLoader from '../../../components/CustomLoader';

const getStudyProgramRequest = loader('./requests/getStudyProgram.gql');
const getAdmissionProcessesRequest = loader('./requests/getAdmissionProcesses.gql');
const getAdmittedStudentsRequest = loader('./requests/getAdmittedStudents.gql');
const getAdmissionYearsRequest = loader('./requests/getAdmissionYears.gql');

const StudyProgramIdView = () => {
  const navigate = useNavigate();
  const params = useParams();
  const { userType } = useSelector((state: any) => state.auth);

  const { studyProgramId } = params;
  const [studyProgramName, setStudyProgramName] = useState<any>();
  const [admissionYears, setAdmissionYears] = useState<any>(null);
  const [admissionYear, setAdmissionYear] = useState<any>();
  const [admissionProcesses, setAdmissionProcesses] = useState<any>([]);
  const [admissionProcess, setAdmissionProcess] = useState<any>();
  const [entrants, setEntrants] = useState<any>([]);
  const [defaultEntrantsValues, setDefaultEntrantsValues] = useState<any>([]);
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();

  const [admissionProcessModalData, setAdmissionProcessModalData] = useState<any>({
    isVisible: false,
  });

  const { data: studyProgramData } = useQuery(
    getStudyProgramRequest,
    {
      variables: { studyProgramId },
      fetchPolicy: 'no-cache',
    },
  );

  const {
    data: admissionYearsData,
    loading: admissionYearsLoading,
    refetch: admissionYearsRefetch,
  } = useQuery(
    getAdmissionYearsRequest,
    {
      fetchPolicy: 'no-cache',
    },
  );

  const {
    data: admissionProcessesData,
    loading: admissionProcessesLoading,
  } = useQuery(
    getAdmissionProcessesRequest,
    {
      variables: { studyProgramId, year: admissionYear },
      fetchPolicy: 'no-cache',
      skip: !admissionYear,
    },
  );

  const {
    data: admittedStudentsData,
    loading: admittedStudentsLoading,
  } = useQuery(
    getAdmittedStudentsRequest,
    {
      variables: {
        studyProgramId,
        admissionProcessId: admissionProcess,
      },
      fetchPolicy: 'no-cache',
      skip: !admissionProcess,
    },
  );

  useEffect(() => {
    if (studyProgramData) {
      const { getStudyProgram } = studyProgramData;
      setStudyProgramName(getStudyProgram?.name);
    }
  }, [studyProgramData]);

  useEffect(() => {
    if (admissionYearsData) {
      const { getAdmissionYears } = admissionYearsData;
      const admissionYearsArray = getAdmissionYears.map((item) => (
        { value: item, label: item }
      ));
      setAdmissionYears(admissionYearsArray);
      setAdmissionYear(admissionYearsArray[0]?.value);
    }
  }, [admissionYearsData]);

  useEffect(() => {
    if (admissionProcessesData) {
      const { getAdmissionProcesses } = admissionProcessesData;
      const admissionProcessesArray = getAdmissionProcesses.map((item) => (
        { value: item.id, label: item.name }
      ));
      setAdmissionProcesses(admissionProcessesArray);
      setAdmissionProcess(admissionProcessesArray[0]?.value);
    }
  }, [admissionProcessesData]);

  useEffect(() => {
    if (admittedStudentsData) {
      const { getAdmittedStudents } = admittedStudentsData;
      const admittedStudentsArray: any = [];
      getAdmittedStudents.students.forEach(({ id, user }: any, index: number) => {
        admittedStudentsArray.push({
          key: index + 1,
          code: id,
          firstName: user.firstName,
          lastName: `${user.lastName} ${user.maternalLastName}`,
          documentNumber: user.useridentificationSet[0].number,
          birthday: user.birthday,
          email: user.email,
        });
      });
      setEntrants(admittedStudentsArray);
      setDefaultEntrantsValues(admittedStudentsArray);
    }
  }, [admittedStudentsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('entrants'),
          studyProgramName,
        ],
      ),
    ).then();
  }, [dispatch, studyProgramName, t]);

  if (admissionYearsLoading) return <CustomLoader />;

  if (!admissionProcesses.length) {
    return (
      <>
        <TitleView
          title={studyProgramName}
          viewBackButton
          onClickBackButton={() => navigate(routesDictionary.entrants.route)}
        />
        <EmptyCard
          title={t('emptyEntrantsTitle')}
          description={t(userType === UNIT_DIRECTOR ? 'emptyEntrantsDescriptionUnitDirector' : '')}
          {...userType === SCHOOL_DIRECTOR && {
            action: (
              <Row align="middle" gutter={[0, 16]} justify="center">
                <Button
                  type="primary"
                  icon={<PlusCircleOutlined />}
                  onClick={() => {
                    setAdmissionProcessModalData({
                      ...admissionProcessModalData,
                      refetch: admissionYearsRefetch,
                      isVisible: true,
                    });
                  }}
                >
                  {t('registerEntrants')}
                </Button>
              </Row>
            ),
          }}
        />
        <AdmissionProcessModal
          admissionProcessModalData={admissionProcessModalData}
          setAdmissionProcessModalData={setAdmissionProcessModalData}
        />
      </>
    );
  }

  return (
    <div>
      <TitleView
        title={studyProgramName}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.entrants.route);
        }}
        sizeAction={280}
        action={(
          <Row justify="end" gutter={[5, 5]}>
            <Col span={10}>
              <Col className="label" span={24}>
                <span className="label">
                  {t('admissionYear')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  labelInValue
                  style={{ width: '100%' }}
                  value={admissionYear}
                  onChange={(newValue) => setAdmissionYear(newValue.value)}
                  options={admissionYears}
                  loading={admissionYearsLoading}
                />
              </Col>
            </Col>
            <Col span={14}>
              <Col className="label" span={24}>
                <span className="label">
                  {t('admissionProcess')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  labelInValue
                  style={{ width: '100%' }}
                  value={admissionProcess}
                  onChange={(newValue) => setAdmissionProcess(newValue.value)}
                  options={admissionProcesses}
                  loading={admissionProcessesLoading}
                />
              </Col>
            </Col>
          </Row>
        )}
      />
      <CustomCard>
        <TitleView
          title={t('entrants')}
          viewBackButton={false}
          sizeAction={200}
        />
        <CustomFilter
          typeFilter="entrantsListFilters"
          operatingPlanStatus=""
          setDataSource={setEntrants}
          defaultDataSource={defaultEntrantsValues}
        />
        <CustomTable
          columns={entrantsColumns(t)}
          dataSource={entrants}
          loading={admittedStudentsLoading}
        />
      </CustomCard>
    </div>
  );
};

export default StudyProgramIdView;
