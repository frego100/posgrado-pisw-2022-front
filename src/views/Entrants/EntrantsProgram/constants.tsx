import { TFunction } from 'react-i18next';

export const entrantsColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'documentNumber',
    title: translate('documentNumber'),
    dataIndex: 'documentNumber',
    sorterType: 'alphabetical',
  },
  {
    key: 'birthday',
    title: translate('birthDate'),
    dataIndex: 'birthday',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
];

export default {};
