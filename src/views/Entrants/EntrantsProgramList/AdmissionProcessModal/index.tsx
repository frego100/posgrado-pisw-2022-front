import React from 'react';
import {
  Button, Form, message, Modal,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useMutation } from '@apollo/client';
import moment from 'moment';
import { formItems } from './constants';
import FormCard from '../../../../components/FormCard';

interface AdmissionProcessModalProps {
    admissionProcessModalData: any
    setAdmissionProcessModalData: any
}

const addAdmissionProcess = loader('./requests/addAdmissionProcess.gql');

const AdmissionProcessModal:React.FC<AdmissionProcessModalProps> = ({
  admissionProcessModalData,
  setAdmissionProcessModalData,
}) => {
  const { t } = useTranslation();
  const [form] = useForm();

  const [addAdmissionProcessMutation, { loading }] = useMutation(addAdmissionProcess);

  const handleOk = () => {
    form.validateFields()
      .then((values) => {
        addAdmissionProcessMutation({
          variables: {
            studentsFile: values.entrantsFile.originFileObj,
            name: values.admissionProcessName,
            year: moment(values.year).format('YYYY'),
          },
        })
          .then(({ data }) => {
            const { addAdmissionProcess: resultData } = data;
            if (resultData.feedback.status === 'SUCCESS') {
              setAdmissionProcessModalData({ ...admissionProcessModalData, isVisible: false });
              message.success(resultData.feedback.message);
              admissionProcessModalData.refetch();
            } else {
              message.error(resultData.feedback.message);
            }
          // eslint-disable-next-line no-unused-vars
          }).catch((e) => {
            form.setFields([{
              name: 'entrantsFile',
              errors: [t('invalidFile')],
            }]);
          });
      });
  };

  const handleCancel = () => {
    setAdmissionProcessModalData({ ...admissionProcessModalData, isVisible: false });
  };

  return (
    <Modal
      title={t('registerAdmissionProcess')}
      centered
      width={600}
      closable
      destroyOnClose
      open={admissionProcessModalData.isVisible}
      bodyStyle={{ maxHeight: '410px' }}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={[
        <Button key="cancel" onClick={handleCancel}>
          {t('cancel')}
        </Button>,
        <Button key="ok" type="primary" onClick={handleOk} loading={loading}>

          {t('confirm')}
        </Button>,
      ]}
    >
      <p style={{
        marginBottom: '30px',
        color: '#081d40',
        textAlign: 'justify',
      }}
      >
        {t('registerAdmissionProcessInfoText')}
      </p>
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
        form={form}
      >
        <FormCard
          withoutTitle
          withoutDeleteIcon
          itemList={formItems(
            form,
            t,
          )}
          readonly={false}
        />
      </Form>
    </Modal>
  );
};

export default AdmissionProcessModal;
