import React from 'react';
import { DatePicker, Input } from 'antd';
import { TFunction } from 'react-i18next';
import CustomUploadFile from '../../../../components/CustomUploadFile';

const amissionProcessItems = (
  form: any,
  t: TFunction,
) => [
  {
    name: 'year',
    component: (
      <DatePicker
        format="YYYY"
        className="component"
        allowClear={false}
        picker="year"
      />
    ),
    rules: [
      {
        required: true,
        message: '¡Campo obligatorio!',
      },
    ],
    label: t('admissionYear'),
    responsive: {
      xs: { span: 24 },
      md: { span: 8 },
    },
    display: true,
  },
  {
    name: 'admissionProcessName',
    component: (
      <Input />
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('admissionProcess'),
    responsive: {
      xs: { span: 24 },
      md: { span: 16 },
    },
    display: true,
  },
  {
    name: 'entrantsFile',
    component: (
      <CustomUploadFile
        disabled={false}
        form={form}
        nameComponent="entrantsFile"
        typeFile=".xlsx"
      />
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('uploadFile'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
];

export const formItems = (
  form: any,
  t: TFunction,
) => [
  ...amissionProcessItems(
    form,
    t,
  ),
];
export default {};
