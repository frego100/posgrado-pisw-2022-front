import React, { useEffect, useState } from 'react';
import { Row, Select } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { AppDispatch } from '../../../store';
import { SCHOOL_DIRECTOR } from '../../../utils/constants';
import { typeProgram } from '../../../components/CustomTable/constants';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import CustomButtonUploadFile from '../../../components/CustomButtonUploadFile';
import CustomTable from '../../../components/CustomTable';
import { entrantsColumns } from './constants';
import AdmissionProcessModal from './AdmissionProcessModal';
import CustomCard from '../../../components/CustomCard';

interface studyProgram {
  key: string,
  code: string,
  type: string,
  name: string,
  actions: {
  }
}

const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getStudyProgramRequest = loader('./requests/getStudyProgram.gql');

const EntrantsView = () => {
  const navigate = useNavigate();

  const [studyPrograms, setStudyPrograms] = useState<studyProgram[]>([]);
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId);
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();

  const [admissionProcessModalData, setAdmissionProcessModalData] = useState<any>({
    isVisible: false,
  });
  const {
    data: graduateUnitsData, loading: graduateUnitsLoading,
  } = useQuery(getGraduateUnitsRequest, { skip: userType !== SCHOOL_DIRECTOR });

  const {
    data: studyProgramsData,
    loading: studyProgramsLoading,
  } = useQuery(
    getStudyProgramRequest,
    {
      variables: {
        graduateUnit_Id: graduateUnit,
      },
      fetchPolicy: 'no-cache',
      skip: !graduateUnit,
    },
  );

  useEffect(() => {
    if (graduateUnitsData && userType === SCHOOL_DIRECTOR) {
      const { getGraduateUnits } = graduateUnitsData;
      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      if (!graduateUnit) setGraduateUnit(graduateUnitsArray[0]?.value);
    }
  }, [graduateUnitsData, dispatch]);

  useEffect(() => {
    if (studyProgramsData) {
      const { getStudyPrograms } = studyProgramsData;
      const studyProgramsArray: studyProgram[] = [];

      getStudyPrograms.edges.forEach(({ node } : any, index: any) => (
        studyProgramsArray.push({
          key: index + 1,
          code: node.code,
          type: typeProgram[node.programType],
          name: node.name,
          actions: {
            studyProgramId: node.id,
            navigate,
          },
        })
      ));
      setStudyPrograms(studyProgramsArray);
    }
  }, [studyProgramsData, dispatch]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('entrants'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('entrants')}
        viewBackButton={false}
        action={
          userType === SCHOOL_DIRECTOR && (
            <Row justify="end" gutter={[0, 16]}>
              <CustomButtonUploadFile
                name={t('registerEntrants')}
                onClick={() => {
                  setAdmissionProcessModalData({
                    ...admissionProcessModalData,
                    isVisible: true,
                  });
                }}
              />
            </Row>
          )
        }
      />
      <CustomCard>
        {userType === SCHOOL_DIRECTOR && (
          <Row align="middle" style={{ width: '300px', marginBottom: '32px' }}>
            <span className="label">
              {t('graduateUnit')}
            </span>
            <Select
              labelInValue
              style={{ width: '100%' }}
              value={graduateUnit}
              onChange={(newValue) => setGraduateUnit(newValue.value)}
              options={graduateUnits}
              loading={graduateUnitsLoading}
            />
          </Row>
        )}
        <CustomTable
          columns={entrantsColumns(t)}
          dataSource={studyPrograms}
          loading={studyProgramsLoading}
        />
      </CustomCard>
      <AdmissionProcessModal
        admissionProcessModalData={admissionProcessModalData}
        setAdmissionProcessModalData={setAdmissionProcessModalData}
      />
    </div>
  );
};

export default EntrantsView;
