import React from 'react';
import {
  DatePicker,
  Input, Radio, Select,
} from 'antd';
import { TFunction } from 'react-i18next';

const { Option } = Select;

// Constants gender
export const GENDER_MALE = 'MALE';
export const GENDER_FEMALE = 'FEMALE';

export const Items = (
  form: any,
  createUserState: boolean,
  requestData: any,
  t: TFunction,
  usertype: any,
  setUserType: any,
  graduateUnit: any,
  setGraduateUnit: any,
  graduateUnits: any,
) => [
  {
    [t('controls')]: [
      {
        name: 'userType',
        component: (
          <Select
          // defaultValue="school"
            className="component"
            disabled={!createUserState}
            value={usertype}
            onChange={(newValue) => { setUserType(newValue); }}
          >
            <Option value="school">{t('school')}</Option>
            <Option value="unit">{t('unit')}</Option>
          </Select>
        ),
        rules: [
          {
            required: true,
            message: '¡Campo obligatorio!',
          },
        ],
        label: t('userType'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'graduateUnit',
        component: (
          <Select
            placeholder={t('graduateUnitPlaceholder')}
            className="component"
            value={graduateUnit}
            options={graduateUnits}
            onChange={(newValue) => { setGraduateUnit(newValue); }}
          />
        ),
        rules: [
          {
            required: true,
            message: '¡Campo obligatorio!',
          },
        ],
        label: t('graduateUnit'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: usertype === 'unit',
      },
    ],
    [t('personalInformation')]: [
      {
        name: 'email',
        component: (
          <Input />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
          {
            type: 'email',
            message: '¡Ingrese un correo electronico válido!',
          },
        ],
        label: t('email'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'firstName',
        component: (
          <Input />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('firstName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'lastName',
        component: (
          <Input />
        ),

        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('fatherLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'maternalLastName',
        component: (
          <Input />
        ),
        rules: [
          {
            required: true,
            message: 'Campo obligatorio!',
          },
        ],
        label: t('maternalLastName'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      /* {
        name: 'status',
        component: (
          <Switch checked checkedChildren="Activo" unCheckedChildren="Inactivo" />
        ),
        rules: [
          {
            required: true,
            message: '¡Campo obligatorio!',
          },
        ],
        label: t('status'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      }, */
      {
        name: 'birthday',
        component: (
          <DatePicker
            style={{ width: '100%' }}
            className="component"
            allowClear={false}
          />
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('birthDate'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
      {
        name: 'gender',
        component: (
          <Radio.Group>
            <Radio value={GENDER_MALE}> Masculino </Radio>
            <Radio value={GENDER_FEMALE}> Femenino </Radio>
          </Radio.Group>
        ),
        rules: [
          {
            required: false,
          },
        ],
        label: t('gender'),
        responsive: {
          xs: { span: 24 },
          md: { span: 12 },
        },
        display: true,
      },
    ],
  },
];

export const formItems = (
  form: any,
  createUserState: boolean,
  modalData: any,
  t: TFunction,
  usertype: any,
  setUserType: any,
  graduateUnit: any,
  setGraduateUnit: any,
  graduateUnits: any,
) => [
  ...Items(
    form,
    createUserState,
    modalData,
    t,
    usertype,
    setUserType,
    graduateUnit,
    setGraduateUnit,
    graduateUnits,
  ),
];
export default {};
