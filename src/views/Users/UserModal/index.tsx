import React, { useEffect, useState } from 'react';
import {
  Button, Col, Form, message, Modal, Row,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { useForm } from 'antd/lib/form/Form';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import moment from 'moment';
import FormCard from '../../../components/FormCard';
import { formItems } from './constants';
import { genders } from '../../../components/ProfessorView/constants';

interface OperatingPlanProcessModalProps {
  modalData: any,
  setModalData: any,
  createUserState: boolean
}
const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const createUnitManagerRequest = loader('./requests/createUnitManager.gql');
const createSchoolManagerRequest = loader('./requests/createSchoolManager.gql');
const editUnitManagerRequest = loader('./requests/editUnitManager.gql');
const editSchoolManagerRequest = loader('./requests/editSchoolManager.gql');
const getUnitManagerRequest = loader('./requests/getUnitManager.gql');
const getSchoolManagerRequest = loader('./requests/getSchoolManager.gql');

const OperatingPlanProcessModal:React.FC<OperatingPlanProcessModalProps> = (
  { modalData, setModalData, createUserState },
) => {
  const { t } = useTranslation();
  const [form] = useForm();

  // value of userType filter
  const [userType, setUserType] = useState(modalData?.usertype ?? 'school');

  useEffect(() => {
    if (form.getFieldValue('userType')) {
      setUserType(form.getFieldValue('userType'));
    }
  }, [form.getFieldValue('userType')]);

  const [graduateUnit, setGraduateUnit] = useState<any>();
  const [graduateUnits, setGraduateUnits] = useState<any>([]);

  const { data: getGraduateUnitsData } = useQuery(
    getGraduateUnitsRequest,
    {
      fetchPolicy: 'no-cache',
      skip: userType === 'school',
    },
  );

  useEffect(() => {
    if (getGraduateUnitsData) {
      const { getGraduateUnits } = getGraduateUnitsData;

      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      setGraduateUnit(graduateUnitsArray[0]);
    }
  }, [getGraduateUnitsData]);

  const [createUnitManagerMutation, { loading: createUnitManagerLoading }] = useMutation(
    createUnitManagerRequest,
  );

  const [createSchoolManagerMutation, { loading: createSchoolManagerLoading }] = useMutation(
    createSchoolManagerRequest,
  );

  const [editUnitManagerMutation, { loading: editUnitManagerLoading }] = useMutation(
    editUnitManagerRequest,
  );

  const [editSchoolManagerMutation, { loading: editSchoolManagerLoading }] = useMutation(
    editSchoolManagerRequest,
  );

  const {
    data: getSchoolManagerData,
  } = useQuery(
    getSchoolManagerRequest,
    {
      variables: { userId: modalData?.userId },
      fetchPolicy: 'no-cache',
      skip: modalData?.usertype !== 'school' ?? !(modalData?.userId),
    },
  );

  useEffect(() => {
    if (getSchoolManagerData) {
      setUserType('school');
      const { getSchoolManager } = getSchoolManagerData;
      const schoolManager = {
        userType: 'school',
        firstName: getSchoolManager.firstName,
        email: getSchoolManager.email,
        lastName: getSchoolManager.lastName,
        maternalLastName: getSchoolManager.maternalLastName,
        birthday: getSchoolManager.birthday ? moment(getSchoolManager.birthday) : null,
        gender: genders[getSchoolManager.gender],
      };
      form.setFieldsValue(schoolManager);
    }
  }, [getSchoolManagerData]);

  const {
    data: getUnitManagerData,
  } = useQuery(
    getUnitManagerRequest,
    {
      variables: { unitManagerId: modalData?.unitManagerId },
      fetchPolicy: 'no-cache',
      skip: modalData?.usertype !== 'unit' ?? !(modalData.unitManagerId),
    },
  );

  useEffect(() => {
    if (getUnitManagerData) {
      setUserType('unit');
      const { getUnitManager } = getUnitManagerData;
      const { user } = getUnitManager;
      const unitManager = {
        userType: 'unit',
        graduateUnit: getUnitManager.graduateUnit.id,
        firstName: user.firstName,
        email: user.email,
        lastName: user.lastName,
        maternalLastName: user.maternalLastName,
        birthday: user.birthday ? moment(user.birthday) : null,
        gender: genders[user.gender],
      };
      form.setFieldsValue(unitManager);
    }
  }, [getUnitManagerData]);

  const handleCancel = () => {
    setModalData({ ...modalData, visible: false });
  };

  const handleUpdateUnitUser = () => {
    form.validateFields()
      .then((values) => {
        editUnitManagerMutation({
          variables: {
            input: {
              unitManagerId: modalData?.unitManagerId,
              email: values.email,
              firstName: values.firstName,
              lastName: values.lastName,
              maternalLastName: values.maternalLastName,
              ...values.birthday && { birthday: moment(values.birthday).format('YYYY-MM-DD') },
              ...values.gender && { gender: values.gender },
              userIdentifications: [],
              graduateUnit: values.graduateUnit,
            },
          },
        })
          .then(({ data }) => {
            const { editUnitManager } = data;
            if (editUnitManager.feedback.status === 'SUCCESS') {
              setModalData({ ...modalData, visible: false });
              message.success(editUnitManager.feedback.message);
              modalData.refetchUnitManager();
            } else {
              message.error(editUnitManager.feedback.message);
            }
          });
      });
  };

  const handleUpdateSchoolUser = () => {
    form.validateFields()
      .then((values) => {
        editSchoolManagerMutation({
          variables: {
            input: {
              userId: modalData?.userId,
              email: values.email,
              firstName: values.firstName,
              lastName: values.lastName,
              maternalLastName: values.maternalLastName,
              ...values.birthday && { birthday: moment(values.birthday).format('YYYY-MM-DD') },
              ...values.gender && { gender: values.gender },
            },
          },
        })
          .then(({ data }) => {
            const { editSchoolManager } = data;
            if (editSchoolManager.feedback.status === 'SUCCESS') {
              setModalData({ ...modalData, visible: false });
              message.success(editSchoolManager.feedback.message);
              modalData.refetchSchoolManager();
            } else {
              message.error(editSchoolManager.feedback.message);
            }
          });
      });
  };

  const handleCreateUnitUser = () => {
    form.validateFields()
      .then((values) => {
        createUnitManagerMutation({
          variables: {
            input: {
              email: values.email,
              firstName: values.firstName,
              lastName: values.lastName,
              maternalLastName: values.maternalLastName,
              ...values.birthday && { birthday: moment(values.birthday).format('YYYY-MM-DD') },
              ...values.gender && { gender: values.gender },
              userIdentifications: [],
              graduateUnit: values.graduateUnit,
            },
          },
        })
          .then(({ data }) => {
            const { createUnitManager: resultData } = data;
            if (resultData.feedback.status === 'SUCCESS') {
              setModalData({ ...modalData, visible: false });
              message.success(resultData.feedback.message);
              form.resetFields();
              modalData.refetchUnitManager();
            } else {
              message.error(resultData.feedback.message);
            }
          }).catch((e) => {
            message.error(e.message);
          });
      });
  };

  const handleCreateSchoolUser = () => {
    form.validateFields()
      .then((values) => {
        createSchoolManagerMutation({
          variables: {
            input: {
              email: values.email,
              firstName: values.firstName,
              lastName: values.lastName,
              maternalLastName: values.maternalLastName,
              ...values.birthday && { birthday: moment(values.birthday).format('YYYY-MM-DD') },
              ...values.gender && { gender: values.gender },
            },
          },
        })
          .then(({ data }) => {
            const { addSchoolManager: resultData } = data;
            if (resultData.feedback.status === 'SUCCESS') {
              setModalData({ ...modalData, visible: false });
              message.success(resultData.feedback.message);
              form.resetFields();
              modalData.refetchSchoolManager();
            } else {
              message.error(resultData.feedback.message);
            }
          }).catch((e) => {
            message.error(e.message);
          });
      });
  };

  const operation = (
    createUserState ? (
      userType === 'unit' ? 'createUnitUser' : 'createSchoolUser'
    ) : (
      userType === 'unit' ? 'editUnitUser' : 'editSchoolUser'
    )
  );

  const typeOperationUser = {
    createUnitUser: {
      handler: handleCreateUnitUser,
      loading: createUnitManagerLoading,
    },
    createSchoolUser: {
      handler: handleCreateSchoolUser,
      loading: createSchoolManagerLoading,
    },
    editUnitUser: {
      handler: handleUpdateUnitUser,
      loading: editUnitManagerLoading,
    },
    editSchoolUser: {
      handler: handleUpdateSchoolUser,
      loading: editSchoolManagerLoading,
    },
  };

  const userData = formItems(
    form,
    createUserState,
    modalData,
    t,
    userType,
    setUserType,
    graduateUnit,
    setGraduateUnit,
    graduateUnits,
  );

  return (
    <Modal
      title={createUserState ? t('newUser') : (t('editUser'))}
      centered
      width={800}
      closable
      destroyOnClose
      open={modalData.visible}
      onCancel={handleCancel}
      bodyStyle={{ maxHeight: '75vh', overflowY: 'auto', overflowX: 'hidden' }}
      footer={[
        <Button key="back" onClick={handleCancel}>
          {t('cancel')}
        </Button>,
        <Button
          key="submit"
          onClick={typeOperationUser[operation].handler}
          type="primary"
          loading={typeOperationUser[operation].loading}
        >
          {t('save')}
        </Button>,
      ]}
    >
      <Form
        name="uploadUser"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        // onFinish={() => form.resetFields()}
        initialValues={{ userType }}
        autoComplete="off"
        scrollToFirstError
        form={form}
      >
        <Row gutter={[10, 10]}>
          {userData.map((itemObject, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <Col span={24} className="col" key={index}>
              {Object.entries(itemObject).map(([key, value]) => (
                <FormCard
                  title={key}
                  itemList={value}
                  key={key}
                  readonly={false}
                  classnameTitle="userTitle"
                />
              ))}
            </Col>
          ))}
        </Row>
      </Form>
    </Modal>
  );
};

export default OperatingPlanProcessModal;
