import React from 'react';
import { TFunction } from 'react-i18next';
import { Button, Col, Row } from 'antd';
import { DeleteFilled, EditFilled } from '@ant-design/icons';

export const userList = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'usertype',
    title: translate('type'),
    dataIndex: 'usertype',
    sorterType: 'numerical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      id, usertype,
      confirmationModalData, setConfirmationModalData,
      editUserModal, setEditUserModal,
    }: any) => (
      usertype === 'school' ? (
        <Row gutter={[12, 0]}>
          <Col>
            <Button
              type="ghost"
              icon={<EditFilled />}
              onClick={() => {
                setEditUserModal({
                  ...editUserModal,
                  visible: true,
                  usertype,
                  userId: id,
                  type: 'editSchoolManager',
                });
              }}
            >
              {translate('edit')}
            </Button>
          </Col>
          <Col>
            <Button
              type="ghost"
              icon={<DeleteFilled />}
              className="delete-button"
              onClick={() => {
                setConfirmationModalData({
                  ...confirmationModalData,
                  isVisible: true,
                  userId: id,
                  type: 'deleteSchoolManager',
                });
              }}
            >
              {translate('delete')}
            </Button>
          </Col>
        </Row>
      ) : (
        <Row gutter={[12, 0]}>
          <Col>
            <Button
              type="ghost"
              icon={<EditFilled />}
              onClick={() => {
                setEditUserModal({
                  ...editUserModal,
                  visible: true,
                  usertype,
                  unitManagerId: id,
                  type: 'editUnitManager',
                });
              }}
            >
              {translate('edit')}
            </Button>
          </Col>
          <Col>
            <Button
              type="ghost"
              icon={<DeleteFilled />}
              className="delete-button"
              onClick={() => {
                setConfirmationModalData({
                  ...confirmationModalData,
                  isVisible: true,
                  unitManagerId: id,
                  type: 'deleteUnitManager',
                });
              }}
            >
              {translate('delete')}
            </Button>
          </Col>
        </Row>
      )
    ),
  },
];

export default { };
