import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import { UserAddOutlined } from '@ant-design/icons';
import {
  Button, Col, Divider, Row, Select,
} from 'antd';
import { AppDispatch } from '../../../store';
import TitleView from '../../../components/TitleView';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomTable from '../../../components/CustomTable';
import { userList } from './constants';
import CustomFilter from '../../../components/CustomFilter';
import './styles.scss';
import CustomCard from '../../../components/CustomCard';
import ConfirmationModal from '../../../components/ConfirmationModal';
import UserModal from '../UserModal';

interface User{
  key: string,
  firstName: string,
  lastName: string,
  email: string,
  userType : String,
  actions: {
  }
}

const getSchoolManagersRequest = loader('./requests/getSchoolManagers.gql');
const getUnitManagersRequest = loader('./requests/getUnitManagersById.gql');
const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');

const UserList = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  // value of userType filter
  const [usertype, setUsertype] = useState<any>('school');

  // Get graduate units
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const [graduateUnit, setGraduateUnit] = useState<any>();

  const { data: getGraduateUnitsData, loading: getGraduateUnitsLoading } = useQuery(
    getGraduateUnitsRequest,
    {
      fetchPolicy: 'no-cache',
      skip: usertype === 'school',
    },
  );

  useEffect(() => {
    if (getGraduateUnitsData) {
      const { getGraduateUnits } = getGraduateUnitsData;

      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      setGraduateUnit(graduateUnitsArray[0]);
    }
  }, [getGraduateUnitsData]);

  // Get Users
  const [users, setUsers] = useState<User[]>([]);
  const [defaultUsers, setDefaultUsers] = useState<any>([]);

  const {
    data: getSchoolManagersData,
    loading: getSchoolManagersLoading,
    refetch: getSchoolManagersRefetch,
  } = useQuery(
    getSchoolManagersRequest,
    {
      fetchPolicy: 'no-cache',
      skip: usertype !== 'school',
    },
  );

  const {
    data: getUnitManagersData,
    loading: getUnitManagersLoading,
    refetch: getUnitManagersRefetch,
  } = useQuery(
    getUnitManagersRequest,
    {
      variables: { unitId: graduateUnit?.value },
      fetchPolicy: 'no-cache',
      skip: usertype !== 'unit' || !graduateUnit,
    },
  );

  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: 'deleteRegistrationUser',
    refetchUnitManager: getUnitManagersRefetch,
    refetchSchoolManager: getSchoolManagersRefetch,
  });

  const [createUserModal, setCreateUserModal] = useState<any>({
    visible: false,
    usertype,
    refetchUnitManager: getUnitManagersRefetch,
    refetchSchoolManager: getSchoolManagersRefetch,
  });

  const [editUserModal, setEditUserModal] = useState<any>({
    visible: false,
    refetchUnitManager: getUnitManagersRefetch,
    refetchSchoolManager: getSchoolManagersRefetch,
  });

  useEffect(() => {
    if (getSchoolManagersData) {
      const { getSchoolManagers } = getSchoolManagersData;
      const schoolManagersArray: User[] = [];
      getSchoolManagers.forEach((schoolManagers: any, index: number) => {
        schoolManagersArray.push({
          // @ts-ignore
          key: index + 1,
          firstName: schoolManagers.firstName,
          lastName: `${schoolManagers.lastName} ${schoolManagers.maternalLastName}`,
          email: schoolManagers.email,
          usertype: t(usertype),
          actions: {
            id: schoolManagers.id,
            usertype,
            confirmationModalData,
            setConfirmationModalData,
            editUserModal,
            setEditUserModal,
          },
        });
      });
      setUsers(schoolManagersArray);
      setDefaultUsers(schoolManagersArray);
    }
  }, [getSchoolManagersData]);

  useEffect(() => {
    if (getUnitManagersData) {
      const { getUnitManagersByUnit } = getUnitManagersData;
      const unitManagersArray: User[] = [];
      getUnitManagersByUnit.forEach((unitManagersData: any, index: number) => {
        const { user } = unitManagersData;

        unitManagersArray.push({
          // @ts-ignore
          key: index + 1,
          firstName: user.firstName,
          lastName: `${user.lastName} ${user.maternalLastName}`,
          email: user.email,
          usertype: t(usertype),
          actions: {
            id: unitManagersData.id,
            usertype,
            confirmationModalData,
            setConfirmationModalData,
            editUserModal,
            setEditUserModal,
          },
        });
      });
      setUsers(unitManagersArray);
      setDefaultUsers(unitManagersArray);
    }
  }, [getUnitManagersData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('users'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div className="user-list">
      <TitleView
        title={t('users')}
        viewBackButton={false}
      />

      <CustomCard>
        <Row className="graduate-unit-filter" gutter={[24, 24]}>
          <Col flex="250px">
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('userType')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={usertype}
                  onChange={(newValue) => setUsertype(newValue)}
                >
                  <Select.Option value="school">{t('school')}</Select.Option>
                  <Select.Option value="unit">{t('unit')}</Select.Option>
                </Select>
              </Col>
            </Row>
          </Col>
          {usertype === 'unit' && (
            <Col xs={24} md={12}>
              <Row gutter={[0, 4]} align="middle">
                <Col span={24}>
                  <span className="label">
                    {t('graduateUnit')}
                  </span>
                </Col>
                <Col span={24}>
                  <Select
                    style={{ width: '100%' }}
                    value={graduateUnit}
                    options={graduateUnits}
                    loading={getGraduateUnitsLoading}
                    labelInValue
                    onChange={(newValue) => setGraduateUnit(newValue)}
                  />
                </Col>
              </Row>
            </Col>
          )}
          <Col flex="250px" style={{ marginLeft: 'auto', marginTop: '25px' }}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <Button
                  type="primary"
                  icon={<UserAddOutlined />}
                  style={{ width: '100%' }}
                  onClick={() => {
                    setCreateUserModal({
                      ...createUserModal,
                      usertype,
                      visible: true,
                    });
                  }}
                >
                  {t('newUser')}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
        <Divider />
        <Row className="w-100">
          <Col span={24}>
            <CustomFilter
              typeFilter="userList"
              setDataSource={setUsers}
              defaultDataSource={defaultUsers}
            />
          </Col>
        </Row>

        <CustomTable
          columns={userList(t)}
          dataSource={users}
          loading={usertype === 'unit' ? getUnitManagersLoading : getSchoolManagersLoading}
        />
      </CustomCard>
      <ConfirmationModal
        confirmationModalData={confirmationModalData}
        setConfirmationModalData={setConfirmationModalData}
      />
      <UserModal
        modalData={createUserModal}
        setModalData={setCreateUserModal}
        createUserState
      />
      <UserModal
        modalData={editUserModal}
        setModalData={setEditUserModal}
        createUserState={false}
      />
    </div>
  );
};

export default UserList;
