import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';
import StudentView from '../../../../components/StudentView';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import { AppDispatch } from '../../../../store';
import routesDictionary from '../../../../routes/routesDictionary';
import TitleView from '../../../../components/TitleView';

const ManualEnrollment = () => {
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const params = useParams();
  const { studyProgramId, subjectId } = params;

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('enrollments'),
          t('manualEnrollment'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('manualEnrollment')}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.programSubjectEnrollments.func(studyProgramId, subjectId));
        }}
      />
      <StudentView updateEnrollment createStudent readOnly />
    </div>
  );
};

export default ManualEnrollment;
