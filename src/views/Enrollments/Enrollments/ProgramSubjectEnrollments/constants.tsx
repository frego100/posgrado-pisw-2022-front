import React from 'react';
import { TFunction } from 'react-i18next';
import {
  Col, Radio, RadioChangeEvent, Row, Tag,
} from 'antd';
import { enrolllmentStatusColor } from '../ProgramEnrollments/constants';
import { ENROLLED } from '../../../../utils/constants';

const onChange = (
  value: any,
  groupEnrollmentId: any,
  setCourseGroupStudents: any,
) => {
  setCourseGroupStudents((oldValues) => oldValues.map((item) => {
    if (item.groupEnrollmentId === groupEnrollmentId) {
      return { ...item, groupId: value };
    }
    return item;
  }));
};

export const studentListTableColumn = (
  translate: TFunction,
  courseGroupStudents: any,
  isEditableGroupStudentsState: any,
) => [
  {
    key: 'key',
    title: 'N°',
    dataIndex: 'key',
  },
  {
    key: 'cui',
    title: translate('cui'),
    dataIndex: 'cui',
    sorterType: 'alphabetical',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'enrollment',
    title: translate('enrollment'),
    dataIndex: 'enrollment',
    sorterType: 'numerical',
  },
  {
    key: 'status',
    title: translate('status'),
    dataIndex: 'status',
    sorterType: 'alphabetical',
    render: (item) => (
      <Tag color={enrolllmentStatusColor[item]} key={item}>
        {item}
      </Tag>
    ),
  },
  ...isEditableGroupStudentsState ? [
    {
      key: 'action',
      title: translate('group'),
      dataIndex: 'action',
      render: ({
        groupEnrollmentId, groupCourseStudent, status,
        setCourseGroupStudents, courseGroupsAvailable,
      }: any) => (
        status === ENROLLED ? (
          <Row gutter={[12, 0]}>
            <Col>
              <Radio.Group
                options={courseGroupsAvailable}
                onChange={
                  ({ target: { value } }:
                    RadioChangeEvent) => onChange(
                    value,
                    groupEnrollmentId,
                    setCourseGroupStudents,
                  )
                }
                value={courseGroupStudents
                  .find(
                    (item) => item.groupEnrollmentId === groupEnrollmentId,
                  ).groupId}
              />
            </Col>
          </Row>
        ) : groupCourseStudent
      ),
    },
  ] : [
    {
      key: 'groupCourseStudent',
      title: translate('group'),
      dataIndex: 'groupCourseStudent',
    },
  ],

];

export default {};
