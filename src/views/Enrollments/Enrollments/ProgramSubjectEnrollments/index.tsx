import React, { useEffect, useState } from 'react';
import {
  Col, Row, Button, message,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import {
  DeleteFilled, FilePdfOutlined, FormOutlined, PlusCircleOutlined,
} from '@ant-design/icons';
import { AppDispatch } from '../../../../store';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import TitleView from '../../../../components/TitleView';
import routesDictionary from '../../../../routes/routesDictionary';
import CustomButtonUploadFile from '../../../../components/CustomButtonUploadFile';
import { UNIT_DIRECTOR } from '../../../../utils/constants';
import CustomTable from '../../../../components/CustomTable';
import { studentListTableColumn } from './constants';
import UploadFileModal from '../../../../components/UploadFileModal';
import CustomFilter from '../../../../components/CustomFilter';
import CustomCard from '../../../../components/CustomCard';
import ConfirmationModal from '../../../../components/ConfirmationModal';
import { getMediaLink } from '../../../../utils/tools';
import EmptyCard from '../../../../components/EmptyCard';

const getCourseGroupsRequest = loader('./requests/getCourse.gql');
const getCourseStudentsRequest = loader('./requests/getCourseStudents.gql');
const setEnrollmentGroupRequest = loader('./requests/setEnrollmentGroup.gql');

const SubjectIdView = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const params = useParams();
  const { studyProgramId, subjectId } = params;
  const { userType } = useSelector((state: any) => state.auth);

  // Filters
  const [defaultValues, setDefaultValues] = useState<any>([]);

  // when assigned group students
  const [isAssignedGroupStudentsState, setIsAssignedGroupStudentsState] = useState<boolean>();
  const [isEditableGroupStudentsState, setIsEditableGroupStudentsState] = useState<boolean>(false);

  // assigned group of students
  const [courseGroupStudents, setCourseGroupStudents] = useState<any>();

  // groups available where students can be enrolled
  const [courseGroupsAvailable, setCourseGroupsAvailable] = useState<any>([]);

  // Data table
  const [courseStudents, setCourseStudents] = useState<any>([]);
  const [course, setCourse] = useState<any>(null);

  const [setEnrollmentGroupMutation, {
    loading: setEnrollmentGroupLoading,
  }] = useMutation(
    setEnrollmentGroupRequest,
  );

  const { data: subjectData, refetch } = useQuery(
    getCourseGroupsRequest,
    {
      variables: { courseId: subjectId },
      fetchPolicy: 'no-cache',
    },
  );

  const {
    data: courseStudentsData,
    loading: courseStudentsLoading,
    refetch: courseStudentsRefetch,
  } = useQuery(
    getCourseStudentsRequest,
    {
      variables: { courseId: subjectId },
      fetchPolicy: 'no-cache',
    },
  );

  const [uploadFileModalData, setUploadFileModalData] = useState<any>({
    isVisible: false,
    type: '',
    text: '',
    refetch: courseStudentsRefetch,
  });

  const handleAssignGroups = () => {
    setEnrollmentGroupMutation({
      variables: {
        input: courseGroupStudents,
      },
    })
      .then(({ data: setEnrollmentGroupData }) => {
        const { setEnrollmentGroup: resultData } = setEnrollmentGroupData;
        if (resultData.feedback.status === 'SUCCESS') {
          message.success(resultData.feedback.message);
          setIsEditableGroupStudentsState(false);
          setIsAssignedGroupStudentsState(true);
          courseStudentsRefetch();
        } else {
          message.error(resultData.feedback.message);
          courseStudentsRefetch();
        }
      });
  };

  const [fileUrl, setFileUrl] = useState<any>();

  useEffect(() => {
    if (subjectData) {
      const { getCourse } = subjectData;
      const { studyPlanSubject } = getCourse;
      setCourseGroupsAvailable(getCourse.coursegroupSet.map((item) => (
        {
          label: item.group,
          value: item.id,
        }
      )));

      setCourse({
        id: getCourse.id,
        courseName: studyPlanSubject.subject,
        hasGroups: getCourse.hasGroups,
        programName: studyPlanSubject.studyPlan.studyProgram.name,
      });
      setFileUrl(getCourse.enrollmentFile);
    }
  }, [subjectData]);

  useEffect(() => {
    if (courseStudentsData) {
      const { getCourseStudents } = courseStudentsData;
      const courseStudentsArray: any[] = [];
      const courseGroupStudentsArray: any[] = [];

      getCourseStudents?.forEach(({
        id, status, group, enrollmentNum, enrollment,
      }: any, index: any) => {
        courseStudentsArray.push({
          key: index + 1,
          cui: enrollment.student.cui,
          firstName: enrollment.student.user.firstName,
          lastName: `${enrollment.student.user.lastName} ${enrollment.student.user.maternalLastName}`,
          email: enrollment.student.user.email,
          enrollment: enrollmentNum,
          groupCourseStudent: group.group,
          status: enrollment.isEnrollmentUpdate === true ? 'Rematricula' : status,
          action: {
            groupEnrollmentId: id,
            groupCourseStudent: group.group,
            status,
            setCourseGroupStudents,
            courseGroupsAvailable,
          },
        });
        courseGroupStudentsArray.push({
          groupEnrollmentId: id,
          groupId: group.id,
        });
      });
      setIsAssignedGroupStudentsState(getCourseStudents[0]?.isAssigned);
      setCourseStudents(courseStudentsArray);
      setCourseGroupStudents(courseGroupStudentsArray);
      setDefaultValues(courseStudentsArray);
    }
  }, [courseStudentsData, isEditableGroupStudentsState]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('enrollments'),
          course?.programName,
          course?.courseName,
        ],
      ),
    ).then();
  }, [dispatch, course, t]);

  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: '',
  });

  if (course && !course.hasGroups && userType === UNIT_DIRECTOR) {
    return (
      <>
        <TitleView
          title={course.courseName}
          viewBackButton
          onClickBackButton={() => {
            navigate(
              routesDictionary.programEnrollments.func(studyProgramId),
              { state: { displaySubjects: true } },
            );
          }}
        />
        <EmptyCard
          title={t('emptyCourseGroupsTitle')}
          description={t('emptyCourseGroupsDescription')}
          action={(
            <Row align="middle" gutter={[0, 16]} justify="center">
              <Button
                type="primary"
                icon={<PlusCircleOutlined />}
                onClick={() => {
                  navigate(routesDictionary.subjectDetails.func(course.id));
                }}
              >
                {t('registerGroups')}
              </Button>
            </Row>
            )}
        />
      </>
    );
  }

  return (
    <div>
      <TitleView
        title={course && course.courseName}
        viewBackButton
        onClickBackButton={() => {
          navigate(
            routesDictionary.programEnrollments.func(studyProgramId),
            { state: { displaySubjects: true } },
          );
        }}
        action={(
          userType === UNIT_DIRECTOR
          && !courseStudentsLoading && (
          <Row gutter={[25, 10]}>
            <Col flex="auto">
              <CustomButtonUploadFile
                name={t('registerStudents')}
                onClick={() => {
                  setUploadFileModalData({
                    isVisible: true,
                    text: t('registerSubjectStudents'),
                    type: 'courseStudentsList',
                    refetch: () => {
                      refetch();
                      courseStudentsRefetch();
                    },
                  });
                }}
              />
            </Col>
          </Row>
          )
        )}
      />
      <CustomCard>
        <TitleView
          title={t('students')}
          viewBackButton={false}
          sizeAction={580}
          style={{ marginTop: 0 }}
          action={
            (userType === UNIT_DIRECTOR
                  && !isEditableGroupStudentsState
                    && defaultValues.length > 0) && (
                    <Row justify="end" gutter={[16, 16]}>
                        {fileUrl && (
                        <Col flex="250px">
                          <Button
                            type="primary"
                            icon={<FilePdfOutlined />}
                            // target="_blank"
                            href={getMediaLink(fileUrl)}
                          >
                            {t('downloadStudentsList')}
                          </Button>
                        </Col>
                        )}
                      <Col flex="250px">
                        <Button
                          type="primary"
                          style={{ width: 250 }}
                          icon={<FormOutlined />}
                          onClick={() => {
                            setIsEditableGroupStudentsState(true);
                          }}
                        >
                          {isAssignedGroupStudentsState
                            ? t('editGroups') : t('assignGroups')}
                        </Button>
                      </Col>
                      <Col flex="45px">
                        <Button
                          type="ghost"
                          className="delete-button"
                          style={{ width: '100%', height: '40px' }}
                          onClick={() => setConfirmationModalData({
                            ...confirmationModalData,
                            type: 'deleteCourseEnrollments',
                            isVisible: true,
                            courseId: subjectId,
                            refetch: () => {
                              refetch();
                              courseStudentsRefetch();
                            },
                          })}
                          icon={<DeleteFilled />}
                        />
                      </Col>
                    </Row>
            )
          }
        />
        <CustomFilter
          typeFilter="subjectStudentListFilters"
          setDataSource={setCourseStudents}
          defaultDataSource={defaultValues}
          courseGroupsAvailable={courseGroupsAvailable}
        />
        <CustomTable
          dataSource={courseStudents}
          loading={courseStudentsLoading}
          columns={studentListTableColumn(t, courseGroupStudents, isEditableGroupStudentsState)}
        />
        {isEditableGroupStudentsState
        && (
        <Row style={{ marginTop: '25px' }} gutter={[10, 10]} justify="end">
          <Col>
            <Button
              type="default"
              style={{ width: 200 }}
              onClick={() => {
                setIsEditableGroupStudentsState(false);
              }}
            >
              {t('cancel')}
            </Button>
          </Col>
          <Col>
            <Button
              type="primary"
              style={{ width: 200 }}
              onClick={handleAssignGroups}
              loading={setEnrollmentGroupLoading}
            >
              {t('save')}
            </Button>
          </Col>
        </Row>
        )}
      </CustomCard>
      <UploadFileModal
        uploadFileModalData={uploadFileModalData}
        setUploadFileModalData={setUploadFileModalData}
      />
      <ConfirmationModal
        confirmationModalData={confirmationModalData}
        setConfirmationModalData={setConfirmationModalData}
        sizeModal={520}
      />
    </div>
  );
};

export default SubjectIdView;
