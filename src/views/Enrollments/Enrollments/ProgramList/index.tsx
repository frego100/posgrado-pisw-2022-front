import React, { useEffect, useState } from 'react';
import {
  Row,
  Select,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { AppDispatch } from '../../../../store';
import { typeProgram } from '../../../../components/CustomTable/constants';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import TitleView from '../../../../components/TitleView';
import CustomTable from '../../../../components/CustomTable';
import { programListColumns } from './constants';
import { SCHOOL_DIRECTOR } from '../../../../utils/constants';
import CustomCard from '../../../../components/CustomCard';

interface studyProgram {
  key: string,
  code: string,
  type: string,
  name: string,
  actions: {
  }
}

const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getStudyProgramsRequest = loader('./requests/getStudyPrograms.gql');

const ProgramListView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();

  const [studyPrograms, setStudyPrograms] = useState<studyProgram[]>([]);
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId);

  const {
    data: graduateUnitsData, loading: graduateUnitsLoading,
  } = useQuery(getGraduateUnitsRequest, { skip: userType !== SCHOOL_DIRECTOR });

  const {
    data: studyProgramsData,
    loading: studyProgramsLoading,
  } = useQuery(
    getStudyProgramsRequest,
    {
      variables: {
        graduateUnit_Id: graduateUnit,
      },
      fetchPolicy: 'no-cache',
      skip: !graduateUnit,
    },
  );

  useEffect(() => {
    if (graduateUnitsData && userType === SCHOOL_DIRECTOR) {
      const { getGraduateUnits } = graduateUnitsData;
      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      if (!graduateUnit) setGraduateUnit(graduateUnitsArray[0]?.value);
    }
  }, [graduateUnitsData, dispatch]);

  useEffect(() => {
    if (studyProgramsData) {
      const { getStudyPrograms } = studyProgramsData;
      const studyProgramsArray: studyProgram[] = [];

      getStudyPrograms.edges.map(({ node } : any, index: any) => (
        studyProgramsArray.push({
          key: index + 1,
          code: node.code,
          type: typeProgram[node.programType],
          name: node.name,
          actions: {
            studyProgramId: node.id,
            navigate,
          },
        })
      ));
      setStudyPrograms(studyProgramsArray);
    }
  }, [studyProgramsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('enrollments'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  // if (studyProgramsLoading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={t('enrollments')}
        viewBackButton={false}
      />
      <CustomCard>
        {userType === SCHOOL_DIRECTOR && (
          <Row align="middle" style={{ width: '300px', marginBottom: '32px' }}>
            <span className="label">
              {t('graduateUnit')}
            </span>
            <Select
              labelInValue
              style={{ width: '100%' }}
              value={graduateUnit}
              onChange={(newValue) => setGraduateUnit(newValue.value)}
              options={graduateUnits}
              loading={graduateUnitsLoading}
            />
          </Row>
        )}
        <CustomTable
          columns={programListColumns(t)}
          dataSource={studyPrograms}
          loading={studyProgramsLoading}
        />
      </CustomCard>
    </div>
  );
};

export default ProgramListView;
