import React from 'react';
import { TFunction } from 'react-i18next';
import { Button } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../../routes/routesDictionary';

export const programListColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'name',
    title: translate('program'),
    dataIndex: 'name',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      studyProgramId, navigate,
    }: any) => (
      <Button
        icon={<EyeOutlined />}
        type="ghost"
        onClick={() => {
          navigate(routesDictionary.programEnrollments.func(studyProgramId));
        }}
      >
        {translate('view')}
      </Button>
    ),
  },
];

export default {};
