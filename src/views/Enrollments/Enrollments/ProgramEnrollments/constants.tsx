import React from 'react';
import { TFunction } from 'react-i18next';
import { Button, Tag } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../../routes/routesDictionary';

export const enrolllmentStatusColor = {
  Rematricula: 'geekblue',
  Matriculado: 'green',
  Abandono: 'volcano',
  Retiro: 'volcano',
};

export const subjectListTableEnrollmentColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectName',
    title: translate('name'),
    dataIndex: 'subjectName',
    sorterType: 'alphabetical',
  },
  {
    key: 'semester',
    title: translate('semester'),
    dataIndex: 'semester',
    sorterType: 'numerical',
  },
  {
    key: 'studentsNumber',
    title: translate('studentsNum'),
    dataIndex: 'studentsNumber',
    sorterType: 'numerical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      studyProgramSubjectId, studyProgramId, navigate,
    }: any) => (
      <Button
        icon={<EyeOutlined />}
        type="ghost"
        onClick={() => {
          navigate(
            routesDictionary
              .programSubjectEnrollments.func(studyProgramId, studyProgramSubjectId),
          );
        }}
      >
        {translate('viewStudents')}
      </Button>
    ),
  },
];

export const enrolledStudentsListTable = (translate: TFunction) => [
  {
    key: 'key',
    title: 'N°',
    dataIndex: 'key',
  },
  {
    key: 'cui',
    title: translate('cui'),
    dataIndex: 'cui',
    sorterType: 'alphabetical',
  },
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'dni',
    title: translate('dni'),
    dataIndex: 'dni',
    sorterType: 'alphabetical',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectsNumber',
    title: translate('courses'),
    dataIndex: 'subjectsNumber',
    sorterType: 'numerical',
  },
  {
    key: 'credits',
    title: translate('credits'),
    dataIndex: 'credits',
    sorterType: 'numerical',
  },
  {
    key: 'status',
    title: translate('status'),
    dataIndex: 'status',
    sorterType: 'alphabetical',
    render: (item) => (
      <Tag color={enrolllmentStatusColor[item]} key={item}>
        {item}
      </Tag>
    ),
  },
];
