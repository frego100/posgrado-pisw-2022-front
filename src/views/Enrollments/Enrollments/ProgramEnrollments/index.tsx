import React, { useEffect, useState } from 'react';
import {
  useLocation, useNavigate, useParams,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import {
  Button,
  Col, Row, Select, Tabs,
} from 'antd';
import { FilePdfOutlined, DeleteFilled } from '@ant-design/icons';
import { AppDispatch } from '../../../../store';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import TitleView from '../../../../components/TitleView';
import routesDictionary from '../../../../routes/routesDictionary';
import CustomButtonUploadFile from '../../../../components/CustomButtonUploadFile';
import './styles.scss';
import CustomFilter from '../../../../components/CustomFilter';
import CustomTable from '../../../../components/CustomTable';
import { UNIT_DIRECTOR } from '../../../../utils/constants';
import { enrolledStudentsListTable, subjectListTableEnrollmentColumns } from './constants';
import UploadFileModal from '../../../../components/UploadFileModal';
import CustomCard from '../../../../components/CustomCard';
import ConfirmationModal from '../../../../components/ConfirmationModal';
import { getMediaLink } from '../../../../utils/tools';
import EmptyView from '../../../../components/EmptyView';

interface Subject {
  key: string,
  code: string,
  name: string,
  semester: number,
  year: string,
  actions: {
  }
}

const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');
// get semester courses and enrollments by period
const getDataByPeriodRequest = loader('./requests/getDataByEnrollmentPeriod.gql');
const getEnrollmentPeriodsByProcessRequest = loader('./requests/getEnrollmentPeriodsByProcess.gql');
const getStudyProgramRequest = loader('./requests/getStudyProgram.gql');

const ProgramEnrollments = () => {
  const navigate = useNavigate();
  const params = useParams();
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const { studyProgramId } = params;
  const { userType } = useSelector((state: any) => state.auth);
  const location = useLocation();
  const subjectTab = location?.state?.displaySubjects;

  // processes
  const [processes, setProcesses] = useState<any>(null);
  const [process, setProcess] = useState<any>();

  // periods by process
  const [enrollmentPeriods, setEnrollmentPeriods] = useState<any>(null);
  const [enrollmentPeriod, setEnrollmentPeriod] = useState<any>();
  const [studyProgramName, setStudyProgramName] = useState<any>();

  // enrollments and courses by period
  const [enrollmentsByPeriod, setEnrollmentByPeriod] = useState<any>();
  const [semesterCourses, setSemesterCourses] = useState<Subject[]>([]);

  // filters
  const [defaultSubjectsValues, setDefaultSubjectsValues] = useState<any>([]);
  const [defaultStudentsValues, setDefaultStudentsValues] = useState<any>([]);

  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: '',
  });

  const { data: studyProgramData } = useQuery(
    getStudyProgramRequest,
    {
      variables: { studyProgramId },
      fetchPolicy: 'no-cache',
    },
  );
  useEffect(() => {
    if (studyProgramData) {
      const { getStudyProgram } = studyProgramData;
      setStudyProgramName(getStudyProgram?.name);
    }
  }, [studyProgramData]);

  const {
    data: getProcessesData,
    loading: getProcessesLoading,
    // refetch: getProcessesRefetch,
  } = useQuery(getOperatingPlanProcessesRequest);

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const operatingPlanProcessesArray = getOperatingPlanProcesses.map((item: any) => (
        { label: item.year, value: item.id }
      ));
      if (!process) setProcess(operatingPlanProcessesArray[0]);
      setProcesses(operatingPlanProcessesArray);
    }
  }, [getProcessesData]);

  const {
    data: periodsData,
    loading: periodsLoading,
    refetch: periodsRefetch,
  } = useQuery(
    getEnrollmentPeriodsByProcessRequest,
    {
      variables: {
        programId: studyProgramId,
        processId: process?.value,
      },
      fetchPolicy: 'no-cache',
      skip: !process,
    },
  );

  const [filesUrl, setFilesUrl] = useState<any>();
  useEffect(() => {
    if (periodsData) {
      const filesData = {};
      const { getEnrollmentPeriodsByProcess } = periodsData;
      const enrollmentPeriodsArray = getEnrollmentPeriodsByProcess?.map((item) => {
        filesData[item.id] = getMediaLink(item.enrollmentFile);
        return (
          { value: item.id, label: `${item.year}-${item.period}` }
        );
      });
      setFilesUrl(filesData);
      setEnrollmentPeriod(enrollmentPeriodsArray[0]);
      setEnrollmentPeriods(enrollmentPeriodsArray);
    }
  }, [periodsData]);

  const {
    data: coursesAndEnrollmentsData,
    loading: coursesAndEnrollmentsLoading,
    refetch: coursesAndEnrollmentsRefetch,
  } = useQuery(
    getDataByPeriodRequest,
    {
      variables: { enrollmentPeriodId: enrollmentPeriod?.value },
      fetchPolicy: 'no-cache',
      skip: !enrollmentPeriod,
    },
  );

  const [uploadFileModalData, setUploadFileModalData] = useState<any>({
    isVisible: false,
    type: '',
    text: '',
  });

  useEffect(() => {
    if (coursesAndEnrollmentsData) {
      const { getSemesterCourses, getEnrollmentsByPeriod } = coursesAndEnrollmentsData;
      const semesterCoursesArray: Subject[] = [];
      const enrollmentsPeriodArray: any[] = [];

      getSemesterCourses?.forEach(({
        id, studyPlanSubject, numberOfStudents,
      }: any, index: number) => {
        semesterCoursesArray.push({
          // @ts-ignore
          key: index + 1,
          code: studyPlanSubject.code,
          subjectName: studyPlanSubject.subject,
          semester: studyPlanSubject.semester,
          studentsNumber: numberOfStudents,
          actions: {
            studyProgramSubjectId: id,
            studyProgramId,
            navigate,
          },
        });
      });
      setSemesterCourses(semesterCoursesArray);
      setDefaultSubjectsValues(semesterCoursesArray);

      getEnrollmentsByPeriod?.forEach((enrollmentData: any, index: number) => {
        const { student, status, isEnrollmentUpdate } = enrollmentData;
        enrollmentsPeriodArray.push({
          // @ts-ignore
          key: index + 1,
          cui: student.cui,
          code: student.code,
          dni: student.user.useridentificationSet[0].number,
          firstName: student.user.firstName,
          lastName: `${student.user.lastName} ${student.user.maternalLastName}`,
          email: student.user.email,
          subjectsNumber: enrollmentData.courses,
          credits: enrollmentData.credits,
          status: isEnrollmentUpdate === true ? 'Rematricula' : status,
        });
      });
      setEnrollmentByPeriod(enrollmentsPeriodArray);
      setDefaultStudentsValues(enrollmentsPeriodArray);
    } else {
      setSemesterCourses(null);
      setDefaultSubjectsValues(null);
      setEnrollmentByPeriod(null);
      setDefaultStudentsValues(null);
    }
  }, [coursesAndEnrollmentsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('enrollments'),
          studyProgramName,
        ],
      ),
    ).then();
  }, [dispatch, studyProgramName, t]);

  if (processes && processes.length === 0) {
    return (
      <EmptyView
        type="emptyOperatingPlanProcess"
        title={studyProgramName}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.enrollments.route);
        }}
        userType={userType}
      />
    );
  }

  return (
    <div>
      <TitleView
        title={studyProgramName}
        viewBackButton
        sizeAction={205}
        onClickBackButton={() => {
          navigate(routesDictionary.enrollments.route);
        }}
        action={(
          <Row justify="end" gutter={[10, 10]}>
            <Col className="label" span={12}>
              <span className="label">
                {t('academicYear')}
              </span>
              <Select
                labelInValue
                value={process}
                style={{ width: '100%' }}
                onChange={(newValue) => setProcess(newValue)}
                options={processes}
                loading={getProcessesLoading}
              />
            </Col>
            <Col className="label" span={12}>
              <span className="label">
                {t('period')}
              </span>
              <Select
                labelInValue
                value={enrollmentPeriod}
                style={{ width: '100%' }}
                onChange={(newValue) => setEnrollmentPeriod(newValue)}
                options={enrollmentPeriods}
                loading={periodsLoading}
              />
            </Col>
          </Row>
        )}
      />
      { enrollmentPeriods && enrollmentPeriods.length === 0
        ? (
          <CustomCard>
            <EmptyView
              type="emptyEnrollmentPeriods"
              userType={userType}
              viewTitle={false}
            />
          </CustomCard>
        ) : (
          <Tabs
            type="card"
            defaultActiveKey={subjectTab ? '2' : '1'}
        // activeKey={activeTab}
        // onChange={(value) => setActiveTab(value)}
            items={[
              {
                label: t('enrolled'),
                key: '1',
                children: (
                  <CustomCard className="tabContainer">
                    <TitleView
                      title={t('enrolled')}
                      viewBackButton={false}
                      sizeAction={520}
                      action={processes?.length > 0 && (
                        <Row justify="end" gutter={[10, 10]}>
                          { defaultStudentsValues?.length > 0 && (
                          <Col flex="250px">
                            <Button
                              type="primary"
                              style={{ width: '100%' }}
                              icon={<FilePdfOutlined />}
                              href={filesUrl[enrollmentPeriod?.value]}
                            >
                              {t('downloadStudentsList')}
                            </Button>
                          </Col>
                          )}
                          {userType === UNIT_DIRECTOR && (
                          <>
                            <Col flex="200px">
                              <CustomButtonUploadFile
                                name={t('registerStudents')}
                                onClick={() => {
                                  setUploadFileModalData({
                                    ...uploadFileModalData,
                                    isVisible: true,
                                    text: t('registerProgramStudents'),
                                    type: 'programStudentsList',
                                    period: enrollmentPeriod,
                                    year: process?.label,
                                    coursesAndEnrollmentsRefetch,
                                    periodsRefetch,
                                  });
                                }}
                              />
                            </Col>
                            { defaultStudentsValues?.length > 0 && (
                            <Col flex="45px">
                              <Button
                                type="ghost"
                                className="delete-button"
                                style={{ width: '100%', height: '40px' }}
                                onClick={() => setConfirmationModalData({
                                  ...confirmationModalData,
                                  type: 'deleteStudentProgramList',
                                  isVisible: true,
                                  period: enrollmentPeriod,
                                  refetch: coursesAndEnrollmentsRefetch,
                                })}
                                icon={<DeleteFilled />}
                              />
                            </Col>
                            )}
                          </>
                          )}
                        </Row>
                      )}
                    />
                    <CustomFilter
                      typeFilter="studentListFilters"
                      setDataSource={setEnrollmentByPeriod}
                      defaultDataSource={defaultStudentsValues}
                    />
                    <CustomTable
                      columns={enrolledStudentsListTable(t)}
                      dataSource={enrollmentsByPeriod}
                      loading={coursesAndEnrollmentsLoading}
                    />
                    <UploadFileModal
                      uploadFileModalData={uploadFileModalData}
                      setUploadFileModalData={setUploadFileModalData}
                    />
                    <ConfirmationModal
                      confirmationModalData={confirmationModalData}
                      setConfirmationModalData={setConfirmationModalData}
                      sizeModal={520}
                    />
                  </CustomCard>
                ),
              },
              {
                label: t('subjects'),
                key: '2',
                children: (
                  <CustomCard className="tabContainer">
                    <TitleView
                      title={t('subjects')}
                      viewBackButton={false}
                      sizeAction={400}
                    />
                    <CustomFilter
                      typeFilter="subjectListFilters"
                      setDataSource={setSemesterCourses}
                      defaultDataSource={defaultSubjectsValues}
                    />
                    <CustomTable
                      columns={subjectListTableEnrollmentColumns(t)}
                      dataSource={semesterCourses}
                      loading={coursesAndEnrollmentsLoading}
                    />
                  </CustomCard>
                ),
              },
            ]}
          />
        )}
    </div>
  );
};

export default ProgramEnrollments;
