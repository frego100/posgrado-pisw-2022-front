import { TFunction } from 'react-i18next';
import { EyeOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React from 'react';
import routesDictionary from '../../../../routes/routesDictionary';

export default {};

export const enrollmentRemovalsColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'program',
    title: translate('program'),
    dataIndex: 'program',
    sorterType: 'alphabetical',
  },
  {
    key: 'student',
    title: translate('student'),
    dataIndex: 'student',
    sorterType: 'alphabetical',
  },
  {
    key: 'withdrawType',
    title: translate('withdrawType'),
    dataIndex: 'withdrawType',
    sorterType: 'alphabetical',
  },
  {
    key: 'date',
    title: translate('date'),
    dataIndex: 'date',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      navigate, enrollmentRemovalId, studyProgramName, process, processes,
    }: any) => (
      <Button
        icon={<EyeOutlined />}
        type="ghost"
        onClick={() => navigate(
          routesDictionary.enrollmentRemovalStudent.func(enrollmentRemovalId),
          { state: { studyProgramName, process, processes } },
        )}
      >
        {translate('view')}
      </Button>
    ),
  },
];
export const enrollmentUpdateListColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'student',
    title: translate('student'),
    dataIndex: 'student',
    sorterType: 'alphabetical',
  },
  {
    key: 'date',
    title: translate('date'),
    dataIndex: 'date',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      navigate, id, studyProgramName, process, processes,
    }: any) => (
      <Button
        icon={<EyeOutlined />}
        type="ghost"
        onClick={() => navigate(
          routesDictionary.enrollmentUpdateStudent.func(id),
          { state: { studyProgramName, process, processes } },
        )}
      >
        {translate('view')}
      </Button>
    ),
  },
];
