import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import {
  Button, Col, Row, Select,
} from 'antd';
import moment from 'moment/moment';
import TitleView from '../../../../components/TitleView';
import CustomCard from '../../../../components/CustomCard';
import { AppDispatch } from '../../../../store';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../../utils/constants';
import CustomTable from '../../../../components/CustomTable';
import { enrollmentRemovalsColumns, enrollmentUpdateListColumns } from './constants';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import routesDictionary from '../../../../routes/routesDictionary';
import { isUndefined } from '../../../../utils/tools';
import EmptyView from '../../../../components/EmptyView';

const getGraduateUnitsRequest = loader('../../Enrollments/ProgramList/requests/getGraduateUnits.gql');
const getStudyProgramsRequest = loader('../../Enrollments/ProgramList/requests/getStudyPrograms.gql');
const getEnrollmentUpdatesRequest = loader('./requests/getEnrollmentUpdates.gql');
const getEnrollmentRemovalsRequest = loader('./requests/getEnrollmentRemovals.gql');
const getProcessesRequest = loader('./requests/getProcesses.gql');

const EnrollmentUpdateList = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);

  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId);
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const [studyPrograms, setStudyPrograms] = useState<any>([]);
  const [studyProgram, setStudyProgram] = useState<any>();
  const [studyProgramName, setStudyProgramName] = useState<any>();

  const { pathname } = useLocation();

  const [isEnrollmentUpdateView] = useState(pathname === '/matricula-reactualizacion');

  const [processes, setProcesses] = useState<any>(null);
  const [process, setProcess] = useState<any>();
  const {
    data: getProcessesData, loading: getProcessesLoading,
  } = useQuery(
    getProcessesRequest,
  );

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const processesArray = getOperatingPlanProcesses.map((item) => (
        { value: item.id, label: item.year }
      ));
      setProcesses(processesArray);
      setProcess(processesArray[0]?.value);
    }
  }, [getProcessesData]);

  const {
    data: graduateUnitsData, loading: graduateUnitsLoading,
  } = useQuery(
    getGraduateUnitsRequest,
    { skip: userType !== SCHOOL_DIRECTOR },
  );

  const {
    data: studyProgramsData,
    loading: studyProgramsLoading,
  } = useQuery(
    getStudyProgramsRequest,
    {
      variables: {
        graduateUnit_Id: graduateUnit,
      },
      fetchPolicy: 'no-cache',
      skip: !graduateUnit,
    },
  );

  useEffect(() => {
    if (graduateUnitsData && userType === SCHOOL_DIRECTOR) {
      const { getGraduateUnits } = graduateUnitsData;
      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      if (!graduateUnit) setGraduateUnit(graduateUnitsArray[0]?.value);
    }
  }, [graduateUnitsData]);

  useEffect(() => {
    if (studyProgramsData) {
      const { getStudyPrograms } = studyProgramsData;
      const studyProgramsArray = getStudyPrograms.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setStudyPrograms(studyProgramsArray);
      setStudyProgram(studyProgramsArray[0].value);
      setStudyProgramName(studyProgramsArray[0].label);
    }
  }, [studyProgramsData]);

  const [enrollmentUpdates, setEnrollmentUpdates] = useState([]);
  const {
    data: getEnrollmentUpdatesData, loading: getEnrollmentUpdatesLoading,
  } = useQuery(
    getEnrollmentUpdatesRequest,
    {
      variables: {
        programId: studyProgram,
        processId: process,
      },
      fetchPolicy: 'no-cache',
      skip: !(isEnrollmentUpdateView && !isUndefined(studyProgram)),
    },
  );

  const {
    data: getEnrollmentRemovalsData, loading: getEnrollmentRemovalsLoading,
  } = useQuery(
    getEnrollmentRemovalsRequest,
    {
      variables: {
        // programId: studyProgram,
        graduateUnit,
        process,
      },
      fetchPolicy: 'no-cache',
      skip: !(!isEnrollmentUpdateView && !isUndefined(studyProgram)),
    },
  );

  const [enrollmentRemovals, setEnrollmentRemovals] = useState([]);

  useEffect(() => {
    if (getEnrollmentRemovalsData) {
      const { getEnrollmentRemovals } = getEnrollmentRemovalsData;
      const enrollmentUpdatesArray: any[] = [];
      getEnrollmentRemovals.forEach((item : any, index: any) => {
        enrollmentUpdatesArray.push({
          key: index + 1,
          program: item.enrollment.enrollmentPeriod.program.name,
          student: `${item.enrollment.student.user.firstName} ${item.enrollment.student.user.lastName} ${item.enrollment.student.user.maternalLastName}`,
          date: moment(item.created).format('YYYY-MM-DD'),
          withdrawType: item.withdrawType,
          actions: {
            navigate,
            enrollmentRemovalId: item.id,
            studyProgramName: item.enrollment.enrollmentPeriod.program.name,
            process,
            processes,
            // studyProgramId: item.enrollment.enrollmentPeriod.program.id,
            // id: item.id,
          },
        });
      });
      setEnrollmentRemovals(enrollmentUpdatesArray);
    }
  }, [getEnrollmentRemovalsData]);

  useEffect(() => {
    if (getEnrollmentUpdatesData) {
      const { getEnrollmentUpdates } = getEnrollmentUpdatesData;
      const enrollmentUpdatesArray: any[] = [];

      getEnrollmentUpdates.forEach((item : any, index: any) => {
        enrollmentUpdatesArray.push({
          key: index + 1,
          program: item.studyProgram.name,
          student: `${item.student.user.firstName} ${item.student.user.lastName} ${item.student.user.maternalLastName}`,
          date: moment(item.created).format('YYYY-MM-DD'),
          actions: {
            navigate,
            id: item.id,
            studyProgramName: item.studyProgram.name,
            process,
            processes,
            // studyProgramId: item.enrollment.enrollmentPeriod.program.id,
          },
        });
      });
      setEnrollmentUpdates(enrollmentUpdatesArray);
    }
  }, [getEnrollmentUpdatesData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t(isEnrollmentUpdateView ? 'enrollmentUpdate' : 'enrollmentRemoval'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  if (processes && processes.length === 0) {
    return (
      <EmptyView
        type="emptyOperatingPlanProcess"
        title={t(isEnrollmentUpdateView ? 'enrollmentUpdate' : 'enrollmentRemoval')}
        viewBackButton={false}
        userType={userType}
      />
    );
  }

  return (
    <div>
      <TitleView
        title={t(isEnrollmentUpdateView ? 'enrollmentUpdate' : 'enrollmentRemoval')}
        viewBackButton={false}
        action={(
          <Row align="middle">
            <span className="label">
              {t('academicYear')}
            </span>
            <Select
              labelInValue
              style={{ width: '100%' }}
              value={process}
              onChange={(newValue) => setProcess(newValue.value)}
              options={processes}
              loading={getProcessesLoading}
            />
          </Row>
        )}
      />
      <CustomCard>
        <Row gutter={[16, 16]} style={{ marginBottom: 32 }}>
          {userType === SCHOOL_DIRECTOR && (
          <Col span={12}>
            <Row align="middle">
              <span className="label">
                {t('graduateUnit')}
              </span>
              <Select
                labelInValue
                style={{ width: '100%' }}
                value={graduateUnit}
                onChange={(newValue) => setGraduateUnit(newValue.value)}
                options={graduateUnits}
                loading={graduateUnitsLoading}
              />
            </Row>
          </Col>
          )}
          <Col span={12}>
            <Row align="middle">
              <span className="label">
                {t('studyProgram')}
              </span>
              <Select
                labelInValue
                style={{ width: '100%' }}
                value={studyProgram}
                onChange={(newValue) => {
                  setStudyProgramName(newValue?.label);
                  setStudyProgram(newValue?.value);
                }}
                options={studyPrograms}
                loading={studyProgramsLoading}
              />
            </Row>
          </Col>
          {userType === UNIT_DIRECTOR && (
          <Col span={12}>
            <Row align="middle" style={{ marginTop: 19 }}>
              <Button
                type="primary"
                style={{ marginLeft: 'auto' }}
                onClick={() => (isEnrollmentUpdateView ? navigate(
                  routesDictionary.createEnrollmentUpdate.route,
                  {
                    state: {
                      studyProgramName, studyProgram, process, processes,
                    },
                  },
                ) : navigate(
                  routesDictionary.createEnrollmentRemoval.route,
                  {
                    state: {
                      studyProgramName, studyProgram, processes, process,
                    },
                  },
                ))}
              >
                {t(isEnrollmentUpdateView ? 'addUpdate' : 'addRemoval')}
              </Button>
            </Row>
          </Col>
          )}
        </Row>
        <CustomTable
          columns={isEnrollmentUpdateView
            ? enrollmentUpdateListColumns(t) : enrollmentRemovalsColumns(t)}
          dataSource={isEnrollmentUpdateView ? enrollmentUpdates : enrollmentRemovals}
          loading={getEnrollmentUpdatesLoading || getEnrollmentRemovalsLoading}
        />
      </CustomCard>
    </div>
  );
};

export default EnrollmentUpdateList;
