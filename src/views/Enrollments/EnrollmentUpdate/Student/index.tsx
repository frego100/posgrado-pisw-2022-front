import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { AppDispatch } from '../../../../store';
import TitleView from '../../../../components/TitleView';
import routesDictionary from '../../../../routes/routesDictionary';
import StudentView from '../../../../components/StudentView';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import { isUndefined } from '../../../../utils/tools';

const EnrollmentUpdateStudent = () => {
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const params = useParams();
  const { enrollmentUpdateId, enrollmentRemovalId } = params;
  const { pathname } = useLocation();
  const location = useLocation();

  const studyProgramName = location?.state?.studyProgramName;
  const process = location?.state?.process;
  const processes = location?.state?.processes;

  const [isEnrollmentUpdateView] = useState(pathname.includes('/matricula-reactualizacion'));

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t(isEnrollmentUpdateView ? 'enrollmentUpdate' : 'enrollmentRemoval'),
        ],
      ),
    ).then();
  }, [dispatch, t]);
  return (
    <div>
      <TitleView
        title={`${studyProgramName} - ${processes?.find((item) => item.value === process).label}`}
        viewBackButton
        onClickBackButton={() => {
          navigate(
            isEnrollmentUpdateView
              ? routesDictionary.enrollmentUpdate.route
              : routesDictionary.enrollmentRemoval.route,
          );
        }}
      />
      <StudentView
        updateEnrollment={isEnrollmentUpdateView}
        createStudent
        readOnly={!isUndefined(enrollmentUpdateId) || !isUndefined(enrollmentRemovalId)}
      />
    </div>
  );
};

export default EnrollmentUpdateStudent;
