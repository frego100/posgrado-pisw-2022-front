import React, {
  Dispatch, SetStateAction, useEffect, useState,
} from 'react';
import {
  Button, Divider, Form, Input, Row,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useApolloClient, useMutation } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useDispatch } from 'react-redux';
import { useForm } from 'antd/es/form/Form';
import { ReactComponent as LogoPosgradoImage } from '../../../media/images/LogoPosgrado.svg';
import { ReactComponent as LogoUnsaImage } from '../../../media/images/LogoUnsa.svg';
import { ReactComponent as SocialNetworksImage } from '../../../media/images/SocialNetworks.svg';
import GoogleLoginButton from '../../../components/GoogleLoginButton';
import { startGoogleSignIn, startLogin } from '../../../store/thunks/auth';
import { AppDispatch } from '../../../store';
import routesDictionary from '../../../routes/routesDictionary';
import CustomLoader from '../../../components/CustomLoader';
import './styles.scss';

const loginPasswordRequest = loader('./requests/loginPasswordMutation.gql');
const googleAuthLinkRequest = loader('./requests/getGoogleAuthLink.gql');
const loginGoogleRequest = loader('./requests/loginGoogleMutation.gql');

interface LoginProps {
  setLogin: Dispatch<SetStateAction<string | null>>
}

const Login : React.FC<LoginProps> = ({ setLogin }) => {
  const { t } = useTranslation();
  const [searchParams] = useSearchParams();
  const dispatch = useDispatch<AppDispatch>();
  const client = useApolloClient();
  const navigate = useNavigate();
  const [error, setError] = useState<string>();
  const [errorForm, setErrorForm] = useState<string>();
  const [form] = useForm();
  const [googleLoading, setGoogleLoading] = useState<boolean>(false);
  const [loginGoogleMutation, { loading }] = useMutation(loginGoogleRequest);
  const code = searchParams.get('code');
  const [loginPasswordMutation,
    { loading: loginPasswordLoading },
  ] = useMutation(loginPasswordRequest);

  useEffect(() => {
    if (code) {
      loginGoogleMutation(
        {
          variables: { code },
        },
      ).then(({ data }) => {
        dispatch(startGoogleSignIn(data))
          .then(() => setLogin(data.login.token));
      })
        .catch((e) => {
          setError(e.message);
          navigate(routesDictionary.login, { replace: true });
        });
    }
  }, [client, code, dispatch]);

  if (loading || code) {
    return (
      <div className="vh-100">
        <CustomLoader />
      </div>
    );
  }

  const handleGoogleLogin = () => {
    setGoogleLoading(true);
    client.query(
      {
        query: googleAuthLinkRequest,
        fetchPolicy: 'no-cache',
      },
    ).then(({ data }) => {
      const { getGoogleOauthLink } = data;
      window.location.href = getGoogleOauthLink;
      setGoogleLoading(false);
    }).catch((e) => {
      // eslint-disable-next-line no-console
      console.log(e);
    });
  };

  const onValuesChange = () => {
    if (errorForm) {
      setErrorForm('');
    }
    if (error) {
      setError('');
    }
  };

  const handleLoginForm = (values) => {
    loginPasswordMutation({
      variables: values,
    })
      .then(({ data }) => {
        dispatch(startLogin(client, data.tokenAuth.token))
          .then(() => {
            setLogin(data.tokenAuth.token);
          });
      })
      .catch((e) => {
        setErrorForm(e.message);
        navigate(routesDictionary.login, { replace: true });
      });
  };

  return (
    <Row className="login-view">
      <Row className="login-view__logo-posgrado-container" justify="end" align="top">
        <LogoPosgradoImage className="login-view__logo-posgrado" />
      </Row>
      <LogoUnsaImage className="login-view__logo-unsa" />
      <div className="login-view__unsa-image" />
      <div className="login-view__form">
        {/* <Row className="headline" justify="center"> */}
        {/*   {t('applicationName')} */}
        {/* </Row> */}
        {/* <div className="login-view__line" /> */}
        <Row justify="center" className="subline">
          {t('welcome')}
        </Row>
        <Row justify="center" gutter={[20, 0]}>
          <Form
            name="basic"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            autoComplete="off"
            form={form}
            style={{ minWidth: 335 }}
            onValuesChange={onValuesChange}
            onFinish={handleLoginForm}
          >
            <Form.Item
              label={t('email')}
              name="email"
              rules={[
                { required: true, message: t('requiredEmail') },
                { type: 'email', message: t('noValidEmail') },
              ]}
              normalize={(value) => value.trim()}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label={t('password')}
              name="password"
              rules={[{ required: true, message: t('requiredPassword') }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{ offset: 0, span: 24 }}
            >
              <Button
                type="primary"
                loading={loginPasswordLoading}
                style={{ width: '100%', marginBottom: '.25rem' }}
                disabled={!!errorForm}
                htmlType="submit"
              >
                {t('login')}
              </Button>
            </Form.Item>
            {errorForm
              && (
                <Row justify="center">
                  <span className="login-view__error">
                    {errorForm}
                  </span>
                </Row>
              )}
          </Form>
        </Row>
        <Divider />
        <Row justify="center">
          <GoogleLoginButton
            loading={googleLoading}
            onClickFunc={handleGoogleLogin}
            title={t('loginGoogle')}
          />
        </Row>
        {error
            && (
            <Row justify="center">
              <span className="login-view__error login-view__error--google">
                {error}
              </span>
            </Row>
            )}
      </div>
      <Row className="login-view__social-networks-container" justify="start">
        <SocialNetworksImage className="login-view__social-networks" />
        <div className="login-view__bottom-line" />
      </Row>
    </Row>
  );
};

export default Login;
