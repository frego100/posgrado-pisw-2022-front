import { Button, Col, Row } from 'antd';
import React from 'react';
import { TFunction } from 'react-i18next';
import { EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../routes/routesDictionary';

export const enrollmentList = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'subject',
    title: translate('subject'),
    dataIndex: 'subject',
    sorterType: 'alphabetical',
  },
  {
    key: 'group',
    title: translate('group'),
    dataIndex: 'group',
    sorterType: 'alphabetical',
  },
  {
    key: 'enrollmentNum',
    title: translate('enrollment'),
    dataIndex: 'enrollmentNum',
    sorterType: 'numerical',
  },
  {
    key: 'status',
    title: translate('status'),
    dataIndex: 'status',
  },
  {
    key: 'grade',
    title: translate('grade'),
    dataIndex: 'grade',
    sorterType: 'numerical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      // eslint-disable-next-line no-unused-vars
      navigate, courseGroupId, subject, group,
    }: any) => (
      <Row gutter={[12, 0]}>
        <Col>
          <Button
            type="ghost"
            icon={<EyeOutlined />}
            onClick={() => {
              navigate(
                routesDictionary.studentSubject.func(courseGroupId),
                { state: { course: { subject, group } } },
              );
            }}
          >
            {translate('view')}
          </Button>
        </Col>
      </Row>
    ),
  },
];

export default {};
