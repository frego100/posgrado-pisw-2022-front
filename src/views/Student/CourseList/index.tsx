import React, { useEffect, useState } from 'react';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import {
  Col, Row, Select,
} from 'antd';
import TitleView from '../../../components/TitleView';
import CustomTable from '../../../components/CustomTable';
import { enrollmentList } from './constants';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import routesDictionary from '../../../routes/routesDictionary';
import { AppDispatch } from '../../../store';
import CustomCard from '../../../components/CustomCard';

const getStudentEnrollmentProcessesRequest = loader('./requests/getStudentEnrollmentProcesses.gql');
const getStudentEnrollmentsRequest = loader('./requests/getStudentEnrollments.gql');
const getStudentGroupEnrollmentsRequest = loader('./requests/getStudentGroupEnrollments.gql');

const EnrollmentList = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { studentId } = useSelector((state: any) => state.auth);

  const [processes, setProcesses] = useState<any>();
  const [process, setProcess] = useState<any>({ value: '' });

  // one enrollment selected from studentEnrollmentsRemoval
  const [studentEnrollment, setStudentEnrollment] = useState<any>();

  // list of all enrolled programs by process, (every program with withdrawals)
  const [studentEnrollments, setStudentEnrollments] = useState<any>([]);

  // list of all enrolled courses by program (or enrollmentID)
  const [studentEnrollmentList, setStudentEnrollmentList] = useState<any>([]);

  const { data: enrollmentProcessesData, loading: enrollmentProcessesLoading } = useQuery(
    getStudentEnrollmentProcessesRequest,
    {
      fetchPolicy: 'no-cache',
    },
  );

  const {
    data: studentEnrollmentsData,
    loading: studentEnrollmentsLoading,
  } = useQuery(
    getStudentEnrollmentsRequest,
    {
      variables: {
        studentId,
        process: process?.value,
      },
      fetchPolicy: 'no-cache',
      skip: !process?.value ?? !studentId,
    },
  );

  const {
    data: groupsEnrollmentData,
    loading: groupsEnrollmentLoading,
  } = useQuery(
    getStudentGroupEnrollmentsRequest,
    {
      variables: { enrollmentId: studentEnrollment?.value },
      fetchPolicy: 'no-cache',
      skip: !(studentEnrollment?.value),
    },
  );

  useEffect(() => {
    if (enrollmentProcessesData) {
      const { getStudentEnrollmentProcesses } = enrollmentProcessesData;
      const enrollmentProcessesArray = getStudentEnrollmentProcesses.map(
        ({ id, year }: any) => ({ value: id, label: year }),
      );
      setProcess(enrollmentProcessesArray[0]);
      setProcesses(enrollmentProcessesArray);
    }
  }, [enrollmentProcessesData]);

  useEffect(() => {
    if (studentEnrollmentsData) {
      const { getStudentEnrollments } = studentEnrollmentsData;
      const enrollmentListArray = getStudentEnrollments.map((element: any) => ({
        value: element.id,
        label: element.enrollmentPeriod.program.name,
      }));
      setStudentEnrollments(enrollmentListArray);
      setStudentEnrollment(enrollmentListArray[0]);
    }
  }, [studentEnrollmentsData]);

  useEffect(() => {
    if (groupsEnrollmentData) {
      const { getStudentGroupEnrollments } = groupsEnrollmentData;
      const groupsEnrollmentListArray: any = [];
      getStudentGroupEnrollments.forEach(({
        id, group, enrollmentNum, status, studentgrade,
      }: any, index: number) => {
        groupsEnrollmentListArray.push({
          id,
          key: index + 1,
          subject: group.course.studyPlanSubject.subject,
          group: group.group,
          enrollmentNum,
          status,
          grade: studentgrade ? studentgrade.score : '-',
          actions: {
            navigate,
            courseGroupId: group.id,
            subject: group.course.studyPlanSubject.subject,
            group: group.group,
          },
        });
      });
      setStudentEnrollmentList(groupsEnrollmentListArray);
    }
  }, [groupsEnrollmentData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.studentSubjects.breadCrumb.map((text) => t(text)),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('myCourses')}
        viewBackButton={false}
        sizeAction={200}
        action={(
          <Row align="middle" gutter={[25, 0]} justify="end" style={{ marginBottom: '32px' }}>
            <Col flex="200px">
              <span className="label">
                {t('academicYear')}
              </span>
            </Col>
            <Col flex="200px">
              <Select
                labelInValue
                value={process?.value}
                style={{ width: '100%' }}
                options={processes}
                loading={enrollmentProcessesLoading}
                onChange={(newValue) => setProcess(newValue)}
              />
            </Col>
          </Row>
          )}
      />
      <CustomCard>
        <Row align="middle" gutter={[25, 0]} style={{ marginBottom: '32px' }}>
          <Col xs={24} md={10}>
            <Row>
              <Col span={24}>
                <span className="label">
                  {t('program')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={studentEnrollment}
                  options={studentEnrollments}
                  loading={studentEnrollmentsLoading}
                  labelInValue
                  onChange={(newValue) => setStudentEnrollment(newValue)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <CustomTable
          dataSource={studentEnrollmentList}
          loading={groupsEnrollmentLoading}
          columns={enrollmentList(t)}
        />
      </CustomCard>
    </div>
  );
};

export default EnrollmentList;
