import React, { useState } from 'react';
// import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import {
  Modal, Row, Card, Radio, RadioChangeEvent, message,
} from 'antd';
import { loader } from 'graphql.macro';
import { useMutation } from '@apollo/client';
import { plainOptions } from './constants';
import './styles.scss';

interface AttendanceModalProps {
  attendanceData: any,
  setAttendanceData: any,
}

const studentCourseAttendanceRegistrationRequest = loader('./requests/StudentCourseAttendanceRegistration.gql');

const AttendanceModal: React.FC<AttendanceModalProps> = ({
  attendanceData, setAttendanceData,
}) => {
  // const [form] = useForm();
  const { t } = useTranslation();

  const [attendanceStatus, setAttendanceStatus] = useState<boolean>(attendanceData.attendance || 'ON_TIME');

  const [studentCourseAttendanceMutation] = useMutation(studentCourseAttendanceRegistrationRequest);

  const handleOk = () => {
    studentCourseAttendanceMutation({
      variables: {
        input: {
          studentCourseAttendanceId: attendanceData.studentCourseAttendanceId,
          attendance: attendanceStatus,
        },
      },
    }).then(({ data }: any) => {
      const { studentCourseAttendanceRegistration: attendanceRegistration } = data;
      if (attendanceRegistration.feedback.status === 'SUCCESS') {
        setAttendanceData({ ...attendanceData, isVisible: false });
        message.success(attendanceRegistration.feedback.message);
        attendanceData.refetch();
      } else {
        message.error(attendanceRegistration.feedback.message);
      }
    });
  };

  const handleCancel = () => {
    setAttendanceData({ ...attendanceData, isVisible: false });
  };

  const updateAttendance = ({ target: { value } }: RadioChangeEvent) => {
    setAttendanceStatus(value);
  };

  return (
    <Modal
      key="attendanceStudentModal"
      title={t('attendance')}
      closable
      open={attendanceData.isVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      centered
      okText={t('registerAttendance')}
      cancelText={t('cancel')}
    >
      <p>{t('registerAttendanceText')}</p>
      <Row gutter={[25, 20]} justify="center">
        <Card size="small" className="attendanceCard">
          <Radio.Group
            options={plainOptions(t)}
            value={attendanceStatus}
            onChange={updateAttendance}
          />
        </Card>
      </Row>
    </Modal>
  );
};

export default AttendanceModal;
