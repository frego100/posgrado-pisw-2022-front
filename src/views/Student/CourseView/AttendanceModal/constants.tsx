import { TFunction } from 'react-i18next';

export const attendaceLabel = {
  ON_TIME: 'present',
  ABSENT: 'absent',
  LATE: 'late',
  EXCUSED_ABSENCE: 'excusedAbsence',
};

export const optionLabel = {
  1: 'ON_TIME',
  2: 'ABSENT',
  3: 'LATE',
  4: 'EXCUSED_ABSENCE',
};

export const plainOptions = (t: TFunction) => [
  {
    label: t('present'),
    value: 'ON_TIME',
  },
  {
    label: t('absent'),
    value: 'ABSENT',
  },
  {
    label: t('late'),
    value: 'LATE',
  },
  {
    label: t('excusedAbsence'),
    value: 'EXCUSED_ABSENCE',
  },
];

export default {};
