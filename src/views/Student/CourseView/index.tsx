import { Card } from 'antd';
import { t } from 'i18next';
import React, { useState, useEffect } from 'react';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useDispatch } from 'react-redux';
import { useNavigate, useLocation, useParams } from 'react-router-dom';
import CustomTable from '../../../components/CustomTable';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import { AttendanceList } from './constants';
import AttendanceModal from './AttendanceModal';
import { optionLabel } from './AttendanceModal/constants';

const getStudentAttendancesRequest = loader('./requests/getStudentAttendances.gql');

const CourseView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const location = useLocation();
  const params = useParams();
  const { courseGroupId } = params;
  const course = location?.state?.course;
  const [studentAttendances, setStudentAtendances] = useState<any>();

  const {
    data: studentAttendancesData,
    loading: studentAttendancesLoading,
    refetch: studentAttendanceRefetch,
  } = useQuery(
    getStudentAttendancesRequest,
    {
      variables: { courseGroupId },
      fetchPolicy: 'no-cache',
      skip: !courseGroupId,
    },
  );

  const [attendanceModal, setAttendanceModal] = useState<any>(
    { isVisible: false, refetch: studentAttendanceRefetch },
  );

  useEffect(() => {
    if (studentAttendancesData) {
      const { getStudentAttendances } = studentAttendancesData;
      const studenAttendancesArray: any[] = [];
      getStudentAttendances.forEach((item) => {
        const { academicSession } = item;
        studenAttendancesArray.push({
          topic: academicSession.courseprogressSet?.tittle,
          date: academicSession.date,
          action: {
            studentCourseAttendanceId: item.id,
            attendance: optionLabel[item.attendance],
            isEnabled: item.isEditableAttendance,
            date: academicSession.date,
            attendanceModal,
            setAttendanceModal,
          },
        });
      });
      const today = new Date();
      setStudentAtendances(studenAttendancesArray.filter((item) => new Date(item.date) < today));
    }
  }, [studentAttendancesData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('myCourses'),
          t('attendance'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={`${course?.subject} - ${course.group}`}
        viewBackButton
        onClickBackButton={() => navigate(routesDictionary.studentSubjects.route)}
      />
      <Card className="custom-card">
        <TitleView
          title={t('attendance')}
          viewBackButton={false}
        />
        <CustomTable
          dataSource={studentAttendances}
          loading={studentAttendancesLoading}
          columns={AttendanceList(t)}
        />
        <AttendanceModal
          attendanceData={attendanceModal}
          setAttendanceData={setAttendanceModal}
        />
      </Card>
    </div>
  );
};

export default CourseView;
