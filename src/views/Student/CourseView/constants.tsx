import {
  Button,
} from 'antd';
import React from 'react';
import { TFunction } from 'react-i18next';
import { isToday } from '../../../utils/tools';
import { attendaceLabel } from './AttendanceModal/constants';

export const AttendanceList = (t: TFunction) => [
  {
    key: 'topic',
    title: t('topic'),
    dataIndex: 'topic',
    sorterType: 'alphabetical',
  },
  {
    key: 'date',
    title: t('date'),
    dataIndex: 'date',
    sorterType: 'alphabetical',
  },
  {
    key: 'action',
    title: t('attendance'),
    dataIndex: 'action',
    render: ({
      studentCourseAttendanceId, attendance, isEnabled, date,
      attendanceModal, setAttendanceModal,
    }) => (
      isEnabled && isToday(date) ? (
        <Button
          type="ghost"
          onClick={() => {
            setAttendanceModal({
              ...attendanceModal,
              attendance,
              studentCourseAttendanceId,
              isVisible: true,
            });
          }}
        >
          {t('registerAttendance')}
        </Button>
      ) : attendance ? t(attendaceLabel[attendance]) : ' ? '
    ),
  },
];

export default {};
