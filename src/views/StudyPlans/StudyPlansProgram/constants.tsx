import { TFunction } from 'react-i18next';
import { STUDY_PLAN_REQUEST_OBSERVED, studyPlanRequestBackend } from './StudyPlanRequestModal/constants';
import { UNIT_DIRECTOR } from '../../../utils/constants';

export const getStudyPlanRequestData = (studyPlanRequest, refetch, userType) => ({
  visible: false,
  ...studyPlanRequest,
  ...studyPlanRequest?.status && {
    status: studyPlanRequestBackend[studyPlanRequest.status],
  },
  refetch,
  ...(studyPlanRequest?.studyPlanFile
      && studyPlanRequestBackend[studyPlanRequest?.status] === STUDY_PLAN_REQUEST_OBSERVED
      && userType === UNIT_DIRECTOR) && {
    studyPlanFile: studyPlanRequest?.studyPlanFile?.split('/')[3],
  },
});

export const studyPlansSubjectColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectName',
    title: translate('name'),
    dataIndex: 'subjectName',
    sorterType: 'alphabetical',
  },
  {
    key: 'credits',
    title: translate('credits'),
    dataIndex: 'credits',
    sorterType: 'numerical',
  },
  {
    key: 'hours',
    title: translate('totalHours'),
    dataIndex: 'hours',
    sorterType: 'numerical',
  },
  {
    key: 'semester',
    title: translate('semester'),
    dataIndex: 'semester',
    sorterType: 'numerical',
  },
];
export default {};
