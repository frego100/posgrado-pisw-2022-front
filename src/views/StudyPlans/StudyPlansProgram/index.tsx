import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  Button,
  Card, Col, Row, Select,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { FormOutlined } from '@ant-design/icons';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import CustomFilter from '../../../components/CustomFilter';
import CustomTable from '../../../components/CustomTable';
import { getStudyPlanRequestData, studyPlansSubjectColumns } from './constants';
import StudyPlanRequestModal from './StudyPlanRequestModal';
import { UNIT_DIRECTOR } from '../../../utils/constants';
import {
  STUDY_PLAN_REQUEST_APPROVED,
  STUDY_PLAN_REQUEST_REJECTED,
} from './StudyPlanRequestModal/constants';

const { Option } = Select;

const getStudyPlansRequest = loader('./requests/getStudyPlans.gql');
const getSubjectsByStudyPlanRequest = loader('./requests/getSubjectsByStudyPlan.gql');

const StudyProgramIdView = () => {
  const navigate = useNavigate();
  const params = useParams();

  const { studyProgramId } = params;
  const [studyProgramName, setStudyProgramName] = useState<any>();
  const [studyPlanId, setStudyPlanId] = useState<any>();
  const [studyPlans, setStudyPlans] = useState<any>([]);
  const [studyPlanRequests, setStudyPlanRequests] = useState<any>([]);

  const [subjects, setSubjects] = useState<any>([]);
  const [defaultStudyPlansValues, setDefaultStudyPlansValues] = useState<any>([]);
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  const [requestModalData, setRequestModalData] = useState<any>({ visible: false });
  const { userType } = useSelector((state: any) => state.auth);

  const [modalData, setModalData] = useState<any>({});

  const handleChangeStudyPlan = (value: any) => {
    setStudyPlanId(value);
  };

  const {
    data: studyPlansData, refetch,
  } = useQuery(
    getStudyPlansRequest,
    {
      variables: { studyProgramId },
      fetchPolicy: 'no-cache',
    },
  );

  useEffect(() => {
    if (studyPlansData) {
      const { getStudyPlans, getStudyPlanRequest } = studyPlansData;
      // const { getStudyPlans, getStudyPlanRequest } = studyPlansData;
      const studyPlansArray: any = [];
      const studyPlanRequestsDict: any = {};

      getStudyPlans.forEach(({
        id, year, studyProgram, studyPlanRequest,
      }: any) => {
        setStudyProgramName(studyProgram?.name);
        studyPlansArray.push(
          <Option key={id}>{ year }</Option>,
        );
        studyPlanRequestsDict[id] = studyPlanRequest;
      });
      setStudyPlanId(getStudyPlans[0]?.id);
      setStudyPlans(studyPlansArray);
      setStudyPlanRequests(studyPlanRequestsDict);
      setRequestModalData(getStudyPlanRequestData(getStudyPlanRequest, refetch, userType));
      // setRequestModalData({
      //   visible: false,
      //   ...getStudyPlanRequest,
      //   ...getStudyPlanRequest?.status && {
      //     status: studyPlanRequestBackend[getStudyPlanRequest.status],
      //   },
      //   refetch,
      //   ...(getStudyPlanRequest?.studyPlanFile
      //       && studyPlanRequestBackend[getStudyPlanRequest?.status]
      //       === STUDY_PLAN_REQUEST_OBSERVED
      //       && userType === UNIT_DIRECTOR) && {
      //     studyPlanFile: getStudyPlanRequest?.studyPlanFile?.split('/')[3],
      //   },
      // });
    }
  }, [studyPlansData]);

  const {
    data: studyPlanSubjectsData,
    loading: studyPlanSubjectsLoading,
  } = useQuery(
    getSubjectsByStudyPlanRequest,
    {
      variables: { studyPlanId },
      fetchPolicy: 'no-cache',
      skip: !studyPlanId,
    },
  );

  useEffect(() => {
    if (studyPlanSubjectsData) {
      const { getStudyPlanSubjects } = studyPlanSubjectsData;
      const subjectsArray: any = [];
      getStudyPlanSubjects.forEach(({
        subject, credits, semester, hours, code,
      }: any, index: number) => {
        subjectsArray.push({
          // @ts-ignore
          key: index + 1,
          // to do
          code,
          subjectName: subject,
          credits,
          hours,
          semester,
        });
      });
      setSubjects(subjectsArray);
      setDefaultStudyPlansValues(subjectsArray);
    }
  }, [studyPlanSubjectsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('studyPlans'),
          studyProgramName,
        ],
      ),
    ).then();
  }, [dispatch, studyProgramName, t]);
  if (studyPlanSubjectsLoading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={studyProgramName}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.studyPlans.route);
        }}
        sizeAction={(userType === UNIT_DIRECTOR) ? 500 : 250}
        action={(
          <Row align="middle" justify="end" gutter={[25, 25]} style={{ marginBottom: '32px' }}>
            {(userType === UNIT_DIRECTOR
              && (!requestModalData.status
              || requestModalData.status === STUDY_PLAN_REQUEST_APPROVED
              || requestModalData.status === STUDY_PLAN_REQUEST_REJECTED)) && (
              <Col flex="250px">
                <br />
                <Button
                  type="primary"
                  style={{ width: '100%' }}
                  icon={<FormOutlined />}
                  onClick={() => {
                    setModalData({ refetch, sendStudyPlan: true, visible: true });
                  }}
                >
                  {t('requestUpdate')}
                </Button>
              </Col>
            )}
            <Col flex="250px">
              <span className="label">
                {t('studyPlan')}
              </span>
              <Select
                style={{ width: '100%' }}
                defaultValue={studyPlanId}
                onChange={handleChangeStudyPlan}
              >
                {studyPlans}
              </Select>
            </Col>
          </Row>
        )}
      />
      <Card className="custom-card">
        <TitleView
          title={t('studyPlan')}
          viewBackButton={false}
          action={(studyPlanRequests[studyPlanId]?.status
              || (!(requestModalData.status === STUDY_PLAN_REQUEST_APPROVED
              || requestModalData.status === STUDY_PLAN_REQUEST_REJECTED)
                  && requestModalData.status)) && (
                  <Row align="middle" gutter={[0, 16]} justify="end">
                    <Button
                      type="primary"
                      icon={<FormOutlined />}
                      onClick={() => setModalData({
                        ...studyPlanRequests[studyPlanId] ? getStudyPlanRequestData(
                          studyPlanRequests[studyPlanId],
                          refetch,
                          userType,
                        ) : requestModalData,
                        visible: true,
                      })}
                    >
                      {t((!requestModalData.status && userType === UNIT_DIRECTOR) ? 'requestUpdate' : 'viewRequest')}
                    </Button>
                  </Row>
          )}
        />
        <CustomFilter
          typeFilter="subjectListFilters"
          operatingPlanStatus=""
          setDataSource={setSubjects}
          defaultDataSource={defaultStudyPlansValues}
        />
        <CustomTable
          columns={studyPlansSubjectColumns(t)}
          dataSource={subjects}
          loading={studyPlanSubjectsLoading}
        />
        <StudyPlanRequestModal
          modalData={modalData}
          setModalData={setModalData}
          userType={userType}
        />
      </Card>
    </div>
  );
};

export default StudyProgramIdView;
