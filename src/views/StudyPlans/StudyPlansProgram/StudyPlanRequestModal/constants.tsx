import React from 'react';
import { FilePdfOutlined, PaperClipOutlined } from '@ant-design/icons';
import { Button, Select } from 'antd';
import { TFunction } from 'react-i18next';
import TextArea from 'antd/lib/input/TextArea';
import { generateUrlDownload, isUndefined } from '../../../../utils/tools';
import CustomUploadFile from '../../../../components/CustomUploadFile';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../../utils/constants';

export const STUDY_PLAN_REQUEST_IN_PROGRESS = 'IN PROGRESS';
export const STUDY_PLAN_REQUEST_SEND = 'SENT';
export const STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR = 'ACADEMIC_REVIEW';
export const STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS = 'BOARD_REVIEW';
export const STUDY_PLAN_REQUEST_OBSERVED = 'OBSERVED';
export const STUDY_PLAN_REQUEST_APPROVED = 'APPROVED';
export const STUDY_PLAN_REQUEST_REJECTED = 'REJECTED';

const { Option } = Select;

export const studyPlanRequestBackend:{ [index: string]: string} = {
  Enviado: STUDY_PLAN_REQUEST_SEND,
  'En Revision: Subdireccion Academica': STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR,
  'En Revision: Consejo Directivo': STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS,
  Observado: STUDY_PLAN_REQUEST_OBSERVED,
  Rechazado: STUDY_PLAN_REQUEST_REJECTED,
  Aprobado: STUDY_PLAN_REQUEST_APPROVED,
};

const displayOptions = {
  [STUDY_PLAN_REQUEST_SEND]: [
    STUDY_PLAN_REQUEST_SEND,
    STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR],
  [STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR]: [
    STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR,
    STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS, STUDY_PLAN_REQUEST_OBSERVED],
  [STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS]: [
    STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS,
    STUDY_PLAN_REQUEST_REJECTED, STUDY_PLAN_REQUEST_APPROVED],
  [STUDY_PLAN_REQUEST_OBSERVED]: [STUDY_PLAN_REQUEST_OBSERVED,
    STUDY_PLAN_REQUEST_SEND],
};
const studyPlanRequestStatus = [
  {
    value: STUDY_PLAN_REQUEST_SEND,
    label: 'Recibido',
  },
  {
    value: STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR,
    label: 'En Revision: Subdireccion Academica',
  },
  {
    value: STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS,
    label: 'En Revision: Consejo Directivo',
  },
  {
    value: STUDY_PLAN_REQUEST_REJECTED,
    label: 'Rechazar',
  },
  {
    value: STUDY_PLAN_REQUEST_OBSERVED,
    label: 'Observar',
  },
  {
    value: STUDY_PLAN_REQUEST_APPROVED,
    label: 'Aprobar',
  },
];
const responseItems = (
  form: any,
  readOnly: boolean,
  responseData: any,
  t: TFunction,
  userType: any,
  statusValue: any,
) => [
  {
    name: 'resolutionFile',
    component: (
      readOnly
        ? (
          <Button
            type="primary"
            target="_blank"
            style={{ width: '100%' }}
            href={generateUrlDownload(responseData.resolutionFile)}
            icon={<FilePdfOutlined />}
            disabled={responseData.status === STUDY_PLAN_REQUEST_OBSERVED}
          >
            {t('download')}
          </Button>
        )
        : (
          <CustomUploadFile
            disabled={readOnly}
            form={form}
            nameComponent="resolutionFile"
            urlFile={responseData.resolutionFile
                && generateUrlDownload(responseData.resolutionFile)}
          />
        )
    ),
    rules: [
      () => ({
        validator() {
          const fieldValue = form.getFieldValue(['resolutionFile']);
          if (fieldValue) {
            return Promise.resolve();
          }
          return Promise.reject(new Error('¡Campo obligatorio!'));
        },
      }),
    ],
    label: t(statusValue ? (statusValue === STUDY_PLAN_REQUEST_APPROVED ? 'approvalResolution' : 'rejectedResolution') : responseData.status === STUDY_PLAN_REQUEST_APPROVED ? 'approvalResolution' : 'rejectedResolution'),
    responsive: {
      xs: { span: 24 },
      md: { span: 10 },
    },
    display:
        ([STUDY_PLAN_REQUEST_APPROVED, STUDY_PLAN_REQUEST_REJECTED].includes(responseData.status))
        || (responseData.status === STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS
            && [STUDY_PLAN_REQUEST_APPROVED, STUDY_PLAN_REQUEST_REJECTED].includes(statusValue)),
  },
  {
    name: 'observation',
    component: (
      <TextArea
        rows={5}
        disabled={
        userType === UNIT_DIRECTOR
            || (userType === SCHOOL_DIRECTOR && responseData.status === STUDY_PLAN_REQUEST_OBSERVED
            )
        }
        autoSize={{
          minRows: 4,
          maxRows: 6,
        }}
      />
    ),
    rules: [
      {
        required: userType === SCHOOL_DIRECTOR,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('observations'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: (responseData.status === STUDY_PLAN_REQUEST_OBSERVED)
        || (statusValue === STUDY_PLAN_REQUEST_OBSERVED),
  },
];
export const requestItems = (
  form: any,
  isEditable: boolean,
  requestData: any,
  t: TFunction,
  userType: any,
  statusValue: any,
  templateStudyPlan: any,
) => [
  {
    name: 'studyPlanFile',
    component: (
      !isEditable
        ? (
          <Button
            type="primary"
            target="_blank"
            style={{ width: '100%' }}
            href={generateUrlDownload(requestData.studyPlanFile)}
            icon={<FilePdfOutlined />}
          >
            {t('download')}
          </Button>
        )
        : (
          <CustomUploadFile
            disabled={!isEditable}
            form={form}
            typeFile=".xlsx"
            nameComponent="studyPlanFile"
            urlFile={requestData.studyPlanFile && generateUrlDownload(requestData.studyPlanFile)}
          />
        )
    ),
    rules: [
      () => ({
        validator() {
          const fieldValue = form.getFieldValue(['studyPlanFile']);
          if (userType !== UNIT_DIRECTOR) {
            return Promise.resolve();
          }
          if (requestData.status === STUDY_PLAN_REQUEST_OBSERVED && typeof fieldValue === 'string') {
            return Promise.reject(new Error('¡Actualiza el documento!'));
          }
          if (fieldValue) {
            return Promise.resolve();
          }
          return Promise.reject(new Error('¡Campo obligatorio!'));
        },
      }),
    ],
    label: t('studyPlan'),
    responsive: {
      xs: { span: 24 },
      md: { span: 10 },
    },
    display: true,
  },
  {
    name: 'status',
    component: (
      <Select className="component">
        {studyPlanRequestStatus.map((item) => {
          if (!displayOptions[statusValue]?.includes(item.value)) {
            // eslint-disable-next-line react/jsx-no-useless-fragment
            return <></>;
          }
          return (
            <Option
              key={item.value}
              value={item.value}
              disabled={statusValue === item.value}
            >
              {item.label}
            </Option>
          );
        })}
      </Select>
    ),
    rules: [
      {
        required: userType === SCHOOL_DIRECTOR,
        message: 'Campo obligatorio!',
      },
      () => ({
        validator(_: any, value: any) {
          if (value !== statusValue) {
            return Promise.resolve();
          }
          return Promise.reject(new Error('¡Actualice el estado!'));
        },
      }),
    ],
    display: (
      userType === SCHOOL_DIRECTOR
        && ![STUDY_PLAN_REQUEST_OBSERVED, STUDY_PLAN_REQUEST_REJECTED, STUDY_PLAN_REQUEST_APPROVED]
          .includes(requestData.status)),
    label: t('status'),
    responsive: {
      xs: { span: 24 },
      md: { span: 10 },
    },
  },
  {
    name: 'studyPlanTemplate',
    component: (
      <a
        className="link-component"
        target="_blank"
        href={generateUrlDownload(templateStudyPlan?.templateFile)}
        rel="noreferrer"
      >
        <PaperClipOutlined />
        {' '}
        {t('downloadStudyPlanTemplate')}
      </a>
    ),
    label: ' ',
    rules: [
      {
        required: false,
      },
    ],
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: userType === UNIT_DIRECTOR,
  },
  {
    name: 'summary',
    component: (
      <TextArea
        rows={5}
        disabled={!isEditable}
        autoSize={{
          minRows: 4,
          maxRows: 6,
        }}
      />
    ),
    rules: [
      {
        required: userType === UNIT_DIRECTOR,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('summaryChanges'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
];

export const formItems = (
  form: any,
  modalData: any,
  t: TFunction,
  userType: any,
  statusValue: any,
  templateStudyPlan: any,
) => [
  ...requestItems(
    form,
    userType === UNIT_DIRECTOR && (modalData.status === STUDY_PLAN_REQUEST_OBSERVED
      || isUndefined(modalData.status)),
    modalData,
    t,
    userType,
    modalData.status,
    templateStudyPlan,
  ),
  ...responseItems(
    form,
    userType === UNIT_DIRECTOR
      || !(modalData.status === STUDY_PLAN_REQUEST_SEND
          || modalData.status === STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS),
    modalData,
    t,
    userType,
    statusValue,
  ),
];
export default {};
