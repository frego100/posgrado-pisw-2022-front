import React, { useEffect, useState } from 'react';
import {
  Alert,
  Button, Col, Form, message, Modal, Row,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { useForm } from 'antd/lib/form/Form';
import FormCard from '../../../../components/FormCard';
import {
  formItems,
  STUDY_PLAN_REQUEST_APPROVED,
  STUDY_PLAN_REQUEST_OBSERVED, STUDY_PLAN_REQUEST_REJECTED,
  STUDY_PLAN_REQUEST_SEND,
  STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR,
  STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS,
} from './constants';
import {
  UNIT_DIRECTOR,
} from '../../../../utils/constants';
import { alertStatus, typeStatus } from '../../../../components/CustomFilter/constants';
import { isUndefined } from '../../../../utils/tools';

interface StudyPlanRequestModalProps {
  modalData: any,
  setModalData: any,
  userType: any
}

const sendStudyPlanRequest = loader('./requests/sendStudyPlanRequest.gql');
const changeStudyPlanRequestStatus = loader('./requests/changeStudyPlanRequestStatus.gql');
const getTemplateByCodeRequest = loader('./requests/getTemplateByCode.gql');

const StudyPlanRequestModal:React.FC<StudyPlanRequestModalProps> = (
  {
    modalData, setModalData, userType,
  },
) => {
  const { t } = useTranslation();
  const params = useParams();
  const { studyProgramId } = params;
  const [form] = useForm();
  const statusValue = Form.useWatch('status', form);
  const [templateStudyPlan, setTemplateStudyPlan] = useState<any>();
  const templateCode = 'PE';

  const [
    sendStudyPlanRequestMutation,
    { loading: sendStudyPlanRequestLoading }] = useMutation(sendStudyPlanRequest);
  const [changeStudyPlanRequestStatusMutation,
    { loading: changeStudyPlanRequestLoading },
  ] = useMutation(changeStudyPlanRequestStatus);

  const { data: templateData } = useQuery(
    getTemplateByCodeRequest,
    {
      variables: { templateCode },
      fetchPolicy: 'no-cache',
    },
  );

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue(modalData);
  }, [modalData]);

  useEffect(() => {
    if (templateData) {
      const { getTemplateByCode } = templateData;
      setTemplateStudyPlan(getTemplateByCode);
    }
  }, [templateData]);

  // eslint-disable-next-line no-unused-vars
  const onFinish = (values: any) => {
  };

  const handleCancel = () => {
    setModalData({ ...modalData, visible: false });
    // form.resetFields();
  };

  const handleChangeStudyPlanRequestMutation = (variables: any) => {
    changeStudyPlanRequestStatusMutation({
      variables,
    })
      .then(({ data }) => {
        const { changeStudyPlanRequestStatus: resultData } = data;
        if (resultData.feedback.status === 'SUCCESS') {
          modalData.refetch();
          setModalData({ visible: false });
          message.success(resultData.feedback.message);
        }
      });
  };

  const handleUpdateStateStudyPlanRequest = () => {
    form.validateFields()
      .then((values) => {
        if (statusValue === STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR
                    || statusValue === STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS) {
          handleChangeStudyPlanRequestMutation({
            studyPlanRequestId: modalData.id,
            status: statusValue,
            observation: '',
          });
        } else if (statusValue === STUDY_PLAN_REQUEST_OBSERVED) {
          handleChangeStudyPlanRequestMutation({
            studyPlanRequestId: modalData.id,
            observation: values.observation || '',
            status: statusValue,
          });
        } else if (statusValue === STUDY_PLAN_REQUEST_APPROVED
                    || statusValue === STUDY_PLAN_REQUEST_REJECTED) {
          handleChangeStudyPlanRequestMutation({
            studyPlanRequestId: modalData.id,
            resolutionFile: values.resolutionFile.originFileObj,
            status: statusValue,
            observation: '',
          });
        }
      });
  };

  const handleSendStudyPlanRequest = () => {
    form.validateFields()
      .then((values) => {
        if (modalData.status === STUDY_PLAN_REQUEST_OBSERVED) {
          handleChangeStudyPlanRequestMutation({
            studyPlanRequestId: modalData.id,
            observation: values.observation || '',
            status: 'SENT',
          });
        } else {
          sendStudyPlanRequestMutation({
            variables: {
              input: {
                program: studyProgramId,
                studyPlanFile: values.studyPlanFile.originFileObj,
                summary: values.summary,
              },
            },
          })
            .then(({ data }) => {
              const { sendStudyPlanRequest: resultData } = data;
              if (resultData.feedback.status === 'SUCCESS') {
                modalData.refetch();
                setModalData({ visible: false });
                message.success(resultData.feedback.message);
              }
            });
        }
      });
  };

  return (
    <Modal
      title={t('updateStudyPlan')}
      centered
      width={900}
      closable
      destroyOnClose
      open={modalData.visible}
      onCancel={handleCancel}
      footer={[
        <Button key="back" onClick={handleCancel}>
          {t('cancel')}
        </Button>,
        ...(userType === UNIT_DIRECTOR
          ? [
            isUndefined(modalData.status)
            || modalData.status === STUDY_PLAN_REQUEST_OBSERVED
              ? (
                <Button key="observe" type="primary" onClick={handleSendStudyPlanRequest} loading={sendStudyPlanRequestLoading}>
                  {t('send')}
                </Button>
              )
              : (
                <Button key="observe" type="primary" onClick={handleCancel}>
                  {t('accept')}
                </Button>
              ),
          ]
          : ((modalData.status === STUDY_PLAN_REQUEST_SEND
                || modalData.status === STUDY_PLAN_REQUEST_SEND_ACADEMIC_DIRECTOR
                || modalData.status === STUDY_PLAN_REQUEST_SEND_BOARD_DIRECTORS
          )
            ? [
              <Button key="observe" type="primary" onClick={handleUpdateStateStudyPlanRequest} loading={changeStudyPlanRequestLoading}>
                {t('update')}
              </Button>,
            ]
            : [
              <Button key="observe" type="primary" onClick={handleCancel}>
                {t('accept')}
              </Button>,
            ])
        ),
      ]}
    >
      <Form
        name="updateStudyPlan"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        onFinish={onFinish}
        autoComplete="off"
        scrollToFirstError
        form={form}
        // initialValues={modalData.sendStudyPlan ? {} : modalData}
      >
        {modalData.status && (
          <Row justify="end" gutter={[0, 4]} style={{ minWidth: 250, maxWidth: 300, marginLeft: 8 }}>
            <Col className="label" span={24}>
              <span className="label">
                {t('status')}
              </span>
            </Col>
            <Col span={24}>
              <Alert
                message={(
                  <span className="status">
                    {typeStatus(userType)[modalData.status] }
                  </span>
                          )}
                type={alertStatus[modalData.status]}
                showIcon
              />
            </Col>
          </Row>
        )}
        <FormCard
          withoutTitle
          withoutDeleteIcon
          itemList={formItems(
            form,
            modalData,
            t,
            userType,
            statusValue,
            templateStudyPlan,
          )}
          readonly={false}
        />
      </Form>
    </Modal>
  );
};

export default StudyPlanRequestModal;
