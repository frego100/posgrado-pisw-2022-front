import React, { useEffect, useState } from 'react';
import {
  Row,
  Select,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { AppDispatch } from '../../../store';
import { SCHOOL_DIRECTOR } from '../../../utils/constants';
import { typeProgram } from '../../../components/CustomTable/constants';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
// import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import CustomTable from '../../../components/CustomTable';
import { studyProgramsColumns } from './constants';
import CustomCard from '../../../components/CustomCard';

const { Option } = Select;

interface studyProgram {
  key: string,
  code: string,
  type: string,
  name: string,
  actions: {
  }
}

const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getStudyProgramsRequest = loader('./requests/getStudyPrograms.gql');

const EntrantsView = () => {
  const navigate = useNavigate();

  const [studyPrograms, setStudyPrograms] = useState<studyProgram[]>([]);
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);
  const [graduateUnit, setGraduateUnit] = useState<any>({
    id: graduateUnitId,
    name: '',
  });
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();

  const handleChangeGraduateUnit = (value: string) => {
    setGraduateUnit({
      id: value,
      name: graduateUnit.name,
    });
  };

  const {
    data: graduateUnitsData,
  } = useQuery(getGraduateUnitsRequest, { skip: userType !== SCHOOL_DIRECTOR });

  const {
    data: studyProgramsData,
    loading: studyProgramsLoading,
  } = useQuery(
    getStudyProgramsRequest,
    {
      variables: {
        graduateUnit_Id: graduateUnit.id,
      },
      fetchPolicy: 'no-cache',
      skip: !graduateUnit.id,
    },
  );

  useEffect(() => {
    if (graduateUnitsData && userType === SCHOOL_DIRECTOR) {
      const { getGraduateUnits } = graduateUnitsData;
      const graduateUnitsArray: any = [];

      getGraduateUnits.edges.forEach(({ node }: any) => {
        graduateUnitsArray.push(
          <Option key={node.id}>{node.name}</Option>,
        );
      });
      setGraduateUnits(graduateUnitsArray);
      if (graduateUnit.id === undefined) {
        setGraduateUnit({
          id: getGraduateUnits.edges[0]?.node.id,
          name: getGraduateUnits.edges[0]?.node.name,
        });
      }
    }
  }, [graduateUnitsData, dispatch]);

  useEffect(() => {
    if (studyProgramsData) {
      const { getStudyPrograms } = studyProgramsData;
      const studyProgramsArray: studyProgram[] = [];

      getStudyPrograms.edges.map(({ node } : any, index: any) => (
        studyProgramsArray.push({
          key: index + 1,
          code: node.code,
          type: typeProgram[node.programType],
          name: node.name,
          actions: {
            studyProgramId: node.id,
            navigate,
          },
        })
      ));
      setStudyPrograms(studyProgramsArray);
    }
  }, [studyProgramsData, dispatch]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('studyPlans'),
          // graduateUnit.name,
        ],
      ),
    ).then();
  }, [dispatch, t]);

  // if (studyProgramsLoading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={t('studyPlans')}
        viewBackButton={false}
      />
      <CustomCard>
        {userType === SCHOOL_DIRECTOR ? (
          <Row align="middle" style={{ width: '300px', marginBottom: '32px' }}>
            <span className="label">
              {t('graduateUnit')}
            </span>
            <Select
              style={{ width: '100%' }}
              value={graduateUnit.id}
              onChange={handleChangeGraduateUnit}
            >
              {graduateUnits}
            </Select>
          </Row>
        ) : null}
        <CustomTable
          columns={studyProgramsColumns(t)}
          dataSource={studyPrograms}
          loading={studyProgramsLoading}
        />
      </CustomCard>
    </div>
  );
};

export default EntrantsView;
