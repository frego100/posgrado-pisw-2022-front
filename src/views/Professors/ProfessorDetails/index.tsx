import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useLocation, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import ProfessorView from '../../../components/ProfessorView';
import CustomLoader from '../../../components/CustomLoader';
import { AppDispatch } from '../../../store';
import routesDictionary from '../../../routes/routesDictionary';
import { SCHOOL_DIRECTOR } from '../../../utils/constants';

const getOperatingPlanNameRequest = loader('./requests/getOperatingPlanName.gql');

const ProfessorId = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  // const params = useParams();
  const { userType } = useSelector((state: any) => state.auth);
  const location = useLocation();
  const studyProgramId = location?.state?.studyProgramId;
  const operatingPlanId = location?.state?.operatingPlanId;

  // const { professorRegistrationId } = params;
  const [studyProgram, setStudyProgram] = useState<any>();
  // const [professorName, setProfessorName] = useState<string>();
  const { data, loading } = useQuery(
    getOperatingPlanNameRequest,
    {
      variables: { programOpPlanId: studyProgramId },
      fetchPolicy: 'no-cache',
    },
  );
  useEffect(() => {
    if (data) {
      const { getProgramOperatingPlan } = data;
      // const { user } = professor;
      setStudyProgram(getProgramOperatingPlan.studyProgram?.name);
    }
  }, [data]);
  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('professors'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  if (loading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={studyProgram}
        viewBackButton
        sizeAction={userType === SCHOOL_DIRECTOR ? 150 : 0}
        action={<div />}
        onClickBackButton={() => {
          navigate(routesDictionary.professors.route);
        }}
      />
      <ProfessorView
        readonly={userType === SCHOOL_DIRECTOR}
        createProfessor={false}
        backProfessorListView
        studyProgramId={studyProgramId}
        graduateUnitId={operatingPlanId}
      />
    </div>
  );
};

export default ProfessorId;
