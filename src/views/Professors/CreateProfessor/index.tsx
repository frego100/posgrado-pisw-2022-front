import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import ProfessorView from '../../../components/ProfessorView';

const getProgramOperatingPlanRequest = loader('./requests/getProgramOperatingPlan.gql');

const CreateProfessorView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const [operatingPlanData, setOperatingPlanData] = useState<any>();
  const params = useParams();
  const { programOperatingPlanId } = params;
  const { t } = useTranslation();

  const { data, loading } = useQuery(
    getProgramOperatingPlanRequest,
    {
      variables: { operatingPlanId: programOperatingPlanId },
      fetchPolicy: 'no-cache',
    },
  );
  useEffect(() => {
    if (data) {
      const { getProgramOperatingPlan } = data;
      setOperatingPlanData(getProgramOperatingPlan);
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('professors'),
          t('newProfessor'),
        ],
      ),
    ).then();
  }, [dispatch, t, operatingPlanData]);

  if (loading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={operatingPlanData?.studyProgram.name}
        viewBackButton
        onClickBackButton={() => {
          navigate(
            routesDictionary.professors.route,
          );
        }}
      />
      <ProfessorView
        readonly={false}
        createProfessor
        backProfessorListView
      />
    </div>
  );
};

export default CreateProfessorView;
