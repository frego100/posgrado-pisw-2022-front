import React from 'react';
import { TFunction } from 'react-i18next';
import { Button, Col, Row } from 'antd';
import { DeleteFilled, EditFilled, EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../routes/routesDictionary';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';

export const professorListColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },

  {
    key: 'document',
    title: translate('document'),
    dataIndex: 'document',
    sorterType: 'alphabetical',
  },
  {
    key: 'country',
    title: translate('country'),
    dataIndex: 'country',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'enabled',
    title: translate('enabled'),
    dataIndex: 'enabled',
    sorterType: 'alphabetical',
  },
  {
    key: 'assignedCourses',
    title: translate('assignedCourses'),
    dataIndex: 'assignedCourses',
    sorterType: 'numerical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      navigate, registrationId, deleted, enabled,
      userType, confirmationModalData, setConfirmationModalData,
      studyProgramId, operatingPlanId,
    }: any) => (
      <Row gutter={[12, 0]}>
        {userType === SCHOOL_DIRECTOR && (
        <Col>
          <Button
            type="ghost"
            icon={<EyeOutlined />}
            onClick={() => {
              navigate(
                routesDictionary.professorDetails.func(registrationId),
                { state: { studyProgramId, operatingPlanId } },
              );
            }}
          >
            {translate('view')}
          </Button>
        </Col>
        )}
        {(userType === UNIT_DIRECTOR) && (
        <>
          <Col>
            <Button
              type="ghost"
              icon={<EditFilled />}
              onClick={() => {
                navigate(
                  routesDictionary.professorDetails.func(registrationId),
                  { state: { studyProgramId, operatingPlanId } },
                );
              }}
            >
              {translate('edit')}
            </Button>
          </Col>
          {deleted && enabled && (
          <Col>
            <Button
              type="ghost"
              icon={<DeleteFilled />}
              className="delete-button"
              onClick={() => {
                setConfirmationModalData({
                  ...confirmationModalData,
                  isVisible: true,
                  type: 'disableProfessorRegistration',
                  registrationId,
                });
              }}
            >
              {translate('disable')}
            </Button>
          </Col>
          )}
        </>
        )}
      </Row>
    ),
  },
];

export default { };
