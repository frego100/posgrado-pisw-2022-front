import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import { FilePdfOutlined } from '@ant-design/icons';
import {
  Button, Col, Divider, Row, Select,
} from 'antd';
import { AppDispatch } from '../../../store';
import TitleView from '../../../components/TitleView';
import { typeDocument } from '../../../components/CustomTable/constants';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomTable from '../../../components/CustomTable';
import { professorListColumns } from './constants';
import CustomFilter from '../../../components/CustomFilter';
import { APPROVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import './styles.scss';
import { isUndefined } from '../../../utils/tools';
import CustomCard from '../../../components/CustomCard';
import { handleDownloadFile } from './functions';
import ConfirmationModal from '../../../components/ConfirmationModal';
import EmptyView from '../../../components/EmptyView';

interface Professor{
  key: string,
  firstName: string,
  lastName: string,
  typeDocument: string,
  numberDocument: string,
  country: string,
  gender: string,
  email: string,
  professorType : String,
  actions: {
  }
}

const getProfessorsRequest = loader('./requests/getProfessors.gql');
const getAllProgramOperatingPlansRequest = loader('./requests/getAllProgramOperatingPlans.gql');
const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');

const ProfessorList = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { graduateUnitId, userType } = useSelector((state: any) => state.auth);

  // Get processes
  const [processes, setProcesses] = useState<any>(null);
  const [process, setProcess] = useState<any>();

  const { data: getProcessesData, loading: getProcessesLoading } = useQuery(
    getOperatingPlanProcessesRequest,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const processesArray = [
        ...getOperatingPlanProcesses
          .map((item) => ({ value: item.id, label: item.year })),
      ];
      setProcess(processesArray[0]);
      setProcesses(processesArray);
    }
  }, [getProcessesData]);

  // Get graduate units
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId || {});

  const { data: getGraduateUnitsData, loading: getGraduateUnitsLoading } = useQuery(
    getGraduateUnitsRequest,
    {
      fetchPolicy: 'cache-first',
      skip: userType === UNIT_DIRECTOR,
    },
  );

  useEffect(() => {
    if (getGraduateUnitsData) {
      const { getGraduateUnits } = getGraduateUnitsData;

      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      setGraduateUnit(graduateUnitsArray[0]);
    }
  }, [getGraduateUnitsData]);

  // Get Programs
  const [operatingPlanPrograms, setOperatingPlanPrograms] = useState<any>([]);
  const [operatingPlanProgram, setOperatingPlanProgram] = useState<any>(null);

  const {
    data: getAllProgramOperatingPlansData,
    loading: getAllProgramOperatingPlansLoading,
  } = useQuery(
    getAllProgramOperatingPlansRequest,
    {
      variables: {
        unit: graduateUnitId || graduateUnit.value,
        process: process?.value,
      },
      fetchPolicy: 'no-cache',
      skip: isUndefined(graduateUnit?.value) && isUndefined(process),
    },
  );

  useEffect(() => {
    if (getAllProgramOperatingPlansData) {
      const { getAllProgramOperatingPlans } = getAllProgramOperatingPlansData;

      const operatingPlanProgramsArray = getAllProgramOperatingPlans.map((programOp) => {
        const { id, studyProgram, operatingPlan } = programOp;
        return { value: id, label: studyProgram.name, status: operatingPlan.status };
      });
      setOperatingPlanPrograms(operatingPlanProgramsArray);
      setOperatingPlanProgram(operatingPlanProgramsArray[0]);
    }
  }, [getAllProgramOperatingPlansData]);

  // Get Professors
  const [professors, setProfessors] = useState<Professor[]>([]);
  const [defaultProfessors, setDefaultProfessors] = useState<any>([]);

  const {
    data: getProfessorsData,
    loading: getProfessorsLoading, refetch: getProfessorsRefetch,
  } = useQuery(
    getProfessorsRequest,
    {
      variables: {
        operatingPlanProgramId: operatingPlanProgram?.value,
        ...(process?.value) && { process: process.value },
        approved: true,
      },
      fetchPolicy: 'no-cache',
      skip: isUndefined(operatingPlanProgram?.value),
    },
  );
  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: '',
    operatingPlanId: '',
    refetch: getProfessorsRefetch,
  });

  useEffect(() => {
    if (getProfessorsData) {
      const { getAllOperatingPlanProfessors } = getProfessorsData;
      const professorsArray: Professor[] = [];
      getAllOperatingPlanProfessors.forEach((professorData: any, index: number) => {
        const { professor } = professorData;

        professorsArray.push({
          // @ts-ignore
          key: index + 1,
          firstName: professor.user.firstName,
          lastName: `${professor.user.lastName} ${professor.user.maternalLastName}`,
          document: `${typeDocument[professor.user.useridentificationSet[0]?.identificationType]}-${professor.user.useridentificationSet[0]?.number}`,
          country: professor.user.country.name || '-',
          email: professor.user.email,
          enabled: professorData.isDeleted ? 'No Habilitado' : 'Habilitado',
          assignedCourses: professorData.assignedCourses,
          professorType: professor.professorType === 'A_1' ? 'Invitado' : 'De la universidad',
          actions: {
            navigate,
            registrationId: professorData.id,
            deleted: professorData.assignedCourses === 0,
            enabled: !professorData.isDeleted,
            userType,
            confirmationModalData,
            setConfirmationModalData,
            studyProgramId: operatingPlanProgram?.value,
            operatingPlanId: graduateUnitId ?? graduateUnit.value,
          },
        });
      });
      setProfessors(professorsArray);
      setDefaultProfessors(professorsArray);
    }
  }, [getProfessorsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('professors'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  if (processes && processes.length === 0) {
    return (
      <EmptyView
        type="emptyOperatingPlanProcess"
        title={t('professors')}
        viewBackButton={false}
        userType={userType}
      />
    );
  }

  return (
    <div className="professor-list">
      <TitleView
        title={t('professors')}
        viewBackButton={false}
        sizeAction={400}
        action={(
          <Row justify="end" gutter={[10, 4]}>
            {!isUndefined(process) && (
            <Col span={16}>
              <br />
              <Button
                type="primary"
                style={{ width: '100%' }}
                onClick={
                  () => handleDownloadFile(graduateUnitId ?? graduateUnit?.value, process?.value)
                }
                icon={<FilePdfOutlined />}
                loading={getGraduateUnitsLoading}
              >
                {t('downloadProfessorList')}
              </Button>
            </Col>
            ) }
            <Col span={8}>
              <span className="label">
                {t('academicYear')}
              </span>
              <Select
                labelInValue
                style={{ width: '100%' }}
                options={processes}
                value={process}
                onChange={(newValue) => setProcess(newValue)}
                loading={getProcessesLoading}
              />
            </Col>
          </Row>
        )}
      />

      <CustomCard>
        <Row className="graduate-unit-filter" gutter={[24, 24]}>
          {userType === SCHOOL_DIRECTOR && (
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('graduateUnit')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={graduateUnit}
                  options={graduateUnits}
                  loading={getGraduateUnitsLoading}
                  labelInValue
                  onChange={(newValue) => setGraduateUnit(newValue)}
                />
              </Col>
            </Row>
          </Col>
          )}
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('program')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={operatingPlanProgram?.value}
                  options={operatingPlanPrograms}
                  loading={getAllProgramOperatingPlansLoading}
                  labelInValue
                  onChange={(value, option) => setOperatingPlanProgram(option)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Divider />
        { operatingPlanProgram && operatingPlanProgram.status !== APPROVED
          ? (
            <EmptyView
              type="operatingPlanNotApproved"
              userType={userType}
              viewTitle={false}
            />
          ) : (
            <>
              <Row className="w-100">
                <Col span={24}>
                  <CustomFilter
                    typeFilter="professorListApproveOperatingPlan"
                    setDataSource={setProfessors}
                    defaultDataSource={defaultProfessors}
                    operatingPlanStatus=""
                    programOperatingPlanId={operatingPlanProgram?.value}
                  />
                </Col>
              </Row>
              <CustomTable
                columns={professorListColumns(t)}
                dataSource={professors}
                loading={getProfessorsLoading}
              />
            </>
          )}
      </CustomCard>
      <ConfirmationModal
        confirmationModalData={confirmationModalData}
        setConfirmationModalData={setConfirmationModalData}
      />
    </div>
  );
};

export default ProfessorList;
