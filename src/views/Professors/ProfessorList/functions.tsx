import {
  XLSX_FORMAT, PROFESSORS_FILE, BACKEND_URL,
} from '../../../utils/constants';

export const dataURItoBlob = async (response) => {
  const blob = await response.blob();
  const newBlob = new Blob([blob], {
    type: XLSX_FORMAT,
  });

  const blobUrl = window.URL.createObjectURL(newBlob);
  const link = document.createElement('a');
  link.href = blobUrl;
  link.setAttribute('download', 'Docentes');
  document.body.appendChild(link);
  link.click();
  link.parentNode.removeChild(link);
  window.URL.revokeObjectURL(blobUrl);
};

export const handleDownloadFile = async (unitId, processId) => {
  const response = await fetch(`${BACKEND_URL}${PROFESSORS_FILE}`, {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ unit_id: unitId, plan_process_id: processId }),
  });
  await dataURItoBlob(response);
};

export default {};
