import React, { useEffect, useState } from 'react';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { Col, Row, Select } from 'antd';
import TitleView from '../../../../components/TitleView';
import CustomTable from '../../../../components/CustomTable';
import { subjectListProfessor } from './constants';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import routesDictionary from '../../../../routes/routesDictionary';
import { AppDispatch } from '../../../../store';
import CustomCard from '../../../../components/CustomCard';
import CustomFilter from '../../../../components/CustomFilter';
import EmptyView from '../../../../components/EmptyView';

const getAllProfessorSubjectsRequest = loader('./requests/getAllProfessorSubjects.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');

const SubjectList = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const { userType } = useSelector((state: any) => state.auth);

  const [processes, setProcesses] = useState<any>();
  const [process, setProcess] = useState<any>({ value: null });

  const [subjectListData, setSubjectListData] = useState<any>(null);
  const [defaultSubjectsValues, setDefaultSubjectsValues] = useState<any>();

  const navigate = useNavigate();

  const {
    data: getProcessesData,
    loading: getProcessesLoading,
  } = useQuery(getOperatingPlanProcessesRequest);

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const operatingPlanProcessesArray = getOperatingPlanProcesses.map((item: any) => (
        { label: item.year, value: item.id }
      ));
      if (!process.value) setProcess(operatingPlanProcessesArray[0]);
      setProcesses(operatingPlanProcessesArray);
    }
  }, [getProcessesData]);

  const { data, loading } = useQuery(
    getAllProfessorSubjectsRequest,
    {
      variables: { processId: process?.value },
      fetchPolicy: 'no-cache',
      skip: !process?.value,
    },
  );

  useEffect(() => {
    if (data) {
      const subjectListArray: any = [];
      const { getAllProfessorCourses } = data;
      getAllProfessorCourses.forEach((element: any) => {
        subjectListArray.push({
          subjectName: element.course.studyPlanSubject.subject,
          group: element.group,
          code: element.course.studyPlanSubject.code,
          startDate: element.endDate,
          endDate: element.endGradesRegisterPeriod,
          actions: {
            courseGroupId: element.id,
            allowGradesRegister: element.allowGradesRegister,
            gradesAssigned: element.gradesAssigned,
            subject: element.course.studyPlanSubject.subject,
            navigate,
          },
        });
      });
      setSubjectListData(subjectListArray);
      setDefaultSubjectsValues(subjectListArray);
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.professorAllCourses.breadCrumb.map((text) => t(text)),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('myCourses')}
        viewBackButton={false}
        sizeAction={280}
        action={(
          <Row justify="end" gutter={[10, 4]}>
            <Col span={12}>
              <span className="label">
                {t('academicYear')}
              </span>
              <Select
                labelInValue
                style={{ width: '100%' }}
                options={processes}
                value={process}
                onChange={(newValue) => setProcess(newValue)}
                loading={getProcessesLoading}
              />
            </Col>
          </Row>
        )}
      />
      { subjectListData && subjectListData.length === 0
        ? (
          <CustomCard>
            <EmptyView
              type="emptyOperatingPlanPrograms"
              userType={userType}
              viewTitle={false}
            />
          </CustomCard>
        ) : (
          <CustomCard>
            <CustomFilter
              typeFilter="subjectListFilters"
              setDataSource={setSubjectListData}
              defaultDataSource={defaultSubjectsValues}
            />
            <CustomTable
              dataSource={subjectListData}
              loading={loading}
              columns={subjectListProfessor(t)}
            />
          </CustomCard>
        )}
    </div>
  );
};

export default SubjectList;
