import React from 'react';
import { TFunction } from 'react-i18next';
import {
  Button, Col, Row, Tooltip,
} from 'antd';
import { EyeOutlined, InfoCircleOutlined } from '@ant-design/icons';
import routesDictionary from '../../../../routes/routesDictionary';

export const subjectListProfessor = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectName',
    title: translate('subject'),
    dataIndex: 'subjectName',
    sorterType: 'alphabetical',
  },
  {
    key: 'group',
    title: translate('group'),
    dataIndex: 'group',
    sorterType: 'alphabetical',
  },
  {
    key: 'startDate',
    title: translate('startDate'),
    dataIndex: 'startDate',
    sorterType: 'alphabetical',
  },
  {
    key: 'endDate',
    title: translate('endDate'),
    dataIndex: 'endDate',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: (
      <Row align="middle">
        <span style={{ marginRight: 10 }}>
          {translate('actions')}
        </span>
        <Tooltip placement="top" title={translate('registerAttendanceTooltip')}>
          <InfoCircleOutlined />
        </Tooltip>
      </Row>
    ),
    dataIndex: 'actions',
    render: ({
      courseGroupId, allowGradesRegister, gradesAssigned, navigate,
    }: any) => (
      (allowGradesRegister || gradesAssigned)
        ? (
          <Row gutter={[12, 0]}>
            <Col>
              <Button
                type="ghost"
                icon={<EyeOutlined />}
                onClick={() => {
                  navigate(
                    routesDictionary.studentList.func(courseGroupId),
                    { state: { allowGradesRegister, gradesAssigned } },
                  );
                }}
              >
                {translate('registerGrades')}
              </Button>
            </Col>
          </Row>
        )
        : <p>Deshabilitado</p>
    ),
  },
];

export default {};
