import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { useDispatch } from 'react-redux';
import { FormOutlined } from '@ant-design/icons';
import {
  Button, Col, message, Row,
} from 'antd';
import TitleView from '../../../../components/TitleView';
import routesDictionary from '../../../../routes/routesDictionary';
import CustomTable from '../../../../components/CustomTable';
import { studentsColumns } from './constants';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import { AppDispatch } from '../../../../store';
import CustomCard from '../../../../components/CustomCard';
import CustomFilter from '../../../../components/CustomFilter';

const editStudentsGradesRequest = loader('./requests/editStudentsGrades.gql');
const setStudentsGradesRequest = loader('./requests/setStudentsGrades.gql');
const getCourseGroupStudentsRequest = loader('./requests/getCourseGroupStudents.gql');

const AcademicProgressView = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const params = useParams();
  const { coursegroupId } = params;
  const dispatch = useDispatch<AppDispatch>();
  const location = useLocation();
  const gradesAssigned = location?.state?.gradesAssigned;

  // when assigned group students
  const [isAssignedGradesState, setIsAssignedGradesState] = useState<boolean>(gradesAssigned);
  const [isEditableGradeStudentsState, setIsEditableGradeStudentsState] = useState<any>(false);

  // assigned grades of students
  const [courseGradesStudents, setCourseGradesStudents] = useState<any>();

  // Students
  const [courseGroupStudents, setCourseGroupStudents] = useState<any>([]);
  // filters
  const [defaultStudentsValues, setDefaultStudentsValues] = useState<any>();

  const [setStudentsGradesMutation,
    { loading: setStudentsGradesLoading },
  ] = useMutation(setStudentsGradesRequest);

  const [editStudentsGradesMutation,
    { loading: editStudentsGradesLoading },
  ] = useMutation(editStudentsGradesRequest);

  const {
    data: getCourseGroupStudentsData,
    loading: courseGroupStudentsLoading,
    refetch: courseGroupStudentsRefetch,
  } = useQuery(getCourseGroupStudentsRequest, {
    variables: { courseGroupId: coursegroupId },
    fetchPolicy: 'no-cache',
    skip: !coursegroupId,
  });

  useEffect(() => {
    if (getCourseGroupStudentsData) {
      const { getCourseGroupStudents } = getCourseGroupStudentsData;
      const getCourseGroupStudentsArray: any[] = [];
      const courseGradeStudentsArray: any[] = [];
      const courseGradeStudentsEditableArray: any[] = [];

      getCourseGroupStudents.forEach(({ id, enrollment, studentgrade }: any, index: number) => {
        const { student } = enrollment;
        getCourseGroupStudentsArray.push({
          key: index + 1,
          cui: student.cui,
          firstName: student.user.firstName,
          lastName: `${student.user.lastName} ${student.user.maternalLastName}`,
          email: student.user.email,
          grades: { studentgrade, studentEnrollmentId: id },
        });
        courseGradeStudentsArray.push({
          studentEnrollment: id,
          score: studentgrade ? studentgrade.score : 0,
        });
        courseGradeStudentsEditableArray.push({
          studentGrade: studentgrade?.id,
          score: studentgrade ? studentgrade.score : 0,
        });
      });
      setCourseGroupStudents(getCourseGroupStudentsArray);
      setDefaultStudentsValues(getCourseGroupStudentsArray);
      setCourseGradesStudents(isAssignedGradesState
        ? courseGradeStudentsEditableArray : courseGradeStudentsArray);
    }
  }, [getCourseGroupStudentsData]);

  const handleSetStudentsGrades = () => {
    setStudentsGradesMutation({
      variables: {
        input: courseGradesStudents,
      },
    })
      .then(({ data: setStudentsGradesData }) => {
        const { setStudentsGrades: resultData } = setStudentsGradesData;
        if (resultData.feedback.status === 'SUCCESS') {
          message.success(resultData.feedback.message);
          setIsEditableGradeStudentsState(false);
          setIsAssignedGradesState(true);
          courseGroupStudentsRefetch();
        } else {
          message.error(resultData.feedback.message);
          courseGroupStudentsRefetch();
        }
      });
  };

  const handleEditStudentsGrades = () => {
    editStudentsGradesMutation({
      variables: {
        input: courseGradesStudents,
      },
    })
      .then(({ data: editStudentsGradesData }) => {
        const { editStudentsGrades: resultData } = editStudentsGradesData;
        if (resultData.feedback.status === 'SUCCESS') {
          message.success(resultData.feedback.message);
          setIsEditableGradeStudentsState(false);
          courseGroupStudentsRefetch();
        } else {
          message.error(resultData.feedback.message);
          message.error(resultData.errors[0].messages[0]);
          courseGroupStudentsRefetch();
        }
      });
  };

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('myCourses'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  // if (loading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={t('students')}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.professorAcademicGrades.route);
        }}
        sizeAction={400}
        action={(!isEditableGradeStudentsState && defaultStudentsValues?.length > 0)
          && (
          <Row justify="end" gutter={[16, 16]}>
            {/* {isAssignedGradesState && (
            <Col span={12}>
              <Button
                type="primary"
                style={{ width: '100%' }}
                icon={<FilePdfOutlined />}
              >
                {t('downloadGrades')}
              </Button>
            </Col>
            )} */}
            <Col span={12}>
              <Button
                type="primary"
                style={{ width: '100%' }}
                icon={<FormOutlined />}
                onClick={() => {
                  setIsEditableGradeStudentsState(true);
                }}
              >
                {isAssignedGradesState ? t('editGrades') : t('assignGrades')}
              </Button>
            </Col>
          </Row>
          )}
      />
      <CustomCard>
        <CustomFilter
          typeFilter="studentListFilters"
          setDataSource={setCourseGroupStudents}
          defaultDataSource={defaultStudentsValues}
        />
        <CustomTable
          loading={courseGroupStudentsLoading}
          dataSource={courseGroupStudents}
          columns={studentsColumns(
            t,
            isEditableGradeStudentsState,
            isAssignedGradesState,
            courseGradesStudents,
            setCourseGradesStudents,
          )}
        />
        {isEditableGradeStudentsState
        && (
        <Row style={{ marginTop: '25px' }} gutter={[10, 10]} justify="end">
          <Col>
            <Button
              type="default"
              style={{ width: 200 }}
              onClick={() => {
                setIsEditableGradeStudentsState(false);
              }}
            >
              {t('cancel')}
            </Button>
          </Col>
          <Col>
            <Button
              type="primary"
              style={{ width: 200 }}
              onClick={isAssignedGradesState ? handleEditStudentsGrades : handleSetStudentsGrades}
              loading={isAssignedGradesState ? editStudentsGradesLoading : setStudentsGradesLoading}
            >
              {t('save')}
            </Button>
          </Col>
        </Row>
        )}
      </CustomCard>
    </div>
  );
};

export default AcademicProgressView;
