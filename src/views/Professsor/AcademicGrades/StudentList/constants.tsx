import React from 'react';
import { TFunction } from 'react-i18next';
import {
  Col, InputNumber, Row,
} from 'antd';

const onChange = (
  value: any,
  studentEnrollmentId: any,
  setCourseGradesStudents: any,
) => {
  setCourseGradesStudents((oldValues) => oldValues.map((item) => {
    if (item.studentEnrollment === studentEnrollmentId) {
      return { ...item, score: value };
    }
    return item;
  }));
};

const onEditGrade = (
  value: any,
  studentGradeId: any,
  setCourseGradesStudents: any,
) => {
  setCourseGradesStudents((oldValues) => oldValues.map((item) => {
    if (item.studentGrade === studentGradeId) {
      return { ...item, score: value };
    }
    return item;
  }));
};

export const studentsColumns = (
  translate: TFunction,
  isEditableGradeStudentsState: any,
  isAssignedGradesState: any,
  courseGradesStudents: any,
  setCourseGradesStudents: any,
) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'cui',
    title: translate('cui'),
    dataIndex: 'cui',
    sorterType: 'alphabetical',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'grades',
    title: translate('grades'),
    dataIndex: 'grades',
    render: (
      { studentgrade, studentEnrollmentId }: any,
    ) => (
      isEditableGradeStudentsState ? (
        <Row gutter={[12, 0]}>
          {isAssignedGradesState ? (
            <Col>
              <InputNumber
                min={0}
                max={20}
                controls={false}
                value={courseGradesStudents?.find(
                  (item) => item.studentGrade === studentgrade?.id,
                )?.score}
                onChange={(value: any) => onEditGrade(
                  value,
                  studentgrade.id,
                  setCourseGradesStudents,
                )}
              />
            </Col>
          ) : (
            <Col>
              <InputNumber
                min={0}
                max={20}
                controls={false}
                value={courseGradesStudents?.find(
                  (item) => item.studentEnrollment === studentEnrollmentId,
                )?.score}
                onChange={(value: any) => onChange(
                  value,
                  studentEnrollmentId,
                  setCourseGradesStudents,
                )}
              />
            </Col>
          )}
        </Row>
      ) : studentgrade?.score ?? '-'
    ),
  },
];
export default {};
