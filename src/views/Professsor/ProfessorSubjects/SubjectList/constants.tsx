import React from 'react';
import { TFunction } from 'react-i18next';
import {
  Button, Col, Progress, Row,
} from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../../routes/routesDictionary';

export const subjectListProfessor = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'name',
    title: translate('subject'),
    dataIndex: 'name',
    sorterType: 'alphabetical',
  },
  {
    key: 'group',
    title: translate('group'),
    dataIndex: 'group',
    sorterType: 'alphabetical',
  },
  {
    key: 'percentage',
    title: translate('percentage'),
    dataIndex: 'percentage',
    sorterType: 'numerical',
    render: (percentage) => <Progress percent={percentage} size="small" strokeColor="#141e42" />,
  },
  {
    key: 'startDate',
    title: translate('startDate'),
    dataIndex: 'startDate',
    sorterType: 'alphabetical',
  },
  {
    key: 'endDate',
    title: translate('endDate'),
    dataIndex: 'endDate',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      courseGroupId, navigate,
    }: any) => (
      <Row gutter={[12, 0]}>
        <Col>
          <Button
            type="ghost"
            icon={<EyeOutlined />}
            onClick={() => {
              navigate(routesDictionary.professorCourse.func(courseGroupId));
            }}
          >
            {translate('view')}
          </Button>
        </Col>
      </Row>
    ),
  },
];

export default {};
