import React, { useEffect, useState } from 'react';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { Col, Row, Select } from 'antd';
import TitleView from '../../../../components/TitleView';
import CustomTable from '../../../../components/CustomTable';
import { subjectListProfessor } from './constants';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import routesDictionary from '../../../../routes/routesDictionary';
import { AppDispatch } from '../../../../store';
import CustomCard from '../../../../components/CustomCard';
import { startsetOperatingPlanProcess } from '../../../../store/thunks/auth';
import EmptyView from '../../../../components/EmptyView';

const getProfessorSubjectsRequest = loader('./requests/getProfessorCourseGroups.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');
const getProfessorOperatingPlanProgramsRefetch = loader('./requests/getProfessorOperatingPlanPrograms.gql');
// const getEnrollmentPeriodsByProcess = loader('./requests/');

const SubjectList = () => {
  const [subjectListData, setSubjectListData] = useState<any>([]);
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const { userType, professorId, operatingPlanProcess } = useSelector((state: any) => state.auth);

  const [processes, setProcesses] = useState<any>();
  const [process, setProcess] = useState<any>({ value: operatingPlanProcess });

  const [studyPrograms, setStudyPrograms] = useState<any>(null);
  const [studyProgram, setStudyProgram] = useState<any>();

  const navigate = useNavigate();

  useEffect(() => {
    dispatch(startsetOperatingPlanProcess(process?.value));
  }, [process?.value]);

  const {
    data: getProcessesData,
    loading: getProcessesLoading,
    // refetch: getProcessesRefetch,
  } = useQuery(getOperatingPlanProcessesRequest);

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const operatingPlanProcessesArray = getOperatingPlanProcesses.map((item: any) => (
        { label: item.year, value: item.id }
      ));
      if (!process.value) setProcess(operatingPlanProcessesArray[0]);
      setProcesses(operatingPlanProcessesArray);
    }
  }, [getProcessesData]);

  const {
    data: getStudyProgramsData,
    loading: getStudyProgramsLoading,
    // refetch: getStudyProgramsRefetch,
  } = useQuery(
    getProfessorOperatingPlanProgramsRefetch,
    {
      variables: { processId: process?.value },
      fetchPolicy: 'no-cache',
      skip: !process?.value || !professorId,
    },
  );

  useEffect(() => {
    if (getStudyProgramsData) {
      const { getProfessorOperatingPlanPrograms } = getStudyProgramsData;
      const programsArray = getProfessorOperatingPlanPrograms.map((item: any) => (
        { label: item.studyProgram?.name, value: item.id }
      ));
      setStudyPrograms(programsArray);
      setStudyProgram(programsArray[0]);
    }
  }, [getStudyProgramsData]);

  const {
    data: getProfessorSubjectsData,
    loading: getProfessorSubjectsLoading,
    // refetch: getProfessorSubjectsRefetch,
  } = useQuery(
    getProfessorSubjectsRequest,
    {
      variables: { professorId, operatingPlanProgramId: studyProgram?.value ?? '-1' },
      fetchPolicy: 'no-cache',
      skip: !professorId,
    },
  );

  useEffect(() => {
    if (getProfessorSubjectsData) {
      const subjectListArray: any = [];
      const { getProfessorCourseGroups } = getProfessorSubjectsData;
      getProfessorCourseGroups.forEach((element: any) => {
        subjectListArray.push({
          name: element.course.studyPlanSubject.subject,
          group: element.group,
          code: element.course.studyPlanSubject.code,
          percentage: element.percentage || 0,
          startDate: element.startDate,
          endDate: element.endDate,
          actions: {
            courseGroupId: element.id,
            navigate,
          },
        });
      });
      setSubjectListData(subjectListArray);
    }
  }, [getProfessorSubjectsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.professorAllCourses.breadCrumb.map((text) => t(text)),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('myCourses')}
        viewBackButton={false}
        sizeAction={280}
        action={(
          <Row justify="end" gutter={[10, 4]}>
            <Col span={12}>
              <span className="label">
                {t('academicYear')}
              </span>
              <Select
                labelInValue
                style={{ width: '100%' }}
                options={processes}
                value={process}
                onChange={(newValue) => setProcess(newValue)}
                loading={getProcessesLoading}
              />
            </Col>
          </Row>
        )}
      />
      { studyPrograms && studyPrograms.length === 0
        ? (
          <CustomCard>
            <EmptyView
              type="emptyOperatingPlanPrograms"
              userType={userType}
              viewTitle={false}
            />
          </CustomCard>
        ) : (
          <CustomCard>
            <Row gutter={[24, 24]} style={{ marginBottom: '32px' }}>
              <Col xs={24} md={12}>
                <Row gutter={[0, 4]} align="middle">
                  <Col span={12}>
                    <span className="label">
                      {t('program')}
                    </span>
                  </Col>
                  <Col span={24}>
                    <Select
                      style={{ width: '100%' }}
                      value={studyProgram}
                      options={studyPrograms}
                      loading={getStudyProgramsLoading}
                      labelInValue
                      onChange={(newValue) => setStudyProgram(newValue)}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
            <CustomTable
              dataSource={subjectListData}
              loading={getProfessorSubjectsLoading}
              columns={subjectListProfessor(t)}
            />
          </CustomCard>
        )}
    </div>
  );
};

export default SubjectList;
