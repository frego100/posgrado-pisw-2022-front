import React from 'react';

import { TFunction } from 'react-i18next';
import { Input, InputNumber } from 'antd';
import TextArea from 'antd/lib/input/TextArea';

export const formItems = (
  form: any,
  readOnly: boolean,
  t: TFunction,
) => [
  {
    name: 'tittle',
    component: (
      <Input disabled={readOnly} />
    ),
    rules: [],
    label: t('topic'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'date',
    component: (
      <Input disabled />
    ),
    rules: [],
    label: t('date'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: true,
  },
  {
    name: 'percentage',
    component: (
      <InputNumber disabled={readOnly} style={{ width: '100%' }} min={0} max={100} />
    ),
    rules: [{ required: true, message: 'Campo obligatorio!' }],
    label: t('percentage'),
    responsive: {
      xs: { span: 24 },
      md: { span: 12 },
    },
    display: true,
  },
  {
    name: 'description',
    component: (
      <TextArea
        rows={5}
        disabled={readOnly}
        autoSize={{
          minRows: 4,
          maxRows: 6,
        }}
      />
    ),
    rules: [],
    label: t('description'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
];

export default { };
