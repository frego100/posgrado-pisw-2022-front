import React, { useEffect } from 'react';
import {
  Form, message, Modal,
} from 'antd';
import { useTranslation } from 'react-i18next';
import { useForm } from 'antd/lib/form/Form';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { formItems } from './constants';
import FormCard from '../../../../../components/FormCard';
import CustomLoader from '../../../../../components/CustomLoader';

interface AcademicProgressModalProps {
  academicProgressModal: any,
  setAcademicProgressModal: any,
  refetch: any,
  handleCancelAcademicProgress: any,
}

const getCourseProgressRequest = loader('../requests/getCourseProgress.gql');
const editAcademicProgressRequest = loader('../requests/editAcademicProgress.gql');

const AcademicProgressModal:React.FC<AcademicProgressModalProps> = ({
  academicProgressModal,
  setAcademicProgressModal,
  refetch,
  handleCancelAcademicProgress,
}) => {
  const { t } = useTranslation();
  const [form] = useForm();

  const { data, loading } = useQuery(
    getCourseProgressRequest,
    {
      variables: { courseprogressId: academicProgressModal.courseprogressId },
      fetchPolicy: 'no-cache',
      skip: !academicProgressModal.visible,
    },
  );
  const [editAcademicProgressMutation,
    { loading: editAcademicProgressLoading },
  ] = useMutation(editAcademicProgressRequest);

  useEffect(() => {
    if (data) {
      const { getCourseProgress } = data;
      const courseProgressData = {
        ...getCourseProgress,
        date: getCourseProgress.academicSession.date,
      };
      form.setFieldsValue(courseProgressData);
    }
  }, [data]);

  const handleSave = () => {
    form.validateFields()
      .then((values) => {
        editAcademicProgressMutation({
          variables: {
            input: {
              courseProgressId: academicProgressModal.courseprogressId,
              tittle: values.tittle,
              description: values.description,
              percentage: values.percentage,
            },
          },
        })
          .then(({ data: editCourseProgressData }: any) => {
            const { editCourseProgress } = editCourseProgressData;
            if (editCourseProgress.feedback.status === 'SUCCESS') {
              setAcademicProgressModal({ visible: false });
              message.success(editCourseProgress.feedback.message);
              form.resetFields();
              refetch();
            }
          });
      });
  };

  const handleClose = () => {
    setAcademicProgressModal({ visible: false });
    form.resetFields();
  };

  return (
    <Modal
      open={academicProgressModal.visible}
      title={academicProgressModal.isEditable ? t('registerAcademicProgress') : t('academicProgress')}
      onCancel={handleCancelAcademicProgress}
      width={700}
      closable={false}
      okButtonProps={{ loading: editAcademicProgressLoading }}
      destroyOnClose
      okText={t(academicProgressModal.isEditable ? 'save' : 'accept')}
      cancelText={t('cancel')}
      onOk={academicProgressModal.isEditable ? handleSave : handleClose}
    >
      {loading ? <CustomLoader />
        : (
          <Form
            layout="vertical"
            form={form}
          >
            <FormCard
              itemList={formItems(form, !academicProgressModal.isEditable, t)}
              readonly={false}
              withoutTitle
              withoutDeleteIcon
            />
          </Form>
        )}
    </Modal>
  );
};

export default AcademicProgressModal;
