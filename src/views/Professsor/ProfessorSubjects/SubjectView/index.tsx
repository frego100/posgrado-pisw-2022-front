import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { useDispatch } from 'react-redux';
import {
  Button, Divider, message, Row, Tabs,
} from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import TitleView from '../../../../components/TitleView';
import routesDictionary from '../../../../routes/routesDictionary';
import CustomTable from '../../../../components/CustomTable';
import { academicProgressColumns, studentsColumns } from './constants';
import AcademicProgressModal from './AcademicProgressModal';
import ConfirmationModal from '../../../../components/ConfirmationModal';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import { AppDispatch } from '../../../../store';
import CustomCard from '../../../../components/CustomCard';
import CustomLoader from '../../../../components/CustomLoader';
import EmptyCard from '../../../../components/EmptyCard';
import CustomFilter from '../../../../components/CustomFilter';

const getAllCoursesProgressRequest = loader('./requests/getAllCoursesProgress.gql');
const createAcademicSessionsRequest = loader('./requests/createAcademicSessions.gql');
const getCourseGroupStudentsRequest = loader('./requests/getCourseGroupStudents.gql');

const AcademicProgressView = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const params = useParams();
  const { coursegroupId } = params;
  const location = useLocation();
  // @ts-ignore
  const academicSessionTab = location?.state?.displayAcademicSession;

  const [academicProgressModal, setAcademicProgressModal] = useState<any>({
    visible: false,
  });
  const dispatch = useDispatch<AppDispatch>();
  // const { professorId } = useSelector((state: any) => state.auth);

  // const [activeTab, setActiveTab] = useState('1');

  const { loading, data, refetch } = useQuery(
    getAllCoursesProgressRequest,
    { variables: { coursegroupId }, fetchPolicy: 'no-cache' },
  );

  const [academicProgress, setAcademicProgress] = useState<any>([]);

  // Students
  const [courseGroupStudents, setCourseGroupStudents] = useState<any>([]);
  // filters
  const [defaultStudentsValues, setDefaultStudentsValues] = useState<any>();
  const [subject, setSubject] = useState<any>();

  const [createAcademicSessionsMutation,
    { loading: createAcademicSessionsLoading },
  ] = useMutation(createAcademicSessionsRequest);

  const {
    data: getCourseGroupStudentsData,
    loading: getCourseGroupStudentsLoading,
    // refetch: getCourseGroupStudentsRefetch,
  } = useQuery(getCourseGroupStudentsRequest, {
    variables: { courseGroupId: coursegroupId },
    fetchPolicy: 'no-cache',
    skip: !coursegroupId,
  });

  useEffect(() => {
    if (getCourseGroupStudentsData) {
      const { getCourseGroupStudents, getCourseGroup } = getCourseGroupStudentsData;
      const getCourseGroupStudentsArray: any[] = [];

      getCourseGroupStudents.forEach((studentCourseData: any, index: any) => {
        const { enrollment, attendances } = studentCourseData;
        const { student } = enrollment;
        getCourseGroupStudentsArray.push({
          key: index + 1,
          cui: student.cui,
          firstName: student.user.firstName,
          lastName: `${student.user.lastName} ${student.user.maternalLastName}`,
          email: student.user.email,
          attendances,
        });
      });
      const { course, group } = getCourseGroup;
      setSubject({ name: course.studyPlanSubject?.subject, group });
      setCourseGroupStudents(getCourseGroupStudentsArray);
      setDefaultStudentsValues(getCourseGroupStudentsArray);
    }
  }, [getCourseGroupStudentsData]);

  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: 'deleteAcademicProgress',
    courseProgressId: '',
    refetch,
  });

  const handleCancelAcademicProgress = () => {
    setAcademicProgressModal({ visible: false });
  };

  const handleCreateAcademicSessions = () => {
    createAcademicSessionsMutation({
      variables: {
        courseGroupId: coursegroupId,
      },
    }).then(({ data: createAcademicSessionsData }) => {
      const { createAcademicSessions } = createAcademicSessionsData;
      if (createAcademicSessions.feedback.status === 'SUCCESS') {
        message.success(createAcademicSessions.feedback.message);
        refetch();
      }
    });
  };

  useEffect(() => {
    if (data) {
      const { getAllCoursesProgress } = data;
      const academicProgressArray: any[] = [];

      getAllCoursesProgress.forEach(({
        id, tittle, description, percentage, academicSession,
      } : any, index: number) => {
        academicProgressArray.push({
          code: index + 1,
          tittle: tittle || '-',
          description: description || '-',
          date: academicSession.date,
          percentage: percentage || '-',
          actions: {
            academicSessionId: academicSession.id,
            courseprogressId: id,
            setAcademicProgressModal,
            date: academicSession.date,
            navigate,
            coursegroupId,
          },
        });
      });
      setAcademicProgress(academicProgressArray);
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('myCourses'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  if (loading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={`${subject?.name} - ${subject?.group}`}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.professorAllCourses.route);
        }}
      />
      <Tabs
        type="card"
        defaultActiveKey={academicSessionTab ? '2' : '1'}
        // activeKey={activeTab}
        // onChange={(value) => setActiveTab(value)}
        items={[
          {
            label: t('students'),
            key: '1',
            children: (
              <CustomCard className="tabContainer">
                <TitleView
                  title={t('students')}
                  viewBackButton={false}
                />
                <CustomFilter
                  typeFilter="studentListFilters"
                  setDataSource={setCourseGroupStudents}
                  defaultDataSource={defaultStudentsValues}
                />
                <CustomTable
                  loading={getCourseGroupStudentsLoading}
                  dataSource={courseGroupStudents}
                  columns={studentsColumns(t)}
                />
              </CustomCard>
            ),
          },
          {
            label: t('academicSessions'),
            key: '2',
            children: (academicProgress.length === 0 ? (
              <CustomCard className="tabContainer">
                <TitleView
                  title={t('academicSessions')}
                  viewBackButton={false}
                />
                <Divider />
                <EmptyCard
                  title={t('emptyAcademicSessions')}
                  description={t('emptyAcademicSessionsDescription')}
                  bordered={false}
                  action={(
                    <Row align="middle" gutter={[0, 16]} justify="center">
                      <Button
                        type="primary"
                        icon={<PlusCircleOutlined />}
                        loading={createAcademicSessionsLoading}
                        onClick={handleCreateAcademicSessions}
                      >
                        {t('createAcademicSessions')}
                      </Button>
                    </Row>
                          )}
                />
              </CustomCard>
            ) : (
              <CustomCard className="tabContainer">
                <TitleView
                  title={t('academicSessions')}
                  viewBackButton={false}
                />
                <CustomTable
                  loading={loading}
                  dataSource={academicProgress}
                  columns={academicProgressColumns(t)}
                />
                <AcademicProgressModal
                  academicProgressModal={academicProgressModal}
                  handleCancelAcademicProgress={handleCancelAcademicProgress}
                  refetch={refetch}
                  setAcademicProgressModal={setAcademicProgressModal}
                />
                <ConfirmationModal
                  confirmationModalData={confirmationModalData}
                  setConfirmationModalData={setConfirmationModalData}
                />
              </CustomCard>
            )
            ),
          },
        ]}
      />
    </div>
  );
};

export default AcademicProgressView;
