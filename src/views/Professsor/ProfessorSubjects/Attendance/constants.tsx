import React from 'react';
import { TFunction } from 'react-i18next';
import {
  Col, Radio, RadioChangeEvent, Row,
} from 'antd';

export const optionLabel = {
  1: 'ON_TIME',
  2: 'ABSENT',
  3: 'LATE',
  4: 'EXCUSED_ABSENCE',
};

const plainOptions = (t: TFunction) => [
  {
    label: t('present'),
    value: 'ON_TIME',
  },
  {
    label: t('absent'),
    value: 'ABSENT',
  },
  {
    label: t('late'),
    value: 'LATE',
  },
  {
    label: t('excusedAbsence'),
    value: 'EXCUSED_ABSENCE',
  },
];

const onChange = (
  value: any,
  setStudentAttendances: any,
  studentCourseAttendanceId: any,
) => {
  setStudentAttendances((oldValues) => oldValues.map((item) => {
    if (item.studentCourseAttendanceId === studentCourseAttendanceId) {
      return { ...item, attendance: value };
    }
    return item;
  }));
};

export const studentColumns = (translate: TFunction, studentAttendances: any) => [
  {
    key: 'key',
    title: translate('N°'),
    dataIndex: 'key',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    width: 500,
    render: ({ studentCourseAttendanceId, setStudentAttendances, isEditable }) => (
      <Row gutter={[12, 0]}>
        <Col>
          <Radio.Group
            options={plainOptions(translate)}
            disabled={!isEditable}
            onChange={
                ({ target: { value } }:
                     RadioChangeEvent) => onChange(
                  value,
                  setStudentAttendances,
                  studentCourseAttendanceId,
                )
              }
            value={studentAttendances
              .find(
                (item) => item.studentCourseAttendanceId === studentCourseAttendanceId,
              ).attendance}
          />
        </Col>
      </Row>
    ),
  },
];

export default {};
