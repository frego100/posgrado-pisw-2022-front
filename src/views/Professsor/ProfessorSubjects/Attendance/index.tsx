import React, { useEffect, useState } from 'react';
import {
  Button, message, Row,
} from 'antd';
import { SaveOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { useDispatch } from 'react-redux';
import TitleView from '../../../../components/TitleView';
import CustomFilter from '../../../../components/CustomFilter';
import CustomTable from '../../../../components/CustomTable';
import { optionLabel, studentColumns } from './constants';
import routesDictionary from '../../../../routes/routesDictionary';
import { isToday } from '../../../../utils/tools';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import { AppDispatch } from '../../../../store';
import CustomCard from '../../../../components/CustomCard';
import ConfirmationModal from '../../../../components/ConfirmationModal';

const getStudentAttendancesRequest = loader('./requests/getStudentAttendances.gql');
const editStudentCourseAttendanceRequest = loader('./requests/editStudentCourseAttendance.gql');

const AttendanceView = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const params = useParams();
  const { coursegroupId, academicSessionId } = params;
  const dispatch = useDispatch<AppDispatch>();

  // State for students table
  const [students, setStudents] = useState<any>([]);
  const [defaultValues, setDefaultValues] = useState<any>([]);

  // State for students attendance
  const [studentAttendances, setStudentAttendances] = useState<any>([]);

  const [isEditableState, setIsEditableState] = useState<any>();

  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    type: 'allowStudentAttendance',
    isVisible: false,
  });

  const { loading, data } = useQuery(
    getStudentAttendancesRequest,
    { variables: { academicSessionId }, fetchPolicy: 'no-cache' },
  );

  const [editStudentCourseAttendanceMutation,
    { loading: editStudentCourseAttendanceLoading },
  ] = useMutation(editStudentCourseAttendanceRequest);

  useEffect(() => {
    if (data) {
      const { getStudentAttendancesProfessor } = data;
      const studentsArray: any[] = [];
      const studentAttendancesArray: any[] = [];
      let isEditable;
      getStudentAttendancesProfessor.forEach((studentAttendance, index: number) => {
        const { user } = studentAttendance.groupEnrollment.enrollment.student;
        isEditable = isToday(studentAttendance.academicSession.date);
        studentsArray.push({
          key: index + 1,
          firstName: user.firstName,
          lastName: `${user.lastName} ${user.maternalLastName}`,
          email: user.email,
          actions: {
            studentCourseAttendanceId: studentAttendance.id,
            setStudentAttendances,
            isEditable,
          },
        });
        studentAttendancesArray.push({
          studentCourseAttendanceId: studentAttendance.id,
          attendance: isToday(studentAttendance.academicSession.date)
            ? optionLabel[studentAttendance.attendance] || 'ON_TIME'
            : optionLabel[studentAttendance.attendance],
        });
      });
      setIsEditableState(isEditable);
      setStudents(studentsArray);
      setDefaultValues(studentsArray);
      setStudentAttendances(studentAttendancesArray);
    }
  }, [data]);

  const handleChangeAttendanceStatus = () => {
    setConfirmationModalData({
      ...confirmationModalData,
      isVisible: true,
      academicSessionId,
    });
  };

  const handleRegisterAttendance = () => {
    editStudentCourseAttendanceMutation({
      variables: {
        input: studentAttendances,
      },
    })
      .then(({ data: editStudentCourseAttendanceData }) => {
        const { editStudentCourseAttendance: resultData } = editStudentCourseAttendanceData;
        if (resultData.feedback.status === 'SUCCESS') {
          message.success(resultData.feedback.message);
          navigate(routesDictionary.professorCourse.func(coursegroupId));
        }
      });
  };

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('myCourses'),
          t('academicSessions'),
          t('attendance'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('attendance')}
        viewBackButton
        onClickBackButton={() => navigate(
          routesDictionary.professorCourse.func(coursegroupId),
          { state: { displayAcademicSession: true } },
        )}
      />
      <CustomCard>
        <CustomFilter
          typeFilter="attendanceStudentListFilters"
          setDataSource={setStudents}
          defaultDataSource={defaultValues}
          handleChangeAttendanceStatus={handleChangeAttendanceStatus}
        />
        <CustomTable
          loading={loading}
          dataSource={students}
          columns={studentColumns(t, studentAttendances)}
        />
        {isEditableState && (
          <Row justify="end">
            <Button
              type="primary"
              onClick={handleRegisterAttendance}
              loading={editStudentCourseAttendanceLoading}
              icon={<SaveOutlined />}
            >
              {t('registerAttendance')}
            </Button>
          </Row>
        )}
        <ConfirmationModal
          confirmationModalData={confirmationModalData}
          setConfirmationModalData={setConfirmationModalData}
        />
      </CustomCard>
    </div>

  );
};

export default AttendanceView;
