import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { AppDispatch } from '../../../store';
import { startDisplayBanner, startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import ProfessorComponent from '../../../components/ProfessorView';
import {
  APPROVED, RECEIVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR,
} from '../../../utils/constants';

const getProgramOperatingPlanRequest = loader('./requests/getProgramOperatingPlan.gql');

const ProfessorView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const params = useParams();
  const { operatingPlanId, programOperatingPlanId } = params;
  const [studyPlanName, setStudyPlanName] = useState<string>('');
  const { t } = useTranslation();
  const { userType } = useSelector((state: any) => state.auth);
  const [operatingPlanData, setOperatingPlanData] = useState<any>({});
  const [processIsActive, setProcessIsActive] = useState();
  const readOnly = userType === SCHOOL_DIRECTOR
    || (userType === UNIT_DIRECTOR && (!processIsActive
    || operatingPlanData.status === RECEIVED
    || operatingPlanData.status === APPROVED));
  const { data, loading } = useQuery(
    getProgramOperatingPlanRequest,
    {
      variables: { operatingPlanId: programOperatingPlanId },
      fetchPolicy: 'no-cache',
    },
  );
  useEffect(() => {
    if (data) {
      const { getProgramOperatingPlan } = data;
      const { studyProgram, operatingPlan } = getProgramOperatingPlan;
      setStudyPlanName(studyProgram.name);
      setOperatingPlanData(getProgramOperatingPlan.operatingPlan);
      setProcessIsActive(operatingPlan.opPlanProcess.isActive);
      if (!operatingPlan.opPlanProcess.isActive
          && userType === UNIT_DIRECTOR
          && operatingPlan.status !== APPROVED
      ) {
        dispatch(startDisplayBanner());
      }
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t(routesDictionary.operatingPlanProgramProfessor.breadCrumb[0]),
          operatingPlanData.graduateUnit?.name,
          studyPlanName,
          t(routesDictionary.operatingPlanProgramProfessor.breadCrumb[1]),
        ],
      ),
    ).then();
  }, [dispatch, t, operatingPlanData]);

  if (loading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={studyPlanName}
        viewBackButton
        sizeAction={readOnly ? 150 : 0}
        onClickBackButton={() => {
          navigate(
            routesDictionary.operatingPlanProgram.func(operatingPlanId, programOperatingPlanId),
          );
        }}
        action={<div />}
      />
      <ProfessorComponent
        readonly={readOnly}
        createProfessor={false}
        backProfessorListView={false}
      />
    </div>
  );
};

export default ProfessorView;
