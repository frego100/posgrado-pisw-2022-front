import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { AppDispatch } from '../../../store';
import { startDisplayBanner, startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import ProfessorView from '../../../components/ProfessorView';
import { APPROVED, UNIT_DIRECTOR } from '../../../utils/constants';

const getProgramOperatingPlanRequest = loader('./requests/getProgramOperatingPlan.gql');

const CreateProfessorView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const [operatingPlanData, setOperatingPlanData] = useState<any>();
  const params = useParams();
  const { operatingPlanId, programOperatingPlanId } = params;
  const { t } = useTranslation();
  const { userType } = useSelector((state: any) => state.auth);

  const { data, loading } = useQuery(
    getProgramOperatingPlanRequest,
    {
      variables: { operatingPlanId: programOperatingPlanId },
      fetchPolicy: 'no-cache',
    },
  );
  const [processIsActive, setProcessIsActive] = useState();

  useEffect(() => {
    if (data) {
      const { getProgramOperatingPlan } = data;
      const { operatingPlan } = getProgramOperatingPlan;
      setOperatingPlanData(getProgramOperatingPlan);
      setProcessIsActive(operatingPlan.opPlanProcess.isActive);
      if (!operatingPlan.opPlanProcess.isActive
          && userType === UNIT_DIRECTOR && operatingPlan.status !== APPROVED) {
        dispatch(startDisplayBanner());
      }
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t(routesDictionary.operatingPlanProgramCreateProfessor.breadCrumb[0]),
          operatingPlanData?.operatingPlan?.graduateUnit?.name,
          operatingPlanData?.studyProgram.name,
          t(routesDictionary.operatingPlanProgramCreateProfessor.breadCrumb[1]),
        ],
      ),
    ).then();
  }, [dispatch, t, operatingPlanData]);

  if (loading) return <CustomLoader />;

  return (
    <div>
      <TitleView
        title={operatingPlanData?.studyProgram.name}
        viewBackButton
        onClickBackButton={() => {
          navigate(
            routesDictionary.operatingPlanProgram.func(operatingPlanId, programOperatingPlanId),
          );
        }}
      />
      <ProfessorView
        readonly={!processIsActive}
        createProfessor
        backProfessorListView={false}
      />
    </div>
  );
};

export default CreateProfessorView;
