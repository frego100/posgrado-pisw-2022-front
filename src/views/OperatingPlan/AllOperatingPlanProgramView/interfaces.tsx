export interface ProgramOperatingPlan {
  key: string,
  code: string,
  type: string,
  name: string,
  status: string,
  actions: {
  }
}

export default {};
