import React from 'react';
import {
  Badge,
  Button, Col, Row,
} from 'antd';
import { CloudDownloadOutlined, EyeOutlined, QuestionOutlined } from '@ant-design/icons';
import { TFunction } from 'react-i18next';
import routesDictionary from '../../../routes/routesDictionary';
import {
  APPROVED, IN_PROGRESS, OBSERVED, RECEIVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR,
} from '../../../utils/constants';

const actions = (
  status: string,
  t: TFunction,
  setObservationsModal: any,
  observationsModal: any,
  programOperatingPlanId: any,
  programOperatingPlanName: any,
  setDocumentsModalData: any,
  navigate: any,
  operatingPlanId: any,
  programOperatingPlanIsApproved: boolean,
):{[index: string] : React.ReactElement} => ({
  [UNIT_DIRECTOR]: (
    <>
      {(status === OBSERVED) && (
        <Col>
          <Badge dot={!programOperatingPlanIsApproved}>
            <Button
              icon={<EyeOutlined />}
              type="ghost"
              onClick={() => {
                setObservationsModal({
                  ...observationsModal,
                  isVisible: true,
                  programOperatingPlanId,
                  programOperatingPlanName,
                  readOnly: true,
                });
              }}
            >
              {t('viewObservations')}
            </Button>
          </Badge>
        </Col>
      )}
      {(status === OBSERVED || status === APPROVED || status === RECEIVED) && (
      <Col>
        <Button
          icon={<CloudDownloadOutlined />}
          type="ghost"
          onClick={() => {
            setDocumentsModalData({
              isVisible: true,
              programOpPlanId: programOperatingPlanId,
            });
          }}
        >
          {t('download')}
        </Button>
      </Col>
      )}
      <Col>
        <Button
          icon={<EyeOutlined />}
          type="ghost"
          onClick={() => {
            navigate(
              routesDictionary.operatingPlanProgram.func(
                operatingPlanId,
                programOperatingPlanId,
              ),
            );
          }}
        >
          {t('view')}
        </Button>
      </Col>
    </>
  ),
  [SCHOOL_DIRECTOR]: (
    status === IN_PROGRESS
      ? (
        <span>
          {t('inProgress')}
        </span>
      )
      : (
        <>
          <Col>
            <Button
              icon={<EyeOutlined />}
              type="ghost"
              onClick={() => {
                navigate(
                  routesDictionary.operatingPlanProgram.func(
                    operatingPlanId,
                    programOperatingPlanId,
                  ),
                );
              }}
            >
              {t('review')}
            </Button>
          </Col>
          <Col>
            <Button
              icon={<QuestionOutlined />}
              type="ghost"
              onClick={() => {
                setObservationsModal({
                  ...observationsModal,
                  isVisible: true,
                  programOperatingPlanId,
                  programOperatingPlanName,
                  readOnly: (status === OBSERVED || status === APPROVED),
                });
              }}
            >
              {(status === OBSERVED || status === APPROVED) ? t('observations') : t('observe')}
            </Button>
          </Col>
          <Col>
            <Button
              icon={<CloudDownloadOutlined />}
              type="ghost"
              onClick={() => {
                setDocumentsModalData({
                  isVisible: true,
                  programOpPlanId: programOperatingPlanId,
                });
              }}
            >
              {t('download')}
            </Button>
          </Col>
        </>
      )
  ),
});

export const allOperatingPlanProgramColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'type',
    title: translate('type'),
    dataIndex: 'type',
    sorterType: 'alphabetical',
  },
  {
    key: 'name',
    title: translate('program'),
    dataIndex: 'name',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      operatingPlanId, programOperatingPlanId, programOperatingPlanName,
      programOperatingPlanIsApproved, navigate, userType, observationsModal,
      setObservationsModal, setDocumentsModalData, status,
    }: any) => (
      <Row gutter={[12, 0]}>
        {actions(
          status,
          translate,
          setObservationsModal,
          observationsModal,
          programOperatingPlanId,
          programOperatingPlanName,
          setDocumentsModalData,
          navigate,
          operatingPlanId,
          programOperatingPlanIsApproved,
        )[userType]}
      </Row>
    )
    ,
  },
];

export default {};
