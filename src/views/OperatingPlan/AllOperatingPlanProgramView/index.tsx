import React, { useEffect, useState } from 'react';
import {
  Button,
  Col,
  Row,
  Select,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { EyeOutlined } from '@ant-design/icons';
import { useQuery } from '@apollo/client';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';
import { loader } from 'graphql.macro';
import moment from 'moment';
import CustomTable from '../../../components/CustomTable';
import CustomFilter from '../../../components/CustomFilter';
import TitleView from '../../../components/TitleView';
import { startDisplayBanner, startHideBanner, startUpdateBreadcrumb } from '../../../store/thunks/ui';
import { AppDispatch } from '../../../store';
import ConfirmationModal from '../../../components/ConfirmationModal';
import { typeProgram } from '../../../components/CustomTable/constants';
import ObservationsButton from '../../../components/ObservationsButton';
import { APPROVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import routesDictionary from '../../../routes/routesDictionary';
import { ProgramOperatingPlan } from './interfaces';
import { allOperatingPlanProgramColumns } from './constants';
import DocumentsModal from '../../../components/DocumentsModal';
import ObservationsModal from '../../../components/ObservationsModal';
import OperatingPlanProcessModal from '../../../components/OperatingPlanProcessModal';
import { startsetOperatingPlanProcess } from '../../../store/thunks/auth';
import EmptyCard from '../../../components/EmptyCard';
import CustomCard from '../../../components/CustomCard';
import CustomLoader from '../../../components/CustomLoader';

const getAllOperatingPlansRequest = loader('./requests/getAllProgramOperatingPlan.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');
const getOperatingPlanProcessRequest = loader('./requests/getOperatingPlanProcess.gql');

const OperatingPlanIdView = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();

  const params = useParams();
  const { operatingPlanId } = params;
  const [processes, setProcesses] = useState<any>(null);
  // eslint-disable-next-line max-len
  const { userType, graduateUnitId, operatingPlanProcess } = useSelector((state: any) => state.auth);
  const [process, setProcess] = useState<any>(operatingPlanProcess);
  const [programOperatingPlans, setProgramOperatingPlans] = useState<ProgramOperatingPlan[]>([]);
  const [defaultValues, setDefaultValues] = useState<any>([]);

  const [sentOPDate, setSentOPDate] = useState();

  const [operatingPlan, setOperatingPlan] = useState<any>({
    status: '', id: '', graduateUnit: { name: '' },
  });

  const [operatingPlanProcessDataModal, setOperatingPlanProcessDataModal] = useState<any>({
    visible: false,
  });

  const {
    data: operatingPlanProcessesData,
    loading: operatingPlanProcessesLoading,
  } = useQuery(getOperatingPlanProcessesRequest);

  const {
    data: operatingPlanProcessData, refetch: refetchProcess,
  } = useQuery(
    getOperatingPlanProcessRequest,
    {
      variables: { process },
      fetchPolicy: 'no-cache',
      skip: !process,
    },
  );

  useEffect(() => {
    dispatch(startsetOperatingPlanProcess(process));
  }, [process]);

  useEffect(() => {
    if (operatingPlanProcessesData) {
      const { getOperatingPlanProcesses } = operatingPlanProcessesData;
      const operatingPlanProcessesArray = getOperatingPlanProcesses.map((
        { id, year }: any,
      ) => (
        { value: id, label: year }
      ));
      if (!process) setProcess(operatingPlanProcessesArray[0]?.value);
      setProcesses(operatingPlanProcessesArray);
    }
  }, [operatingPlanProcessesData]);

  useEffect(() => {
    const operatingPlanStatus = operatingPlan?.status;
    if (operatingPlanProcessData && operatingPlanStatus) {
      const { getOperatingPlanProcess } = operatingPlanProcessData;
      setOperatingPlanProcessDataModal({
        ...({
          processId: getOperatingPlanProcess.id,
          year: getOperatingPlanProcess.year,
          startDate: moment(getOperatingPlanProcess.startDate).format('YYYY-MM-DD'),
          endDate: moment(getOperatingPlanProcess.endDate).format('YYYY-MM-DD'),
          ...getOperatingPlanProcess.postponementDate
          && { postponementDate: moment(getOperatingPlanProcess.postponementDate) },
          processLetter: getOperatingPlanProcess?.processLetter,
          descriptiveIndexFile: getOperatingPlanProcess?.descriptiveIndexFile,
          refetch: refetchProcess,
          isActive: getOperatingPlanProcess.isActive,
        }),
      });
      if (userType === UNIT_DIRECTOR) {
        if (operatingPlanStatus !== APPROVED && !getOperatingPlanProcess.isActive) {
          dispatch(startDisplayBanner());
        } else dispatch(startHideBanner());
      }
    }
  }, [operatingPlanProcessData, operatingPlan?.status]);

  const { data, loading, refetch } = useQuery(
    getAllOperatingPlansRequest,
    {
      variables: {
        process,
        unit: operatingPlanId || graduateUnitId,
      },
      fetchPolicy: 'no-cache',
      skip: !process,
    },
  );
  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: '',
    operatingPlanId: '',
    refetch,
  });

  const [observationsModal, setObservationsModal] = useState<any>({
    isVisible: false,
    programOperatingPlanId: '',
    readOnly: false,
  });

  const [documentsModalData, setDocumentsModalData] = useState<any>({
    isVisible: false,
    programOpPlanId: '',
  });

  useEffect(() => {
    setObservationsModal({
      isVisible: false,
      programOperatingPlanId: '',
      readOnly: false,
    });
  }, [operatingPlan?.status]);

  useEffect(() => {
    if (data) {
      const { getAllProgramOperatingPlans } = data;
      const operatingPlansArray: ProgramOperatingPlan[] = [];
      getAllProgramOperatingPlans.map(({
        id, isApproved, studyProgram, operatingPlan: operatingPlanData,
      } : any, index: number) => (
        operatingPlansArray.push({
          // @ts-ignore
          key: index + 1,
          code: studyProgram.code,
          type: typeProgram[studyProgram.programType],
          name: studyProgram.name,
          actions: {
            operatingPlanId,
            programOperatingPlanId: id,
            programOperatingPlanName: studyProgram.name,
            programOperatingPlanIsApproved: isApproved,
            navigate,
            userType,
            observationsModal,
            setObservationsModal,
            setDocumentsModalData,
            status: operatingPlanData.status,
          },
        })
      ));
      setSentOPDate(getAllProgramOperatingPlans[0]?.operatingPlan?.sendDate);
      setOperatingPlan(getAllProgramOperatingPlans[0]?.operatingPlan);
      setProgramOperatingPlans(operatingPlansArray);
      setDefaultValues(operatingPlansArray);
      setConfirmationModalData({
        ...confirmationModalData,
        operatingPlanId: getAllProgramOperatingPlans[0]?.operatingPlan.id,
      });
    }
  }, [data, dispatch]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.allOperatingPlanProgram.breadCrumb.map((text) => t(text)),
          operatingPlan?.graduateUnit.name,
        ],
      ),
    ).then();
  }, [dispatch, t, operatingPlan]);

  if (loading) return <CustomLoader />;

  if (processes && processes.length === 0 && userType === UNIT_DIRECTOR) {
    return (
      <>
        <TitleView
          title={t('operatingPlans')}
          viewBackButton={false}
        />
        <EmptyCard
          title={t('emptyOperatingPlanTitle')}
          description={t('emptyOperatingPlanDescriptionUnitDirector')}
        />
      </>
    );
  }

  return (
    <div>
      <TitleView
        sequenceIndex={0}
        title={operatingPlan?.graduateUnit.name}
        {...userType === SCHOOL_DIRECTOR ? {
          viewBackButton: true,
          onClickBackButton: (() => {
            navigate(routesDictionary.allOperatingPlan.route);
          }),
        } : {
          viewBackButton: false,
        }
        }
        sizeAction={(process && userType === UNIT_DIRECTOR) ? 350 : 200}
        action={(
          <Row align="middle" gutter={[25, 25]} style={{ marginBottom: '32px' }}>
            <Col flex="200px">
              <span className="label">
                {t('academicYear')}
              </span>
              <Select
                labelInValue
                value={process}
                style={{ width: '100%' }}
                options={processes}
                onChange={(newValue) => setProcess(newValue.value)}
                loading={operatingPlanProcessesLoading}
              />
            </Col>
            {userType === UNIT_DIRECTOR && (
            <Col flex="150px">
              <br />
              <Button
                style={{ width: '100%' }}
                type="ghost"
                icon={<EyeOutlined />}
                onClick={() => setOperatingPlanProcessDataModal({
                  ...operatingPlanProcessDataModal, visible: true,
                })}
              >
                {t('review')}
              </Button>
            </Col>
            )}
          </Row>
          )}
      />
      <CustomCard>
        <CustomFilter
          typeFilter="operatingPlanFilters"
          operatingPlanStatus={operatingPlan?.status}
          sentOPDate={sentOPDate}
          approvalDocument={{
            approvalRdName: operatingPlan?.approvalRdName,
            approvalRdUrl: operatingPlan?.approvalRdUrl,
          }}
          setDataSource={setProgramOperatingPlans}
          defaultDataSource={defaultValues}
          sentLate={operatingPlan?.sentLate}
        />
        <CustomTable
          loading={loading}
          dataSource={programOperatingPlans}
          columns={allOperatingPlanProgramColumns(t)}
        />
        <Row justify="end">
          <ObservationsButton
            userType={userType}
            operatingPlanStatus={operatingPlan?.status}
            confirmationModalData={confirmationModalData}
            setConfirmationModalData={setConfirmationModalData}
            isActive={operatingPlanProcessDataModal.isActive}
          />
          <ConfirmationModal
            confirmationModalData={confirmationModalData}
            setConfirmationModalData={setConfirmationModalData}
          />
          <DocumentsModal
            isVisible={documentsModalData.isVisible}
            setIsVisible={setDocumentsModalData}
            programOpPlanId={documentsModalData.programOpPlanId}
          />
          <ObservationsModal
            observationsModal={observationsModal}
            setObservationsModal={setObservationsModal}
          />
          <OperatingPlanProcessModal
            modalData={operatingPlanProcessDataModal}
            setModalData={setOperatingPlanProcessDataModal}
            readOnly
          />
        </Row>
      </CustomCard>
    </div>
  );
};

export default OperatingPlanIdView;
