import {
  Button, Col, Row,
} from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import React from 'react';
import { TFunction } from 'react-i18next';
import routesDictionary from '../../../routes/routesDictionary';

export const allOperatingPlanColumns = (t: TFunction) => [
  {
    key: 'unitDirector',
    title: t('unitDirector'),
    dataIndex: 'unitDirector',
    sorterType: 'alphabetical',
  },
  {
    key: 'graduateUnit',
    title: t('graduateUnit'),
    dataIndex: 'graduateUnit',
    sorterType: 'alphabetical',
  },
  {
    key: 'receptionDate',
    title: t('receptionDate'),
    dataIndex: 'receptionDate',
    sorterType: 'alphabetical',
  },
  {
    key: 'approvalDate',
    title: t('approvalDate'),
    dataIndex: 'approvalDate',
    sorterType: 'alphabetical',
  },
  {
    key: 'state',
    title: t('state'),
    dataIndex: 'state',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: t('actions'),
    dataIndex: 'actions',
    render: ({
      operatingPlanId, navigate,
    }: any) => (
      <Row>
        <Col>
          <Button
            type="ghost"
            icon={<EyeOutlined />}
            onClick={() => {
              navigate(
                routesDictionary.allOperatingPlanProgram.func(operatingPlanId),
              );
            }}
          >
            {t('review')}
          </Button>
        </Col>
      </Row>
    ),
  },
];

export default {};
