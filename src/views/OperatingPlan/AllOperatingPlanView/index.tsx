import React, { useEffect, useState } from 'react';
import {
  Button, Col, Descriptions, Row, Select,
} from 'antd';
import { useQuery } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { loader } from 'graphql.macro';
import { PlusCircleOutlined, EyeOutlined } from '@ant-design/icons';
import { AnimateGroup } from 'react-simple-animate';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import CustomTable from '../../../components/CustomTable';
import { allOperatingPlanColumns } from './constants';
import { OperatingPlan } from './interfaces';
import routesDictionary from '../../../routes/routesDictionary';
import OperatingPlanProcessModal from '../../../components/OperatingPlanProcessModal';
import { startsetOperatingPlanProcess } from '../../../store/thunks/auth';
import EmptyCard from '../../../components/EmptyCard';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import CustomLoader from '../../../components/CustomLoader';
import CustomCard from '../../../components/CustomCard';

const getOperatingPlansDataRequest = loader('./requests/getOperatingPlansData.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');

const { Item } = Descriptions;

const OperatingPlanView = () => {
  const [processes, setProcesses] = useState<any>([]);
  const { operatingPlanProcess, userType } = useSelector((state: any) => state.auth);

  const [process, setProcess] = useState<any>(operatingPlanProcess);
  const [operatingPlans, setOperatingPlans] = useState<OperatingPlan[]>([]);

  // approved and not aproved operaitns plans
  const [countApprovedOP, setCountApprovedOp] = useState<any>();
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  const navigate = useNavigate();

  const [operatingPlanProcessDataModal, setOperatingPlanProcessDataModal] = useState<any>({
    visible: false,
  });

  const handleChangeProcess = (value: any) => {
    setProcess(value);
  };

  const {
    data: operatingPlanProcessesData,
    loading: operatingPlanProcessesLoading,
    refetch: operatingPlanProcessesRefetch,
  } = useQuery(getOperatingPlanProcessesRequest);

  const {
    data, loading, refetch,
  } = useQuery(getOperatingPlansDataRequest, {
    variables: { process },
    fetchPolicy: 'no-cache',
    skip: !process,
  });

  const [operatingPlanProcessModal, setOperatingPlanProcessModal] = useState<any>({
    visible: false,
    refetch: operatingPlanProcessesRefetch,
  });

  useEffect(() => {
    dispatch(startsetOperatingPlanProcess(process));
  }, [process]);

  useEffect(() => {
    if (operatingPlanProcessesData) {
      const { getOperatingPlanProcesses } = operatingPlanProcessesData;
      const operatingPlanProcessesArray: any = [];

      getOperatingPlanProcesses.forEach(({ id, year }: any) => {
        operatingPlanProcessesArray.push({ label: year, value: id });
      });
      if (!process) {
        setProcess(getOperatingPlanProcesses[0]?.id);
      }
      setProcesses(operatingPlanProcessesArray.length === 0 ? null : operatingPlanProcessesArray);
    }
  }, [operatingPlanProcessesData]);

  useEffect(() => {
    if (data) {
      const { getOperatingPlans, getOperatingPlanProcess } = data;
      const operatingPlansArray: OperatingPlan[] = [];
      setOperatingPlanProcessDataModal({
        ...({
          processId: getOperatingPlanProcess.id,
          year: getOperatingPlanProcess.year,
          startDate: moment(getOperatingPlanProcess.startDate).format('YYYY-MM-DD'),
          endDate: moment(getOperatingPlanProcess.endDate).format('YYYY-MM-DD'),
          processLetter: getOperatingPlanProcess?.processLetter,
          descriptiveIndexFile: getOperatingPlanProcess?.descriptiveIndexFile,
          refetch,
          ...getOperatingPlanProcess.postponementDate
          && { postponementDate: moment(getOperatingPlanProcess.postponementDate) },
        }),
      });

      const countApprovedObj = JSON.parse(getOperatingPlanProcess?.countApproved);
      setCountApprovedOp(countApprovedObj);

      getOperatingPlans.forEach(({
        status, graduateUnit, sendDate, approvalDate,
      }: any) => {
        operatingPlansArray.push({
          key: graduateUnit.id,
          unitDirector: graduateUnit.unitmanagerSet[0]?.user
            ? `${graduateUnit.unitmanagerSet[0]?.user.firstName}  
               ${graduateUnit.unitmanagerSet[0]?.user.lastName}`
            : '-',
          graduateUnit: graduateUnit.name,
          receptionDate: sendDate ? moment(sendDate).format('YYYY-MM-DD') : '-',
          approvalDate: approvalDate ? moment(approvalDate).format('YYYY-MM-DD') : '-',
          state: status,
          actions: {
            operatingPlanId: graduateUnit.id,
            navigate,
          },
        });
      });
      setOperatingPlans(operatingPlansArray);
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.allOperatingPlan.breadCrumb.map((text) => t(text)),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  if (operatingPlanProcessesLoading) return <CustomLoader />;

  if (!processes) {
    return (
      <>
        <TitleView
          title={t('operatingPlans')}
          viewBackButton={false}
        />
        <EmptyCard
          title={t('emptyOperatingPlanTitle')}
          description={t(userType === UNIT_DIRECTOR ? 'emptyOperatingPlanDescriptionUnitDirector' : 'emptyOperatingPlanDescriptionSchoolDirector')}
          {...userType === SCHOOL_DIRECTOR && {
            action: (
              <Row align="middle" gutter={[0, 16]} justify="center">
                <Button
                  type="primary"
                  icon={<PlusCircleOutlined />}
                  onClick={() => setOperatingPlanProcessModal({
                    ...operatingPlanProcessModal, visible: true,
                  })}
                >
                  {t('openOperatingPlan')}
                </Button>
              </Row>
            ),
          }}
        />
        <OperatingPlanProcessModal
          modalData={operatingPlanProcessModal}
          setModalData={setOperatingPlanProcessModal}
          readOnly={false}
        />
      </>
    );
  }
  return (
    <AnimateGroup play>
      <TitleView
        sequenceIndex={0}
        title={t('operatingPlans')}
        viewBackButton={false}
        action={(
          <Row align="middle" gutter={[0, 16]} justify="end">
            <Col>
              <Button
                type="primary"
                icon={<PlusCircleOutlined />}
                onClick={() => setOperatingPlanProcessModal({
                  ...operatingPlanProcessModal, visible: true,
                })}
              >
                {t('operatingPlan')}
              </Button>
            </Col>
          </Row>
          )}
      />
      <CustomCard>
        {process
          && (
            <Row align="middle" gutter={[15, 5]} style={{ marginBottom: '32px' }}>
              <Col flex="200px">
                <span className="label">
                  {t('academicYear')}
                </span>
                <Select
                  style={{ width: '100%' }}
                  value={process}
                  onChange={handleChangeProcess}
                  options={processes}
                />
              </Col>
              <Col flex="200px">
                <br />
                <Button
                  style={{ width: '100%' }}
                  type="ghost"
                  icon={<EyeOutlined />}
                  onClick={() => setOperatingPlanProcessDataModal({
                    ...operatingPlanProcessDataModal, visible: true,
                  })}
                >
                  {t('review')}
                </Button>
              </Col>
              <Col style={{ marginLeft: 'auto' }}>
                <span className="label">{t('summery')}</span>
                <Descriptions bordered size="small" contentStyle={{ fontSize: 12 }} labelStyle={{ fontSize: 12 }}>
                  <Item label={t('approved')}>{countApprovedOP?.approved}</Item>
                  <Item label={t('notApproved')}>{countApprovedOP?.not_yet_approved}</Item>
                </Descriptions>
              </Col>
            </Row>
          )}
        <CustomTable
          columns={allOperatingPlanColumns(t)}
          dataSource={operatingPlans}
          loading={loading}
        />
        <OperatingPlanProcessModal
          modalData={operatingPlanProcessModal}
          setModalData={setOperatingPlanProcessModal}
          readOnly={false}
        />
        <OperatingPlanProcessModal
          modalData={operatingPlanProcessDataModal}
          setModalData={setOperatingPlanProcessDataModal}
          readOnly
        />
      </CustomCard>
    </AnimateGroup>
  );
};

export default OperatingPlanView;
