export interface OperatingPlan {
  key: string,
  unitDirector: string,
  graduateUnit: string,
  receptionDate: string,
  approvalDate: string,
  state: string,
  actions: {
  }
}

export default {};
