import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { AppDispatch } from '../../../store';
import { startDisplayBanner, startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import SubjectComponent from '../../../components/SubjectView';
import { APPROVED, RECEIVED, UNIT_DIRECTOR } from '../../../utils/constants';
import routesDictionary from '../../../routes/routesDictionary';

const getCourseRequest = loader('./requests/getCourse.gql');

const SubjectView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();
  const { userType } = useSelector((state: any) => state.auth);

  const [subject, setSubject] = useState<any>();
  const params = useParams();
  const {
    operatingPlanId,
    programOperatingPlanId, subjectId,
  } = params;

  const location = useLocation();
  // @ts-ignore
  const professorView = location?.state?.professorView;
  const professorRegistrationId = location?.state?.professorRegistrationId;

  const [operatingPlanProgram, setOperatingPlanProgram] = useState<any>();
  const [processIsActive, setProcessIsActive] = useState();
  const { data, loading, refetch } = useQuery(
    getCourseRequest,
    {
      variables: { subjectId },
      fetchPolicy: 'no-cache',
    },
  );

  useEffect(() => {
    if (data) {
      const { getCourse } = data;
      const { operatingPlanProgram: opData } = getCourse;

      getCourse.coursegroupSet = getCourse.coursegroupSet
        .filter((group: any) => group.isInOperatingPlan);
      setSubject(getCourse);
      setOperatingPlanProgram(getCourse?.operatingPlanProgram);
      setProcessIsActive(opData.operatingPlan.opPlanProcess.isActive);
      if (!opData.operatingPlan.opPlanProcess.isActive
          && userType === UNIT_DIRECTOR && opData.operatingPlan.status !== APPROVED) {
        dispatch(startDisplayBanner());
      }
    }
  }, [data]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t(routesDictionary.operatingPlanProgramSubject.breadCrumb[0]),
          operatingPlanProgram?.operatingPlan?.graduateUnit?.name,
          operatingPlanProgram?.studyProgram?.name,
          t(routesDictionary.operatingPlanProgramSubject.breadCrumb[1]),
        ],
      ),
    ).then();
  }, [dispatch, t, operatingPlanProgram]);

  if (loading || !subject) {
    return <CustomLoader />;
  }

  return (
    <div>
      <TitleView
        title={operatingPlanProgram.studyProgram.name}
        viewBackButton
        onClickBackButton={() => {
          navigate(
            professorView
              ? routesDictionary
                .operatingPlanProgramProfessor
                .func(operatingPlanId, programOperatingPlanId, professorRegistrationId)
              : routesDictionary.operatingPlanProgram.func(operatingPlanId, programOperatingPlanId),
            { state: { displaySubjects: true } },

          );
        }}
      />
      <SubjectComponent
        readonly={userType !== UNIT_DIRECTOR
            || !processIsActive
            || operatingPlanProgram.operatingPlan.status === RECEIVED
            || operatingPlanProgram.operatingPlan.status === APPROVED}
        subjectData={subject}
        refetch={refetch}
      />
    </div>
  );
};
export default SubjectView;
