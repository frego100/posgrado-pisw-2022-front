import React, { useEffect, useState } from 'react';
import {
  Button, Form, message, Modal,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { formItems } from './constants';
import FormCard from '../../../../components/FormCard';

const getTemplateByCodeRequest = loader('./requests/getTemplateByCode.gql');
const uploadProffesorListRequest = loader('./requests/uploadProfessorsList.gql');
interface ProfessorListModalProps {
    professorListModalData: any
    setProfessorListModalData: any
}

const ProfessorListModal:React.FC<ProfessorListModalProps> = ({
  professorListModalData,
  setProfessorListModalData,
}) => {
  const { t } = useTranslation();
  const [form] = useForm();
  const [templateProfessorRegistration, setTemplateProfessorRegistration] = useState<any>();
  const templateCode = 'RDPF';

  const { data: templateData } = useQuery(
    getTemplateByCodeRequest,
    {
      variables: { templateCode },
      fetchPolicy: 'no-cache',
    },
  );

  const [uploadProffesorListMutation, { loading }] = useMutation(
    uploadProffesorListRequest,
  );

  useEffect(() => {
    if (templateData) {
      const { getTemplateByCode } = templateData;
      setTemplateProfessorRegistration(getTemplateByCode);
    }
  }, [templateData]);

  const handleOk = () => {
    form.validateFields()
      .then((values) => {
        uploadProffesorListMutation({
          variables: {
            input: {
              operatingPlanProgram: professorListModalData.programOperatingPlanId,
              professorsList: values.uploadProfessorsList.originFileObj,
            },
          },
        })
          .then(({ data }) => {
            const { uploadProfessorsList: resultData } = data;
            if (resultData.feedback.status === 'SUCCESS') {
              setProfessorListModalData({ isVisible: false });
              message.success(resultData.feedback.message);
              message.warning(t('afterUploadProfessorList'));
              professorListModalData.refetch();
            } else {
              message.error(resultData.feedback.message);
              professorListModalData.refetch();
            }
          }).catch(() => {
            form.setFields([{
              name: 'uploadProfessorsList',
              errors: [t('invalidFile')],
            }]);
          });
      });
  };

  const handleCancel = () => {
    setProfessorListModalData({ ...professorListModalData, isVisible: false });
  };

  return (
    <Modal
      title={t('registerProfessorList')}
      centered
      width={500}
      bodyStyle={{ maxHeight: '330px' }}
      closable
      destroyOnClose
      open={professorListModalData.isVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={[
        <Button key="cancel" onClick={handleCancel}>
          {t('cancel')}
        </Button>,
        <Button key="ok" type="primary" onClick={handleOk} loading={loading}>
          {t('confirm')}
        </Button>,
      ]}
    >
      <p style={{ textAlign: 'justify' }}>
        {t('uploadProfessorFileInfoText')}
      </p>
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
        form={form}
      >
        <FormCard
          withoutTitle
          withoutDeleteIcon
          itemList={formItems(
            form,
            templateProfessorRegistration,
            t,
          )}
          readonly={false}
        />
      </Form>
    </Modal>
  );
};

export default ProfessorListModal;
