import { TFunction } from 'react-i18next';
import { PaperClipOutlined } from '@ant-design/icons';
import React from 'react';
import CustomUploadFile from '../../../../components/CustomUploadFile';
import { generateUrlDownload } from '../../../../utils/tools';

const listProfessorsItems = (
  form: any,
  templateProfessorRegistration: any,
  t: TFunction,
) => [
  {
    name: 'professorTemplate',
    component: (
      <a
        className="link-component"
        target="_blank"
        href={generateUrlDownload(templateProfessorRegistration?.templateFile)}
        rel="noreferrer"
      >
        <PaperClipOutlined />
        {' '}
        {t('downloadProfessorTemplate')}
      </a>
    ),
    rules: [
      {
        required: false,
      },
    ],
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'uploadProfessorsList',
    component: (
      <CustomUploadFile
        disabled={false}
        form={form}
        nameComponent="uploadProfessorsList"
        typeFile=".xlsx"
      />
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('uploadProfessorList'),
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
];

export const formItems = (
  form: any,
  templateProfessorRegistration,
  t: TFunction,
) => [
  ...listProfessorsItems(
    form,
    templateProfessorRegistration,
    t,
  ),
];
export default {};
