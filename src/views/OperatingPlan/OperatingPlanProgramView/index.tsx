import React, { useEffect, useState } from 'react';
import {
  Button, Col, Row, Tabs, message,
} from 'antd';
import { UserAddOutlined, PaperClipOutlined, FileAddFilled } from '@ant-design/icons';
import { useQuery, useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'antd/lib/form/Form';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { AppDispatch } from '../../../store';
import { APPROVED, RECEIVED, UNIT_DIRECTOR } from '../../../utils/constants';
import CustomLoader from '../../../components/CustomLoader';
import CustomUploadFile from '../../../components/CustomUploadFile';
import { displayComponent, generateUrlDownload, getMediaLink } from '../../../utils/tools';
import ConfirmationModal from '../../../components/ConfirmationModal';
import { startDisplayBanner, startUpdateBreadcrumb } from '../../../store/thunks/ui';
import CustomTable from '../../../components/CustomTable';
import CustomFilter from '../../../components/CustomFilter';
// eslint-disable-next-line no-unused-vars
import { typeDocument, typeGender } from '../../../components/CustomTable/constants';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import './styles.scss';
import { Professor, Subject } from './interfaces';
import { operatingPlanProfessorsColumns, operatingPlanSubjectsColumns } from './constants';
import FileProfessorsModal from './FileProfessorsModal';
import CustomCard from '../../../components/CustomCard';

const { TabPane } = Tabs;

const getAllOperatingPlanProfessorsRequest = loader('./requests/getAllOperatingPlanProfessors.gql');
const getAllOperatingPlanSubjectsRequest = loader('./requests/getAllOperatingPlanSubjects.gql');
const uploadOperatingPlanDocumentRequest = loader('./requests/uploadOperatingPlanDocumentMutation.gql');

const ProgramOperatingPlanIdView = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const [form] = useForm();
  const params = useParams();
  const { operatingPlanId, programOperatingPlanId } = params;
  const location = useLocation();
  // @ts-ignore
  const subjects = location?.state?.displaySubjects;
  const { t } = useTranslation();
  const [operatingPlanData, setOperatingPlanData] = useState<any>({});
  const [operatingPlanTemplate, setOperatingPlanTemplate] = useState<any>();
  const { userType } = useSelector((state: any) => state.auth);
  const [operatingPlanProfessors, setOperatingPlanProfessors] = useState<Professor[]>([]);
  const [operatingPlanSubjects, setOperatingPlanSubjects] = useState<Subject[]>([]);
  const [defaultProfessorsValues, setDefaultProfessorsValues] = useState<any>([]);
  const [defaultSubjectsValues, setDefaultSubjectsValues] = useState<any>([]);
  const [professorModalData, setProfessorModalData] = useState<any>({
    visible: false, programOperatingPlanId,
  });

  const [viewSubjects] = useState<boolean>(!!subjects);

  const { data: professorsData, loading: professorsLoading, refetch: professorsRefetch } = useQuery(
    getAllOperatingPlanProfessorsRequest,
    {
      variables: {
        operatingPlanProgramId: programOperatingPlanId,
        isInOperatingPlan: true,
      },
      fetchPolicy: 'no-cache',
    },
  );

  const { data: subjectsData, loading: subjectsLoading, refetch: subjectsRefetch } = useQuery(
    getAllOperatingPlanSubjectsRequest,
    {
      variables: { operatingPlanId: programOperatingPlanId },
      fetchPolicy: 'no-cache',
    },
  );

  const [confirmationModalData, setConfirmationModalData] = useState<any>({
    isVisible: false,
    type: '',
    operatingPlanId: '',
    refetch: professorsRefetch,
  });

  const [UploadOpPlanDocument] = useMutation(uploadOperatingPlanDocumentRequest);

  useEffect(() => {
    if (professorsData) {
      const {
        getAllOperatingPlanProfessors,
        getProgramOperatingPlan,
      } = professorsData;
      const professorsArray: any[] = [];
      const {
        studyProgram, planDocument, planDocumentUrl, operatingPlan,
      } = getProgramOperatingPlan;
      getAllOperatingPlanProfessors?.forEach((professorData: any, index: number) => {
        const { professor } = professorData;
        professorsArray.push({
          // @ts-ignore
          key: index + 1,
          code: professor.id,
          firstName: professor.user.firstName,
          lastName: `${professor.user.lastName} ${professor.user.maternalLastName}`,
          documentIdentification: (
            `${typeDocument[professor.user.useridentificationSet[0]?.identificationType]}
            - ${professor.user.useridentificationSet[0]?.number}`
          ),
          numberDocument: professor.user.useridentificationSet[0]?.number,
          email: professor.user.email,
          country: professor.user.country.name || '-',
          professorType: professor.professorType === 'A_1' ? 'Invitado' : 'De la universidad',
          actions: {
            navigate,
            registrationId: professorData.id,
            confirmationModalData,
            setConfirmationModalData,
            params,
            readOnly: !operatingPlan.opPlanProcess.isActive || (userType === UNIT_DIRECTOR
                && (
                  getProgramOperatingPlan.operatingPlan.status === RECEIVED
              || getProgramOperatingPlan.operatingPlan.status === APPROVED)),
            userType,
          },
        });
      });

      setOperatingPlanData({
        name: studyProgram.name,
        planDocument,
        planDocumentUrl,
        id: operatingPlan.id,
        status: operatingPlan.status,
        unitName: operatingPlan.graduateUnit.name,
        isActive: operatingPlan.opPlanProcess.isActive,
      });
      if (!operatingPlan.opPlanProcess.isActive
          && userType === UNIT_DIRECTOR && operatingPlan.status !== APPROVED) {
        dispatch(startDisplayBanner());
      }
      form.setFieldsValue({
        studyPlan: planDocument,
      });
      setOperatingPlanProfessors(professorsArray);
      setDefaultProfessorsValues(professorsArray);
      setOperatingPlanTemplate(operatingPlan?.opPlanProcess?.descriptiveIndexFile);
    }
  }, [professorsData]);

  useEffect(() => {
    if (subjectsData) {
      const { getAllCourses, getProgramOperatingPlan } = subjectsData;
      const subjectsArray: Subject[] = [];
      const {
        studyProgram, planDocument, planDocumentUrl, operatingPlan,
      } = getProgramOperatingPlan;

      getAllCourses.forEach(({
        id, status, studyPlanSubject, numberOfGroups,
      }: any) => {
        subjectsArray.push({
          key: studyPlanSubject.id,
          // @ts-ignore
          subjectName: studyPlanSubject.subject,
          code: studyPlanSubject.code,
          credits: studyPlanSubject.credits,
          hours: studyPlanSubject.hours,
          semester: studyPlanSubject.semester,
          year: studyPlanSubject.studyPlan.year,
          numberOfGroups,
          state: status ? 'Completado' : 'No completado',
          actions: {
            operatingPlanSubjectId: id,
            navigate,
            dispatch,
            params,
            readOnly: !operatingPlan.opPlanProcess.isActive || (userType === UNIT_DIRECTOR
              && (operatingPlanData.status === RECEIVED || operatingPlanData.status === APPROVED)),
          },
        });
      });

      setOperatingPlanData({
        name: studyProgram.name,
        planDocument,
        planDocumentUrl,
        id: operatingPlan.id,
        status: operatingPlan.status,
        unitName: operatingPlan.graduateUnit.name,
        isActive: operatingPlan.opPlanProcess.isActive,
      });
      form.setFieldsValue({
        studyPlan: planDocument,
      });
      setOperatingPlanSubjects(subjectsArray);
      setDefaultSubjectsValues(subjectsArray);
      setOperatingPlanTemplate(operatingPlan?.opPlanProcess?.descriptiveIndexFile);
    }
  }, [subjectsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.operatingPlanProgram.breadCrumb.map((text) => t(text)),
          operatingPlanData.unitName,
          operatingPlanData.name,
        ],
      ),
    ).then();
  }, [dispatch, t, operatingPlanData]);

  if (viewSubjects ? subjectsLoading : professorsLoading) {
    return <CustomLoader />;
  }

  return (
    <div>
      <TitleView
        title={operatingPlanData.name}
        viewBackButton
        sizeAction={operatingPlanTemplate ? 430 : 250}
        onClickBackButton={() => {
          navigate(
            routesDictionary.allOperatingPlanProgram.func(operatingPlanId),
          );
        }}
        action={userType === UNIT_DIRECTOR ? (
          <Row gutter={[20, 10]} justify="end">
            <Col>
              <Row className="label">
                {t('operatingPlan')}
              </Row>
              <Row>
                <CustomUploadFile
                  disabled={
                    operatingPlanData.status === RECEIVED
                    || operatingPlanData.status === APPROVED
                  }
                  form={form}
                  nameComponent="studyPlan"
                  urlFile={operatingPlanData.planDocumentUrl
                      && generateUrlDownload(operatingPlanData.planDocumentUrl)}
                  uploadFunction={() => {
                    UploadOpPlanDocument({
                      variables: {
                        input: {
                          opPlanProgramId: programOperatingPlanId,
                          opPlanFile: form.getFieldValue('studyPlan').originFileObj,
                        },
                      },
                    }).then(({ data }: any) => {
                      const { uploadPlanDocument } = data;
                      if (uploadPlanDocument.feedback.status === 'SUCCESS') {
                        message.success(uploadPlanDocument.feedback.message);
                        if (viewSubjects) {
                          subjectsRefetch();
                        } else {
                          professorsRefetch();
                        }
                      }
                    });
                  }}
                />
              </Row>
              {
                operatingPlanTemplate
                && (
                  <Row style={{ marginTop: 5 }}>
                    <a
                      target="_blank"
                      href={getMediaLink(operatingPlanTemplate)}
                      rel="noreferrer"
                    >
                      <PaperClipOutlined />
                      {' '}
                      {t('downloadOperatingPlanTemplate')}
                    </a>
                  </Row>

                )
              }
            </Col>
          </Row>
        ) : null}
      />
      <Tabs type="card" defaultActiveKey={viewSubjects ? '2' : '1'}>
        <TabPane tab={t('professors')} key="1">
          <CustomCard className="tabContainer">
            <TitleView
              title={t('professors')}
              viewBackButton={false}
              sizeAction={400}
              action={
                displayComponent(
                  userType,
                  operatingPlanData.status,
                  <Row align="middle" gutter={[10, 16]} justify="end">
                    <Col>
                      <Button
                        type="primary"
                        icon={<UserAddOutlined />}
                        onClick={() => {
                          navigate(
                            routesDictionary
                              .operatingPlanProgramCreateProfessor.func(
                                operatingPlanId,
                                programOperatingPlanId,
                              ),
                          );
                        }}
                      >
                        {t('newProfessor')}
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        type="primary"
                        icon={<FileAddFilled />}
                        onClick={() => {
                          setProfessorModalData({
                            ...professorModalData,
                            refetch: professorsRefetch,
                            isVisible: true,
                          });
                        }}
                      >
                        {t('registerProfessorList')}
                      </Button>
                    </Col>
                  </Row>,
                  operatingPlanData.isActive,
                )
              }
            />
            <CustomFilter
              typeFilter="professorListFilters"
              operatingPlanStatus={operatingPlanData.status}
              setDataSource={setOperatingPlanProfessors}
              defaultDataSource={defaultProfessorsValues}
            />
            <CustomTable
              loading={professorsLoading}
              dataSource={operatingPlanProfessors}
              columns={operatingPlanProfessorsColumns(t)}
            />
          </CustomCard>
        </TabPane>
        <TabPane tab={t('subjects')} key="2">
          <CustomCard className="tabContainer">
            <TitleView
              title={t('subjects')}
              viewBackButton={false}
            />
            <CustomFilter
              typeFilter="subjectListFilters"
              operatingPlanStatus={operatingPlanData.status}
              setDataSource={setOperatingPlanSubjects}
              defaultDataSource={defaultSubjectsValues}
            />
            <CustomTable
              dataSource={operatingPlanSubjects}
              loading={subjectsLoading}
              columns={operatingPlanSubjectsColumns(t, userType)}
            />
          </CustomCard>
        </TabPane>

      </Tabs>
      <ConfirmationModal
        confirmationModalData={confirmationModalData}
        setConfirmationModalData={setConfirmationModalData}
      />
      <FileProfessorsModal
        professorListModalData={professorModalData}
        setProfessorListModalData={setProfessorModalData}
      />

    </div>
  );
};

export default ProgramOperatingPlanIdView;
