export interface OperatingPlan {
  name: string,
  planDocument: any,
  planDocumentUrl: any,
}

export interface Professor{
  key: string,
  code: string,
  firstName: string,
  lastName: string,
  typeDocument: string,
  numberDocument: string,
  academicGrade: string,
  country: string,
  gender: string,
  email: string,
  professorType : String,
  actions: {
  }
}

export interface Subject {
  key: string,
  code: string,
  name: string,
  program?: string,
  credits: number,
  hours: number,
  semester: number,
  year: string,
  actions: {
  }
}
