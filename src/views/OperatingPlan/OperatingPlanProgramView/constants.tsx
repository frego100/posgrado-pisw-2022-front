import {
  Button, Col, Row, Tooltip,
} from 'antd';
import {
  DeleteFilled, EditFilled, EyeOutlined, InfoCircleOutlined,
} from '@ant-design/icons';
import React from 'react';
import { TFunction } from 'react-i18next';
import routesDictionary from '../../../routes/routesDictionary';
import { UNIT_DIRECTOR } from '../../../utils/constants';

export const operatingPlanSubjectsColumns = (translate: TFunction, userType: string) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectName',
    title: translate('name'),
    dataIndex: 'subjectName',
    sorterType: 'alphabetical',
  },
  {
    key: 'credits',
    title: translate('credits'),
    dataIndex: 'credits',
    sorterType: 'numerical',
  },
  {
    key: 'hours',
    title: translate('totalHours'),
    dataIndex: 'hours',
    sorterType: 'numerical',
  },
  {
    key: 'semester',
    title: translate('semester'),
    dataIndex: 'semester',
    sorterType: 'numerical',
  },
  {
    key: 'numberOfGroups',
    title: translate('numberOfGroups'),
    dataIndex: 'numberOfGroups',
    sorterType: 'numerical',
  },
  {
    key: 'year',
    title: translate('year'),
    dataIndex: 'year',
  },
  ...(userType === UNIT_DIRECTOR
    ? [
      {
        key: 'state',
        title: (
          <Row align="middle">
            <span style={{ marginRight: 10 }}>
              {translate('status')}
            </span>
            <Tooltip title={translate('statusTooltip')}>
              <InfoCircleOutlined />
            </Tooltip>
          </Row>
        ),
        dataIndex: 'state',
      },
    ]
    : []
  ),
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      operatingPlanSubjectId,
      navigate,
      params,
      readOnly,
    }: any) => {
      const { operatingPlanId, programOperatingPlanId } = params;
      return (
        <Row gutter={[12, 0]}>
          {readOnly ? (
            <Col>
              <Button
                type="ghost"
                icon={<EyeOutlined />}
                onClick={() => {
                  navigate(
                    routesDictionary.operatingPlanProgramSubject.func(
                      operatingPlanId,
                      programOperatingPlanId,
                      operatingPlanSubjectId,
                    ),
                  );
                }}
              >
                {translate('view')}
              </Button>
            </Col>
          ) : (
            userType === UNIT_DIRECTOR
              ? (
                <Col>
                  <Button
                    type="ghost"
                    icon={<EditFilled />}
                    onClick={() => {
                      navigate(
                        routesDictionary.operatingPlanProgramSubject.func(
                          operatingPlanId,
                          programOperatingPlanId,
                          operatingPlanSubjectId,
                        ),
                      );
                    }}
                  >
                    {translate('edit')}
                  </Button>
                </Col>
              )
              : (
                <Col>
                  <Button
                    type="ghost"
                    icon={<EyeOutlined />}
                    onClick={() => {
                      navigate(
                        routesDictionary.operatingPlanProgramSubject.func(
                          operatingPlanId,
                          programOperatingPlanId,
                          operatingPlanSubjectId,
                        ),
                      );
                    }}
                  >
                    {translate('view')}
                  </Button>
                </Col>
              )
          )}
        </Row>
      );
    },
  },
];

export const operatingPlanProfessorsColumns = (translate: TFunction) => [
  {
    key: 'key',
    title: 'N°',
    dataIndex: 'key',
  },
  {
    key: 'firstName',
    title: translate('firstName'),
    dataIndex: 'firstName',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastName',
    title: translate('lastName'),
    dataIndex: 'lastName',
    sorterType: 'alphabetical',
  },
  {
    key: 'documentIdentification',
    title: translate('document'),
    dataIndex: 'documentIdentification',
    sorterType: 'alphabetical',
  },
  /* {
    key: 'typeDocument',
    title: translate('documentType'),
    dataIndex: 'typeDocument',
  },
  {
    key: 'numberDocument',
    title: translate('documentNumber'),
    dataIndex: 'numberDocument',
  }, */
  {
    key: 'country',
    title: translate('country'),
    dataIndex: 'country',
    sorterType: 'alphabetical',
  },
  {
    key: 'email',
    title: translate('email'),
    dataIndex: 'email',
    sorterType: 'alphabetical',
  },
  {
    key: 'professorType',
    title: translate('professorType'),
    dataIndex: 'professorType',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      navigate,
      registrationId,
      confirmationModalData,
      setConfirmationModalData,
      params,
      readOnly,
      userType,
    }: any) => {
      const { operatingPlanId, programOperatingPlanId } = params;
      return (
        <Row gutter={[12, 0]}>
          {readOnly ? (
            <Col>
              <Button
                type="ghost"
                icon={<EyeOutlined />}
                onClick={() => {
                  navigate(
                    routesDictionary.operatingPlanProgramProfessor.func(
                      operatingPlanId,
                      programOperatingPlanId,
                      registrationId,
                    ),
                  );
                }}
              >
                {translate('view')}
              </Button>
            </Col>
          ) : (
            userType === UNIT_DIRECTOR
              ? (
                <>
                  <Col>
                    <Button
                      type="ghost"
                      icon={<EditFilled />}
                      onClick={() => {
                        navigate(
                          routesDictionary.operatingPlanProgramProfessor.func(
                            operatingPlanId,
                            programOperatingPlanId,
                            registrationId,
                          ),
                        );
                      }}
                    >
                      {translate('edit')}
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      type="ghost"
                      icon={<DeleteFilled />}
                      className="delete-button"
                      onClick={() => {
                        setConfirmationModalData({
                          ...confirmationModalData,
                          isVisible: true,
                          type: 'deleteRegistrationProfessor',
                          registrationId,
                        });
                      }}
                    >
                      {translate('delete')}
                    </Button>
                  </Col>
                </>
              )
              : (
                <Button
                  type="ghost"
                  icon={<EyeOutlined />}
                  onClick={() => {
                    navigate(
                      routesDictionary.operatingPlanProgramProfessor.func(
                        operatingPlanId,
                        programOperatingPlanId,
                        registrationId,
                      ),
                    );
                  }}
                >
                  {translate('view')}
                </Button>
              )
          )}
        </Row>
      );
    },
  },
];

export default {};
