import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useSelector } from 'react-redux';
import SubjectView from '../../../components/SubjectView';
import CustomLoader from '../../../components/CustomLoader';
import TitleView from '../../../components/TitleView';
import routesDictionary from '../../../routes/routesDictionary';
import { UNIT_DIRECTOR } from '../../../utils/constants';

const getSubjectRequest = loader('./requests/getSubject.gql');

const SubjectDetails = () => {
  const navigate = useNavigate();
  const params = useParams();

  const { subjectId } = params;
  const { userType } = useSelector((state: any) => state.auth);

  const [subject, setSubject] = useState<any>();
  const [operatingPlanProgram, setOperatingPlanProgram] = useState<any>();

  const { data, loading, refetch } = useQuery(
    getSubjectRequest,
    {
      variables: { subjectId },
      fetchPolicy: 'no-cache',
    },
  );

  useEffect(() => {
    if (data) {
      const { getCourse } = data;
      getCourse.coursegroupSet = getCourse.coursegroupSet.filter((group: any) => !group.isDeleted);
      setSubject(getCourse);
      setOperatingPlanProgram(getCourse?.operatingPlanProgram);
    }
  }, [data]);

  if (loading || !subject) {
    return <CustomLoader />;
  }

  return (
    <div>
      <TitleView
        title={operatingPlanProgram.studyProgram.name}
        viewBackButton
        onClickBackButton={() => {
          navigate(routesDictionary.subjects.route);
        }}
      />
      <SubjectView
        readonly={userType !== UNIT_DIRECTOR}
        subjectData={subject}
        refetch={refetch}
        backSubjectListView
      />
    </div>
  );
};

export default SubjectDetails;
