import React, { useEffect, useState } from 'react';
import {
  Col, Divider, Row,
  Select,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { AppDispatch } from '../../../store';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import TitleView from '../../../components/TitleView';
import CustomTable from '../../../components/CustomTable';
import { subjectListColumns } from './constants';
import routesDictionary from '../../../routes/routesDictionary';
import CustomFilter from '../../../components/CustomFilter';
import { APPROVED, SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import { isUndefined } from '../../../utils/tools';
import '../../Professors/ProfessorList/styles.scss';
import CustomCard from '../../../components/CustomCard';
import EmptyView from '../../../components/EmptyView';

interface Subject {
  key: string,
  code: string,
  name: string,
  credits: number,
  hours: number,
  semester: number,
  studyPlan: string,
  actions: {
  }
}

const getSubjectsWithApprovePlanRequest = loader('./requests/getSubjectsWithApprovePlan.gql');
const getAllProgramOperatingPlansRequest = loader('./requests/getAllProgramOperatingPlans.gql');
const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getOperatingPlanProcessesRequest = loader('./requests/getOperatingPlanProcesses.gql');

const SubjectView = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);

  // Get processes
  const [processes, setProcesses] = useState<any>(null);
  const [process, setProcess] = useState<any>();

  const { data: getProcessesData, loading: getProcessesLoading } = useQuery(
    getOperatingPlanProcessesRequest,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  useEffect(() => {
    if (getProcessesData) {
      const { getOperatingPlanProcesses } = getProcessesData;
      const processesArray = [
        ...getOperatingPlanProcesses
          .map((item) => ({ value: item.id, label: item.year })),
      ];
      setProcess(processesArray[0]);
      setProcesses(processesArray);
    }
  }, [getProcessesData]);

  // Get graduate units
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId || {});

  const { data: getGraduateUnitsData, loading: getGraduateUnitsLoading } = useQuery(
    getGraduateUnitsRequest,
    {
      fetchPolicy: 'cache-first',
      skip: userType === UNIT_DIRECTOR,
    },
  );

  useEffect(() => {
    if (getGraduateUnitsData) {
      const { getGraduateUnits } = getGraduateUnitsData;

      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      setGraduateUnit(graduateUnitsArray[0]);
    }
  }, [getGraduateUnitsData]);

  // Get Programs
  const [operatingPlanPrograms, setOperatingPlanPrograms] = useState<any>([]);
  const [operatingPlanProgram, setOperatingPlanProgram] = useState<any>(null);

  const {
    data: getAllProgramOperatingPlansData,
    loading: getAllProgramOperatingPlansLoading,
  } = useQuery(
    getAllProgramOperatingPlansRequest,
    {
      variables: {
        unit: graduateUnitId || graduateUnit.value,
        process: process?.value,
      },
      fetchPolicy: 'cache-and-network',
      skip: userType === UNIT_DIRECTOR ? !graduateUnitId : isUndefined(graduateUnit?.value)
      && isUndefined(process),
    },
  );

  useEffect(() => {
    if (getAllProgramOperatingPlansData) {
      const { getAllProgramOperatingPlans } = getAllProgramOperatingPlansData;

      const operatingPlanProgramsArray = getAllProgramOperatingPlans.map((programOp) => {
        const { studyProgram, operatingPlan } = programOp;
        return { value: studyProgram.id, label: studyProgram.name, status: operatingPlan.status };
      });
      setOperatingPlanPrograms(operatingPlanProgramsArray);
      setOperatingPlanProgram(operatingPlanProgramsArray[0]);
    }
  }, [getAllProgramOperatingPlansData]);

  // Get Subjects
  const [defaultValues, setDefaultValues] = useState<any>([]);
  const [programSubjects, setProgramSubjects] = useState<Subject[]>([]);

  const { data: subjectsData, loading: subjectsLoading } = useQuery(
    getSubjectsWithApprovePlanRequest,
    {
      variables: {
        studyProgramId: operatingPlanProgram?.value,
        ...(process?.value && process?.value !== 'all') && { process: process.value },
      },
      fetchPolicy: 'cache-and-network',
      skip: isUndefined(operatingPlanProgram?.value),
    },
  );

  useEffect(() => {
    if (subjectsData) {
      const { getCoursesApproved } = subjectsData;
      const subjectsArray: Subject[] = [];
      getCoursesApproved.forEach(({ id, studyPlanSubject, numberOfGroups }: any, index: number) => {
        subjectsArray.push({
          // @ts-ignore
          key: index + 1,
          numberOfGroups,
          code: studyPlanSubject.code,
          subjectName: studyPlanSubject.subject,
          credits: studyPlanSubject.credits,
          hours: studyPlanSubject.hours,
          semester: studyPlanSubject.semester,
          studyPlan: studyPlanSubject.studyPlan.year,
          actions: {
            operatingPlanSubjectId: id,
            navigate,
            userType,
          },
        });
      });
      setProgramSubjects(subjectsArray);
      setDefaultValues(subjectsArray);
    }
  }, [subjectsData]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          ...routesDictionary.subjects.breadCrumb.map((text) => t(text)),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  // When there is no operating plan process created
  const emptyProcesses = processes?.length === 0;
  const operatingPlanProgramIsNotApproved = operatingPlanProgram
    && operatingPlanProgram.status !== APPROVED;

  // const loadingUnitDirector = userType === UNIT_DIRECTOR && getAllProgramOperatingPlansLoading;
  // const loadingSchoolDirector = userType === SCHOOL_DIRECTOR && getGraduateUnitsLoading;
  // if (loadingUnitDirector) return <CustomLoader />;
  // if (loadingSchoolDirector) return <CustomLoader />;

  if (emptyProcesses) {
    return (
      <EmptyView
        type="emptyOperatingPlanProcess"
        title={t('subjects')}
        viewBackButton={false}
        userType={userType}
      />
    );
  }

  return (
    <div className="professor-list">
      <TitleView
        title={t('subjects')}
        viewBackButton={false}
        action={(
          <Row justify="end" gutter={[10, 4]}>
            <Col className="label" span={24}>
              <span className="label">
                {t('academicYear')}
              </span>
            </Col>
            <Col span={24}>
              <Select
                labelInValue
                style={{ width: '100%' }}
                options={processes}
                value={process}
                onChange={(newValue) => setProcess(newValue)}
                loading={getProcessesLoading}
              />
            </Col>
          </Row>
        )}
      />
      <CustomCard>
        <Row className="graduate-unit-filter" gutter={[24, 24]}>
          {userType === SCHOOL_DIRECTOR && (
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('graduateUnit')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={graduateUnit}
                  options={graduateUnits}
                  loading={getGraduateUnitsLoading}
                  labelInValue
                  onChange={(newValue) => setGraduateUnit(newValue)}
                />
              </Col>
            </Row>
          </Col>
          )}
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('program')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={operatingPlanProgram?.value}
                  options={operatingPlanPrograms}
                  loading={getAllProgramOperatingPlansLoading}
                  labelInValue
                  onChange={(value, option) => setOperatingPlanProgram(option)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Divider />
        { operatingPlanProgramIsNotApproved
          ? (
            <EmptyView
              type="operatingPlanNotApproved"
              userType={userType}
              viewTitle={false}
            />
          ) : (
            <>
              <CustomFilter
                typeFilter="subjectListApprovePlan"
                operatingPlanStatus=""
                setDataSource={setProgramSubjects}
                defaultDataSource={defaultValues}
              />
              <CustomTable
                columns={subjectListColumns(t)}
                dataSource={programSubjects}
                loading={subjectsLoading}
              />
            </>
          )}
      </CustomCard>
    </div>
  );
};

export default SubjectView;
