import React from 'react';
import { TFunction } from 'react-i18next';
import { Button, Row } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import routesDictionary from '../../../routes/routesDictionary';

export const subjectListColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'subjectName',
    title: translate('name'),
    dataIndex: 'subjectName',
    sorterType: 'alphabetical',
  },
  {
    key: 'credits',
    title: translate('credits'),
    dataIndex: 'credits',
    sorterType: 'numerical',
  },
  {
    key: 'hours',
    title: translate('totalHours'),
    dataIndex: 'hours',
    sorterType: 'numerical',
  },
  {
    key: 'semester',
    title: translate('semester'),
    dataIndex: 'semester',
    sorterType: 'numerical',
  },
  {
    key: 'studyPlan',
    title: translate('studyPlan'),
    dataIndex: 'studyPlan',
    sorterType: 'alphabetical',
  },
  {
    key: 'numberOfGroups',
    title: translate('numberOfGroups'),
    dataIndex: 'numberOfGroups',
    sorterType: 'numerical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      operatingPlanSubjectId, navigate,
    }: any) => (
      <Row>
        <Button
          type="ghost"
          icon={<EyeOutlined />}
          onClick={() => {
            navigate(routesDictionary.subjectDetails.func(operatingPlanSubjectId));
          }}
        >
          {translate('view')}
        </Button>
      </Row>
    ),
  },
];

export default {};
