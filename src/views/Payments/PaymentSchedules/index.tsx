import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Col, Divider, Form, message, Row, Select,
} from 'antd';
import { loader } from 'graphql.macro';
import { useMutation, useQuery } from '@apollo/client';
import { useTranslation } from 'react-i18next';
import { useForm } from 'antd/lib/form/Form';
import moment from 'moment';
import TitleView from '../../../components/TitleView';
import { startUpdateBreadcrumb } from '../../../store/thunks/ui';
import { AppDispatch } from '../../../store';
import './styles.scss';
import CustomCard from '../../../components/CustomCard';
import CustomTable from '../../../components/CustomTable';
import { paymentSchedulesList } from './constants';
import { SCHOOL_DIRECTOR, UNIT_DIRECTOR } from '../../../utils/constants';
import PaymentScheduleForm from './PaymentScheduleForm';
import { maxNumber, semesterOptions, yearRange } from './functions';
import EmptyCard from '../../../components/EmptyCard';

const getPaymentSchedulesByProgramRequest = loader('./requests/getPaymentSchedules.gql');
const getPaymentScheduleRequest = loader('./requests/getPaymentSchedule.gql');
const getGraduateUnitsRequest = loader('./requests/getGraduateUnits.gql');
const getStudyProgramsRequest = loader('./requests/getStudyPrograms.gql');
const editFeePaymentRequest = loader('./requests/editFeePayment.gql');

const PaymentSchedules = () => {
  const dispatch = useDispatch<AppDispatch>();
  const [form] = useForm();
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);
  const { t } = useTranslation();

  const [editFeePaymentMutation] = useMutation(editFeePaymentRequest);

  // Graduate Units
  const [graduateUnits, setGraduateUnits] = useState<any>(null);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId);
  const {
    data: graduateUnitsData, loading: graduateUnitsLoading,
  } = useQuery(getGraduateUnitsRequest, { skip: userType !== SCHOOL_DIRECTOR });
  useEffect(() => {
    if (graduateUnitsData && userType === SCHOOL_DIRECTOR) {
      const { getGraduateUnits } = graduateUnitsData;
      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      setGraduateUnit(graduateUnitsArray[0]?.value);
    }
  }, [graduateUnitsData]);

  // Study Programs
  const [studyPrograms, setStudyPrograms] = useState<any>(null);
  const [studyProgramId, setStudyProgramId] = useState<any>();
  const [semestersByProgram, setSemestersByProgram] = useState<any>(null);
  const {
    data: studyProgramsData,
    loading: studyProgramsLoading,
  } = useQuery(
    getStudyProgramsRequest,
    {
      variables: {
        graduateUnit_Id: graduateUnit,
      },
      fetchPolicy: 'no-cache',
      skip: !graduateUnit,
    },
  );
  useEffect(() => {
    if (studyProgramsData) {
      const { getStudyPrograms } = studyProgramsData;
      const studyProgramsArray = getStudyPrograms.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));

      const numOfSemestersByProgram = getStudyPrograms?.edges?.[0].node?.numOfSemesters;
      const semestersByProgramArray = semesterOptions(numOfSemestersByProgram);
      semestersByProgramArray?.shift();

      setStudyPrograms(studyProgramsArray);
      setStudyProgramId(studyProgramsArray[0]?.value);
      setSemestersByProgram(semestersByProgramArray);
    }
  }, [studyProgramsData]);

  // Admission years registered by study program to SCHOOL DIRECTOR
  const [admissionYears, setAdmissionYears] = useState<any>(
    userType === UNIT_DIRECTOR
      ? yearRange(2017, new Date().getFullYear())
      : null,
  );
  const [admissionYear, setAdmissionYear] = useState<any>(admissionYears?.[0]?.value);
  const { data: paymentSchedulesByProgramData } = useQuery(
    getPaymentSchedulesByProgramRequest,
    {
      variables: { studyProgramId },
      fetchPolicy: 'no-cache',
      skip: !studyProgramId || userType !== SCHOOL_DIRECTOR,
    },
  );
  useEffect(() => {
    if (paymentSchedulesByProgramData && userType === SCHOOL_DIRECTOR) {
      const { getPaymentSchedules } = paymentSchedulesByProgramData;

      const paymentSchedulesByProgramArray = getPaymentSchedules?.map(
        (item) => ({ value: item.admissionYear, label: item.admissionYear }),
      );
      setAdmissionYears(paymentSchedulesByProgramArray);
      setAdmissionYear(paymentSchedulesByProgramArray?.[0]?.value);
    }
  }, [paymentSchedulesByProgramData]);

  const [editingRow, setEditingRow] = useState<any>(null);
  const handleEdit = (record) => {
    setEditingRow(record.key);
    form.setFieldsValue({ ...record });
  };
  const handleCancel = () => {
    setEditingRow(null);
  };

  // Payment schedule with all fee payments by study program and admission year
  const [paymentSchedule, setPaymentSchedule] = useState<any>(null);
  const [feePayments, setFeePayments] = useState<any>(null);
  const [defaultFeePaymentsValues, setDefaultFeePaymentsValues] = useState<any>(null);
  const {
    data: paymentScheduleData, loading: paymentScheduleLoading,
    refetch: paymentScheduleRefetch,
  } = useQuery(
    getPaymentScheduleRequest,
    {
      variables: { studyProgramId, year: admissionYear },
      fetchPolicy: 'no-cache',
      skip: !studyProgramId || !admissionYear,
    },
  );
  useEffect(() => {
    if (paymentScheduleData) {
      const { getPaymentSchedule } = paymentScheduleData;
      if (getPaymentSchedule) {
        const { feepaymentSet, ...rest } = getPaymentSchedule;

        const numOfSemesters = maxNumber(feepaymentSet, 'semester');
        setPaymentSchedule({ ...rest, numOfSemesters });

        const feePaymentsArray = feepaymentSet.map(
          ({ id, deadline, ...restFeepayment }) => (
            {
              key: id,
              deadline: deadline ? moment(deadline) : null,
              ...restFeepayment,
              actions: { handleEdit, handleCancel },
            }),
        );
        setFeePayments(feePaymentsArray);
        setDefaultFeePaymentsValues(feePaymentsArray);
      }
    } else {
      setPaymentSchedule(null);
      setDefaultFeePaymentsValues(null);
      setFeePayments(null);
    }
  }, [paymentScheduleData]);

  const handleSave = () => {
    form.validateFields()
      .then((values) => {
        const { deadline, ...rest } = values;
        const newData = {
          feePaymentId: editingRow,
          deadline: deadline ? moment(deadline).format('YYYY-MM-DD') : null,
          ...rest,
        };
        editFeePaymentMutation({
          variables: {
            input: newData,
          },
        })
          .then(({ data }) => {
            const { editFeePayment } = data;
            if (editFeePayment.feedback.status === 'SUCCESS') {
              message.success(editFeePayment.feedback.message);
              paymentScheduleRefetch();
              setEditingRow(null);
            } else {
              message.error(editFeePayment.feedback.message);
              message.error(editFeePayment.errors?.[0].messages);
            }
          });
      });
  };

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('paymentSchedules'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('paymentSchedules')}
        viewBackButton={false}
      />
      <CustomCard>
        <Row className="graduate-unit-filter" gutter={[24, 24]}>
          {userType === SCHOOL_DIRECTOR && (
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('graduateUnit')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={graduateUnit}
                  options={graduateUnits}
                  loading={graduateUnitsLoading}
                  labelInValue
                  onChange={(newValue) => setGraduateUnit(newValue.value)}
                />
              </Col>
            </Row>
          </Col>
          )}
          <Col xs={24} md={12}>
            <Row gutter={[0, 4]} align="middle">
              <Col span={24}>
                <span className="label">
                  {t('program')}
                </span>
              </Col>
              <Col span={24}>
                <Select
                  style={{ width: '100%' }}
                  value={studyProgramId}
                  options={studyPrograms}
                  loading={studyProgramsLoading}
                  labelInValue
                  onChange={(newValue) => setStudyProgramId(newValue.value)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Divider />
        { admissionYears && admissionYears.length === 0 && userType === SCHOOL_DIRECTOR
          ? (
            <EmptyCard
              title={t('emptyPaymentSchedules')}
              description={t('emptyPaymentSchedulesDescriptionSchoolDirector')}
            />
          )
          : (
            <>
              <PaymentScheduleForm
                defaultFeePaymentsValues={defaultFeePaymentsValues}
                setFeePayments={setFeePayments}
                admissionYears={admissionYears}
                paymentSchedule={paymentSchedule}
                admissionYear={admissionYear}
                setAdmissionYear={setAdmissionYear}
                studyProgramId={studyProgramId}
                refetch={paymentScheduleRefetch}
              />
              { !paymentScheduleLoading && !defaultFeePaymentsValues && userType === UNIT_DIRECTOR
                ? (
                  <EmptyCard
                    title={t('emptyPaymentSchedules')}
                    description={t('emptyPaymentSchedulesDescription')}
                  />
                ) : (
                  <Form form={form} onFinish={handleSave}>
                    <CustomTable
                      columns={paymentSchedulesList(t, editingRow, userType, semestersByProgram)}
                      dataSource={feePayments}
                      loading={paymentScheduleLoading}
                    />
                  </Form>
                )}
            </>
          )}
      </CustomCard>
    </div>
  );
};

export default PaymentSchedules;
