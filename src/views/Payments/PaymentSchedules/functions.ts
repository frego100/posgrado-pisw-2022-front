export const yearRange = (start, end) => (
  Array.from({ length: end - start + 1 }, (_, index) => {
    const year = end - index;
    return { value: year.toString(), label: year.toString() };
  })
);

export const semesterOptions = (length) => {
  const options = Array.from({ length }, (_, index) => (
    { label: (index + 1).toString(), value: index + 1 }
  ));
  options.unshift({ label: 'Todos', value: 0 });
  return options;
};

export const maxNumber = (array, key) => (
  array?.reduce((previus, current) => (
    (previus[key] > current[key]) ? previus : current))?.[key]
);

export const filterData = (key: any, value: any, defaultValues: any[]) => {
  if (value === 0) return defaultValues;
  return defaultValues.filter((item) => item[key] === value);
};
