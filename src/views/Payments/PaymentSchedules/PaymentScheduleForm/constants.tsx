import {
  Button, Input, InputNumber, Select,
} from 'antd';
import React from 'react';
import { TFunction } from 'react-i18next';

export const Items = (
  t: TFunction,
  handleCreatePaymentSchedule,
  admissionYears,
  admissionYear,
  handleChangeAdmissionYear,
  handleChangeSemester,
  semesters,
  readOnly,
) => [
  {
    name: 'admissionYear',
    component: (
      <Select
        labelInValue
        style={{ width: '100%' }}
        value={admissionYear}
        onChange={handleChangeAdmissionYear}
        options={admissionYears}
      />
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
    ],
    label: t('entrants'),
    flex: '200px',
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'numOfPayments',
    component: (
      readOnly
        ? (
          <Input disabled />
        )
        : (
          <InputNumber
            step={1}
            min={1}
            max={100}
            controls={false}
            style={{ width: '100%' }}
          />
        )
    ),
    rules: [
      {
        required: true,
        message: 'Campo obligatorio!',
      },
      {
        pattern: /^[0-9\b]+$/,
        message: '¡Ingrese un numero válido!',
      },
    ],
    label: t('numOfPayments'),
    flex: '200px',
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'interestRate',
    component: (
      <Input
        style={{ width: '100%' }}
        disabled={readOnly}
      />
    ),
    rules: [
      {
        pattern: /^(100|(\d{1,2}(\.\d{1,2})?))$/,
        message: 'Ingrese un número válido!',
      },
    ],
    label: `${t('interestRate')} (%)`,
    flex: '200px',
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: true,
  },
  {
    name: 'saveButton',
    component: (
      <Button onClick={handleCreatePaymentSchedule} type="primary">
        {t('save')}
      </Button>
    ),
    label: t('emptyCharacter'),
    flex: '200px',
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: !readOnly,
  },
  {
    name: 'semester',
    component: (
      <Select
        style={{ width: '100%' }}
        options={semesters}
        onChange={handleChangeSemester}
        labelInValue
      />
    ),
    label: t('semester'),
    flex: '200px',
    responsive: {
      xs: { span: 24 },
      md: { span: 24 },
    },
    display: semesters && semesters.length > 0,
  },
];

export default Items;
