import React, { useEffect, useState } from 'react';
import {
  Col, Form, message, Row,
} from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useTranslation } from 'react-i18next';
import { useMutation } from '@apollo/client';
import { loader } from 'graphql.macro';
import { useSelector } from 'react-redux';
import Items from './constants';
import { SCHOOL_DIRECTOR } from '../../../../utils/constants';
import { filterData, semesterOptions } from '../functions';

interface PaymentScheduleFormProps {
    defaultFeePaymentsValues: any,
    setFeePayments: any,
    admissionYears: any,
    studyProgramId: any,
    admissionYear: any,
    setAdmissionYear: any,
    paymentSchedule: any,
    refetch: any,
}

const createPaymentScheduleRequest = loader('../requests/createPaymentSchedule.gql');

const PaymentScheduleForm:React.FC<PaymentScheduleFormProps> = ({
  defaultFeePaymentsValues, setFeePayments, admissionYears,
  studyProgramId, admissionYear, setAdmissionYear, paymentSchedule, refetch,
}) => {
  const { t } = useTranslation();
  const { userType } = useSelector((state: any) => state.auth);
  const [form] = useForm();
  const readOnly = !!paymentSchedule || userType === SCHOOL_DIRECTOR;

  const [semesters, setSemesters] = useState<any>(null);

  useEffect(() => {
    if (paymentSchedule) {
      const { interestRate, numOfSemesters, ...rest } = paymentSchedule;
      setSemesters(semesterOptions(numOfSemesters));
      form.setFieldsValue({
        interestRate: interestRate && `${interestRate} %`, semester: 0, ...rest,
      });
    } else {
      form.resetFields(['numOfPayments', 'interestRate', 'semester']);
      setSemesters(null);
    }
  }, [paymentSchedule]);

  const handleChangeAdmissionYear = (newValue) => {
    setAdmissionYear(newValue.value);
  };

  const handleChangeSemester = ({ value }) => {
    const filtered = filterData('semester', value, defaultFeePaymentsValues);
    setFeePayments(filtered);
  };

  const [createPaymentScheduleMutation] = useMutation(createPaymentScheduleRequest);
  const handleCreatePaymentSchedule = () => {
    form.validateFields()
      .then((values) => {
        createPaymentScheduleMutation({
          variables: {
            studyProgramId,
            admissionYear,
            numOfPayments: values.numOfPayments,
            interestRate: values.interestRate,
          },
        })
          .then(({ data }) => {
            const { createPaymentSchedule } = data;
            if (createPaymentSchedule.feedback.status === 'SUCCESS') {
              message.success(createPaymentSchedule.feedback.message);
              refetch();
            } else {
              message.error(createPaymentSchedule.feedback.message);
            }
          });
      });
  };

  return (
    <Form
      name="basic"
      initialValues={{ admissionYear }}
      labelCol={{ span: 24 }}
      wrapperCol={{ span: 24 }}
      autoComplete="off"
      form={form}
    >
      <Row gutter={[20, 0]}>
        {
        Items(
          t,
          handleCreatePaymentSchedule,
          admissionYears,
          admissionYear,
          handleChangeAdmissionYear,
          handleChangeSemester,
          semesters,
          readOnly,
        ).map(({
          name,
          label,
          display,
          rules,
          component,
          flex,
        }) => (
          display
            ? (
              <Col flex={flex} key={name}>
                <Form.Item
                  label={label}
                  rules={rules}
                  key={name}
                  name={name}
                >
                  {component}
                </Form.Item>
              </Col>
            )
            : <React.Fragment key={name} />
        ))
        }
      </Row>
    </Form>
  );
};

export default PaymentScheduleForm;
