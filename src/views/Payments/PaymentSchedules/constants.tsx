import {
  Button, DatePicker, Form, Input, Select,
} from 'antd';
import React from 'react';
import { TFunction } from 'react-i18next';
import { UNIT_DIRECTOR } from '../../../utils/constants';
import './styles.scss';

export const paymentSchedulesList = (
  translate: TFunction,
  editingRow,
  userType,
  semestersByProgram,
) => [
  {
    key: 'numOfPayment',
    title: translate('quotaNumber'),
    dataIndex: 'numOfPayment',
    width: 150,
    sorterType: 'numerical',
    render: (text) => `P${text}`,
  },
  {
    key: 'amount',
    title: translate('amount'),
    dataIndex: 'amount',
    sorterType: 'alphabetical',
    render: (text, record) => {
      if (editingRow === record.key && userType === UNIT_DIRECTOR) {
        return (
          <Form.Item
            name="amount"
            style={{ margin: 0 }}
            rules={[
              {
                required: true,
                message: 'Campo obligatorio!',
              },
              {
                pattern: /^(?:\d{1,4}|\d{1,4}\.\d{1,2})$/,
                message: 'Ingrese un número válido!',
              },
            ]}
          >
            <Input maxLength={7} />
          </Form.Item>
        );
      }
      return text ? `S/. ${text}` : '-';
    },
  },
  {
    key: 'deadline',
    title: translate('dueDate'),
    dataIndex: 'deadline',
    sorterType: 'date',
    render: (text, record) => {
      if (editingRow === record.key && userType === UNIT_DIRECTOR) {
        return (
          <Form.Item
            name="deadline"
            style={{ margin: 0 }}
          >
            <DatePicker />
          </Form.Item>
        );
      }
      return text ? text.format('YYYY-MM-DD') : '-';
    },
  },
  {
    key: 'semester',
    title: translate('semester'),
    dataIndex: 'semester',
    sorterType: 'numerical',
    editable: true,
    render: (text, record) => {
      if (editingRow === record.key && userType === UNIT_DIRECTOR) {
        return (
          <Form.Item
            name="semester"
            style={{ margin: 0 }}
          >
            <Select
              options={semestersByProgram}
              placeholder={translate('select')}
            />
          </Form.Item>
        );
      }
      return text || '-';
    },
  },
  ...(userType === UNIT_DIRECTOR
    ? [
      {
        key: 'actions',
        title: translate('actions'),
        dataIndex: 'actions',
        width: 250,
        render: ({ handleEdit, handleCancel }, record) => {
          const isEditing = record.key === editingRow;
          return isEditing ? (
            <div style={{ display: 'flex', gap: 10 }}>
              <Button type="primary" htmlType="submit" className="save-button">
                {translate('save')}
              </Button>
              <Button type="ghost" onClick={handleCancel}>{translate('cancel')}</Button>
            </div>
          ) : (
            <Button
              type="ghost"
              disabled={editingRow !== null}
              onClick={() => handleEdit(record)}
            >
              {translate('edit')}
            </Button>
          );
        },
      },
    ] : []
  ),
];

export default { };
