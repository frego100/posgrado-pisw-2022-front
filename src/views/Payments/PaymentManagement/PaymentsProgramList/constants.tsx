import React from 'react';
import { TFunction } from 'react-i18next';
import { EyeOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import routesDictionary from '../../../../routes/routesDictionary';

export const studyProgramsColumns = (translate: TFunction) => [
  {
    key: 'code',
    title: translate('code'),
    dataIndex: 'code',
    sorterType: 'alphabetical',
  },
  {
    key: 'name',
    title: translate('program'),
    dataIndex: 'name',
    sorterType: 'alphabetical',
  },
  {
    key: 'lastPaymentUpdate',
    title: translate('lastUpdate'),
    dataIndex: 'lastPaymentUpdate',
    sorterType: 'alphabetical',
  },
  {
    key: 'actions',
    title: translate('actions'),
    dataIndex: 'actions',
    render: ({
      studyProgramId, studyProgramName, navigate,
    }: any) => (
      <Button
        icon={<EyeOutlined />}
        type="ghost"
        onClick={() => {
          navigate(
            routesDictionary.programPaymentManagement.func(studyProgramId),
            { state: { studyProgramName } },
          );
        }}
      >
        {translate('view')}
      </Button>
    ),
  },
];

export default {};
