import React, { useEffect, useState } from 'react';
import { Row, Select } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import moment from 'moment';
import { AppDispatch } from '../../../../store';
import { SCHOOL_DIRECTOR } from '../../../../utils/constants';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import TitleView from '../../../../components/TitleView';
import CustomCard from '../../../../components/CustomCard';
import CustomTable from '../../../../components/CustomTable';
import { studyProgramsColumns } from './constants';

interface studyProgram {
  key: string,
  code: string,
  lastPaymentUpdate: string,
  name: string,
  actions: {
  }
}

const getGraduateUnitsRequest = loader('../requests/getGraduateUnits.gql');
const getStudyProgramRequest = loader('../requests/getStudyPrograms.gql');

const EntrantsView = () => {
  const navigate = useNavigate();

  const [studyPrograms, setStudyPrograms] = useState<studyProgram[]>([]);
  const [graduateUnits, setGraduateUnits] = useState<any>([]);
  const { userType, graduateUnitId } = useSelector((state: any) => state.auth);
  const [graduateUnit, setGraduateUnit] = useState<any>(graduateUnitId);
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();

  const {
    data: graduateUnitsData, loading: graduateUnitsLoading,
  } = useQuery(getGraduateUnitsRequest, { skip: userType !== SCHOOL_DIRECTOR });

  const {
    data: studyProgramsData,
    loading: studyProgramsLoading,
  } = useQuery(
    getStudyProgramRequest,
    {
      variables: {
        graduateUnit_Id: graduateUnit,
      },
      fetchPolicy: 'no-cache',
      skip: !graduateUnit,
    },
  );

  useEffect(() => {
    if (graduateUnitsData && userType === SCHOOL_DIRECTOR) {
      const { getGraduateUnits } = graduateUnitsData;
      const graduateUnitsArray = getGraduateUnits.edges.map(({ node }: any) => (
        { value: node.id, label: node.name }
      ));
      setGraduateUnits(graduateUnitsArray);
      if (!graduateUnit) setGraduateUnit(graduateUnitsArray[0]?.value);
    }
  }, [graduateUnitsData, dispatch]);

  useEffect(() => {
    if (studyProgramsData) {
      const { getStudyPrograms } = studyProgramsData;
      const studyProgramsArray: studyProgram[] = [];

      getStudyPrograms.edges.forEach(({ node } : any, index: any) => (
        studyProgramsArray.push({
          key: index + 1,
          code: node.code,
          name: node.name,
          lastPaymentUpdate: node.lastPaymentUpdate
            ? moment(node.lastPaymentUpdate).format('YYYY-MM-DD') : '-',
          actions: {
            studyProgramId: node.id,
            studyProgramName: node.name,
            navigate,
          },
        })
      ));
      setStudyPrograms(studyProgramsArray);
    }
  }, [studyProgramsData, dispatch]);

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('payments'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  return (
    <div>
      <TitleView
        title={t('payments')}
        viewBackButton={false}
      />
      <CustomCard>
        {userType === SCHOOL_DIRECTOR && (
          <Row align="middle" style={{ width: '300px', marginBottom: '32px' }}>
            <span className="label">
              {t('graduateUnit')}
            </span>
            <Select
              labelInValue
              style={{ width: '100%' }}
              value={graduateUnit}
              onChange={(newValue) => setGraduateUnit(newValue.value)}
              options={graduateUnits}
              loading={graduateUnitsLoading}
            />
          </Row>
        )}
        <CustomTable
          columns={studyProgramsColumns(t)}
          dataSource={studyPrograms}
          loading={studyProgramsLoading}
        />
      </CustomCard>
    </div>
  );
};

export default EntrantsView;
