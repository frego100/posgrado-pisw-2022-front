import {
  XLSX_FORMAT, BACKEND_URL,
} from '../../../../utils/constants';

export const dataURItoBlob = async (response) => {
  const blob = await response.blob();
  const fileName = response.headers.get('content-disposition')?.split('filename=')[1];

  const newBlob = new Blob([blob], {
    type: XLSX_FORMAT,
  });

  const blobUrl = window.URL.createObjectURL(newBlob);
  const link = document.createElement('a');
  link.href = blobUrl;
  link.setAttribute('download', fileName ?? 'Pagos');
  document.body.appendChild(link);
  link.click();
  link.parentNode.removeChild(link);
  window.URL.revokeObjectURL(blobUrl);
};

export const handleDownloadFile = async (paymentScheduleId, fileURI) => {
  const response = await fetch(`${BACKEND_URL}${fileURI}`, {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ payment_schedule_id: paymentScheduleId }),
  });
  await dataURItoBlob(response);
};

export default {};
