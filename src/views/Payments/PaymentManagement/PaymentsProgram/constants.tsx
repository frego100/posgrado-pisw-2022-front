export const summaryPaymentsTooltip = (t: any):{ [index:string]:any } => (
  {
    expectedNetCollection: t('expectedNetCollectionInfo'),
    expectedGrossCollection: t('expectedGrossCollectionInfo'),
    collectionPercentage: t('collectionPercentageInfo'),
  }
);

export default {};
