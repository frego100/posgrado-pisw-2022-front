import React from 'react';
import { Col, Row, Tooltip } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { summaryPaymentsTooltip } from './constants';

export const formattedSemesters = (data, t) => {
  let currentSemesterIndex = 1;
  return data.reduce((acc, item, index) => {
    if (index && item.semester !== data[index - 1].semester) {
      currentSemesterIndex += 1;
    }
    const paymentTitleKey = `P${item.numOfPayment.toString().padStart(2, '0')}`;
    const paymentTitle = (
      <Row justify="center">
        {`P${item.numOfPayment.toString()}`}
        <br />
        {item.amount ? `(S/. ${item.amount})` : ''}
      </Row>
    );

    if (!acc[currentSemesterIndex]) {
      acc[currentSemesterIndex] = {
        title: item.semester ? `${t('semester')} ${item.semester}` : null,
        children: [],
      };
    }

    acc[currentSemesterIndex].children.push({
      title: paymentTitle,
      dataIndex: paymentTitleKey,
      key: paymentTitleKey,
      render: (childData) => {
        if (!childData) return '-';
        if (childData.type) {
          return (
            <Col>
              {!childData.value ? '-'
                : childData.type !== 'collectionPercentage'
                  ? `S/. ${childData.value}`
                  : `${childData.value} %`}
            </Col>
          );
        }
        return (
          <Row wrap={false}>
            {childData.discount && (
              <Col>
                <Tooltip title={`El estudiante posee descuento de ${childData.discount}%`}>
                  <ExclamationCircleOutlined style={{ marginRight: 5 }} />
                </Tooltip>
              </Col>
            )}
            <Col>
              {childData.amountPaid ? `S/. ${childData.amountPaid}` : '-'}
            </Col>
          </Row>
        );
      },
    });

    return acc;
  }, []);
};
export const getTableColumns = (data, t) => {
  if (!data || !(data && data.feepaymentSet)) return null;

  const semesters = formattedSemesters(data.feepaymentSet, t);

  return [
    {
      title: 'CUI',
      dataIndex: 'cui',
      key: 'cui',
      width: 100,
      fixed: 'left',
      sorterType: 'alphabetical',
    },
    {
      title: t('lastName'),
      dataIndex: 'lastName',
      key: 'lastName',
      width: 160,
      fixed: 'left',
      sorterType: 'alphabetical',
    },
    {
      title: t('firstName'),
      dataIndex: 'firstName',
      key: 'firstName',
      width: 160,
      fixed: 'left',
      sorterType: 'alphabetical',
    },
    ...semesters.filter((item) => item),
  ];
};

export const getSummaryTableColumns = (data, t) => {
  if (!data || !(data && data.feepaymentSet)) return null;

  const semesters = formattedSemesters(data.feepaymentSet, t);

  return [
    {
      title: t('summaryPayments'),
      dataIndex: 'rowName',
      key: 'rowName',
      width: 220,
      fixed: 'left',
      render: (rowName) => (
        <Row wrap={false} gutter={5}>
          <Col>
            {t(rowName)}
          </Col>
          {summaryPaymentsTooltip(t)[rowName] && (
          <Col>
            <Tooltip title={summaryPaymentsTooltip(t)[rowName]}>
              <ExclamationCircleOutlined style={{ marginRight: 5 }} />
            </Tooltip>
          </Col>
          )}
        </Row>
      ),
    },
    ...semesters.filter((item) => item),
  ];
};
