import React, { useEffect, useState } from 'react';
import {
  Button,
  Col, Divider, Row, Select,
} from 'antd';

import { useTranslation } from 'react-i18next';
import { useNavigate, useParams, useLocation } from 'react-router-dom';

import { loader } from 'graphql.macro';
import { useQuery } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { ContainerFilled, FilePdfOutlined } from '@ant-design/icons';
import moment from 'moment';
import TitleView from '../../../../components/TitleView';
import CustomCard from '../../../../components/CustomCard';
import CustomTable from '../../../../components/CustomTable';
import { getSummaryTableColumns, getTableColumns } from './utils';
import UploadFileModal from '../../../../components/UploadFileModal';
import EmptyCard from '../../../../components/EmptyCard';
import { startUpdateBreadcrumb } from '../../../../store/thunks/ui';
import { AppDispatch } from '../../../../store';
import { PAYMENTS_FILE, UNIT_DIRECTOR } from '../../../../utils/constants';
import CustomFilter from '../../../../components/CustomFilter';
import routesDictionary from '../../../../routes/routesDictionary';
import { handleDownloadFile } from './functions';

const getPaymentSchedulesRequest = loader('../requests/getPaymentSchedules.gql');
const getAdmittedStudentsByYearRequest = loader('../requests/getAdmittedStudentsByYear.gql');
const getPaymentScheduleRequest = loader('../requests/getPaymentSchedule.gql');

const PaymentManagement = () => {
  const navigate = useNavigate();
  const params = useParams();
  const location = useLocation();

  const { t } = useTranslation();
  const { userType } = useSelector((state: any) => state.auth);
  const { studyProgramId } = params;
  const studyProgramName = location?.state?.studyProgramName;

  const [paymentSchedule, setPaymentSchedule] = useState<any>();
  const [entrantsOption, setEntrantsOption] = useState<any>();
  const [entrantsDropdown, setEntrantsDropdown] = useState<any>([]);
  const [tableData, setTableData] = useState<any>([]);
  const [defaultTableData, setDefaultTableData] = useState<any>([]);
  const [paymentLastUpdate, setPaymentLastUpdate] = useState<any>();

  const [summaryTableData, setSummaryTableData] = useState<any>();
  const dispatch = useDispatch<AppDispatch>();

  const {
    // data: paymentSchedulesData,
    loading: paymentSchedulesLoading,
  } = useQuery(getPaymentSchedulesRequest, {
    variables: {
      studyProgramId,
    },
    fetchPolicy: 'no-cache',
    skip: !studyProgramId,
    onCompleted: (data) => {
      const { getPaymentSchedules } = data;
      setEntrantsDropdown(
        getPaymentSchedules.map((item) => (
          { ...item, value: item.id, label: item.admissionYear }
        )),
      );
      setEntrantsOption(getPaymentSchedules[0]);
    },
  });

  const {
    data: paymentScheduleData,
    loading: paymentScheduleLoading,
    refetch: paymentScheduleRefetch,
  } = useQuery(getPaymentScheduleRequest, {
    variables: {
      studyProgramId,
      year: entrantsOption && entrantsOption.admissionYear,
    },
    fetchPolicy: 'no-cache',
    // errorPolicy: 'ignore',
    skip: !studyProgramId || !entrantsOption?.admissionYear,
  });
  useEffect(() => {
    if (paymentScheduleData) {
      const { getPaymentSchedule } = paymentScheduleData;
      setPaymentSchedule(getPaymentSchedule);
      const lastUpdate = getPaymentSchedule?.lastUpdate
        ? moment(getPaymentSchedule.lastUpdate).format('YYYY-MM-DD, HH:mm')
        : null;
      setPaymentLastUpdate(lastUpdate);

      const summaryRows = {
        rowName: [
          'currentTotalCollection', 'expectedNetCollection', 'expectedGrossCollection', 'collectionPercentage',
        ],
      };

      const summaryDataArray = summaryRows.rowName.map((rowName) => {
        const rowData = { rowName };
        getPaymentSchedule?.feepaymentSet?.forEach((item, index) => {
          const prop = rowName;
          const propValue = item[prop] !== null ? item[prop] : '';
          rowData[`P${(index + 1).toString().padStart(2, '0')}`] = {
            value: propValue,
            type: rowName,
          };
        });
        return rowData;
      });

      setSummaryTableData(summaryDataArray);
    }
  }, [paymentScheduleData]);

  const {
    data: tableQueryData, loading: tableLoading, refetch,
  } = useQuery(getAdmittedStudentsByYearRequest, {
    variables: {
      studyProgramId,
      admissionYear: paymentSchedule && paymentSchedule.admissionYear,
      paymentScheduleId: paymentSchedule && paymentSchedule.id,
    },
    fetchPolicy: 'no-cache',
    skip: (!studyProgramId || !paymentSchedule),
  });

  useEffect(() => {
    if (tableQueryData) {
      const { getAdmittedStudentsByYear } = tableQueryData;

      const admittedStudents = getAdmittedStudentsByYear.map((item) => {
        const payments = {};

        item.payments.forEach((payment, index) => {
          const paymentKey = `P${(index + 1).toString().padStart(2, '0')}`;
          payments[paymentKey] = payment;
        });
        return {
          ...item.user,
          lastName: `${item.user.lastName} ${item.user.maternalLastName}`,
          cui: item.user.student.cui ?? '',
          ...payments,
        };
      });

      setTableData(admittedStudents);
      setDefaultTableData(admittedStudents);
    }
  }, [tableQueryData]);

  const [modalData, setModalData] = useState<any>({
    isVisible: false,
    refetch,
    paymentScheduleRefetch,
    text: t('updatePaymentRecord'),
    type: 'updatePaymentRecord',
    admissionYear: paymentSchedule && paymentSchedule.admissionYear,
    paymentScheduleId: paymentSchedule && paymentSchedule.id,
  });

  useEffect(() => {
    dispatch(
      startUpdateBreadcrumb(
        [
          t('payments'),
        ],
      ),
    ).then();
  }, [dispatch, t]);

  const tableColumns = getTableColumns(paymentSchedule, t);
  const summaryTableColumns = getSummaryTableColumns(paymentSchedule, t);

  return (
    <div>
      <TitleView
        title={studyProgramName}
        viewBackButton
        onClickBackButton={() => navigate(routesDictionary.paymentManagement.route)}
      />
      <CustomCard>
        {
          entrantsDropdown.length ? (
            <>
              <Row className="graduate-unit-filter" gutter={[24, 24]}>
                <Col xs={24} md={6}>
                  <Row align="middle" gutter={[0, 4]}>
                    <span className="label">
                      {t('entrants')}
                    </span>
                    <Select
                      labelInValue
                      style={{ width: '100%' }}
                      value={entrantsOption && entrantsOption.id}
                      onChange={(value, option) => setEntrantsOption(option)}
                      options={entrantsDropdown}
                      loading={paymentSchedulesLoading}
                    />
                  </Row>
                </Col>
                <Row className="ml-auto" gutter={[10, 15]}>
                  {
                    tableData.length ? (
                      <Col>
                        <Button
                          type="primary"
                          onClick={() => handleDownloadFile(paymentSchedule?.id, PAYMENTS_FILE)}
                          icon={<FilePdfOutlined />}
                        >
                          {t('downloadReport')}
                        </Button>
                      </Col>
                    ) : <div />
                  }
                  {
                    userType === UNIT_DIRECTOR ? (
                      <Col>
                        <Button
                          type="primary"
                          onClick={() => setModalData({
                            ...modalData,
                            isVisible: true,
                            admissionYear: paymentSchedule && paymentSchedule.admissionYear,
                            paymentScheduleId: paymentSchedule && paymentSchedule.id,
                            refetch,
                          })}
                          icon={<ContainerFilled />}
                        >
                          {t('updateReport')}
                        </Button>
                        <UploadFileModal
                          uploadFileModalData={modalData}
                          setUploadFileModalData={setModalData}
                        />
                      </Col>
                    ) : <div />
                  }
                </Row>
              </Row>
              <Divider />
              {
              !tableData.length && !tableLoading ? (
                <EmptyCard
                  title={t('emptyEntrantsByYearTitle')}
                  description={
                    userType === UNIT_DIRECTOR
                      ? t('emptyEntrantsByYearUnitDirector')
                      : t('emptyEntrantsByYearSchoolDirector')
                  }
                />
              ) : (
                <>
                  <CustomFilter
                    typeFilter="paymentStudentsFilters"
                    setDataSource={setTableData}
                    defaultDataSource={defaultTableData}
                    paymentsLastUptdate={paymentLastUpdate}
                  />
                  <CustomTable
                    bordered
                    dataSource={tableData || []}
                    loading={tableLoading}
                    columns={tableColumns || []}
                  />
                  <Divider />
                  <CustomTable
                    bordered
                    dataSource={summaryTableData || []}
                    loading={paymentScheduleLoading}
                    columns={summaryTableColumns || []}
                    showPagination={false}
                  />
                </>
              )
            }
            </>
          ) : (
            <EmptyCard
              title={t('emptyPaymentSchedules')}
              description={
                userType === UNIT_DIRECTOR
                  ? t('emptyPaymentSchedulesDescription')
                  : t('emptyPaymentSchedulesDescriptionSchoolDirector')
              }
            />
          )
        }
      </CustomCard>
    </div>
  );
};
export default PaymentManagement;
