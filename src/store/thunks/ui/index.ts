import {
  displayProgressBar, hideBanner, hideProgressBar, showBanner, updateBreadcrumb,
} from '../../slices/ui';

// eslint-disable-next-line no-unused-vars
export const startDisplayProgressBar = () => async (dispatch: (arg0: any) => void) => {
  dispatch(displayProgressBar());
};
// eslint-disable-next-line no-unused-vars
export const startHideProgressBar = () => async (dispatch: (arg0: any) => void) => {
  dispatch(hideProgressBar());
};

export const startUpdateBreadcrumb = (text: string[]) => async (
  // eslint-disable-next-line no-unused-vars
  dispatch: (arg0: any) => void,
) => {
  dispatch(updateBreadcrumb({ text }));
};

// eslint-disable-next-line no-unused-vars
export const startDisplayBanner = () => async (dispatch: (arg0: any) => void) => {
  dispatch(showBanner());
};
// eslint-disable-next-line no-unused-vars
export const startHideBanner = () => async (dispatch: (arg0: any) => void) => {
  dispatch(hideBanner());
};
